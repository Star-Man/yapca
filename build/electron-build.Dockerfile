FROM node:20
RUN dpkg --add-architecture i386 && apt update
RUN apt install -y wine wine32 wine64 libwine libwine:i386 libarchive-tools rpm
RUN mkdir -p /buildOutput
COPY ./config/docker* /yapca/config
COPY ./frontend/ /yapca/frontend
WORKDIR /yapca/frontend

# No yarn berry support yet
# https://github.com/quasarframework/quasar/issues/14618
RUN yarn install 


RUN mkdir /build-out
RUN yarn build -m electron -T linux
WORKDIR /yapca/frontend/dist/electron/Packaged
RUN mv *.pacman* *.rpm* *.deb* /build-out

WORKDIR /yapca/frontend
RUN yarn build -m electron -T win
WORKDIR /yapca/frontend/dist/electron/Packaged
RUN mv *.exe* /build-out

WORKDIR /build-out
SHELL ["/bin/bash", "-c"]
RUN for file in *; do (if [[ -f "$file" ]]; then sha512sum -- "$file" > "${file}.sha512"; fi) done
CMD cp * /yapca/buildOutput