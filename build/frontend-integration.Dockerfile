FROM node:20-alpine
RUN apk add chromium firefox

ARG YAPCA_ENV
ENV YAPCA_ENV $YAPCA_ENV
RUN corepack enable
RUN yarn global add http-server

RUN mkdir -p /builds/Star-Man/yapca/frontend
WORKDIR /builds/yapca/yapca/frontend


COPY ./frontend/*.* .
RUN yarn install --immutable

COPY ./config/ /builds/yapca/yapca/config
COPY ./frontend/src ./src
RUN yarn build

COPY ./frontend/tests/e2e ./tests/e2e
