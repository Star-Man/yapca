FROM python:3.11-alpine
ENV PYTHONUNBUFFERED 1
RUN apk add --no-cache postgresql-libs && \
    apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev \
    zlib-dev jpeg-dev
RUN pip install --upgrade pip
RUN pip install poetry==1.7.1
RUN poetry config virtualenvs.create false

RUN mkdir -p /builds/Star-Man/yapca
COPY ./backend/ /builds/yapca/yapca/backend
COPY ./config/ /builds/yapca/yapca/config/

WORKDIR /builds/yapca/yapca/backend
RUN poetry install
