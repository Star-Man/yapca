FROM thyrlian/android-sdk:8.0 AS base
ARG ANDROID_KEYSTORE_PASSWORD
ARG ANDROID_PRIVATEKEY_PASSWORD
ARG ANDROID_KEYSTORE_BASE64
ENV ANDROID_KEYSTORE_PASSWORD $ANDROID_KEYSTORE_PASSWORD
ENV ANDROID_PRIVATEKEY_PASSWORD $ANDROID_PRIVATEKEY_PASSWORD
ENV ANDROID_KEYSTORE_BASE64 $ANDROID_KEYSTORE_BASE64

ENV NODE_MAJOR 20

RUN sdkmanager "platforms;android-30" "build-tools;30.0.3"
RUN apt install -y curl zipalign ca-certificates gnupg
RUN mkdir -p /etc/apt/keyrings
RUN curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
RUN echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list
RUN apt update
RUN apt install -y nodejs
RUN mkdir -p /buildOutput

FROM base AS build
COPY ./config/ /config
COPY ./frontend/ /frontend
WORKDIR /frontend
RUN corepack enable
RUN yarn install --immutable

# Pass in env vars and create keystore
RUN echo $ANDROID_KEYSTORE_BASE64 > src-capacitor/android.keystore_base64 
RUN base64 -d -w 0 src-capacitor/android.keystore_base64 > src-capacitor/android.keystore
RUN rm src-capacitor/android.keystore_base64

# Build android app
RUN yarn quasar build -m capacitor -T android
WORKDIR /frontend/dist/capacitor/android/apk/release

RUN zipalign 4 app-release-unsigned.apk app-release.apk
RUN $ANDROID_SDK_ROOT/build-tools/30.0.3/apksigner sign \
    --ks /frontend/src-capacitor/android.keystore \
    --ks-key-alias yapca_keystore --ks-pass pass:$ANDROID_KEYSTORE_PASSWORD \
    app-release.apk
    

RUN rm /frontend/src-capacitor/android.keystore
RUN for file in *; do (if [[ -f "$file" ]]; then sha512sum -- "$file" > "${file}.sha512"; fi) done
CMD mv *.apk* /buildOutput