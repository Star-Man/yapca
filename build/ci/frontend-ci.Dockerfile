ARG REGISTRY
ARG IMAGE_VERSION
FROM ${REGISTRY}/frontend-base:${IMAGE_VERSION}
ARG YAPCA_ENV
ENV YAPCA_ENV $YAPCA_ENV
WORKDIR /yapca/frontend
COPY ./frontend/src src
COPY ./frontend/postcss.config.js .
COPY ./frontend/quasar.config.js .
COPY ./frontend/quasar.extensions.json .
COPY ./frontend/tsconfig.json .
COPY ./frontend/index.html .
COPY ./frontend/.nvmrc .
COPY ./frontend/.eslintrc.js .
COPY ./frontend/.editorconfig .
COPY ./frontend/.prettierrc .

RUN yarn build
EXPOSE 3000/tcp
CMD http-server -s -p 3000 -P http://localhost:3000? dist/spa