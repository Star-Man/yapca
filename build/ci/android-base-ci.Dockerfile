FROM thyrlian/android-sdk:8.0 AS base

ENV NODE_MAJOR 20


RUN sdkmanager "platforms;android-30" "build-tools;30.0.3"
RUN apt update
RUN apt install -y --no-install-recommends \
    curl zipalign ca-certificates gnupg \
    && apt clean

# Install node
RUN mkdir -p /etc/apt/keyrings
RUN curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
RUN echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list
RUN apt update && apt install -y --no-install-recommends nodejs && apt clean

RUN mkdir -p /buildOutput
FROM base AS build
RUN mkdir -p /yapca/frontend
WORKDIR /yapca/frontend
COPY ./frontend/package.json package.json
COPY ./frontend/yarn.lock yarn.lock
COPY ./frontend/.yarnrc.yml .yarnrc.yml
RUN node -v

RUN corepack enable
RUN yarn install --immutable