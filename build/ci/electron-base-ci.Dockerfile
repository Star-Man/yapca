FROM node:20
RUN dpkg --add-architecture i386 && apt update
RUN apt install -y wine wine32 wine64 libwine libwine:i386 libarchive-tools rpm
RUN mkdir -p /buildOutput
RUN mkdir /build-out
RUN mkdir -p /yapca/frontend
WORKDIR /yapca/frontend
RUN corepack enable

COPY ./frontend/package.json package.json
COPY ./frontend/yarn.lock yarn.lock
COPY ./frontend/.yarnrc.yml .yarnrc.yml
RUN ln -sf /bin/bash /bin/sh
RUN yarn install --immutable