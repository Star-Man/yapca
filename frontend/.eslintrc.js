const { resolve } = require('node:path');

module.exports = {
  root: true,
  ignorePatterns: [
    'dist/*',
    'tests/unit/reports/*',
    '**/node_modules/*',
    'coverage/*',
    'src-capacitor/*',
    'tests/unit/tests/reports'
  ],
  env: {
    amd: true
  },
  extends: ['@sxzz/eslint-config'],
  rules: {
    'prettier/prettier': [
      'error',
      {
        trailingComma: 'none'
      }
    ],
    'vue/component-tags-order': [
      'error',
      {
        order: ['template', 'script', 'style']
      }
    ]
  },
  overrides: [
    {
      files: ['src/**/*.ts', 'src/**/*.vue', 'src-electron/**/*.ts'],
      extends: [
        '@sxzz/eslint-config',
        'plugin:@typescript-eslint/recommended',
        'plugin:@typescript-eslint/recommended-requiring-type-checking',
        'plugin:@typescript-eslint/strict'
      ],
      parserOptions: {
        extraFileExtensions: ['.vue'],
        parser: '@typescript-eslint/parser',
        project: resolve(__dirname, './tsconfig.json'),
        tsconfigRootDir: __dirname,
        ecmaVersion: 2018,
        sourceType: 'module'
      },
      parser: 'vue-eslint-parser',
      rules: {
        '@typescript-eslint/non-nullable-type-assertion-style': 'off',
        '@typescript-eslint/explicit-function-return-type': 'error',
        '@typescript-eslint/no-invalid-void-type': 'off'
      }
    },
    {
      files: ['tests/e2e/**/*.ts'],
      extends: [
        '@sxzz/eslint-config',
        'plugin:@typescript-eslint/recommended',
        'plugin:@typescript-eslint/recommended-requiring-type-checking',
        'plugin:@typescript-eslint/strict'
      ],
      parserOptions: {
        extraFileExtensions: ['.vue'],
        parser: '@typescript-eslint/parser',
        project: resolve(__dirname, './tsconfig.json'),
        tsconfigRootDir: __dirname,
        ecmaVersion: 2018,
        sourceType: 'module'
      },
      parser: 'vue-eslint-parser',
      rules: {
        '@typescript-eslint/no-unsafe-return': 'off',
        '@typescript-eslint/no-unsafe-member-access': 'off',
        '@typescript-eslint/no-unsafe-call': 'off',
        'unicorn/prefer-dom-node-text-content': 'off',
        '@typescript-eslint/no-non-null-assertion': 'off'
      }
    },
    {
      files: ['tests/unit/**/*.ts'],
      extends: [
        '@sxzz/eslint-config',
        'plugin:@typescript-eslint/recommended',
        'plugin:@typescript-eslint/recommended-requiring-type-checking',
        'plugin:@typescript-eslint/strict'
      ],
      parserOptions: {
        extraFileExtensions: ['.vue'],
        parser: '@typescript-eslint/parser',
        project: resolve(__dirname, './tsconfig.json'),
        tsconfigRootDir: __dirname,
        ecmaVersion: 2018,
        sourceType: 'module'
      },
      parser: 'vue-eslint-parser',
      rules: {
        '@typescript-eslint/no-unsafe-assignment': 'off',
        '@typescript-eslint/no-unsafe-call': 'off',
        '@typescript-eslint/no-unsafe-member-access': 'off',
        '@typescript-eslint/no-unsafe-return': 'off',
        '@typescript-eslint/no-unsafe-argument': 'off'
      }
    }
  ],
  plugins: ['@typescript-eslint', 'vue']
};
