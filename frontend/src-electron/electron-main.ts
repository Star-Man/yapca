/* eslint-disable unicorn/prefer-node-protocol */
/* eslint-disable @typescript-eslint/no-var-requires */
import path from 'path';
import os from 'os';
import { BrowserWindow, app, dialog, nativeTheme } from 'electron';
import contextMenu from 'electron-context-menu';

// needed in case process is undefined under Linux
// eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
const platform = process.platform || os.platform();

try {
  if (platform === 'win32' && nativeTheme.shouldUseDarkColors) {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-call
    require('node:fs').unlinkSync(
      path.join(app.getPath('userData'), 'DevTools Extensions')
    );
  }
} catch {}

contextMenu({
  showSaveImageAs: true,
  showSelectAll: false,
  showSearchWithGoogle: false
});

let mainWindow: BrowserWindow | undefined;

function createWindow(url = ''): void {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    icon: path.resolve(__dirname, 'icons/icon.png'), // tray icon
    width: 1000,
    height: 600,
    minHeight: 400,
    minWidth: 400,
    useContentSize: true,
    webPreferences: {
      contextIsolation: true,
      // More info: https://v2.quasar.dev/quasar-cli-vite/developing-electron-apps/electron-preload-script
      preload: path.resolve(__dirname, process.env.QUASAR_ELECTRON_PRELOAD)
    }
  });
  mainWindow.removeMenu();
  mainWindow
    .loadURL(`${process.env.APP_URL}#/${url}`)
    .catch((err: Error) => err);

  if (process.env.DEBUGGING) {
    // if on DEV or Production with debug enabled
    mainWindow.webContents.openDevTools();
  } else {
    // we're on production; no access to devtools pls
    mainWindow.webContents.on('devtools-opened', () => {
      mainWindow?.webContents.closeDevTools();
    });
  }

  mainWindow.on('closed', () => {
    mainWindow = undefined;
  });
}

function loadSecondInstanceUrl(url: string): void {
  if (mainWindow) {
    mainWindow
      .loadURL(`${process.env.APP_URL}#/${url}`)
      .catch((err: Error) => err);
  }
}

function getUrlFromArgs(args: string[] | undefined): string {
  let url = '';
  if (
    args &&
    args.length > 0 &&
    (process.platform === 'win32' || process.platform === 'linux')
  ) {
    // Keep only command line / deep linked arguments
    const urlArray = args.splice(1);
    const foundUrl = urlArray.find(selectedUrl =>
      selectedUrl.includes('yapca://')
    );
    if (foundUrl) {
      url = foundUrl.slice(8);
    }
  }
  return url;
}

const gotTheLock = app.requestSingleInstanceLock();

if (!gotTheLock) {
  app.quit();
} else {
  app.on('ready', () => {
    const url = getUrlFromArgs(process.argv);
    createWindow(url);
  });

  app.on('window-all-closed', () => {
    if (platform !== 'darwin') {
      app.quit();
    }
  });

  app.on('activate', () => {
    if (mainWindow === undefined) {
      const url = getUrlFromArgs(process.argv);
      createWindow(url);
    }
  });

  app.on('second-instance', (_, argv) => {
    // Someone tried to run a second instance, we should focus our window.
    if (mainWindow) {
      if (mainWindow.isMinimized()) {
        mainWindow.restore();
      }
      mainWindow.focus();
      const url = getUrlFromArgs(argv);
      if (url) {
        loadSecondInstanceUrl(url);
      }
    }
  });

  // Handle the protocol. In this case, we choose to show an Error Box.
  app.on('open-url', (_, url) => {
    dialog.showErrorBox('Welcome Back', `You arrived from: ${url}`);
  });
}

if (process.defaultApp) {
  if (process.argv.length >= 2) {
    app.setAsDefaultProtocolClient('yapca', process.execPath, [
      path.resolve(process.argv[1])
    ]);
  }
} else {
  app.setAsDefaultProtocolClient('yapca');
}
