import { route } from 'quasar/wrappers';
import {
  createMemoryHistory,
  createRouter,
  createWebHashHistory,
  createWebHistory
} from 'vue-router';

import { useBasicStore } from 'src/stores/basicStore';
import { useUserStore } from 'src/stores/userStore';
import { useCentreStore } from 'src/stores/centresStore';
import routes from './routes';
import type Centre from 'src/types/Centre';
import type Group from 'src/types/Group';
import type User from 'src/types/User';
import type {
  NavigationGuardNext,
  RouteLocationNormalized,
  RouterHistory
} from 'vue-router';

const isGroup = (user: User, groupName: string): boolean => {
  if (user.groups) {
    return user.groups.some((group: Group | number) => {
      if (typeof group === 'object') {
        return group.name === groupName;
      }
      return false;
    });
  }
  return false;
};

const isClient = (user: User): boolean => {
  if (user.groups) {
    return user.groups.some((group: Group | number) => {
      if (typeof group === 'object') {
        return group.name === 'client';
      }
      return false;
    });
  }
  return false;
};

const checkSubscribedCentres = (
  to: RouteLocationNormalized,
  next: NavigationGuardNext
): void | Promise<void> => {
  const basicStore = useBasicStore();
  const userStore = useUserStore();
  const centreStore = useCentreStore();
  if (
    (!userStore.user.centres || userStore.user.centres.length === 0) &&
    !isClient(userStore.user)
  ) {
    if (Object.keys(centreStore.centres).length > 1) {
      return next({
        name: 'centreSelect',
        query: {
          nextUrl: to.fullPath
        }
      });
    }
    return basicStore.http
      .patch(`users/${userStore.user.id ?? ''}/update/`, {
        centres: [centreStore.centres[Object.keys(centreStore.centres)[0]].id]
      })
      .then(() => next());
  }
  return next();
};

const checkCreatedCentres = async (
  to: RouteLocationNormalized,
  next: NavigationGuardNext
): Promise<void> => {
  const userStore = useUserStore();
  const centreStore = useCentreStore();
  if (Object.keys(centreStore.centres).length === 0) {
    try {
      await centreStore.getCentres();
    } catch {
      return next({
        name: 'errorPage',
        state: {
          errorMessage:
            'Error connecting to backend server, please contact support.'
        }
      });
    }
  }
  const centres = Object.keys(centreStore.centres).map(centreKey => {
    return { ...centreStore.centres[centreKey] };
  });
  if (centres.length === 0) {
    if (isGroup(userStore.user, 'admin')) {
      return next({
        path: '/centre-setup',
        query: {
          nextUrl: to.fullPath
        }
      });
    }
    return next({
      name: 'errorPage',
      state: {
        errorMessage:
          'No centre has been setup, please request an admin to set one up'
      }
    });
  }
  const completeCentres = centres.filter(
    (centre: Centre) => centre.setupComplete
  );
  if (completeCentres.length === 0 && isGroup(userStore.user, 'admin')) {
    const imcompleteCentre = centres.find(
      (centre: Centre) => !centre.setupComplete
    );
    if (imcompleteCentre) {
      centreStore.setSetupCentre(centreStore.centres[imcompleteCentre.id]);
      return next({
        name: 'centreSetup',
        query: {
          nextUrl: to.fullPath
        }
      });
    }
  }
  centreStore.setSetupCentre(null);
  return checkSubscribedCentres(to, next);
};

export default route(() => {
  let createHistory: (base?: string | undefined) => RouterHistory;

  if (process.env.SERVER) {
    createHistory = createMemoryHistory;
  } else if (process.env.VUE_ROUTER_MODE === 'history') {
    createHistory = createWebHistory;
  } else {
    createHistory = createWebHashHistory;
  }

  const Router = createRouter({
    scrollBehavior: () => ({ left: 0, top: 0 }),
    routes,
    history: createHistory(process.env.VUE_ROUTER_BASE)
  });

  Router.beforeEach((to, _from, next) => {
    return routerGuard(to, _from, next);
  });

  return Router;
});

const authPagesCheck = (
  to: RouteLocationNormalized
): { name: string; query?: { nextUrl: string } } => {
  const basicStore = useBasicStore();
  const userStore = useUserStore();

  if (!basicStore.token) {
    return {
      name: 'auth',
      query: { nextUrl: to.fullPath }
    };
  }
  const httpDefaults = basicStore.http.defaults as {
    headers: { common: { Authorization: string } };
  };
  httpDefaults.headers.common.Authorization = `Token ${basicStore.token}`;
  const isAdmin = isGroup(userStore.user, 'admin');
  if (to.matched.some(record => record.meta.requiresAdmin) && !isAdmin) {
    return {
      name: 'Clients'
    };
  }

  const isClient = isGroup(userStore.user, 'client');
  if (to.matched.some(record => record.meta.medicalUser) && isClient) {
    return {
      name: 'My Medical Info'
    };
  }
  if (to.matched.some(record => record.meta.canViewMedicalInfo)) {
    if (!isClient) {
      return {
        name: 'Clients'
      };
    }
    if (!userStore.user.permissions?.includes('client_can_view_own_data')) {
      return {
        name: 'calendar'
      };
    }
  }
  if (
    to.matched.some(record => record.meta.canViewCalendar) &&
    isClient &&
    !userStore.user.permissions?.includes('client_can_view_calendar')
  ) {
    return {
      name: 'Clients'
    };
  }
  return { name: '' };
};

const routerGuard = async (
  to: RouteLocationNormalized,
  _from: RouteLocationNormalized,
  next: NavigationGuardNext
): Promise<void> => {
  const basicStore = useBasicStore();
  const userStore = useUserStore();
  if (
    !basicStore.appServerAddress &&
    (basicStore.platform.electron || basicStore.platform.nativeMobile) &&
    to.name !== 'serverSelect'
  ) {
    return next({
      name: 'serverSelect',
      query: { serverAddress: to.query.address, nextUrl: to.fullPath }
    });
  }
  if (to.matched.some(record => record.meta.requiresAuth)) {
    const authPageNext = authPagesCheck(to);
    if (authPageNext.name) {
      return next(authPageNext);
    }
  }
  if (to.name === 'auth' && basicStore.token) {
    return next({ name: 'Clients' });
  }
  if (to.name === 'auth') {
    userStore.user.id = null;
    basicStore.token = '';
  }
  if (to.matched.some(record => record.meta.checkCentres)) {
    return checkCreatedCentres(to, next);
  }
  return next();
};
