import { Screen } from 'quasar';
import isXs from './isXs';

const tableContainerHeight = (extra = 0): string => {
  const height = Screen.height - extra;
  if (isXs.value) {
    return height.toString();
  }
  return 'auto';
};

export default tableContainerHeight;
