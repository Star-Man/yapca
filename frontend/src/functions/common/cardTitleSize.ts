const cardTitleSize = (cardTitleElement: HTMLDivElement): number => {
  return cardTitleElement.clientHeight;
};
export default cardTitleSize;
