const autoColSize = (
  breakpoint: 'xs' | 'sm' | 'md' | 'lg' | 'xl',
  itemsLength: number,
  itemIndex: number
): string => {
  return `col-12 col-${breakpoint}-${
    itemIndex === itemsLength - 1 && itemsLength % 2 !== 0 ? '12' : '6'
  }`;
};
export default autoColSize;
