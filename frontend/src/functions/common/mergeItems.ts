import { useCentreStore } from 'src/stores/centresStore';
import { useClientStore } from 'src/stores/clientsStore';
import type Client from 'src/types/Client';
import type ClientNeed from 'src/types/ClientNeed';
import type Need from 'src/types/Need';
import type Policy from 'src/types/Policy';

type Item = Need | ClientNeed | Policy | Client;

const mergeItems = (
  items: Partial<Item>[],
  itemName: 'policies' | 'needs' | 'clientNeeds' | 'clients'
): void => {
  const initialValue = {};
  const newItems = items.reduce(
    (itemObject: Record<string, Partial<Item>>, item: Partial<Item>) => {
      if (item.id) {
        return {
          ...itemObject,
          [item.id.toString()]: item
        };
      }
      return itemObject;
    },
    initialValue
  );
  if (itemName === 'clientNeeds' || itemName === 'clients') {
    const clientStore = useClientStore();
    clientStore[itemName] = {
      ...clientStore[itemName],
      ...newItems
    };
  } else {
    const centreStore = useCentreStore();
    centreStore[itemName] = {
      ...centreStore[itemName],
      ...newItems
    };
  }
};

export default mergeItems;
