import { useBasicStore } from 'src/stores/basicStore';
import { useSocketStore } from 'src/stores/websocketStore';
import clearStores from './clearStores';
import type { NavigationFailure, Router } from 'vue-router';

const logout = async (
  router: Router,
  redirect = true
): Promise<void | NavigationFailure | undefined> => {
  const basicStore = useBasicStore();
  const wsStore = useSocketStore();

  clearStores(['basic']);
  if (redirect) {
    await router.replace({ name: 'auth' });
  }
  clearStores(['socket', 'centre', 'user', 'client']);
  return wsStore.WS_RECONNECT(basicStore.appServerAddress, basicStore.token);
};
export default logout;
