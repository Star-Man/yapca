import { useUserStore } from 'src/stores/userStore';
import { computed } from 'vue';

const isClient = computed(() => {
  const userStore = useUserStore();
  return userStore.user.group === 'client';
});

export default isClient;
