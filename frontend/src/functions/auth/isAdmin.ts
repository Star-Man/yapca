import { useUserStore } from 'src/stores/userStore';
import { computed } from 'vue';

const isAdmin = computed(() => {
  const userStore = useUserStore();
  return userStore.user.group === 'admin';
});

export default isAdmin;
