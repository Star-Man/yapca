import { useBasicStore } from 'src/stores/basicStore';
import { useClientStore } from 'src/stores/clientsStore';
import axiosFailed from '../proxy/AxiosFailed';
import type ClientResponse from 'src/types/axios/ClientResponse';

const getClient = async (clientId: number): Promise<void> => {
  const basicStore = useBasicStore();
  const clientStore = useClientStore();
  return basicStore.http
    .get(`clients/${clientId}/`)
    .then((response: ClientResponse) => {
      if (response.data.id) {
        clientStore.clients[response.data.id] = response.data;
        if (
          clientStore.activeClient &&
          clientStore.activeClient.id === response.data.id
        ) {
          clientStore.activeClient = response.data;
        }
      }
    })
    .catch(axiosFailed);
};

export default getClient;
