import { useClientStore } from 'src/stores/clientsStore';
import { useCentreStore } from 'src/stores/centresStore';

const getClientNeedById = (
  clientNeedId: number
): {
  plan: string;
  needDescription: string;
  centre: string;
} => {
  const clientStore = useClientStore();
  const centreStore = useCentreStore();
  const needData = { plan: '', needDescription: '', centre: '' };
  const clientNeed = clientStore.clientNeeds[clientNeedId];
  needData.needDescription = centreStore.needs[clientNeed.need].description;
  needData.plan = clientNeed.plan;
  needData.centre = centreStore.centres[clientNeed.centre].name;
  return needData;
};

export default getClientNeedById;
