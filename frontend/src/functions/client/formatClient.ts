import { useClientStore } from 'src/stores/clientsStore';
import { useUserStore } from 'src/stores/userStore';
import { differenceInYears, format, parseISO } from 'date-fns';
import formatDateTime from '../date/formatDateTime';
import type Client from 'src/types/Client';

const formatDate = (date: string): string => {
  const parsedDate = parseISO(date);
  const age = differenceInYears(new Date(), parsedDate);
  return `${format(parsedDate, 'dd/MM/yy')} (${age})`;
};

const formatClient = (clientId: number): Client => {
  const userStore = useUserStore();
  const clientStore = useClientStore();

  const client = { ...clientStore.clients[clientId] };
  if (
    typeof client.admittedBy === 'number' &&
    client.admittedBy in userStore.users &&
    userStore.users[client.admittedBy].displayName
  ) {
    client.admittedBy = userStore.users[client.admittedBy].displayName;
  } else {
    client.admittedBy = 'Unknown';
  }
  client.createdDate = formatDateTime(client.createdDate, false);
  client.dateOfBirth = formatDate(client.dateOfBirth);
  return client;
};

export default formatClient;
