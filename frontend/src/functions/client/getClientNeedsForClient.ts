import { useBasicStore } from 'src/stores/basicStore';
import axiosFailed from '../proxy/AxiosFailed';
import mergeItems from '../common/mergeItems';
import type ClientNeedsResponse from 'src/types/axios/ClientNeedsResponse';

const getClientNeedsForClient = async (clientId: number): Promise<void> => {
  const basicStore = useBasicStore();
  return basicStore.http
    .get(`clients/${clientId}/clientNeeds`)
    .then((clientNeedsResponse: ClientNeedsResponse) => {
      return mergeItems(clientNeedsResponse.data, 'clientNeeds');
    })
    .catch(axiosFailed);
};

export default getClientNeedsForClient;
