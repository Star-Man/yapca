import axios from 'axios';
import getBackendPath from './getBackendPath';
import type { AxiosInstance } from 'axios';

export default (backendPath = ''): AxiosInstance => {
  if (!backendPath) {
    const apiProtocol =
      process.env.VUE_APP_API_PROTOCOL !== 'undefined'
        ? (process.env.VUE_APP_API_PROTOCOL as string)
        : '';
    return axios.create({
      baseURL: `${apiProtocol}://${getBackendPath()}/api/`,
      timeout: 5000
    });
  }
  return axios.create({
    baseURL: `${backendPath}/api/`,
    timeout: 5000
  });
};
