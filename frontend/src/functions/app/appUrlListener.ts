import { App } from '@capacitor/app';
import type { Router } from 'vue-router';

export const appUrlListener = (router: Router): void => {
  App.addListener('appUrlOpen', (data: { url: string }) => {
    const urlScheme = data.url.slice(0, 8);
    if (urlScheme === 'yapca://') {
      const url = `/${data.url.slice(8)}`;
      const [path, queryString] = url.split('?');
      const queries = JSON.parse(
        `{"${decodeURI(queryString)
          .replace(/"/g, '\\"')
          .replace(/&/g, '","')
          .replace(/=/g, '":"')}"}`
      ) as object;
      router.push({ path, query: { ...queries } }).catch((err: Error) => err);
    }
  }).catch((err: Error) => err);
};
