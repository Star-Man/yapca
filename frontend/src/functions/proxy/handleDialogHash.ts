const handleDialogHash = (newHash: string, dialogValue: string): boolean => {
  const hashes = newHash.slice(1).split('&');
  if (hashes.includes(dialogValue)) {
    return true;
  }
  return false;
};

export default handleDialogHash;
