import { useUserStore } from 'src/stores/userStore';
import type UsersResponse from 'src/types/axios/UsersResponse';
import type User from 'src/types/User';

const setUsers = (response: UsersResponse): void => {
  const initialValue = {};
  const userStore = useUserStore();
  const newUsers = response.data.reduce(
    (userObject: Record<string, User>, user: User) => {
      if (user.id && user.id !== userStore.user.id) {
        if (
          user.groups &&
          user.groups.length > 0 &&
          typeof user.groups[0] === 'object'
        ) {
          // eslint-disable-next-line no-param-reassign
          user.group = user.groups[0].name;
        }
        return {
          ...userObject,
          [user.id.toString()]: user
        };
      }
      return userObject;
    },
    initialValue
  );
  userStore.users = newUsers;
  if (userStore.user.id) {
    userStore.users[userStore.user.id] = userStore.user;
  }
};

export default setUsers;
