import { useBasicStore } from 'src/stores/basicStore';
import axiosFailed from '../proxy/AxiosFailed';
import type Centre from 'src/types/Centre';
import type { AxiosResponse } from 'axios';

const updateCentre = (
  centre: Partial<Centre>
): Promise<void | AxiosResponse> | null => {
  const basicStore = useBasicStore();
  if (centre.id) {
    return basicStore.http
      .patch(`centres/${centre.id}/update/`, centre)
      .catch(() => axiosFailed());
  }
  return null;
};

export default updateCentre;
