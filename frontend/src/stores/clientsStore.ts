import { defineStore } from 'pinia';
import { useCentreStore } from './centresStore';
import { useUserStore } from './userStore';
import type AbsenceRecord from 'src/types/AbsenceRecord';
import type CarePlan from 'src/types/CarePlan';
import type FeedingRisk from 'src/types/FeedingRIsk';
import type VisitingDay from 'src/types/VisitingDay';
import type ClientDocument from 'src/types/ClientDocument';
import type ClientNeed from 'src/types/ClientNeed';
import type Client from 'src/types/Client';
export const useClientStore = defineStore('clients', {
  state: () => ({
    clients: {} as Record<string, Client>,
    activeClient: null as Client | null,
    clientsLoading: true,
    setupClient: null as Client | null,
    clientNeeds: {} as Record<string, ClientNeed>,
    carePlans: {} as Record<string, CarePlan>,
    absenceRecords: {} as Record<string, AbsenceRecord>
  }),
  actions: {
    setSetupClient(client: Client | null) {
      this.setupClient = client;
    },
    updateClient(client: Client) {
      if (client.id) {
        const clientId = client.id;
        if (!(clientId in this.clients)) {
          this.clients[clientId] = client;
        } else {
          const keys = Object.keys(client) as (keyof Client)[];
          keys.forEach((key: keyof Client) => {
            if (client[key] !== null) {
              (this.clients[clientId][key] as typeof key) = client[
                key
              ] as typeof key;
            }
          });
        }
        if (this.activeClient && client.id === this.activeClient.id) {
          this.activeClient = this.clients[clientId];
        }
      }
    },
    deleteClient(client: { id: number }) {
      // eslint-disable-next-line @typescript-eslint/no-dynamic-delete
      delete this.clients[client.id];
      if (client.id === this.activeClient?.id) {
        this.activeClient = null;
      }
    },
    deleteClientDocument(documentData: {
      document: ClientDocument;
      clientId: number;
      newClientSize: number;
    }) {
      if (documentData.clientId in this.clients) {
        const clientDocuments = this.clients[documentData.clientId].documents;
        if (clientDocuments) {
          this.clients[documentData.clientId].documents =
            clientDocuments.filter(
              document => document.id !== documentData.document.id
            );
        }
        this.clients[documentData.clientId].storageSize =
          documentData.newClientSize;
      }
    },
    updateClientDocument(documentData: {
      document: ClientDocument;
      newClientSize: number;
    }) {
      if (documentData.document.client in this.clients) {
        const client = this.clients[documentData.document.client];
        const clientDocuments = client.documents;
        let filteredDocuments: ClientDocument[] = [];
        if (clientDocuments) {
          filteredDocuments = clientDocuments.filter(
            document => document.id !== documentData.document.id
          );
        }
        filteredDocuments.push(documentData.document);
        client.documents = filteredDocuments;
        client.storageSize = documentData.newClientSize;
      }
    },
    updateClientVisitingDays(visitingDayData: {
      clientId: number;
      visitingDays: VisitingDay[];
    }) {
      if (visitingDayData.clientId in this.clients) {
        const centres = visitingDayData.visitingDays.map(
          visitingDay => visitingDay.centre
        );
        let filteredCurrentDays: VisitingDay[] = [];
        if (this.clients[visitingDayData.clientId].visitingDays) {
          const visitingDays = this.clients[visitingDayData.clientId]
            .visitingDays as VisitingDay[];
          filteredCurrentDays = visitingDays.filter(
            visitingDay => !centres.includes(visitingDay.centre)
          );
        }
        this.clients[visitingDayData.clientId].visitingDays = [
          ...filteredCurrentDays,
          ...visitingDayData.visitingDays
        ];
      }
    },
    updateClientNeed(clientNeed: ClientNeed) {
      this.clientNeeds[clientNeed.id] = clientNeed;
    },
    updateClientPermissions(clientPermissions: {
      clientId: number;
      permissions: string[];
    }) {
      const userStore = useUserStore();
      if (clientPermissions.clientId in this.clients) {
        this.clients[clientPermissions.clientId].permissions =
          clientPermissions.permissions;
        if (this.activeClient?.id === clientPermissions.clientId) {
          this.activeClient.permissions = clientPermissions.permissions;
          userStore.user.permissions = clientPermissions.permissions;
        }
      }
    },
    updateFeedingRisks(feedingRiskData: {
      clientId: number;
      feedingRisks: FeedingRisk[];
    }) {
      if (feedingRiskData.clientId in this.clients) {
        this.clients[feedingRiskData.clientId].feedingRisks =
          feedingRiskData.feedingRisks;
      }
    },
    async updateClientCentres(clientCentreData: {
      clientId: number;
      centreIds: number[];
    }) {
      if (clientCentreData.clientId in this.clients) {
        this.clients[clientCentreData.clientId].centres =
          clientCentreData.centreIds;
      }
      const userStore = useUserStore();
      if (userStore.user.group === 'client') {
        const centreStore = useCentreStore();
        return centreStore.getCentres();
      }
      return null;
    },
    updateCarePlan(carePlan: CarePlan) {
      this.carePlans[carePlan.id] = carePlan;
    },
    updateCarePlans(carePlans: CarePlan[]) {
      carePlans.forEach(carePlan => (this.carePlans[carePlan.id] = carePlan));
    },
    updateAttendanceRecords(absenceRecords: AbsenceRecord[]) {
      absenceRecords.forEach(
        absenceRecord => (this.absenceRecords[absenceRecord.id] = absenceRecord)
      );
    },
    deleteCarePlans(carePlans: CarePlan[]) {
      carePlans.forEach(
        carePlan =>
          // eslint-disable-next-line @typescript-eslint/no-dynamic-delete
          delete this.carePlans[carePlan.id]
      );
    }
  }
});
