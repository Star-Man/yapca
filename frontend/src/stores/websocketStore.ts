import { defineStore } from 'pinia';
import { useBasicStore } from 'src/stores/basicStore';
import { useUserStore } from 'src/stores/userStore';
import getBackendPath from 'src/functions/backend/getBackendPath';
import { useCentreStore } from './centresStore';
import { useClientStore } from './clientsStore';
import type { WsMessage } from 'src/types/websockets/WsMessage';

const wsProtocol = (): string => {
  return process.env.VUE_APP_WS_PROTOCOL !== 'undefined'
    ? (process.env.VUE_APP_WS_PROTOCOL as string)
    : 'wss';
};

export const useSocketStore = defineStore('socket', {
  state: () => ({ ws: undefined } as { ws?: WebSocket }),
  actions: {
    WS_RECONNECT(appServerAddress: string, token: string) {
      let path = '';
      if (appServerAddress) {
        const url = appServerAddress.replace(/(^\w+:|^)\/\//, '');
        path = `${wsProtocol()}://${url}/ws/main/?token=${token}`;
      } else {
        path = `${wsProtocol()}://${getBackendPath()}/ws/main/?token=${token}`;
      }
      this.ws = new WebSocket(path);
      setupWs(this.ws);
    }
  }
});

const setupWs = (ws: WebSocket): void => {
  const basicStore = useBasicStore();
  const userStore = useUserStore();
  const centreStore = useCentreStore();
  const clientStore = useClientStore();

  ws.addEventListener('message', (event: { data: WsMessage | string }) => {
    let eventData = event.data as WsMessage;
    if (typeof event.data === 'string') {
      eventData = JSON.parse(event.data) as WsMessage;
    }
    switch (eventData.action) {
      case 'updateRegisterAbility':
        basicStore.registerAbility = eventData.data.canRegister;
        break;
      case 'updateUser':
        userStore.updateUser(eventData.data);
        break;
      case 'updateUserCentres':
        userStore.updateUserCentres(eventData.data).catch((err: Error) => err);
        break;
      case 'updateCentre':
        centreStore.updateCentre(eventData.data).catch((err: Error) => err);
        break;
      case 'deleteCentre':
        centreStore.deleteCentre(eventData.data);
        break;
      case 'deleteClientDocument':
        clientStore.deleteClientDocument(eventData.data);
        break;
      case 'updateClientDocument':
        clientStore.updateClientDocument(eventData.data);
        break;
      case 'deleteClient':
        clientStore.deleteClient(eventData.data);
        break;
      case 'updateClient':
        clientStore.updateClient(eventData.data);
        break;
      case 'updateNeed':
        centreStore.updateNeed(eventData.data);
        break;
      case 'updatePolicy':
        centreStore.updatePolicy(eventData.data);
        break;
      case 'updateEvent':
        centreStore.updateEvent();
        break;
      case 'deleteEvent':
        centreStore.updateEvent();
        break;
      case 'updateClientVisitingDays':
        clientStore.updateClientVisitingDays(eventData.data);
        break;
      case 'updateClientNeed':
        clientStore.updateClientNeed(eventData.data);
        break;
      case 'updateFeedingRisks':
        clientStore.updateFeedingRisks(eventData.data);
        break;
      case 'updateClientCentres':
        clientStore
          .updateClientCentres(eventData.data)
          .catch((err: Error) => err);
        break;
      case 'updateCarePlan':
        clientStore.updateCarePlan(eventData.data);
        break;
      case 'updateCarePlans':
        clientStore.updateCarePlans(eventData.data);
        break;
      case 'createAttendanceRecords':
        clientStore.updateAttendanceRecords(eventData.data);
        break;
      case 'deleteCarePlans':
        clientStore.deleteCarePlans(eventData.data);
        break;
      case 'updateClientPermissions':
        clientStore.updateClientPermissions(eventData.data);
        break;
      default:
        console.error('Received unknown ws action', eventData);
    }
  });

  ws.addEventListener('close', () => {
    setTimeout(() => {
      useSocketStore().WS_RECONNECT(
        basicStore.appServerAddress,
        basicStore.token
      );
    }, 1000);
  });
};
