/* eslint-disable @typescript-eslint/no-unsafe-argument */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { store } from 'quasar/wrappers';
import { createPinia } from 'pinia';
import { storePlugin } from 'pinia-plugin-store';
import Utf8 from 'crypto-js/enc-utf8';
import Base64 from 'crypto-js/enc-base64';
import type { Router } from 'vue-router';

declare module 'pinia' {
  export interface PiniaCustomProperties {
    readonly router: Router;
  }
}

function encrypt(value: string): string {
  return Base64.stringify(Utf8.parse(value));
}

function decrypt(value: string): string {
  return Base64.parse(value).toString(Utf8);
}

export default store(() => {
  const pinia = createPinia();
  const plugin = storePlugin({
    stores: [
      { name: 'basic' },
      { name: 'centres' },
      { name: 'clients' },
      { name: 'user' },
      { name: 'socket' }
    ],
    encrypt,
    decrypt
  });
  pinia.use(plugin);
  return pinia;
});
