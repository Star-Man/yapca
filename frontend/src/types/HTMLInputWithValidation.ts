export default interface HTMLInputWithValidation extends HTMLInputElement {
  validate: () => Promise<boolean>;
}
