export default interface CarePlanAggregate {
  totalNeeds: number;
  date: string;
  clientId: number;
  clientName: string;
}
