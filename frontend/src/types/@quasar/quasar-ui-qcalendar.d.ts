declare module '@quasar/quasar-ui-qcalendar/src/index.js' {
  import type QCalendarEvent from '../QCalendarEvent';

  export function indexOf(
    infoWeek: {
      id: number;
      left: number;
      size: number;
      event: QCalendarEvent;
    }[],
    event: (event: { left: number; id: number }) => boolean
  ): number;
}
