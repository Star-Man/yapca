export default interface VisitingDay {
  centre: number;
  day: number;
  client: number;
}
