export default interface ClientDocument {
  id: number;
  name: string;
  originalName: string;
  document: string;
  client: number;
  createdDate: string;
  size: number;
}
