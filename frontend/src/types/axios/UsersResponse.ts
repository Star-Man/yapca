import type User from '../User';

export default interface UsersResponse {
  data: User[];
}
