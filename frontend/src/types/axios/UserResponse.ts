import type User from '../User';

export default interface UserResponse {
  data: User;
}
