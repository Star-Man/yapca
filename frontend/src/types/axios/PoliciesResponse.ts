import type Policy from '../Policy';

export default interface PoliciesResponse {
  data: Policy[];
}
