import type Centre from '../Centre';

export default interface CentreResponse {
  data: Centre;
}
