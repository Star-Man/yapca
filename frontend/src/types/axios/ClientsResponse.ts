import type Client from '../Client';

export default interface ClientsResponse {
  data: Client[];
}
