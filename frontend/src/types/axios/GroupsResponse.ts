import type Group from '../Group';

export default interface GroupsResponse {
  data: Group[];
}
