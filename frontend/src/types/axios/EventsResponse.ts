import type CalendarEvent from '../CalendarEvent';

export default interface EventsResponse {
  data: CalendarEvent[];
}
