import type AbsenceRecord from '../AbsenceRecord';

export default interface AbsencesResponse {
  data: AbsenceRecord[];
}
