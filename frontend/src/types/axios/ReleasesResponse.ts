export default interface ReleasesResponse {
  data: {
    tag_name: string;
    assets: { links: { name: string; url: string }[] };
  }[];
}
