import type CarePlan from '../CarePlan';

export default interface CarePlansResponse {
  data: CarePlan[];
}
