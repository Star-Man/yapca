import type User from '../User';

export default interface LoginResponse {
  data: {
    key: string;
    user: User;
  };
}
