import type Need from '../Need';

export default interface NeedsResponse {
  data: Need[];
}
