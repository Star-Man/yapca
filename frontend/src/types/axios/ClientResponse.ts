import type Client from '../Client';

export default interface ClientResponse {
  data: Client;
}
