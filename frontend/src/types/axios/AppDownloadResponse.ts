export default interface AppDownloadResponse {
  data: Blob;
}
