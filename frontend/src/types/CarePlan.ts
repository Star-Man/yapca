import type ClientNeed from './ClientNeed';
import type Need from './Need';

export default interface CarePlan {
  id: number;
  date: string;
  need: number;
  client: number;
  inputted: boolean;
  comment: string;
  completed: boolean;
  clientNeed?: ClientNeed;
  needObject?: Need;
  addedBy: number;
}
