export default interface QCalendarEvent {
  id: number | null;
  title: string;
  start: string;
  end: string;
  bgcolor: string;
  icon?: string;
  time?: string;
  duration?: number;
}
