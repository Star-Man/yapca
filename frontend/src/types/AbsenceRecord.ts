export default interface AbsenceRecord {
  id: number;
  date: string;
  client: number;
  centre: number;
  reasonForInactivity: string;
  reasonForAbsence: string;
  active: boolean;
  addedBy: number | null;
}
