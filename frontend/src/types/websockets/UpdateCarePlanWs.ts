import type CarePlan from '../CarePlan';

export default interface UpdateCarePlanWs {
  action: 'updateCarePlan';
  data: CarePlan;
}
