import type Policy from '../Policy';

export default interface UpdatePolicyWs {
  action: 'updatePolicy';
  data: Policy;
}
