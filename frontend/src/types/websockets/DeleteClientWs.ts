export default interface DeleteClientWs {
  action: 'deleteClient';
  data: {
    id: number;
  };
}
