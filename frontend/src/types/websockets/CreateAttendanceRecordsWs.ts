import type AbsenceRecord from '../AbsenceRecord';

export default interface CreateAttendanceRecordsWs {
  action: 'createAttendanceRecords';
  data: AbsenceRecord[];
}
