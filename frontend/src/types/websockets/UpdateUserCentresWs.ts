export default interface UpdateUserCentresWs {
  action: 'updateUserCentres';
  data: { userId: number; centreIds: number[] };
}
