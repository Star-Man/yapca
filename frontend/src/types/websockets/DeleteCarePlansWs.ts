import type CarePlan from '../CarePlan';

export default interface DeleteCarePlansWs {
  action: 'deleteCarePlans';
  data: CarePlan[];
}
