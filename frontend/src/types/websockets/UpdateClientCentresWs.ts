export default interface UpdateClientCentresWs {
  action: 'updateClientCentres';
  data: { clientId: number; centreIds: number[] };
}
