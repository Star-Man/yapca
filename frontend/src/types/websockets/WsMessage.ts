import type CreateAttendanceRecordsWs from './CreateAttendanceRecordsWs';
import type UpdateCarePlansWs from './UpdateCarePlansWs';
import type UpdateClientCentresWs from './UpdateClientCentresWs';
import type UpdateFeedingRisksWs from './UpdateFeedingRisksWs';
import type UpdateClientNeedWs from './UpdateClientNeedWs';
import type UpdateClientVisitingDaysWs from './UpdateClientVisitingDaysWs';
import type DeleteEventWs from './DeleteEventWs';
import type UpdateNeedWs from './UpdateNeedWs';
import type DeleteClientWs from './DeleteClientWs';
import type UpdateClientDocumentWs from './UpdateClientDocumentWs';
import type DeleteClientDocumentWs from './DeleteClientDocumentWs';
import type UpdateUserCentresWs from './UpdateUserCentresWs';
import type UpdateRegisterAbilityWs from './UpdateRegisterAbilityWs';
import type UpdateUserWs from './UpdateUserWs';
import type UpdateCentresWs from './UpdateCentreWs';
import type DeleteCentreWs from './DeleteCentreWs';
import type UpdateClientWs from './UpdateClientWs';
import type UpdatePolicyWs from './UpdatePolicyWs';
import type updateEventWs from './updateEventWs';
import type UpdateCarePlanWs from './UpdateCarePlanWs';
import type DeleteCarePlansWs from './DeleteCarePlansWs';
import type UpdateClientPermissionsWs from './UpdateClientPermissionsWs';

export type WsMessage =
  | UpdateRegisterAbilityWs
  | UpdateUserWs
  | UpdateUserCentresWs
  | UpdateCentresWs
  | DeleteCentreWs
  | DeleteClientDocumentWs
  | UpdateClientDocumentWs
  | DeleteClientWs
  | UpdateClientWs
  | UpdateNeedWs
  | UpdatePolicyWs
  | updateEventWs
  | DeleteEventWs
  | UpdateClientVisitingDaysWs
  | UpdateClientNeedWs
  | UpdateFeedingRisksWs
  | UpdateClientCentresWs
  | UpdateCarePlanWs
  | UpdateCarePlansWs
  | CreateAttendanceRecordsWs
  | DeleteCarePlansWs
  | UpdateClientPermissionsWs;
