import type Client from '../Client';

export default interface UpdateClientWs {
  action: 'updateClient';
  data: Client;
}
