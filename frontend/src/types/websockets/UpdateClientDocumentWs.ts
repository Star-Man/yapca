import type ClientDocument from '../ClientDocument';

export default interface UpdateClientDocumentWs {
  action: 'updateClientDocument';
  data: {
    document: ClientDocument;
    newClientSize: number;
  };
}
