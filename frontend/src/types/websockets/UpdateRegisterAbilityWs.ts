export default interface UpdateRegisterAbilityWs {
  action: 'updateRegisterAbility';
  data: {
    id: number;
    canRegister: boolean;
  };
}
