import type VisitingDay from '../VisitingDay';

export default interface UpdateClientVisitingDaysWs {
  action: 'updateClientVisitingDays';
  data: {
    clientId: number;
    visitingDays: VisitingDay[];
  };
}
