import type FeedingRisk from '../FeedingRIsk';

export default interface UpdateFeedingRisksWs {
  action: 'updateFeedingRisks';
  data: { clientId: number; feedingRisks: FeedingRisk[] };
}
