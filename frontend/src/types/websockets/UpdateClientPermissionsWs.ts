export default interface UpdateClientPermissionsWs {
  action: 'updateClientPermissions';
  data: {
    clientId: number;
    permissions: string[];
  };
}
