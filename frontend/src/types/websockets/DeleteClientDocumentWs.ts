import type ClientDocument from '../ClientDocument';

export default interface DeleteClientDocumentWs {
  action: 'deleteClientDocument';
  data: {
    document: ClientDocument;
    clientId: number;
    newClientSize: number;
  };
}
