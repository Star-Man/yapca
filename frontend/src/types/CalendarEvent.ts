export default interface CalendarEvent {
  id: number | null;
  name: string;
  start: string;
  color: string;
  end?: string;
  centre?: number;
  centreName?: string;
  centreId?: number;
  timePeriod?: number;
  eventType?: 'Public Holiday' | 'User Close Event' | 'Online' | 'On-Site';
  allDay?: boolean;
}
