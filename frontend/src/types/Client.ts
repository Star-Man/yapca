import type AgreedPolicy from './AgreedPolicy';
import type NextOfKin from './NextOfKin';
import type ClientDocument from './ClientDocument';
import type FeedingRisk from './FeedingRIsk';
import type Medication from './Medication';
import type VisitingDay from './VisitingDay';

export default interface Client {
  id: null | number;
  admittedBy: number | string;
  createdDate: string;
  agreedPolicies?: (number | AgreedPolicy)[];
  needs?: number[];
  visitingDays?: VisitingDay[];
  nextOfKin?: NextOfKin[];
  centres?: number[];
  isActive: null | boolean;
  firstName: string;
  surname: string;
  storageSize: number;
  basicInfoSetup: boolean;
  additionalInfoSetup: boolean;
  servicesSetup: boolean;
  abilitySetup: boolean;
  dietSetup: boolean;
  visitingDaysSetup: boolean;
  setupComplete: boolean;
  phoneNumber: string;
  dateOfBirth: string;
  address: string;
  gender: 'Unknown' | 'Female' | 'Male';
  publicHealthNurseName: string;
  publicHealthNursePhoneNumber: string;
  homeHelpName: string;
  homeHelpPhoneNumber: string;
  gpName: string;
  gpPhoneNumber: string;
  chemistName: string;
  chemistPhoneNumber: string;
  medicalHistory: string;
  surgicalHistory: string;
  medications?: Medication[];
  mobility:
    | 'Walks Unaided'
    | 'Walks With Stick'
    | 'Walks With Frame'
    | 'Walks With Walker'
    | 'Uses Wheelchair';
  toileting: 'Fully Continent' | 'Occasionally Incontinent' | 'Incontinent';
  hygiene: 'Self' | 'Needs Assistance' | 'Full Hoist Shower';
  sight: 'Normal' | 'Glasses' | 'Impaired';
  hearing: 'Normal' | 'Impaired';
  usesDentures: boolean;
  caseFormulation: string;
  allergies: string;
  intolerances: string;
  requiresAssistanceEating: boolean;
  thicknerGrade: 0 | 1 | 2 | 3 | 4 | 5;
  feedingRisks?: FeedingRisk[];
  documents: undefined | ClientDocument[];
  reasonForInactivity:
    | 'Deceased'
    | 'Entered Long Term Care'
    | 'No Longer Attending'
    | 'Needs Unmet By Service';
  accountStatus: 'Active' | 'Pending' | 'Inactive' | 'No Account';
  permissions?: string[];
}
