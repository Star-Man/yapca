export default interface NextOfKin {
  id: number;
  name: string;
  phoneNumber: string;
}
