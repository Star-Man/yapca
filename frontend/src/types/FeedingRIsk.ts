export default interface FeedingRisk {
  id: number;
  feedingRisk: 'Chewing' | 'Swallowing' | 'Poor Appetite';
}
