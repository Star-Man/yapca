export default interface ClientNeed {
  id: number;
  centre: number;
  need: number;
  plan: string;
  isActive: boolean;
}
