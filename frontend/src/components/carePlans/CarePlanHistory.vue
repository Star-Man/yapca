<template>
  <q-card-section>
    <q-input
      v-model="search"
      for="historySearch"
      dense
      :label="`Search ${clientName}'s History`"
    >
      <template #append>
        <q-icon :name="mdiMagnify" />
      </template>
    </q-input>

    <responsive-table
      table-id="carePlanHistoryTable"
      :loading="loading"
      :headers="carePlanHeaders"
      :rows="readableCarePlans"
      :filter="search"
      no-results-label="No care plans found"
      :no-data-label="`${clientName} has no previous care plans`"
      default-sort="date"
      :default-sort-decending="true"
      loading-label="Loading Historic Care Plans..."
      :sort-method="customSort"
    >
      <template #completed="{ cellData }">
        <q-icon
          v-if="cellData.completed"
          :name="mdiThumbUp"
          color="positive"
          class="completedIcon"
          size="sm"
        />
        <q-icon
          v-else
          size="sm"
          class="notCompletedIcon"
          :name="mdiThumbDown"
          color="negative"
        />
      </template>
    </responsive-table>
  </q-card-section>
</template>
<script lang="ts" setup>
import { mdiMagnify, mdiThumbDown, mdiThumbUp } from '@mdi/js';
import { useClientStore } from 'src/stores/clientsStore';
import { useUserStore } from 'src/stores/userStore';
import { computed, ref, watch } from 'vue';
import { useCentreStore } from 'src/stores/centresStore';
import getClientNeedById from 'src/functions/client/getClientNeedById';
import formatDateTime from 'src/functions/date/formatDateTime';
import { useBasicStore } from 'src/stores/basicStore';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import ResponsiveTable from 'src/components/ui/ResponsiveTable.vue';
import sortAlphabetically from 'src/functions/common/sortAlphabetically';
import type CarePlansResponse from 'src/types/axios/CarePlansResponse';
import type CarePlan from 'src/types/CarePlan';

interface Props {
  clientId: number | undefined;
}
const props = withDefaults(defineProps<Props>(), {
  clientId: undefined
});

const basicStore = useBasicStore();
const userStore = useUserStore();
const clientStore = useClientStore();
const centreStore = useCentreStore();

const loading = ref(true);
const search = ref('');

const carePlanHeaders = [
  {
    label: 'Date',
    name: 'date',
    field: 'date',
    sortable: true
  },
  {
    label: 'Centre',
    name: 'centre',
    field: 'centre',
    sortable: true
  },
  {
    label: 'Need',
    name: 'need',
    field: 'need',
    sortable: true
  },
  {
    label: 'Plan',
    name: 'plan',
    field: 'plan',
    sortable: true
  },
  {
    label: 'Comment',
    name: 'comment',
    field: 'comment',
    sortable: true
  },
  {
    label: 'Completed?',
    name: 'completed',
    field: 'completed',
    sortable: true
  },
  {
    label: 'Added By',
    name: 'addedBy',
    field: 'addedBy',
    sortable: true
  }
];

const userCentres = computed(() => {
  return userStore.user.centres as number[];
});

const clientName = computed(() => {
  if (props.clientId && props.clientId in clientStore.clients) {
    return clientStore.clients[props.clientId].firstName;
  }
  return '';
});

const carePlans = computed(() => {
  return Object.keys(clientStore.carePlans)
    .map(carePlanKey => {
      return { ...clientStore.carePlans[carePlanKey] };
    })
    .filter((carePlan: CarePlan) => carePlan.inputted)
    .filter((carePlan: CarePlan) => carePlan.client === props.clientId)
    .filter((carePlan: CarePlan) => carePlan.need in clientStore.clientNeeds)
    .filter((carePlan: CarePlan) => {
      const clientNeed = clientStore.clientNeeds[carePlan.need];
      const needId = clientNeed.need;
      const clientCentreId = clientNeed.centre;
      if (clientCentreId in centreStore.centres) {
        const centre = centreStore.centres[clientCentreId];
        if (centre.needs) {
          const centreNeeds = centre.needs;
          return centreNeeds.includes(needId);
        }
      }
      return false;
    })
    .filter((carePlan: CarePlan) =>
      userCentres.value.includes(clientStore.clientNeeds[carePlan.need].centre)
    );
});

const readableCarePlans = computed(() => {
  return carePlans.value.map((carePlan: CarePlan) => {
    const clientNeed = getClientNeedById(carePlan.need);
    let addedBy = 'Unknown';
    if (carePlan.addedBy in userStore.users) {
      addedBy = userStore.users[carePlan.addedBy].displayName;
    }
    return {
      id: carePlan.id,
      date: formatDateTime(carePlan.date, false, false),
      need: clientNeed.needDescription,
      plan: clientNeed.plan,
      comment: carePlan.comment,
      completed: carePlan.completed,
      centre: clientNeed.centre,
      addedBy
    };
  });
});

const getCarePlanHistory = (): Promise<void> | null => {
  if (props.clientId) {
    loading.value = true;
    return basicStore.http
      .get(`carePlansHistory/${props.clientId}/`)
      .then((response: CarePlansResponse) => {
        const initialValue = {};
        const newCarePlans = response.data.reduce(
          (carePlanObject: Record<string, CarePlan>, carePlan: CarePlan) => {
            if (carePlan.id) {
              const updatedCarePlan = { ...carePlan };
              if (carePlan.needObject) {
                centreStore.needs[carePlan.needObject.id] = carePlan.needObject;
                delete updatedCarePlan.needObject;
              }
              if (carePlan.clientNeed) {
                clientStore.clientNeeds[carePlan.clientNeed.id] =
                  carePlan.clientNeed;
                delete updatedCarePlan.clientNeed;
              }
              return {
                ...carePlanObject,
                [carePlan.id.toString()]: updatedCarePlan
              };
            }
            return carePlanObject;
          },
          initialValue
        );
        clientStore.carePlans = {
          ...clientStore.carePlans,
          ...newCarePlans
        };
        loading.value = false;
      })
      .catch(axiosFailed);
  }
  return null;
};
watch(
  () => props.clientId,
  () => {
    return getCarePlanHistory();
  }
);

const customSort = (
  items: readonly CarePlan[],
  sortBy: string,
  isDesc: boolean
): readonly CarePlan[] => {
  const carePlanSortBy = sortBy as keyof CarePlan;
  const carePlans = [...items];
  carePlans.sort((a: CarePlan, b: CarePlan) => {
    if (carePlanSortBy === 'date' && b.id && a.id) {
      if (isDesc) {
        return (
          new Date(clientStore.carePlans[b.id].date).getTime() -
          new Date(clientStore.carePlans[a.id].date).getTime()
        );
      }
      return (
        new Date(clientStore.carePlans[a.id].date).getTime() -
        new Date(clientStore.carePlans[b.id].date).getTime()
      );
    }
    return sortAlphabetically(
      a[carePlanSortBy] as string,
      b[carePlanSortBy] as string,
      isDesc
    );
  });
  return carePlans;
};

getCarePlanHistory()?.catch((err: Error) => err);
</script>
