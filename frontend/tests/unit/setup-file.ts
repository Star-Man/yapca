import * as dotenv from 'dotenv';

dotenv.config({
  path: '../config/local-testing.env'
});
