import { useUserStore } from 'src/stores/userStore';

export default (): void => {
  const userStore = useUserStore();
  userStore.users = {
    1: {
      id: 1,
      username: 'testUser',
      displayName: 'Test User',
      accentColor: '#FFFFFF',
      darkTheme: false,
      group: 'admin',
      groups: [{ id: 4, name: 'admin' }],
      isActive: true,
      centres: [1],
      userClientTableHiddenRows: [],
      email: 'test1@test.test'
    },
    2: {
      id: 2,
      username: 'testUser2',
      displayName: 'Test User 2',
      accentColor: '#FFFFFF',
      darkTheme: false,
      group: 'nurse',
      groups: [{ id: 3, name: 'nurse' }],
      isActive: true,
      centres: [2],
      userClientTableHiddenRows: [],
      email: 'test2@test.test'
    },
    3: {
      id: 3,
      username: 'testUser3',
      displayName: 'Test User 3',
      accentColor: '#FFFFFF',
      darkTheme: false,
      group: 'nurse',
      groups: [{ id: 3, name: 'nurse' }],
      isActive: true,
      centres: [1, 2],
      userClientTableHiddenRows: [],
      email: 'test3@test.test',
      client: undefined
    },
    4: {
      id: 4,
      username: 'testClientUser1',
      displayName: '',
      accentColor: '#FFFFFF',
      darkTheme: false,
      group: 'client',
      groups: [{ id: 3, name: 'client' }],
      isActive: true,
      centres: undefined,
      userClientTableHiddenRows: [],
      email: 'test4@test.test',
      client: 2
    }
  };
  userStore.user = {
    id: 1,
    username: 'testUser',
    displayName: 'Test User',
    accentColor: '#FFFFFF',
    darkTheme: false,
    group: 'admin',
    groups: [{ id: 4, name: 'admin' }],
    isActive: true,
    centres: [1],
    userClientTableHiddenRows: [],
    email: 'test1@test.test'
  };
};
