import { useClientStore } from 'src/stores/clientsStore';
import type Client from 'src/types/Client';

const baseClient: Client = {
  id: 1,
  admittedBy: 1,
  isActive: true,
  gender: 'Female',
  createdDate: '2020-06-26T21:05:01',
  agreedPolicies: [],
  centres: [1],
  visitingDays: [],
  firstName: 'test name 1',
  surname: 'test surname 1',
  basicInfoSetup: true,
  setupComplete: true,
  visitingDaysSetup: true,
  additionalInfoSetup: true,
  servicesSetup: true,
  abilitySetup: true,
  address: 'Test Address 1',
  phoneNumber: '11111111',
  dateOfBirth: '1911-11-10',
  nextOfKin: [],
  publicHealthNurseName: 'PHN 1',
  publicHealthNursePhoneNumber: 'PHN 1111111',
  homeHelpName: 'HH 1',
  homeHelpPhoneNumber: 'HH 1111111',
  gpName: 'GP 1',
  gpPhoneNumber: 'GP 1111111',
  chemistName: 'C 1',
  chemistPhoneNumber: 'C 1111111',
  medicalHistory: 'Client 1 Medical History',
  surgicalHistory: 'Client 1 Surgical History',
  medications: [],
  mobility: 'Walks Unaided',
  toileting: 'Incontinent',
  hygiene: 'Self',
  sight: 'Glasses',
  hearing: 'Normal',
  usesDentures: true,
  caseFormulation: 'Example Case Formulation 1',
  dietSetup: true,
  allergies: 'Allergies List 1',
  intolerances: 'Intolerances List 1',
  requiresAssistanceEating: true,
  thicknerGrade: 3,
  feedingRisks: [
    {
      id: 1,
      feedingRisk: 'Chewing'
    }
  ],
  needs: [],
  documents: [],
  storageSize: 60,
  reasonForInactivity: 'Deceased',
  accountStatus: 'No Account'
};

export default (): void => {
  const clientsStore = useClientStore();
  clientsStore.clients = {
    1: { ...baseClient },
    2: {
      ...baseClient,
      id: 2,
      firstName: 'test name 2',
      surname: 'test surname 2',
      dateOfBirth: '1979-03-11',
      visitingDays: [
        { day: 4, centre: 4, client: 2 },
        { day: 5, centre: 4, client: 2 },
        { day: 6, centre: 5, client: 2 }
      ],
      homeHelpPhoneNumber: 'HH 2222222',
      homeHelpName: 'HH 2',
      gpPhoneNumber: 'GP 2222222',
      gpName: 'GP 2',
      chemistPhoneNumber: 'C 2222222',
      chemistName: 'C 2',
      caseFormulation: 'Example Case Formulation 2',
      address: 'Test Address 2',
      allergies: 'Allergies List 2',
      isActive: false,
      setupComplete: false,
      storageSize: 10,
      publicHealthNurseName: 'PHN 2',
      publicHealthNursePhoneNumber: 'PHN 2222222',
      reasonForInactivity: 'Needs Unmet By Service',
      surgicalHistory: 'Client 2 Surgical History',
      thicknerGrade: 1,
      phoneNumber: '2222222',
      medicalHistory: 'Client 2 Medical History',
      intolerances: 'Intolerances List 2',
      feedingRisks: [
        {
          id: 2,
          feedingRisk: 'Swallowing'
        }
      ],
      dietSetup: false,
      accountStatus: 'Active'
    },
    3: {
      ...baseClient,
      id: 3,
      firstName: 'test name 3',
      surname: 'test surname 3',
      dateOfBirth: '1891-01-03',
      visitingDays: [
        { day: 4, centre: 4, client: 3 },
        { day: 2, centre: 2, client: 3 },
        { day: 3, centre: 3, client: 3 },
        { day: 4, centre: 2, client: 3 },
        { day: 1, centre: 2, client: 3 }
      ],
      storageSize: 40,
      requiresAssistanceEating: false,
      publicHealthNursePhoneNumber: 'PHN 3333333',
      publicHealthNurseName: 'PHN 3',
      thicknerGrade: 5,
      surgicalHistory: 'Client 3 Surgical History',
      phoneNumber: '3333333',
      medicalHistory: 'Client 3 Medical History',
      intolerances: 'Intolerances List 3',
      homeHelpPhoneNumber: 'HH 3333333',
      homeHelpName: 'HH 3',
      gpPhoneNumber: 'GP 3333333',
      gpName: 'GP 3',
      gender: 'Unknown',
      feedingRisks: [
        {
          id: 1,
          feedingRisk: 'Poor Appetite'
        }
      ],
      chemistPhoneNumber: 'C 3333333',
      chemistName: 'C 3',
      caseFormulation: 'Example Case Formulation 3',
      address: 'Test Address 3',
      allergies: 'Allergies List 3',
      accountStatus: 'Active'
    },
    4: {
      ...baseClient,
      id: 4,
      firstName: 'test name 4',
      surname: 'test surname 4',
      dateOfBirth: '1991-07-15',
      feedingRisks: [
        {
          id: 1,
          feedingRisk: 'Poor Appetite'
        },
        {
          id: 2,
          feedingRisk: 'Chewing'
        }
      ],
      nextOfKin: [
        { id: 1, name: 'Test NoK 1', phoneNumber: '11111111' },
        { id: 2, name: 'Test NoK 2', phoneNumber: '22222222' },
        { id: 3, name: 'Test NoK 3', phoneNumber: '33333333' },
        { id: 4, name: 'Test NoK 4', phoneNumber: '44444444' }
      ],
      documents: [
        {
          id: 1,
          name: 'testFile1',
          originalName: 'testFile1.txt',
          document: 'testserver/testFile1.txt',
          client: 4,
          size: 400,
          createdDate: '2022-01-01'
        },
        {
          id: 2,
          name: 'testFile2',
          originalName: 'testFile2.png',
          document: 'testserver/testFile2.png',
          client: 4,
          size: 600,
          createdDate: '2022-01-02'
        },
        {
          id: 3,
          name: 'testFile3',
          originalName: 'testFile3.pdf',
          document: 'testserver/testFile3.pdf',
          client: 4,
          size: 302,
          createdDate: '2022-01-03'
        }
      ],
      medications: [
        {
          id: 1,
          name: 'Test Med 1',
          dosage: '1',
          frequency: '5',
          inactiveDate: '2020-03-12'
        },
        {
          id: 2,
          name: 'Test Med 2',
          dosage: '2',
          frequency: '6',
          inactiveDate: undefined
        },
        {
          id: 3,
          name: 'Test Med 3',
          dosage: '3',
          frequency: '7',
          inactiveDate: '3020-03-12'
        },
        {
          id: 4,
          name: 'Test Med 4',
          dosage: '4',
          frequency: '8',
          inactiveDate: undefined
        }
      ],
      needs: [1, 2, 3],
      storageSize: 20,
      centres: [3],
      setupComplete: false,
      visitingDays: [
        { day: 1, centre: 3, client: 4 },
        { day: 3, centre: 3, client: 4 },
        { day: 4, centre: 2, client: 4 },
        { day: 1, centre: 2, client: 4 },
        { day: 6, centre: 3, client: 4 }
      ],
      surgicalHistory: 'Client 4 Surgical History',
      thicknerGrade: 2,
      phoneNumber: '4444444',
      medicalHistory: 'Client 4 Medical History',
      intolerances: 'Intolerances List 4',
      homeHelpPhoneNumber: 'HH 4444444',
      homeHelpName: 'HH 4',
      gpPhoneNumber: 'GP 4444444',
      gpName: 'GP 4',
      publicHealthNursePhoneNumber: 'PHN 4444444',
      publicHealthNurseName: 'PHN 4',
      gender: 'Male',
      address: 'Test Address 4',
      chemistName: 'C 4',
      chemistPhoneNumber: 'C 4444444',
      allergies: 'Allergies List 4',
      caseFormulation: 'Example Case Formulation 4',
      accountStatus: 'No Account'
    },
    5: {
      ...baseClient,
      id: 5,
      storageSize: 0
    },
    6: {
      ...baseClient,
      id: 6,
      storageSize: 0
    },
    7: {
      ...baseClient,
      id: 7,
      storageSize: 0
    },
    8: {
      ...baseClient,
      id: 8,
      storageSize: 0
    },
    9: {
      ...baseClient,
      id: 9,
      storageSize: 0
    },
    10: {
      ...baseClient,
      id: 10,
      storageSize: 0
    },
    11: {
      ...baseClient,
      id: 11,
      storageSize: 0
    },
    12: {
      ...baseClient,
      id: 12,
      storageSize: 0,
      isActive: false
    },
    13: {
      ...baseClient,
      id: 13,
      storageSize: 0
    },
    14: {
      ...baseClient,
      id: 14,
      storageSize: 0
    }
  };
  clientsStore.setupClient = { ...baseClient };
  clientsStore.clientNeeds = {
    '1': {
      id: 1,
      need: 1,
      centre: 3,
      plan: 'Example Plan 1',
      isActive: true
    },
    '2': {
      id: 2,
      need: 2,
      centre: 3,
      plan: 'Example Plan 2',
      isActive: true
    },
    '3': {
      id: 3,
      need: 3,
      centre: 3,
      plan: 'Example Plan 3',
      isActive: true
    }
  };
  clientsStore.clientsLoading = true;
  clientsStore.absenceRecords = {
    '1': {
      id: 1,
      date: '2011-12-11',
      centre: 1,
      client: 1,
      active: false,
      reasonForInactivity: 'Deceased',
      reasonForAbsence: '',
      addedBy: null
    },
    '4': {
      id: 4,
      date: '2011-12-13',
      centre: 2,
      client: 3,
      active: false,
      reasonForInactivity: 'Deceased',
      reasonForAbsence: '',
      addedBy: 1
    },
    '5': {
      id: 5,
      date: '2011-12-14',
      centre: 2,
      client: 1,
      active: false,
      reasonForInactivity: 'Deceased',
      reasonForAbsence: '',
      addedBy: 1
    },
    '6': {
      id: 6,
      date: '2011-12-16',
      centre: 2,
      client: 1,
      active: true,
      reasonForInactivity: '',
      reasonForAbsence: 'Busy',
      addedBy: 999
    }
  };
};
