const CarePlanAPIGet = {
  data: [
    {
      id: 10,
      date: '2011-12-12',
      need: 2,
      client: 4,
      inputted: true,
      comment: 'example comment 10',
      completed: false,
      needObject: {
        id: 3,
        description: 'test need 3'
      },
      clientNeed: {
        id: 5,
        centre: 3,
        need: 3,
        plan: 'example plan',
        isActive: true
      }
    },
    {
      id: 11,
      date: '2011-12-14',
      need: 3,
      client: 4,
      inputted: true,
      comment: 'example comment 11',
      completed: false
    },
    {
      date: '2011-12-12',
      need: 2,
      client: 4,
      inputted: true,
      comment: 'example comment 11',
      completed: false
    }
  ]
};
export default CarePlanAPIGet;
