import { useCentreStore } from 'src/stores/centresStore';
import type IncludedCountries from 'src/types/IncludedCountries';

const baseCentre = {
  id: 1,
  name: 'Test Centre 1',
  created: 'before',
  openingDaysFirstSetupComplete: true,
  closingDaysFirstSetupComplete: true,
  policies: [2, 3],
  policiesFirstSetupComplete: true,
  needs: [],
  needsFirstSetupComplete: true,
  setupComplete: true,
  openingDays: [2, 4, 6],
  editingCentreName: false,
  nameValid: false,
  color: '#FFFFFF',
  closedOnPublicHolidays: false,
  country: 'JP' as keyof IncludedCountries,
  state: ''
};

export default (): void => {
  const centreStore = useCentreStore();
  centreStore.policies = {
    '2': { id: 2, description: 'Test Policy 2' },
    '3': { id: 3, description: 'Test Policy 3' }
  };
  centreStore.needs = {
    '1': { id: 1, description: 'Test Need 1' },
    '2': { id: 2, description: 'Test Need 4' },
    '3': { id: 3, description: 'Test Need 3' }
  };
  centreStore.centres = {
    1: { ...baseCentre },
    2: {
      ...baseCentre,
      id: 2,
      name: 'Test Centre 2',
      policies: [],
      closedOnPublicHolidays: true,
      country: 'AU',
      state: 'QLD',
      setupComplete: false
    },
    3: {
      ...baseCentre,
      id: 3,
      name: 'Test Centre 3',
      policies: [],
      needs: [1, 2, 3],
      country: 'US',
      state: 'TX'
    },
    4: {
      ...baseCentre,
      id: 4
    },
    5: {
      ...baseCentre,
      id: 5
    },
    6: {
      ...baseCentre,
      id: 6
    },
    7: {
      ...baseCentre,
      id: 7
    },
    8: {
      ...baseCentre,
      id: 8
    },
    9: {
      ...baseCentre,
      id: 9
    },
    10: {
      ...baseCentre,
      id: 10
    },
    11: {
      ...baseCentre,
      id: 11
    }
  };
  centreStore.setupCentre = {
    ...baseCentre,
    id: 2,
    name: 'Test Centre',
    country: 'AU',
    state: 'QLD',
    closedOnPublicHolidays: true,
    nameValid: true,
    setupComplete: false,
    needsFirstSetupComplete: false,
    openingDaysFirstSetupComplete: false,
    closingDaysFirstSetupComplete: false,
    created: '',
    policiesFirstSetupComplete: false
  };

  centreStore.events = [
    {
      id: 1,
      name: 'event 1',
      color: '#FAFAFA',
      start: '2022-01-08T00:00:00.000Z',
      end: '2022-01-10T00:00:00.000Z',
      centre: 2,
      eventType: 'User Close Event',
      timePeriod: 172800
    },
    {
      id: 2,
      name: 'event 2',
      color: '#AAAAAA',
      start: '2022-05-03T00:00:00.000Z',
      end: '2022-05-04T00:00:00.000Z',
      eventType: 'Public Holiday',
      timePeriod: 86400
    },
    {
      id: 3,
      name: 'event 3',
      color: '#AAAAAA',
      start: '2022-07-03T00:00:00.000Z',
      end: '2022-07-04T00:00:00.000Z',
      centre: 400,
      eventType: 'User Close Event',
      timePeriod: 86400
    },
    {
      id: 4,
      name: 'event 4',
      color: '#FAFAFA',
      start: '2022-10-02T00:00:00.000Z',
      end: '2022-10-03T00:00:00.000Z',
      centre: 2,
      eventType: 'Public Holiday',
      timePeriod: 86400
    }
  ];
};
