import { formatISO, startOfToday, startOfYesterday } from 'date-fns';
import { useClientStore } from 'src/stores/clientsStore';

export default (): void => {
  const clientsStore = useClientStore();

  clientsStore.carePlans = {
    '1': {
      id: 1,
      date: formatISO(startOfToday()),
      need: 1,
      client: 1,
      inputted: false,
      comment: 'example comment 1',
      completed: false,
      addedBy: 1
    },
    '2': {
      id: 2,
      date: formatISO(startOfToday()),
      need: 2,
      client: 1,
      inputted: false,
      comment: 'example comment 2',
      completed: false,
      addedBy: 2
    },
    '3': {
      id: 3,
      date: formatISO(startOfToday()),
      need: 3,
      client: 1,
      inputted: false,
      comment: 'example comment 3',
      completed: false,
      addedBy: 3
    },
    '4': {
      id: 4,
      date: formatISO(startOfYesterday()),
      need: 2,
      client: 1,
      inputted: false,
      comment: 'example comment 4',
      completed: false,
      addedBy: 1
    },
    '5': {
      id: 5,
      date: formatISO(startOfYesterday()),
      need: 1,
      client: 1,
      inputted: false,
      comment: 'example comment 5',
      completed: false,
      addedBy: 2
    },
    '6': {
      id: 6,
      date: '2019-11-20',
      need: 1,
      client: 2,
      inputted: false,
      comment: 'example comment 6',
      completed: false,
      addedBy: 3
    },
    '7': {
      id: 7,
      date: '2019-11-20',
      need: 2,
      client: 2,
      inputted: false,
      comment: 'example comment 7',
      completed: false,
      addedBy: 1
    },
    '8': {
      id: 8,
      date: '2019-11-20',
      need: 2,
      client: 2,
      inputted: true,
      comment: 'example comment 8',
      completed: false,
      addedBy: 2
    },
    '9': {
      id: 9,
      date: '2019-11-20',
      need: 2000,
      client: 2,
      inputted: false,
      comment: 'example comment 9',
      completed: false,
      addedBy: 3
    },
    '10': {
      id: 10,
      date: '2019-11-21',
      need: 1,
      client: 1,
      inputted: false,
      comment: 'example comment 10',
      completed: false,
      addedBy: 1
    },
    '11': {
      id: 11,
      date: '2019-11-22',
      need: 1,
      client: 1,
      inputted: false,
      comment: 'example comment 11',
      completed: false,
      addedBy: 1
    },
    '12': {
      id: 11,
      date: '2019-11-23',
      need: 1,
      client: 1,
      inputted: false,
      comment: 'example comment 12',
      completed: false,
      addedBy: 1
    },
    '13': {
      id: 13,
      date: '2019-11-24',
      need: 1,
      client: 1,
      inputted: false,
      comment: 'example comment 13',
      completed: false,
      addedBy: 1
    },
    '14': {
      id: 14,
      date: '2019-11-25',
      need: 1,
      client: 1,
      inputted: false,
      comment: 'example comment 14',
      completed: false,
      addedBy: 1
    },
    '15': {
      id: 15,
      date: '2019-11-26',
      need: 1,
      client: 1,
      inputted: false,
      comment: 'example comment 15',
      completed: false,
      addedBy: 1
    },
    '16': {
      id: 16,
      date: '2019-11-27',
      need: 1,
      client: 1,
      inputted: false,
      comment: 'example comment 16',
      completed: false,
      addedBy: 1
    },
    '17': {
      id: 17,
      date: '2019-11-28',
      need: 1,
      client: 1,
      inputted: false,
      comment: 'example comment 17',
      completed: false,
      addedBy: 1
    },
    '18': {
      id: 18,
      date: '2011-12-11',
      need: 3,
      client: 4,
      inputted: true,
      comment: 'example comment 18',
      completed: true,
      addedBy: 3
    },
    '19': {
      id: 19,
      date: '2011-12-12',
      need: 2,
      client: 4,
      inputted: true,
      comment: 'example comment 19',
      completed: false,
      addedBy: 1
    },
    '20': {
      id: 20,
      date: '2011-11-18',
      need: 2,
      client: 4,
      inputted: false,
      comment: 'example comment 20',
      completed: true,
      addedBy: 1
    },
    '21': {
      id: 21,
      date: '2011-11-18',
      need: 1,
      client: 4,
      inputted: false,
      comment: 'example comment 21',
      completed: false,
      addedBy: 2
    }
  };
};
