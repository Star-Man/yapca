/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { nextTick } from 'vue';
import { flushPromises, shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import App from 'src/App.vue';
import logout from 'src/functions/auth/logout';
import setup from 'tests/unit/functions/setup';
import { Notify, getCssVar } from 'quasar';
import { mockCentres, mockClients, mockUsers } from 'tests/unit/mocks';
import { getRouter } from 'vue-router-mock';
import axiosFailed from 'src/functions/proxy/AxiosFailed';

import fileDownload from 'js-file-download';
import type { SpyInstance } from 'vitest';

setup();

vi.mock('js-file-download');

describe('App.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let notifySpy: SpyInstance;
  let httpGet: SpyInstance;
  let get: SpyInstance;

  let releaseLinks: {
    data: {
      tag_name: string;
      assets: {
        links: {
          name: string;
          url: string;
        }[];
      };
    }[];
  };

  beforeEach(() => {
    vi.resetAllMocks();
    mockCentres();
    mockUsers();
    vi.mock('src/functions/proxy/AxiosFailed');
    wrapper = shallowMount(App);
    notifySpy = vi.spyOn(Notify, 'create');
    wrapper.vm.basicStore.clientVersion = '4.3.2';
    releaseLinks = {
      data: [
        {
          tag_name: '3.2.1',
          assets: {
            links: [
              {
                name: 'Windows Release',
                url: 'windowsurl.exe'
              },
              {
                name: 'Windows Checksum',
                url: 'windowsurl.exe.sha512'
              },
              {
                name: 'Linux RPM Release',
                url: 'linuxurl.rpm'
              },
              {
                name: 'Linux RPM Checksum',
                url: 'linuxurl.rpm.sha512'
              },
              {
                name: 'Linux Deb Release',
                url: 'linuxurl.deb'
              },
              {
                name: 'Linux Deb Checksum',
                url: 'linuxurl.deb.sha512'
              }
            ]
          }
        }
      ]
    };
    httpGet = vi.spyOn(wrapper.vm.connection, 'get');
    get = vi.spyOn(wrapper.vm.basicStore.http, 'get');
  });

  it('setting token sets users and clients', async () => {
    wrapper.vm.clientStore.clients = {};
    get
      .mockResolvedValueOnce({
        data: [
          {
            id: 1,
            admittedBy: 3,
            isActive: true,
            createdDate: '2020-06-26T21:05:01',
            agreedPolicies: [],
            centres: [1, 2],
            visitingDays: [],
            firstName: 'test name',
            surname: 'test surname',
            visitingDaysSetup: true,
            additionalInfoSetup: true,
            servicesSetup: true,
            abilitySetup: true,
            dietSetup: true,
            address: 'Test Address 1',
            phoneNumber: '11111111',
            dateOfBirth: '1911-11-10',
            nextOfKin: [],
            setupComplete: true
          },
          {
            id: undefined,
            admittedBy: 3
          }
        ]
      })
      .mockResolvedValueOnce({
        data: [
          {
            id: 3,
            username: 'test',
            displayName: 'Test User',
            centres: [1, 2],
            groups: [{ id: 1, name: 'nurse' }]
          }
        ]
      });
    wrapper.vm.basicStore.backendDataLoaded = false;
    wrapper.vm.basicStore.token = 'newtoken3';
    await nextTick();
    await flushPromises();
    expect(get).toHaveBeenCalledTimes(2);
    expect(get).toHaveBeenCalledWith('users/shared-data/');
    expect(get).toHaveBeenCalledWith('clients/');
    expect(wrapper.vm.clientStore.clients).toStrictEqual({
      '1': {
        id: 1,
        admittedBy: 3,
        isActive: true,
        createdDate: '2020-06-26T21:05:01',
        agreedPolicies: [],
        centres: [1, 2],
        visitingDays: [],
        firstName: 'test name',
        surname: 'test surname',
        visitingDaysSetup: true,
        additionalInfoSetup: true,
        servicesSetup: true,
        abilitySetup: true,
        dietSetup: true,
        address: 'Test Address 1',
        phoneNumber: '11111111',
        dateOfBirth: '1911-11-10',
        nextOfKin: [],
        setupComplete: true
      }
    });
    expect(axiosFailed).not.toHaveBeenCalled();
  });

  it('test isActive() function returns store value', () => {
    wrapper.vm.userStore.user.isActive = true;
    expect(wrapper.vm.isActive).toStrictEqual(true);
  });

  it('test activeClient() function returns store value', () => {
    wrapper.vm.clientStore.activeClient = { id: 4 };
    expect(wrapper.vm.activeClient).toStrictEqual({ id: 4 });
  });

  it('test centres() function returns store value', () => {
    wrapper.vm.userStore.user.centres = [2, 4, 5];
    expect(wrapper.vm.centres).toStrictEqual([2, 4, 5]);
  });

  it('test darkTheme() function returns store value', () => {
    wrapper.vm.userStore.user.darkTheme = true;
    expect(wrapper.vm.darkTheme).toStrictEqual(true);
  });

  it('test accentColor() function returns store value', () => {
    wrapper.vm.userStore.user.accentColor = '#CCCCCC';
    expect(wrapper.vm.accentColor).toStrictEqual('#CCCCCC');
  });

  it('changing isActive does not logout if no user id in store', async () => {
    vi.mock('src/functions/auth/logout', () => ({
      default: vi.fn()
    }));

    wrapper.vm.userStore.user.id = null;
    wrapper.vm.userStore.user.isActive = false;
    await nextTick();
    expect(logout).toHaveBeenCalledTimes(0);
  });

  it('changing activeClient does not logout if not client', async () => {
    vi.mock('src/functions/auth/logout', () => ({
      default: vi.fn()
    }));

    wrapper.vm.userStore.user.group = 'nurse';
    wrapper.vm.userStore.user.activeClient = null;
    await nextTick();
    expect(logout).toHaveBeenCalledTimes(0);
  });

  it('does not logout if isActive is true', async () => {
    wrapper.vm.userStore.user.id = 4;
    wrapper.vm.userStore.user.isActive = true;
    await nextTick();
    expect(logout).toHaveBeenCalledTimes(0);
  });

  it('logs out user if isActive is false and id exists', async () => {
    wrapper.vm.userStore.user.id = 4;
    wrapper.vm.userStore.user.isActive = false;
    expect(logout).toHaveBeenCalledTimes(0);
    await nextTick();
    expect(logout).toHaveBeenCalled();
    expect(notifySpy).toHaveBeenCalled();
    expect(notifySpy).toHaveBeenCalledWith({
      message: 'An admin has disabled your account',
      type: 'info'
    });
  });

  it('logs out user if activeClient is null and is client', async () => {
    wrapper.vm.userStore.user.group = 'client';
    wrapper.vm.clientStore.activeClient = null;
    expect(logout).toHaveBeenCalledTimes(0);
    await nextTick();
    expect(logout).toHaveBeenCalled();
    expect(notifySpy).toHaveBeenCalled();
    expect(notifySpy).toHaveBeenCalledWith({
      message: 'An admin has deleted your account',
      type: 'info'
    });
  });

  it('updateUserTheme() sets current theme to passed theme', async () => {
    wrapper.vm.userStore.user.darkTheme = true;
    await nextTick();
    expect(wrapper.vm.$q.dark.isActive).toStrictEqual(true);

    wrapper.vm.userStore.user.darkTheme = false;
    await nextTick();
    expect(wrapper.vm.$q.dark.isActive).toStrictEqual(false);
  });

  it('updateUserAccentColor() sets accent color to passed value', async () => {
    wrapper.vm.userStore.user.accentColor = '#FA3162';
    await nextTick();
    expect(getCssVar('primary')).toStrictEqual('#FA3162');
  });

  it('updateUserAccentColor() sets a default value if null passed', async () => {
    wrapper.vm.userStore.user.accentColor = null;
    await nextTick();
    expect(getCssVar('primary')).toStrictEqual('#1976D2');
  });

  it('centre watcher does nothing if centres object null', async () => {
    const changeSubscription = vi.spyOn(
      wrapper.vm.context,
      'changeSubscription'
    );
    const goToCentreSelect = vi.spyOn(wrapper.vm.context, 'goToCentreSelect');
    wrapper.vm.userStore.user.centres = null;
    await nextTick();
    expect(changeSubscription).toHaveBeenCalledTimes(0);
    expect(goToCentreSelect).toHaveBeenCalledTimes(0);
  });

  it('centre watcher does nothing if centres array not empty', async () => {
    const changeSubscription = vi.spyOn(
      wrapper.vm.context,
      'changeSubscription'
    );
    const goToCentreSelect = vi.spyOn(wrapper.vm.context, 'goToCentreSelect');
    wrapper.vm.userStore.user.centres = [1];
    await nextTick();
    expect(changeSubscription).toHaveBeenCalledTimes(0);
    expect(goToCentreSelect).toHaveBeenCalledTimes(0);
  });

  it('centre watcher calls changeSubscription() when state centres = 1', async () => {
    wrapper.vm.centreStore.centres = {
      1: { id: 1, name: 'test centre 1' }
    };

    expect(wrapper.vm.centres).toStrictEqual([1]);
    const changeSubscription = vi.spyOn(
      wrapper.vm.context,
      'changeSubscription'
    );
    const goToCentreSelect = vi.spyOn(wrapper.vm.context, 'goToCentreSelect');
    wrapper.vm.userStore.user.centres = [];
    await nextTick();
    expect(changeSubscription).toHaveBeenCalledTimes(1);
    expect(goToCentreSelect).toHaveBeenCalledTimes(0);
  });

  it('centre watcher calls goToCentreSelect() when state centres > 1', async () => {
    const changeSubscription = vi.spyOn(
      wrapper.vm.context,
      'changeSubscription'
    );
    const goToCentreSelect = vi.spyOn(wrapper.vm.context, 'goToCentreSelect');
    wrapper.vm.userStore.user.centres = [];
    await nextTick();
    expect(changeSubscription).toHaveBeenCalledTimes(0);
    expect(goToCentreSelect).toHaveBeenCalledTimes(1);
  });

  it('goToCentreSelect() sets snackbar', () => {
    wrapper.vm.goToCentreSelect();
    expect(notifySpy).toHaveBeenCalledWith({
      message:
        'Your only subscribed centre was removed, please select new centres to subscribe to',
      type: 'info'
    });
  });

  it('goToCentreSelect() calls router', async () => {
    await wrapper.vm.goToCentreSelect();
    expect(wrapper.router.push).toHaveBeenCalledWith({
      name: 'centreSelect',
      query: { nextUrl: '/' }
    });
  });

  it('changeSubscription() sets snackbar', () => {
    wrapper.vm.changeSubscription();
    expect(notifySpy).toHaveBeenCalledWith({
      message:
        'Your only subscribed centre was removed, you were auto subscribed to Test Centre 1',
      type: 'info'
    });
  });

  it('changeSubscription() sends patch request to server', async () => {
    wrapper.vm.userStore.user.group = 'admin';
    wrapper.vm.userStore.user.id = 6;
    wrapper.vm.basicStore.token = 'AAAAAAAA';
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockResolvedValueOnce({});
    await wrapper.vm.changeSubscription();
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPost).toHaveBeenCalledWith('users/centres/update/', {
      centre: 1,
      user: 6
    });
  });

  it('changeSubscription() calls axiosFailed() on failure ', async () => {
    wrapper.vm.userStore.user.group = 'admin';
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockRejectedValueOnce({});
    await wrapper.vm.changeSubscription();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('user theme is updated on create', () => {
    wrapper.vm.context.updateUserTheme = vi.fn();
    wrapper.vm.context.updateUserAccentColor = vi.fn();
    wrapper.vm.wsStore.WS_RECONNECT = vi.fn();
    wrapper.vm.basicStore.backendPath = 'http://www.example.com';
    wrapper.vm.created();
    expect(wrapper.vm.context.updateUserTheme).toHaveBeenCalledOnce();
    expect(wrapper.vm.context.updateUserAccentColor).toHaveBeenCalledOnce();
    expect(wrapper.vm.wsStore.WS_RECONNECT).toHaveBeenCalledWith(
      '',
      'AAAAAAAA'
    );
  });

  it('getAppReleases notifies if latest release is newer', async () => {
    httpGet.mockResolvedValueOnce({ data: [{ tag_name: '4.3.3' }] });
    await wrapper.vm.getAppReleases();
    expect(notifySpy).toHaveBeenCalledWith({
      message: 'A new version of this app is available!',
      type: 'info',
      actions: [
        {
          color: 'white',
          handler: expect.any(Function),
          label: 'Download'
        }
      ]
    });
  });

  it('getAppReleases does not notify if latest release is equal', async () => {
    httpGet.mockResolvedValueOnce({ data: [{ tag_name: '4.3.2' }] });
    await wrapper.vm.getAppReleases();
    expect(notifySpy).toHaveBeenCalledTimes(0);
  });

  it('getAppReleases does not notify if latest release is older', async () => {
    httpGet.mockResolvedValueOnce({ data: [{ tag_name: '3.2.1' }] });
    await wrapper.vm.getAppReleases();
    expect(notifySpy).toHaveBeenCalledTimes(0);
  });

  it('getAppReleases sets button function to download', async () => {
    httpGet.mockResolvedValueOnce({
      data: [{ tag_name: '4.3.3', assets: { links: [{ url: 'test.com' }] } }]
    });
    const checkAppType = vi.spyOn(wrapper.vm.context, 'checkAppType');
    await wrapper.vm.getAppReleases();
    await notifySpy.mock.calls[0][0].actions[0].handler();
    expect(checkAppType).toBeCalledWith({
      data: [{ tag_name: '4.3.3', assets: { links: [{ url: 'test.com' }] } }]
    });
  });

  it('getAppReleases() shows error on failure ', async () => {
    httpGet.mockRejectedValueOnce({});
    await wrapper.vm.getAppReleases();
    expect(notifySpy).toHaveBeenCalledWith({
      message: 'Failed to check for app updates',
      type: 'negative'
    });
  });

  it('downloadNewAppVersion displays topbar message', async () => {
    httpGet.mockResolvedValueOnce({ data: [] });
    wrapper.vm.appReleaseLinks = releaseLinks;
    await wrapper.vm.downloadNewAppVersion(releaseLinks);
    expect(wrapper.vm.basicStore.topBar).toStrictEqual({
      text: 'Downloading new app version',
      color: 'info',
      active: false,
      indeterminate: false,
      value: 0
    });
  });

  it('downloadNewAppVersion gets os url', async () => {
    wrapper.vm.basicStore.platform.win = true;
    httpGet.mockResolvedValueOnce({ data: { test: 'test' } });
    await wrapper.vm.checkAppType(releaseLinks);
    expect(httpGet).toHaveBeenCalledTimes(1);
    expect(httpGet).toHaveBeenCalledWith('windowsurl.exe', {
      headers: { Accept: '*/*' },
      onDownloadProgress: expect.any(Function),
      responseType: 'blob'
    });
    expect(fileDownload).toHaveBeenCalledTimes(1);
    expect(fileDownload).toBeCalledWith({ test: 'test' }, 'yapca-3.2.1.exe');
    wrapper.vm.basicStore.platform.win = false;
    httpGet.mockResolvedValueOnce({ data: { linux: 'linux' } });
    await wrapper.vm.checkAppType(releaseLinks);
    expect(httpGet).toHaveBeenCalledTimes(1);
    expect(fileDownload).toHaveBeenCalledTimes(1);
  });

  it('checkAppType does not download without url', async () => {
    wrapper.vm.basicStore.platform.win = true;
    releaseLinks.data[0].assets.links = [];
    await wrapper.vm.checkAppType(releaseLinks);
    expect(httpGet).toHaveBeenCalledTimes(0);
    expect(fileDownload).toHaveBeenCalledTimes(0);
  });

  it('setSelectedDistro downloads linux url', async () => {
    wrapper.vm.basicStore.platform.win = false;

    httpGet.mockResolvedValueOnce({ data: { test: 'test' } });
    await wrapper.vm.checkAppType(releaseLinks);
    await wrapper.vm.setSelectedDistro('rpm');
    expect(httpGet).toHaveBeenCalledTimes(1);
    expect(httpGet).toHaveBeenCalledWith('linuxurl.rpm', {
      headers: { Accept: '*/*' },
      onDownloadProgress: expect.any(Function),
      responseType: 'blob'
    });
    expect(fileDownload).toHaveBeenCalledTimes(1);
    expect(fileDownload).toBeCalledWith({ test: 'test' }, 'yapca-3.2.1.rpm');
  });

  it('setAppDownloadUrl does not set url if no links', () => {
    wrapper.vm.appReleaseLinks = [];
    wrapper.vm.appDownloadUrl = 'exampleurl';
    wrapper.vm.setAppDownloadUrl();
    expect(wrapper.vm.appDownloadUrl).toStrictEqual('exampleurl');
  });

  it('setSelectedDistro does nothing if no appDownloadUrl', async () => {
    wrapper.vm.downloadNewAppVersion = vi.fn();
    await wrapper.vm.setSelectedDistro('rpm');
    expect(wrapper.vm.downloadNewAppVersion).toHaveBeenCalledTimes(0);
  });

  it('downloadNewAppVersion creates snackbar message on complete', async () => {
    wrapper.vm.basicStore.platform.win = true;
    httpGet.mockResolvedValueOnce({ data: { test: 'test' } });
    await wrapper.vm.checkAppType(releaseLinks);
    expect(notifySpy).toHaveBeenCalledWith({
      message: 'Update downloaded, please run the downloaded file',
      type: 'positive'
    });
  });

  it('downloadNewAppVersion sends progress to function', async () => {
    wrapper.vm.basicStore.platform.win = true;
    const setAppDownloadProgress = vi.spyOn(
      wrapper.vm.context,
      'setAppDownloadProgress'
    );
    httpGet.mockResolvedValueOnce({ data: [] });
    await wrapper.vm.checkAppType(releaseLinks);
    const config = httpGet.mock.calls[0][1] as {
      // eslint-disable-next-line @typescript-eslint/ban-types
      onDownloadProgress: Function;
    };
    config.onDownloadProgress({ loaded: 214, total: 634 });
    expect(setAppDownloadProgress).toHaveBeenCalledWith({
      loaded: 214,
      total: 634
    });
  });

  it('downloadNewAppVersion() calls axiosFailed() on failure ', async () => {
    wrapper.vm.basicStore.platform.win = true;
    httpGet.mockRejectedValueOnce({});
    await wrapper.vm.checkAppType(releaseLinks);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('setAppDownloadProgress() gets progress', () => {
    wrapper.vm.setAppDownloadProgress({ loaded: 3802, total: 28179 });
    expect(wrapper.vm.basicStore.topBar.value).toStrictEqual(0.13);
  });

  it('ws is not setup when page is serverSetup', () => {
    wrapper.vm.wsStore.WS_RECONNECT = vi.fn();
    getRouter().currentRoute.value.name = 'serverSelect';
    wrapper.vm.created();
    expect(wrapper.vm.wsStore.WS_RECONNECT).toHaveBeenCalledTimes(0);
  });

  it('checks releases if electron version', async () => {
    wrapper.vm.linuxDistroSelectDialog = true;
    wrapper.vm.context.getAppReleases = vi.fn();
    httpGet.mockRejectedValue({});
    wrapper.vm.basicStore.platform.electron = true;
    await nextTick();
    expect(wrapper.vm.context.getAppReleases).toHaveBeenCalledTimes(1);
  });

  it('sets active client in store if logging in as client user', async () => {
    wrapper.vm.basicStore.backendDataLoaded = false;
    mockClients();
    get.mockResolvedValue(true);
    wrapper.vm.userStore.user.group = 'client';
    wrapper.vm.basicStore.token = 'newtoken';
    expect(wrapper.vm.clientStore.activeClient).toStrictEqual(null);
    await nextTick();
    await flushPromises();
    expect(wrapper.vm.clientStore.activeClient?.firstName).toStrictEqual(
      'test name 1'
    );
    expect(wrapper.vm.clientStore.activeClient?.id).toStrictEqual(1);
  });
});
