/* eslint-disable @typescript-eslint/no-explicit-any */
import { createPinia, setActivePinia } from 'pinia';
import { useUserStore } from 'src/stores/userStore';
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import { useSocketStore } from 'src/stores/websocketStore';
import { useBasicStore } from 'src/stores/basicStore';
import { Server } from 'mock-socket';
import { flushPromises } from '@vue/test-utils';
import { useCentreStore } from 'src/stores/centresStore';
import { useClientStore } from 'src/stores/clientsStore';
import { nextTick } from 'vue';
import setup from '../../functions/setup';
import { mockClients } from '../../mocks';

setup();

describe('Websocket Store', () => {
  let userStore: any;
  let websocketStore: any;
  let basicStore: any;
  let centreStore: any;
  let clientStore: any;
  let mockServer: Server;

  beforeEach(() => {
    process.env.VUE_APP_API_HOST = 'test';
    process.env.VUE_APP_API_PORT = undefined;
    process.env.VUE_APP_API_PATH = undefined;
    vi.useFakeTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    setActivePinia(createPinia());
    userStore = useUserStore();
    centreStore = useCentreStore();
    clientStore = useClientStore();
    basicStore = useBasicStore();
    websocketStore = useSocketStore();
    mockServer = new Server('ws://test/ws/main/?token=');
    websocketStore.WS_RECONNECT('', 'test', '');
  });

  afterEach(() => {
    mockServer.close();
  });

  it('can updateClient', () => {
    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'updateClient',
          data: { id: 5, firstName: 'new first name' }
        })
      );
    });
    vi.runOnlyPendingTimers();

    expect(clientStore.clients[5]).toStrictEqual({
      id: 5,
      firstName: 'new first name'
    });
  });

  it('can updateClientVisitingDays', async () => {
    mockClients();
    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'updateClientVisitingDays',
          data: {
            clientId: 3,
            visitingDays: [
              { centre: 2, day: 4, client: 3 },
              { centre: 2, day: 5, client: 3 }
            ]
          }
        })
      );
    });
    vi.runOnlyPendingTimers();
    await flushPromises();
    expect(clientStore.clients[3].visitingDays).toStrictEqual([
      { day: 4, centre: 4, client: 3 },
      { day: 3, centre: 3, client: 3 },
      { centre: 2, day: 4, client: 3 },
      { centre: 2, day: 5, client: 3 }
    ]);
  });

  it('can set registerAbility', () => {
    basicStore.registerAbility = false;

    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'updateRegisterAbility',
          data: { canRegister: true }
        })
      );
    });
    vi.runOnlyPendingTimers();
    expect(basicStore.registerAbility).toStrictEqual(true);
  });

  it('can update policies', () => {
    centreStore.policies = {
      4: { id: 4 },
      23: { id: 23 }
    };
    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'updatePolicy',
          data: { id: 23, description: 'new name' }
        })
      );
    });
    vi.runOnlyPendingTimers();
    expect(centreStore.policies).toStrictEqual({
      4: { id: 4 },
      23: { id: 23, description: 'new name' }
    });
  });

  it('can update clientNeeds', async () => {
    clientStore.clientNeeds = {
      4: { id: 4, plan: 'test' }
    };
    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'updateClientNeed',
          data: { id: 4, centre: 2, need: 1, plan: 'new plan', isActive: true }
        })
      );
    });
    vi.runOnlyPendingTimers();
    await flushPromises();
    expect(clientStore.clientNeeds).toStrictEqual({
      4: { id: 4, centre: 2, need: 1, plan: 'new plan', isActive: true }
    });
  });

  it('updating client centres as client gets centres from api ', async () => {
    const getCentres = vi.spyOn(centreStore, 'getCentres');
    userStore.user.group = 'client';
    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'updateClientCentres',
          data: { clientId: 1, centreIds: [1, 2] }
        })
      );
    });
    vi.runOnlyPendingTimers();
    await flushPromises();
    expect(getCentres).toHaveBeenCalledTimes(1);
  });

  it('can update clientCentres', async () => {
    userStore.user.group = 'nurse';
    await nextTick();
    const getCentres = vi.spyOn(centreStore, 'getCentres');
    expect(getCentres).toHaveBeenCalledTimes(0);
    clientStore.clients[4] = {
      id: 4,
      centres: []
    };
    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'updateClientCentres',
          data: { clientId: 4, centreIds: [3, 4, 6, 1] }
        })
      );
    });
    vi.runOnlyPendingTimers();
    expect(clientStore.clients[4].centres).toStrictEqual([3, 4, 6, 1]);
    expect(getCentres).toHaveBeenCalledTimes(0);
  });

  it('can update client permissions', async () => {
    userStore.user.group = 'client';
    clientStore.clients[85] = {
      85: { id: 85 }
    };
    clientStore.activeClient = { id: 85 };

    await nextTick();
    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'updateClientPermissions',
          data: { clientId: 85, permissions: ['client_can_view_calendar'] }
        })
      );
    });
    vi.runOnlyPendingTimers();
    expect(clientStore.clients[85].permissions).toStrictEqual([
      'client_can_view_calendar'
    ]);
    expect(clientStore.activeClient.permissions).toStrictEqual([
      'client_can_view_calendar'
    ]);
  });

  it('can update needs', () => {
    centreStore.needs = {
      3: { id: 3 },
      15: { id: 15 }
    };
    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'updateNeed',
          data: { id: 3, description: 'new need' }
        })
      );
    });
    vi.runOnlyPendingTimers();
    expect(centreStore.needs).toStrictEqual({
      3: { id: 3, description: 'new need' },
      15: { id: 15 }
    });
  });

  it('can updateUser', () => {
    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'updateUser',
          data: { id: 3, displayName: 'newName' }
        })
      );
    });
    vi.runOnlyPendingTimers();
    expect(userStore.users[3]).toStrictEqual({ id: 3, displayName: 'newName' });
  });

  it('can update existing client', async () => {
    clientStore.activeClient = { id: 7 };
    clientStore.clients[7] = { id: 7 };
    await nextTick();
    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'updateClient',
          data: { id: 7, surname: 'new surname' }
        })
      );
    });
    vi.runOnlyPendingTimers();

    expect(clientStore.clients[7]).toStrictEqual({
      id: 7,
      surname: 'new surname'
    });
    expect(clientStore.activeClient).toStrictEqual({
      id: 7,
      surname: 'new surname'
    });
  });

  it('can update single care plan', () => {
    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'updateCarePlan',
          data: { id: 5, date: '2022-01-11' }
        })
      );
    });
    vi.runOnlyPendingTimers();
    expect(clientStore.carePlans).toStrictEqual({
      5: { id: 5, date: '2022-01-11' }
    });
  });

  it('can update multiple care plans', () => {
    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'updateCarePlans',
          data: [
            { id: 6, date: '2022-01-12' },
            { id: 7, date: '2022-01-13' }
          ]
        })
      );
    });
    vi.runOnlyPendingTimers();
    expect(clientStore.carePlans).toStrictEqual({
      6: { id: 6, date: '2022-01-12' },
      7: { id: 7, date: '2022-01-13' }
    });
  });

  it('can create attendance records', () => {
    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'createAttendanceRecords',
          data: [
            { id: 8, date: '2022-01-14' },
            { id: 9, date: '2022-01-15' }
          ]
        })
      );
    });
    vi.runOnlyPendingTimers();
    expect(clientStore.absenceRecords).toStrictEqual({
      8: { id: 8, date: '2022-01-14' },
      9: { id: 9, date: '2022-01-15' }
    });
  });

  it('can updateFeedingRisks', () => {
    mockClients();
    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'updateFeedingRisks',
          data: {
            clientId: 5,
            feedingRisks: [
              {
                id: 3,
                feedingRisk: 'Swallowing'
              },
              {
                id: 4,
                feedingRisk: 'Poor Appetite'
              }
            ]
          }
        })
      );
    });
    vi.runOnlyPendingTimers();
    expect(clientStore.clients[5].feedingRisks).toStrictEqual([
      {
        id: 3,
        feedingRisk: 'Swallowing'
      },
      {
        id: 4,
        feedingRisk: 'Poor Appetite'
      }
    ]);
  });

  it('updateFeedingRisks does nothing when no client', () => {
    mockClients();
    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'updateFeedingRisks',
          data: {
            clientId: 70,
            feedingRisks: [
              {
                id: 9,
                feedingRisk: 'Swallowing'
              }
            ]
          }
        })
      );
    });
    vi.runOnlyPendingTimers();
    expect(clientStore.clients[70]).toStrictEqual(undefined);
  });

  it('can updateUserCentres', async () => {
    userStore.users[3] = { id: 3 };
    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'updateUserCentres',
          data: {
            userId: 3,
            centreIds: [2, 4]
          }
        })
      );
    });
    vi.runOnlyPendingTimers();
    await flushPromises();
    expect(userStore.users[3]).toStrictEqual({ id: 3, centres: [2, 4] });
  });

  it('can delete centres', () => {
    userStore.users[3] = { id: 3 };
    centreStore.centres = {
      2: { id: 2 }
    };
    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'deleteCentre',
          data: { id: 2 }
        })
      );
    });
    vi.runOnlyPendingTimers();
    expect(centreStore.centres).toStrictEqual({});
  });

  it('can delete care plans', () => {
    clientStore.carePlans = {
      7: { id: 7 },
      8: { id: 8 },
      9: { id: 9 }
    };
    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'deleteCarePlans',
          data: [{ id: 7 }, { id: 9 }]
        })
      );
    });
    vi.runOnlyPendingTimers();
    expect(clientStore.carePlans).toStrictEqual({ 8: { id: 8 } });
  });

  it('can update events', () => {
    centreStore.eventsNeedUpdating = false;
    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'updateEvent'
        })
      );
    });
    vi.runOnlyPendingTimers();
    expect(centreStore.eventsNeedUpdating).toStrictEqual(true);
  });

  it('can delete events', () => {
    centreStore.eventsNeedUpdating = false;
    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'deleteEvent'
        })
      );
    });
    vi.runOnlyPendingTimers();
    expect(centreStore.eventsNeedUpdating).toStrictEqual(true);
  });

  it('can delete clients', () => {
    clientStore.clients = {
      9: { id: 9 },
      10: { id: 10 }
    };
    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'deleteClient',
          data: { id: 9 }
        })
      );
    });
    vi.runOnlyPendingTimers();
    expect(clientStore.clients).toStrictEqual({
      10: { id: 10 }
    });
  });

  it('deleting active client removes activeClient from store', () => {
    clientStore.activeClient = { id: 12 };
    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'deleteClient',
          data: { id: 12 }
        })
      );
    });
    vi.runOnlyPendingTimers();
    expect(clientStore.activeClient).toStrictEqual(null);
  });

  it('can delete client documents', () => {
    clientStore.clients = {
      2: { id: 2, documents: [{ id: 60 }, { id: 61 }], storageSize: 600 },
      3: { id: 3, documents: [{ id: 60 }, { id: 61 }], storageSize: 500 }
    };
    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'deleteClientDocument',
          data: { document: { id: 60 }, clientId: 2, newClientSize: 200 }
        })
      );
    });
    vi.runOnlyPendingTimers();
    expect(clientStore.clients).toStrictEqual({
      2: { id: 2, documents: [{ id: 61 }], storageSize: 200 },
      3: { id: 3, documents: [{ id: 60 }, { id: 61 }], storageSize: 500 }
    });
  });

  it('can update client documents', () => {
    clientStore.clients = {
      5: { id: 5, documents: [{ id: 71 }, { id: 42 }], storageSize: 900 }
    };
    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'updateClientDocument',
          data: {
            document: { id: 71, client: 5, data: 'newdocumentdata' },
            newClientSize: 1100
          }
        })
      );
    });
    vi.runOnlyPendingTimers();
    expect(clientStore.clients).toStrictEqual({
      5: {
        id: 5,
        documents: [{ id: 42 }, { id: 71, client: 5, data: 'newdocumentdata' }],
        storageSize: 1100
      }
    });
  });

  it('can update centres', async () => {
    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'updateCentre',
          data: {
            id: 2,
            name: 'test centre'
          }
        })
      );
    });
    vi.runOnlyPendingTimers();
    await flushPromises();
    expect(centreStore.centres).toStrictEqual({
      2: {
        id: 2,
        name: 'test centre'
      }
    });
  });
  it('handles unknown action', () => {
    global.console = { ...global.console, error: vi.fn() };
    const consoleError = vi.spyOn(global.console, 'error');
    mockServer.on('connection', server => {
      server.send(
        JSON.stringify({
          action: 'unknown',
          data: {}
        })
      );
    });
    vi.runOnlyPendingTimers();
    expect(consoleError).toHaveBeenCalledWith('Received unknown ws action', {
      action: 'unknown',
      data: {}
    });
  });

  it('handles missing ws protocol', () => {
    process.env.VUE_APP_WS_PROTOCOL = undefined;
    process.env.VUE_APP_API_HOST = 'test';
    process.env.VUE_APP_API_PORT = undefined;
    process.env.VUE_APP_API_PATH = undefined;
    websocketStore.WS_RECONNECT('', '');
    expect(websocketStore.ws.url).toStrictEqual('wss://test/ws/main/?token=');
  });
});
