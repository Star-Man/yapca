import { shallowMount } from '@vue/test-utils';
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import UserChangeCentreWarningDialog from 'src/components/admin/UserChangeCentreWarningDialog.vue';
import setup from 'tests/unit/functions/setup';
import type { SpyInstance } from 'vitest';

setup();
describe('UserChangeCentreWarningDialog.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let emit: SpyInstance;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    wrapper = shallowMount(UserChangeCentreWarningDialog);
    emit = vi.spyOn(wrapper.vm.context, 'emit');
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = undefined;
  });

  it('emits continueCentreUpdate on continueCentreUpdate', () => {
    wrapper.vm.continueCentreUpdate();
    expect(emit).toHaveBeenCalledWith('continue-centre-update');
  });
});
