import { flushPromises, mount } from '@vue/test-utils';
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import EditUserDialog from 'src/components/admin/EditUserDialog.vue';
import setup from 'tests/unit/functions/setup';
import { Notify } from 'quasar';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { mockClients } from 'tests/unit/mocks';
import { nextTick } from 'vue';
import type { SpyInstance } from 'vitest';

setup();

process.env.VUE_APP_MAX_STORAGE = '830';
process.env.VUE_APP_MAX_FILE_SIZE = '10';

describe('EditUserDialog.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let notifySpy: SpyInstance;
  let httpPatch: SpyInstance;
  let httpPost: SpyInstance;
  let httpDelete: SpyInstance;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    mockClients();
    wrapper = mount(EditUserDialog, {
      props: {
        selectedUser: {
          id: 3
        },
        groups: [
          { name: 'admin', id: 1 },
          { name: 'testGroup', id: 2 }
        ]
      }
    });
    notifySpy = vi.spyOn(Notify, 'create');
    httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpDelete = vi.spyOn(wrapper.vm.basicStore.http, 'delete');
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = undefined;
  });

  it('test updateUserGroup()', async () => {
    httpPatch.mockResolvedValueOnce({ data: {} });
    await wrapper.setProps({
      selectedUser: {
        id: 6,
        groups: [{ id: 2 }]
      }
    });
    wrapper.vm.selectedGroup = { value: 8, label: 'admin' };
    await nextTick();
    expect(httpPatch).toHaveBeenCalledWith('users/6/update/', { groups: [8] });
    expect(axiosFailed).not.toHaveBeenCalled();
  });

  it('updateUserGroup handles missing id', async () => {
    await wrapper.setProps({
      selectedUser: {
        groups: [{ id: 12 }]
      }
    });
    wrapper.vm.selectedGroup = { value: 3, label: 'admin' };
    httpPatch.mockResolvedValueOnce({ data: {} });
    await nextTick();
    expect(httpPatch).not.toBeCalled();
  });

  it('test updateUserGroup() when server error', async () => {
    httpPatch.mockRejectedValueOnce({});
    await wrapper.setProps({
      selectedUser: {
        id: 6,
        groups: [{ id: 2 }]
      }
    });
    wrapper.vm.selectedGroup = { value: 3, label: 'admin' };
    await nextTick();
    await flushPromises();
    expect(httpPatch).toHaveBeenCalledWith('users/6/update/', { groups: [3] });
    expect(axiosFailed).toHaveBeenCalled();
  });

  it('closeEmailDialog() closes dialog', async () => {
    wrapper.vm.showChangeEmailDialog = false;
    const openChangeEmailDialogButton = wrapper.find(
      '#openChangeEmailDialogButton'
    );
    await openChangeEmailDialogButton.trigger('click');
    expect(wrapper.vm.showChangeEmailDialog).toStrictEqual(true);
    wrapper.vm.closeEmailDialog();
    expect(wrapper.vm.showChangeEmailDialog).toStrictEqual(false);
  });

  it('closeOtpResetModal() closes dialog', () => {
    wrapper.vm.showResetOtpModal = true;
    wrapper.vm.closeOtpResetModal();
    expect(wrapper.vm.showResetOtpModal).toStrictEqual(false);
  });

  it('test deleteUser()', async () => {
    const setUserActive = vi.spyOn(wrapper.vm.context, 'setUserActive');
    httpPatch.mockResolvedValue({ data: {} });
    await wrapper.setProps({
      selectedUser: {
        displayName: 'TEST'
      }
    });
    await wrapper.vm.deleteUser();
    expect(notifySpy).toHaveBeenCalledWith({
      message: 'TEST has been deleted',
      type: 'info'
    });
    expect(setUserActive).toBeCalledWith({ displayName: 'TEST' }, false);
  });

  it('deleteUser handles missing display name', async () => {
    httpPatch.mockResolvedValue({ data: {} });
    await wrapper.setProps({
      selectedUser: {
        displayName: null
      }
    });
    await wrapper.vm.deleteUser();
    expect(notifySpy).toHaveBeenCalledWith({
      message: 'User has been deleted',
      type: 'info'
    });
  });

  it('test setUserActive()', async () => {
    expect(axiosFailed).not.toHaveBeenCalled();
    httpPatch.mockRejectedValueOnce({});
    const user = {
      id: 1,
      displayName: 'TEST'
    };
    await wrapper.vm.setUserActive(user, true);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
    httpPatch.mockResolvedValueOnce({ data: {} });
    await wrapper.vm.setUserActive(user, true);
    expect(httpPatch).toHaveBeenCalledWith('users/1/update/', {
      isActive: true
    });
  });

  it('setting prop group sets selectedGroup', async () => {
    await wrapper.setProps({
      selectedUser: {
        group: 'admin'
      }
    });
    expect(wrapper.vm.selectedGroup).toStrictEqual({
      value: 1,
      label: 'admin'
    });
  });

  it('can remove user centres', async () => {
    httpDelete.mockResolvedValueOnce({});
    await wrapper.setProps({
      selectedUser: {
        id: 9
      }
    });
    await wrapper.vm.updateSelectedCentres(3, false);
    expect(httpPost).not.toHaveBeenCalled();
    expect(httpDelete).toHaveBeenCalledWith('users/centres/update/', {
      data: {
        centre: 3,
        user: 9
      }
    });
  });

  it('can add user centres', async () => {
    httpDelete.mockResolvedValueOnce({});
    await wrapper.setProps({
      selectedUser: {
        id: 2
      }
    });
    await wrapper.vm.updateSelectedCentres(8, true);
    expect(httpDelete).not.toHaveBeenCalled();
    expect(httpPost).toHaveBeenCalledWith('users/centres/update/', {
      centre: 8,
      user: 2
    });
  });

  it('clicking resetOtpButton opens dialog', async () => {
    wrapper.vm.showResetOtpModal = false;
    const resetOtpButton = wrapper.find('#resetOtpButton');
    await resetOtpButton.trigger('click');
    expect(wrapper.vm.showResetOtpModal).toStrictEqual(true);
  });

  it('clicking deleteUserButton opens dialog', async () => {
    wrapper.vm.showDeleteUserDialog = false;
    const deleteUserButton = wrapper.find('#deleteUserButton');
    await deleteUserButton.trigger('click');
    expect(wrapper.vm.showDeleteUserDialog).toStrictEqual(true);
  });

  it('can set selected group', () => {
    wrapper.vm.setSelectedGroup({ value: 40, label: 'newGroup' });
    expect(wrapper.vm.selectedGroup).toStrictEqual({
      value: 40,
      label: 'newGroup'
    });
  });

  it('closeClientDialog() does that', () => {
    wrapper.vm.showClientDialog = true;
    wrapper.vm.closeClientDialog();
    expect(wrapper.vm.showClientDialog).toStrictEqual(false);
  });

  it('setDeleteUserDialogFalse does that', () => {
    wrapper.vm.showDeleteUserDialog = true;
    wrapper.vm.setDeleteUserDialogFalse();
    expect(wrapper.vm.showDeleteUserDialog).toStrictEqual(false);
  });

  it('client header color changes based on client active', async () => {
    expect(wrapper.vm.selectedClientHeaderColor).toStrictEqual('grey');
    await wrapper.setProps({
      selectedUser: {
        group: 'client',
        client: 4
      }
    });
    expect(wrapper.vm.selectedClientHeaderColor).toStrictEqual('primary');
    wrapper.vm.clientStore.clients[4].isActive = false;
    expect(wrapper.vm.selectedClientHeaderColor).toStrictEqual('grey');
  });

  it('clicking goToClientPageButton button sets clientDialog', async () => {
    await wrapper.setProps({
      selectedUser: {
        id: 2,
        group: 'client',
        client: 4
      }
    });
    await nextTick();
    expect(wrapper.vm.showClientDialog).toStrictEqual(false);
    await wrapper.find('#goToClientPageButton').trigger('click');
    expect(wrapper.vm.showClientDialog).toStrictEqual(true);
  });
});
