import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import AddDocumentModal from 'src/components/clients/AddDocumentModal.vue';
import setup from 'tests/unit/functions/setup';
import { nextTick } from 'vue';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { mockClients } from '../../../mocks';

setup();

const testFile = new File(['test data 1'], 'testFile.txt', {
  type: 'text/html'
});
describe('AddDocumentModal.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockClients();
    vi.mock('src/functions/client/getClient', () => ({
      default: vi.fn()
    }));

    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));

    wrapper = shallowMount(AddDocumentModal, {
      props: {
        clientId: 4,
        documents: [
          {
            file: testFile,
            newName: 'testFile',
            uploaded: false,
            uploadStatus: 0,
            removed: false
          },
          {
            file: new File(['test data 2'], 'testFile2.txt', {
              type: 'text/html'
            }),
            newName: 'testFile2',
            uploaded: true,
            uploadStatus: 0,
            removed: false
          },
          {
            file: new File(['test data 3'], 'testFile3.txt', {
              type: 'text/html'
            }),
            newName: 'testFile3',
            uploaded: true,
            uploadStatus: 0,
            removed: true
          }
        ]
      }
    });
  });

  it('incompleteDocuments() returns non-uploaded documents', () => {
    expect(wrapper.vm.incompleteDocuments).toStrictEqual([
      {
        name: 'testFile',
        tempName: 'testFile',
        originalFileName: 'testFile.txt',
        size: 11,
        document: {
          file: expect.any(File),
          newName: 'testFile',
          uploaded: false,
          uploadStatus: 0,
          removed: false
        }
      }
    ]);
  });

  it('totalUsage returns total document usage for clients', () => {
    expect(wrapper.vm.totalUsage).toStrictEqual(100);
    expect(wrapper.vm.maxStorage).toStrictEqual(127);
  });

  it('documentUsage returns total document size usage for selected documents', () => {
    expect(wrapper.vm.documentUsage).toStrictEqual(11);
  });

  it('documentsOverMaxLimit returns if the uploaded documents are over max', async () => {
    expect(wrapper.vm.documentsOverMaxLimit).toStrictEqual(false);
    await wrapper.setProps({
      documents: [
        {
          file: new File(['oooooooooooooooooooooooooooo'], 'testFile.txt', {
            type: 'text/html'
          }),
          newName: 'testFile',
          uploaded: false,
          uploadStatus: 0,
          removed: false
        }
      ]
    });
    expect(wrapper.vm.documentUsage).toStrictEqual(28);
    expect(wrapper.vm.documentsOverMaxLimit).toStrictEqual(true);
  });

  it('checkCloseModal() closes modal when all documents have finished', async () => {
    wrapper.vm.uploading = true;
    const emit = vi.spyOn(wrapper.vm.context, 'emit');
    await wrapper.setProps({
      clientId: 4,
      documents: [
        {
          file: { name: 'testFile.txt' },
          newName: 'testFile',
          uploaded: true,
          uploadStatus: 0,
          removed: false
        }
      ]
    });
    expect(wrapper.vm.uploading).toStrictEqual(false);
    expect(emit).toHaveBeenCalledTimes(1);
    expect(emit).toHaveBeenCalledWith('close-dialog');
  });

  it('checkCloseModal() does nothing when documents unfinished', async () => {
    const emit = vi.spyOn(wrapper.vm.context, 'emit');
    wrapper.vm.uploading = true;
    await wrapper.setProps({
      clientId: 4,
      documents: [
        {
          file: { name: 'testFile.txt' },
          newName: 'testFile',
          uploaded: false,
          uploadStatus: 0,
          removed: false
        }
      ]
    });
    expect(emit).toHaveBeenCalledTimes(0);
  });

  it('uploadDocuments() sends post to server', async () => {
    wrapper.vm.basicStore.token = 'AAAAAAAA';
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockResolvedValueOnce({});
    await wrapper.vm.uploadDocuments();
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPost).toHaveBeenCalledTimes(1);
    const file1 = new FormData();
    file1.append('client', '4');
    file1.append('document', testFile);
    file1.append('name', 'testFile');
    file1.append('originalName', 'testFile.txt');
    expect(httpPost).toHaveBeenCalledWith('clientDocuments/upload/', file1, {
      headers: {
        Authorization: 'Token AAAAAAAA',
        'Content-Type': 'multipart/form-data'
      },
      onUploadProgress: expect.anything()
    });
  });

  it('uploadDocuments() calls axiosFailed() on failure', async () => {
    wrapper.vm.basicStore.token = 'AAAAAAAA';
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockRejectedValueOnce({});
    await wrapper.vm.uploadDocuments();
    await nextTick();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('uploadDocuments() calls nothing when no clientId', async () => {
    await wrapper.setProps({
      documents: [
        {
          file: { name: 'testFile.txt' },
          newName: 'testFile',
          uploaded: true,
          uploadStatus: 0,
          removed: false
        }
      ],
      clientId: null
    });
    await wrapper.vm.$nextTick();
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    await wrapper.vm.uploadDocuments();
    await nextTick();
    expect(httpPost).toHaveBeenCalledTimes(0);
  });

  it('uploadDocuments() sets document to remove if it has an error', async () => {
    await wrapper.setProps({
      documents: [
        {
          file: { name: 'testFile.txt' },
          newName: 'testFile',
          uploaded: false,
          uploadStatus: 0,
          removed: false,
          error: 'example'
        }
      ]
    });
    await nextTick();
    await wrapper.vm.uploadDocuments();
    await nextTick();
    expect(wrapper.vm.documents[0].removed).toStrictEqual(true);
  });

  it('setDocumentProgress() changes the progress of a document', () => {
    const document = { uploadStatus: 0 };
    wrapper.vm.setDocumentProgress({ loaded: 21, total: 100 }, document);
    expect(document).toStrictEqual({ uploadStatus: 21 });
  });

  it('onUploadProgress() calls setDocumentProgress', async () => {
    const setDocumentProgress = vi.spyOn(
      wrapper.vm.context,
      'setDocumentProgress'
    );
    wrapper.vm.basicStore.token = 'AAAAAAAA';
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockResolvedValueOnce({});
    httpPost.mockResolvedValueOnce({});
    await wrapper.vm.uploadDocuments();
    const config = httpPost.mock.calls[0][2] as {
      // eslint-disable-next-line @typescript-eslint/ban-types
      onUploadProgress: Function;
    };
    config.onUploadProgress({ loaded: 10, total: 100 });
    expect(setDocumentProgress).toHaveBeenCalledTimes(1);
    expect(setDocumentProgress).toHaveBeenCalledWith(
      { loaded: 10, total: 100 },
      {
        file: expect.any(File),
        newName: 'testFile',
        uploadStatus: 10,
        uploaded: false,
        removed: false
      }
    );
  });

  it('namelessDocumentsExist() returns true if any documents have no name', async () => {
    expect(wrapper.vm.namelessDocumentsExist).toStrictEqual(false);
    wrapper.vm.incompleteDocuments[0].tempName = '';
    await nextTick();
    expect(wrapper.vm.namelessDocumentsExist).toStrictEqual(true);
  });
});

describe('AddDocumentModal.vue different process.env', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockClients();
    vi.mock('src/functions/client/getClient', () => ({
      default: vi.fn()
    }));

    vi.mock('src/functions/proxy/AxiosFailed');
    process.env.VUE_APP_MAX_STORAGE = undefined;
    wrapper = shallowMount(AddDocumentModal, {
      props: {
        clientId: 4,
        documents: []
      }
    });
  });

  it('max storage is 0 when undefined', () => {
    expect(wrapper.vm.maxStorage).toStrictEqual(0);
  });
});
