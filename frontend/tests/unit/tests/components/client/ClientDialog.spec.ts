import { mount } from '@vue/test-utils';
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import ClientDialog from 'src/components/clients/ClientDialog.vue';
import setup from 'tests/unit/functions/setup';
import { nextTick } from 'vue';
import { Notify } from 'quasar';
import getCentreDataForClient from 'src/functions/proxy/getCentreDataForClient';
import getClient from 'src/functions/client/getClient';
import { mockCentres, mockClients } from '../../../mocks';
import type { SpyInstance } from 'vitest';

setup();

describe('ClientDialog.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let notifySpy: SpyInstance;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    mockCentres();
    mockClients();
    vi.mock('src/functions/proxy/AxiosFailed');
    vi.mock('src/functions/client/getClient');
    vi.mock('src/functions/client/getClientNeedsForClient');
    vi.mock('src/functions/proxy/getCentreDataForClient');
    wrapper = mount(ClientDialog, {
      props: {
        clientId: 4
      }
    });
    wrapper.vm.userStore.user.centres = [2, 3];
    wrapper.vm.userStore.user.group = 'nurse';

    notifySpy = vi.spyOn(Notify, 'create');
  });

  afterEach(() => {
    wrapper = undefined;
  });

  it('changing client centres does nothinf if client user', async () => {
    expect(getCentreDataForClient).toHaveBeenCalledTimes(0);
    wrapper.vm.userStore.user.group = 'client';
    wrapper.vm.context.closeDialog = vi.fn();
    wrapper.vm.clientStore.clients[wrapper.vm.clientId].centres = [2];
    await nextTick();
    expect(getCentreDataForClient).toHaveBeenCalledTimes(0);
  });

  it('closeDialog() closes dialog', () => {
    wrapper.vm.showDeleteClientConfirmDialog = true;
    wrapper.vm.closeDialog();
    expect(wrapper.vm.showDeleteClientConfirmDialog).toStrictEqual(false);
  });

  it('openVisitingCentresEditDialog() opens dialog', () => {
    wrapper.vm.showVisitingCentresEditDialog = false;
    wrapper.vm.openVisitingCentresEditDialog();
    expect(wrapper.vm.showVisitingCentresEditDialog).toStrictEqual(true);
  });

  it('showUnagreedPolicyDialog() opens dialog when there are unagreed policies', async () => {
    wrapper.vm.unagreedPolicyDialog = false;
    wrapper.vm.userStore.user.centres = [3];
    wrapper.vm.centreStore.centres[3].policies = [2];
    await nextTick();
    expect(wrapper.vm.showUnagreedPolicyDialog).toStrictEqual(true);
    expect(wrapper.vm.unagreedPolicies).toStrictEqual([
      {
        centre: 3,
        unagreedPolicies: [{ description: 'Test Policy 2', id: 2 }]
      }
    ]);
  });

  it('showUnagreedPolicyDialog() does not open when user not subbed to centre', async () => {
    wrapper.vm.unagreedPolicyDialog = false;
    wrapper.vm.userStore.user.centres = [];
    wrapper.vm.centreStore.centres[3].policies = [2];
    await nextTick();
    expect(wrapper.vm.showUnagreedPolicyDialog).toStrictEqual(false);
  });

  it('showUnagreedPolicyDialog() closes dialog when all policies are agreed', async () => {
    wrapper.vm.unagreedPolicyDialog = true;

    wrapper.vm.centreStore.centres[3].policies = [2];
    wrapper.vm.clientStore.clients[4].agreedPolicies = [
      { centre: 3, policy: 2 }
    ];
    await nextTick();
    expect(wrapper.vm.showUnagreedPolicyDialog).toStrictEqual(false);
  });

  it('clientCentres() returns client centre ids sorted by name', () => {
    wrapper.vm.centreStore.centres = {
      1: { id: 1, name: 'bcd' },
      2: { id: 2, name: 'def' },
      3: { id: 3, name: 'abc' }
    };
    wrapper.vm.clientStore.clients[wrapper.vm.clientId].centres = [1, 2, 3];
    expect(wrapper.vm.clientCentres).toStrictEqual([3, 1, 2]);
  });

  it('clientCentres() returns empty array when no clientId', async () => {
    await wrapper.setProps({ clientId: null });
    expect(wrapper.vm.clientCentres).toStrictEqual([]);
  });

  it('changing client centres gets policy info if centres overlap', async () => {
    wrapper.vm.context.closeDialog = vi.fn();
    wrapper.vm.clientStore.clients[wrapper.vm.clientId].centres = [2];
    await nextTick();
    expect(wrapper.vm.context.closeDialog).toHaveBeenCalledTimes(0);
    expect(getCentreDataForClient).toHaveBeenCalled();
    expect(notifySpy).toHaveBeenCalledTimes(0);
  });

  it('changing client centres closes dialog if no overlap', async () => {
    wrapper.vm.context.closeDialog = vi.fn();
    wrapper.vm.clientStore.clients[wrapper.vm.clientId].centres = [1];
    await nextTick();
    expect(wrapper.vm.context.closeDialog).toHaveBeenCalledTimes(1);
    expect(notifySpy).toHaveBeenCalledWith({
      message: 'Your access to this client has been removed by an admin',
      type: 'info'
    });
  });

  it('unagreedCentrePolicies() ignores deleted centres', () => {
    wrapper.vm.clientStore.clients[4].centres = [3, 15, 16, 17];
    delete wrapper.vm.centreStore.centres[3];
    expect(wrapper.vm.unagreedCentrePolicies).toStrictEqual([]);
  });

  it('unagreedCentrePolicies() returns empty array when no clientId', async () => {
    await wrapper.setProps({ clientId: null });
    expect(wrapper.vm.unagreedCentrePolicies).toStrictEqual([]);
  });

  it('clientBasicInfo() gets new entry when client becomes inactive', async () => {
    expect(wrapper.vm.clientBasicInfo.length).toStrictEqual(10);
    wrapper.vm.clientStore.clients[4].isActive = false;
    await nextTick();
    expect(wrapper.vm.clientBasicInfo.length).toStrictEqual(11);
    expect(wrapper.vm.clientBasicInfo[4].text).toStrictEqual(
      'Reason For Inactivity'
    );
  });

  it('clientBasicInfo() removes entry when client becomes active', async () => {
    wrapper.vm.clientStore.clients[4].isActive = false;
    await nextTick();
    wrapper.vm.clientStore.clients[4].isActive = true;
    await nextTick();
    expect(wrapper.vm.clientBasicInfo.length).toStrictEqual(10);
  });

  it('setUnagreedPolicyDialog shows or hides dialog', () => {
    wrapper.vm.setUnagreedPolicyDialog(false);
    expect(wrapper.vm.showUnagreedPolicyDialog).toStrictEqual(false);

    wrapper.vm.setUnagreedPolicyDialog(true);
    expect(wrapper.vm.showUnagreedPolicyDialog).toStrictEqual(true);
  });

  it('setVisitingCentresEditDialog shows or hides dialog', () => {
    wrapper.vm.showVisitingCentresEditDialogFalse();
    expect(wrapper.vm.showVisitingCentresEditDialog).toStrictEqual(false);

    wrapper.vm.setVisitingCentresEditDialog(true);
    expect(wrapper.vm.showVisitingCentresEditDialog).toStrictEqual(true);
  });

  it('clientCentreIds is empty when no clientId', async () => {
    await wrapper.setProps({ clientId: null });
    expect(wrapper.vm.clientCentreIds).toStrictEqual([]);
  });

  it('centreName gets name of centre', () => {
    expect(wrapper.vm.centreName(3)).toStrictEqual('Test Centre 3');
    expect(wrapper.vm.centreName(400)).toStrictEqual('');
  });

  it('updating clientId calls updateClientData', async () => {
    wrapper.vm.context.updateClientData = vi.fn();
    await wrapper.setProps({ clientId: 20 });
    expect(wrapper.vm.context.updateClientData).toHaveBeenCalledTimes(1);
  });

  it('updateClientData does nothing if no clientId', async () => {
    expect(getClient).toHaveBeenCalledTimes(1);
    await wrapper.setProps({ clientId: null });
    await wrapper.vm.updateClientData();
    expect(getClient).toHaveBeenCalledTimes(1);
  });

  it('calculates unagreed centre policies', () => {
    wrapper.vm.centreStore.policies[1] = {
      id: 1,
      description: 'Test Policy 1'
    };
    wrapper.vm.centreStore.centres[2].policies = [2];
    wrapper.vm.centreStore.centres[3].policies = [3, 1];
    expect(wrapper.vm.unagreedCentrePolicies).toStrictEqual([
      {
        centre: 3,
        unagreedPolicies: [
          { id: 1, description: 'Test Policy 1' },
          { id: 3, description: 'Test Policy 3' }
        ]
      }
    ]);
  });

  it('setCentreExpanderActive sets centre active', () => {
    expect(wrapper.vm.clientCentreIds[0].active).toStrictEqual(false);
    wrapper.vm.clientCentreIds[0].setActive();
    expect(wrapper.vm.clientCentreIds[0].active).toStrictEqual(true);
  });

  it('setCentreExpanderInactive sets centre inactive', () => {
    wrapper.vm.clientCentreIds[0].active = true;
    wrapper.vm.clientCentreIds[0].setInactive();
    expect(wrapper.vm.clientCentreIds[0].active).toStrictEqual(false);
  });
});
