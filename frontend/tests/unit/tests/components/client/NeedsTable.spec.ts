import { shallowMount } from '@vue/test-utils';
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import NeedsTable from 'src/components/clients/NeedsTable.vue';
import setup from 'tests/unit/functions/setup';
import { nextTick } from 'vue';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import getCentreRelatedData from 'src/functions/centre/getCentreRelatedData';
import { useUserStore } from 'src/stores/userStore';
import { mockCentres, mockClients } from '../../../mocks';

setup();

describe('NeedsTable.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockCentres();
    mockClients();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    vi.mock('src/functions/centre/getCentreRelatedData');
    wrapper = shallowMount(NeedsTable, { props: { clientId: 4, centreId: 3 } });
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = undefined;
  });

  it('needs() returns client needs for table', () => {
    wrapper.vm.clientStore.clients[4].needs.push({
      need: 500,
      centre: 3,
      plan: 'Example Plan 500'
    });
    expect(wrapper.vm.needs).toStrictEqual([
      { id: 1, need: 'Test Need 1', plan: 'Example Plan 1' },
      { id: 2, need: 'Test Need 4', plan: 'Example Plan 2' },
      { id: 3, need: 'Test Need 3', plan: 'Example Plan 3' }
    ]);
  });

  it('needs() is empty when no clientId', async () => {
    await wrapper.setProps({ clientId: null });
    expect(wrapper.vm.needs).toStrictEqual([]);
  });

  it('updatePlan() sends plan data to server', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce({});
    await wrapper.vm.updatePlan(1, 'plan', 'New Plan Name');
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPatch).toHaveBeenCalledWith('clientNeeds/1/update/', {
      plan: 'New Plan Name'
    });
  });

  it('updatePlan() calls axiosFailed() on failure', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockRejectedValueOnce({});
    await wrapper.vm.updatePlan(1, 'plan', 'New Plan Name');
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('getNeeds() is called on changing centreId', async () => {
    await wrapper.setProps({ centreId: 400 });
    await nextTick();
    expect(getCentreRelatedData).toHaveBeenCalledTimes(2);
    expect(getCentreRelatedData).toHaveBeenCalledWith(400, 'needs');
  });

  it('getNeeds() returns null on no centreId', async () => {
    await wrapper.setProps({ centreId: null });
    await nextTick();
    expect(getCentreRelatedData).toHaveBeenCalledTimes(1);
  });

  it('noDataLabel is different if client', () => {
    const userStore = useUserStore();

    userStore.user.group = 'nurse';
    expect(wrapper.vm.noDataLabel).toStrictEqual(
      'This client has no needs for this centre, click the pencil to add some'
    );

    userStore.user.group = 'client';
    expect(wrapper.vm.noDataLabel).toStrictEqual(
      'You have no needs for this centre'
    );
  });
});
