import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import ClientDaySelector from 'src/components/clients/ClientDaySelector.vue';
import setup from 'tests/unit/functions/setup';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { mockCentres, mockClients } from '../../../mocks';

setup();

describe('ClientDaySelector.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockCentres();
    mockClients();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    wrapper = shallowMount(ClientDaySelector, {
      props: {
        centreId: 3,
        clientId: 4
      }
    });
  });

  it('updateDays() sends a patch request to server', async () => {
    wrapper.vm.loaded = true;
    wrapper.vm.selectedDays = [2, 3, 4];
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce({});
    await wrapper.vm.updateDays(4);
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPatch).toHaveBeenCalledWith('clients/4/update/', {
      visitingDays: [
        { centre: 3, day: 3 },
        { centre: 3, day: 4 }
      ]
    });
  });

  it('updateDays() does not call patch if not loaded', async () => {
    wrapper.vm.loaded = false;
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    await wrapper.vm.updateDays(4);
    expect(httpPatch).toHaveBeenCalledTimes(0);
  });

  it('updateDays() calls axiosFailed() on failure', async () => {
    wrapper.vm.loaded = true;
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockRejectedValueOnce({});
    await wrapper.vm.updateDays();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('filteredDays returns array day values that are centre opening days', () => {
    expect(wrapper.vm.filteredDays).toStrictEqual([1, 3, 5]);
  });

  it('days() is an empty array if clientId is undefined', async () => {
    await wrapper.setProps({ clientId: null });
    expect(wrapper.vm.days).toStrictEqual([]);
  });

  it('filteredDays returns empty if there is no centreId', async () => {
    await wrapper.setProps({ centreId: null });
    expect(wrapper.vm.filteredDays).toStrictEqual([]);
  });
});
