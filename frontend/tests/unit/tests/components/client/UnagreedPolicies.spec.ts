import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import UnagreedPolicies from 'src/components/clients/UnagreedPolicies.vue';
import setup from 'tests/unit/functions/setup';
import { mockCentres } from '../../../mocks';

setup();

describe('UnagreedPolicies.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockCentres();
    vi.mock('src/functions/proxy/AxiosFailed');
    wrapper = shallowMount(UnagreedPolicies, {
      props: {
        unagreedPolicies: [
          {
            centre: 3,
            unagreedPolicies: [{ description: 'Test Policy 2', id: 2 }]
          }
        ]
      }
    });
  });

  it('gets correct props', () => {
    expect(wrapper.vm.unagreedPolicies).toStrictEqual([
      {
        centre: 3,
        unagreedPolicies: [{ description: 'Test Policy 2', id: 2 }]
      }
    ]);
  });
});
