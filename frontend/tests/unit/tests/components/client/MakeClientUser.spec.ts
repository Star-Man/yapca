import { mount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import MakeClientUser from 'src/components/clients/MakeClientUser.vue';
import setup from 'tests/unit/functions/setup';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { nextTick } from 'vue';
import { Notify } from 'quasar';
import { mockClients } from '../../../mocks';
import type { SpyInstance } from 'vitest';

setup();

describe('MakeClientUser.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let httpPost: SpyInstance;
  let emit: SpyInstance;
  let notifySpy: SpyInstance;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockClients();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    wrapper = mount(MakeClientUser, {
      props: {
        clientId: 4
      }
    });
    emit = vi.spyOn(wrapper.vm.context, 'emit');
    httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    notifySpy = vi.spyOn(Notify, 'create');
  });

  it('changing email clears existing errors', async () => {
    wrapper.vm.emailErrors = [1, 2, 3];
    wrapper.vm.email = 'newemail';
    await nextTick();
    expect(wrapper.vm.emailErrors).toStrictEqual([]);
  });

  it('sendRegistrationEmail calls email endpoint', async () => {
    await wrapper
      .find('#addNewClientEmailInput')
      .setValue('testemail@test.com');
    await wrapper.find('#canViewOwnDataToggle').trigger('click');
    await wrapper.find('#canViewCalendarToggle').trigger('click');
    wrapper.vm.context.sendEmailSuccess = vi.fn();
    wrapper.vm.selectedCentres = [2, 3, 5];
    httpPost.mockResolvedValueOnce(true);
    await wrapper.vm.sendRegistrationEmail();
    expect(httpPost).toHaveBeenCalledWith(
      `request-client-registration-email/4/`,
      {
        email: 'testemail@test.com',
        canViewCalendar: false,
        canViewOwnData: true
      }
    );
    expect(wrapper.vm.context.sendEmailSuccess).toHaveBeenCalledTimes(1);
  });

  it('sendRegistrationEmail returns null if no clientId', async () => {
    await wrapper.setProps({ clientId: null });
    await wrapper.vm.sendRegistrationEmail();
    expect(httpPost).toHaveBeenCalledTimes(0);
    expect(axiosFailed).toHaveBeenCalledTimes(0);
  });

  it('sendEmailSuccess closes dialog', () => {
    wrapper.vm.sendEmailSuccess();
    expect(emit).toHaveBeenCalledWith('close-dialog');
    expect(notifySpy).toHaveBeenCalledWith({
      message: 'Invite Sent!',
      type: 'positive'
    });
  });

  it('emailErrors are set on failure', async () => {
    const err = {
      response: {
        data: {
          email: ['test email error']
        }
      }
    };
    httpPost.mockRejectedValueOnce(err);

    await wrapper.vm.sendRegistrationEmail();
    expect(wrapper.vm.emailErrors).toStrictEqual(['test email error']);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
    expect(axiosFailed).toHaveBeenCalledWith(err);
  });

  it('isValid returns true if form valid', () => {
    wrapper.vm.email = 'test@test.com';

    expect(wrapper.vm.isValid).toStrictEqual(true);
    wrapper.vm.email = 'testest.com';
    expect(wrapper.vm.isValid).toStrictEqual(false);
  });

  it('email rules handle empty value and invalid email', async () => {
    const emailInput = wrapper.find('#addNewClientEmailInput');
    await emailInput.setValue('');
    expect(wrapper.vm.emailRules[0](emailInput.element.value)).toStrictEqual(
      'Email is required'
    );
    await emailInput.setValue('test');
    expect(wrapper.vm.emailRules[1](emailInput.element.value)).toStrictEqual(
      'Invalid Email Address'
    );
    await emailInput.setValue('asdasd@test.com');
    expect(wrapper.vm.emailRules[1](emailInput.element.value)).toStrictEqual(
      true
    );
  });

  it('validate does nothing if isValid is false', () => {
    const sendRegistrationEmail = vi.spyOn(
      wrapper.vm.context,
      'sendRegistrationEmail'
    );
    wrapper.vm.validate();
    expect(sendRegistrationEmail).toHaveBeenCalledTimes(0);
  });

  it('validate calls sendResistration when isValid is true', () => {
    const sendRegistrationEmail = vi.spyOn(
      wrapper.vm.context,
      'sendRegistrationEmail'
    );
    wrapper.vm.selectedCentres = [1];
    wrapper.vm.email = 'email@test.com';
    wrapper.vm.displayName = 'newDisplayName';

    wrapper.vm.validate();
    expect(sendRegistrationEmail).toHaveBeenCalledTimes(1);
  });

  it('setClientUserActivation calls deactivation endpoint', async () => {
    wrapper.vm.clientStore.clients[4].accountStatus = 'Active';
    httpPost.mockResolvedValueOnce(true);
    await wrapper.vm.setClientUserDeactivated();
    expect(httpPost).toHaveBeenCalledWith(`set-client-user-activation/4/`, {
      active: false,
      sendEmail: true
    });
    expect(notifySpy).toHaveBeenCalledWith({
      message: 'Account Deactivated!',
      type: 'positive'
    });
  });

  it('setClientUserActivation calls reactivation endpoint', async () => {
    wrapper.vm.clientStore.clients[4].accountStatus = 'Inactive';
    httpPost.mockResolvedValueOnce(true);
    await wrapper.vm.setClientUserActivated();
    expect(httpPost).toHaveBeenCalledWith(`set-client-user-activation/4/`, {
      active: true,
      sendEmail: true
    });
    expect(notifySpy).toHaveBeenCalledWith({
      message: 'Account Reactivated!',
      type: 'positive'
    });
  });

  it('setClientUserActivation does nothing if no clientId', async () => {
    await wrapper.setProps({ clientId: null });
    httpPost.mockResolvedValueOnce(true);
    await wrapper.vm.setClientUserActivation(true);
    expect(httpPost).toHaveBeenCalledTimes(0);
  });

  it('setNotifyByEmail does that', () => {
    wrapper.vm.setNotifyByEmail(false);
    expect(wrapper.vm.notifyByEmail).toStrictEqual(false);
  });
});
