import { mount, shallowMount } from '@vue/test-utils';
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import NeedsList from 'src/components/clients/NeedsList.vue';
import setup from 'tests/unit/functions/setup';
import { nextTick } from 'vue';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { mockCentres, mockClients } from '../../../mocks';

setup();

describe('NeedsList.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockCentres();
    mockClients();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    wrapper = shallowMount(NeedsList, { props: { clientId: 4, centreId: 3 } });
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = undefined;
  });

  it('clientNeeds() returns client needs from store', async () => {
    wrapper.vm.clientStore.clients[4].needs = [1, 2, 3, 999];
    await nextTick();
    expect(wrapper.vm.clientNeeds).toStrictEqual([
      {
        id: 1,
        centre: 3,
        need: 1,
        plan: 'Example Plan 1',
        isActive: true,
        valid: true
      },
      {
        id: 2,
        centre: 3,
        need: 2,
        plan: 'Example Plan 2',
        isActive: true,
        valid: true
      },
      {
        id: 3,
        centre: 3,
        need: 3,
        plan: 'Example Plan 3',
        isActive: true,
        valid: true
      }
    ]);
  });

  it('centreNeeds() returns centre needs from store', () => {
    expect(wrapper.vm.centreNeeds).toStrictEqual([
      {
        id: 1,
        description: 'Test Need 1',
        label: 'Test Need 1',
        plan: 'Example Plan 1',
        clientNeedId: 1,
        value: 1
      },
      {
        id: 3,
        description: 'Test Need 3',
        label: 'Test Need 3',
        plan: 'Example Plan 3',
        clientNeedId: 3,
        value: 3
      },
      {
        id: 2,
        description: 'Test Need 4',
        label: 'Test Need 4',
        plan: 'Example Plan 2',
        clientNeedId: 2,
        value: 2
      }
    ]);
  });

  it('clientNeeds() returns empty array when needs are null', () => {
    wrapper.vm.clientStore.clients[4].needs = undefined;
    expect(wrapper.vm.clientNeeds).toStrictEqual([]);
  });

  it('updateNeedsModel sets selectedNeeds', async () => {
    wrapper.vm.clientStore.clients[4].needs = [1, 2, 3, 4];
    wrapper.vm.clientStore.clientNeeds = {
      '1': {
        id: 1,
        centre: 3,
        need: 1,
        plan: 'Example Plan 1',
        isActive: true
      },
      '2': {
        centre: 3,
        need: 2,
        plan: 'Example Plan 2',
        isActive: false
      },
      '3': { centre: 2, need: 2, plan: 'Example Plan 3', isActive: true },
      '4': {
        centre: 3,
        need: 60,
        plan: 'Example Plan 60',
        isActive: true
      }
    };
    await nextTick();
    expect(wrapper.vm.selectedNeeds).toStrictEqual([
      {
        id: 1,
        description: 'Test Need 1',
        label: 'Test Need 1',
        plan: 'Example Plan 1',
        clientNeedId: 1,
        isActive: true,
        value: 1
      }
    ]);
  });

  it('updateNeedsModel does not set selectedNeeds if no centre', async () => {
    wrapper.vm.centreStore.centres[3] = {};
    wrapper.vm.clientStore.clients[4].needs = [
      { centre: 3, need: 1 },
      { centre: 3, need: 2 }
    ];
    await nextTick();
    expect(wrapper.vm.selectedNeeds).toStrictEqual([]);
  });

  it('updateNeeds sends patch to server when updating', async () => {
    const selectedNeeds = [{ clientNeedId: 3 }];
    wrapper.vm.previousSelectedNeeds = [];
    await nextTick();
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce({});
    await wrapper.vm.updateNeeds(selectedNeeds);
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPatch).toHaveBeenCalledWith('clientNeeds/3/update/', {
      isActive: true
    });
  });

  it('updateNeeds sends patch to server when deselecting', async () => {
    const clientNeed2 = { clientNeedId: 2 };
    wrapper.vm.selectedNeeds = [clientNeed2];
    wrapper.vm.previousSelectedNeeds = [clientNeed2, { clientNeedId: 3 }];
    await nextTick();
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce({});
    await wrapper.vm.updateNeeds(wrapper.vm.selectedNeeds);
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPatch).toHaveBeenCalledWith('clientNeeds/3/update/', {
      isActive: false
    });
  });

  it('updateNeeds sends post to server when creating new clientNeed', async () => {
    const selectedNeeds = [{ plan: 'new plan', id: 2 }];
    wrapper.vm.previousSelectedNeeds = [];
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockResolvedValueOnce({});
    await wrapper.vm.updateNeeds(selectedNeeds);
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPost).toHaveBeenCalledWith('clientNeeds/', {
      centre: 3,
      client: 4,
      need: 2,
      plan: 'new plan'
    });
  });

  it('updateNeeds calls axiosFailed when post fails', async () => {
    const selectedNeeds = [{ plan: 'new plan', id: 2 }];
    wrapper.vm.previousSelectedNeeds = [];
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockRejectedValueOnce({});
    await wrapper.vm.updateNeeds(selectedNeeds);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('updateNeeds does not send patch to server when no selected needs', async () => {
    wrapper.vm.selectedNeeds = [];
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce({});
    await wrapper.vm.updateNeeds();
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPatch).toHaveBeenCalledTimes(0);
  });

  it('updateNeeds calls axiosFailed on patch failure', async () => {
    const selectedNeeds = [{ clientNeedId: 3 }];
    wrapper.vm.previousNeeds = [];
    await nextTick();
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockRejectedValueOnce({});
    await wrapper.vm.updateNeeds(selectedNeeds);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('openPlanDialog() does that', () => {
    wrapper.vm.selectedNeed = null;
    wrapper.vm.editPlanDialog = false;
    wrapper.vm.planDescription = '';
    wrapper.vm.openPlanDialog({ plan: 'test' });
    expect(wrapper.vm.selectedNeed).toStrictEqual({ plan: 'test' });
    expect(wrapper.vm.showEditPlanDialog).toStrictEqual(true);
    expect(wrapper.vm.planDescription).toStrictEqual('test');

    wrapper.vm.openPlanDialog({ plan: undefined });
    expect(wrapper.vm.planDescription).toStrictEqual('');
  });

  it('updatePlan() sends patch to server', async () => {
    wrapper.vm.selectedNeed = { clientNeedId: 4 };
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce({});
    await wrapper.vm.updatePlan(undefined, '', 'test plan');
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPatch).toHaveBeenCalledWith('clientNeeds/4/update/', {
      plan: 'test plan'
    });
  });

  it('updatePlan() calls axiosFailed on failure', async () => {
    wrapper.vm.selectedNeed = { clientNeedId: 4 };
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockRejectedValueOnce({});
    await wrapper.vm.updatePlan(undefined, '', 'test plan');
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('updatePlan() does not send patch no selectedNeed', async () => {
    wrapper.vm.selectedNeed = null;
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    await wrapper.vm.updatePlan(undefined, '', 'test plan');
    expect(httpPatch).toHaveBeenCalledTimes(0);
  });

  it('required() returns string when selected needs are empty', () => {
    expect(wrapper.vm.required([])).toStrictEqual(
      'At least one need is required'
    );
  });

  it('required() boolean when selected needs are not empty', () => {
    expect(wrapper.vm.required([{}])).toStrictEqual(true);
  });

  it('centreNeeds() returns empty array when no centreId', async () => {
    await wrapper.setProps({ centreId: null });
    expect(wrapper.vm.centreNeeds).toStrictEqual([]);
  });

  it('selectBehaviour is different at different breakpoints', async () => {
    wrapper.vm.$q.screen.xs = true;
    expect(wrapper.vm.selectBehaviour).toStrictEqual('dialog');
    wrapper.vm.$q.screen.xs = false;
    await nextTick();
    expect(wrapper.vm.selectBehaviour).toStrictEqual('menu');
  });
});

describe('NeedsList.vue full mount', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockCentres();
    mockClients();
    vi.mock('src/functions/proxy/AxiosFailed');
    wrapper = mount(NeedsList, { props: { clientId: 4, centreId: 3 } });
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = undefined;
  });

  it('needsFilter can filter selectedNeeds', async () => {
    await wrapper.find('#clientNeedsInput').setValue('Test Need 1');
    await wrapper.find('#clientNeedsInput').trigger('change');
    const update = vi.fn();
    wrapper.vm.needsFilter('4', update);
    update.mock.calls[0][0]();
    expect(wrapper.vm.filterNeedsOptions).toStrictEqual([
      {
        plan: 'Example Plan 2',
        value: 2,
        label: 'Test Need 4',
        id: 2,
        description: 'Test Need 4',
        clientNeedId: 2
      }
    ]);
  });

  it('needsFilter does not filter when no value input', () => {
    const update = vi.fn();
    wrapper.vm.needsFilter('', update);
    update.mock.calls[0][0]();
    expect(wrapper.vm.filterNeedsOptions.length).toStrictEqual(3);
  });

  it('clicking needsViewCentreButton shows dialog', async () => {
    wrapper.vm.showCentreDialog = false;
    await wrapper.find('#needsViewCentreButton').trigger('click');
    expect(wrapper.vm.showCentreDialog).toStrictEqual(true);
  });

  it('closeCentreDialog does that', () => {
    wrapper.vm.showCentreDialog = true;
    wrapper.vm.closeCentreDialog();
    expect(wrapper.vm.showCentreDialog).toStrictEqual(false);
  });

  it('noNeedsText is visible when no needs', async () => {
    expect(wrapper.find('#noNeedsText').exists()).toStrictEqual(false);
    await wrapper.setProps({ centreId: null });
    expect(wrapper.find('#noNeedsText').exists()).toStrictEqual(true);
  });
});
