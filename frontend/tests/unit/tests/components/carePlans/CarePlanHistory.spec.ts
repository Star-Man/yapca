import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import CarePlanHistory from 'src/components/carePlans/CarePlanHistory.vue';
import setup from 'tests/unit/functions/setup';
import { nextTick } from 'vue';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import {
  mockCarePlanAPIGet,
  mockCarePlans,
  mockCentres,
  mockClients,
  mockUsers
} from '../../../mocks';
import type { SpyInstance } from 'vitest';

setup();

describe('CarePlanHistory.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let httpGet: SpyInstance;
  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockCentres();
    mockClients();
    mockUsers();
    mockCarePlans();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    wrapper = shallowMount(CarePlanHistory, { props: { clientId: 4 } });
    wrapper.vm.userStore.user.centres = [3];
    httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
  });

  it('clientName returns the client name', () => {
    expect(wrapper.vm.clientName).toStrictEqual('test name 4');
  });

  it("clientName returns empty string if client doesn't exist", async () => {
    await wrapper.setProps({
      clientId: 7498
    });
    await nextTick();
    expect(wrapper.vm.clientName).toStrictEqual('');
  });

  it('readableCarePlans returns care plan data for table', () => {
    expect(wrapper.vm.readableCarePlans).toStrictEqual([
      {
        id: 18,
        need: 'Test Need 3',
        plan: 'Example Plan 3',
        comment: 'example comment 18',
        date: '11/12/11',
        completed: true,
        centre: 'Test Centre 3',
        addedBy: 'Test User 3'
      },
      {
        id: 19,
        need: 'Test Need 4',
        plan: 'Example Plan 2',
        date: '12/12/11',
        comment: 'example comment 19',
        completed: false,
        centre: 'Test Centre 3',
        addedBy: 'Test User'
      }
    ]);
    wrapper.vm.clientStore.clientNeeds[2].centre = 999;
    expect(wrapper.vm.readableCarePlans).toStrictEqual([
      {
        id: 18,
        need: 'Test Need 3',
        plan: 'Example Plan 3',
        comment: 'example comment 18',
        date: '11/12/11',
        completed: true,
        centre: 'Test Centre 3',
        addedBy: 'Test User 3'
      }
    ]);
  });

  it('getCarePlanHistory() gets carePlan values from store', async () => {
    httpGet.mockResolvedValue(mockCarePlanAPIGet);
    wrapper.vm.clientStore.clients[4].needs.push({
      id: 4,
      need: 999,
      centre: 3,
      plan: 'Example Plan 4'
    });
    await wrapper.vm.getCarePlanHistory();
    expect(Object.keys(wrapper.vm.clientStore.carePlans).length).toStrictEqual(
      21
    );
    expect(httpGet).toHaveBeenCalledWith('carePlansHistory/4/');
    expect(wrapper.vm.centreStore.needs[3]).toStrictEqual({
      id: 3,
      description: 'test need 3'
    });
    expect(wrapper.vm.clientStore.clientNeeds[5]).toStrictEqual({
      id: 5,
      centre: 3,
      need: 3,
      plan: 'example plan',
      isActive: true
    });
  });

  it('getCarePlanHistory() throws axiosFailed on failure', async () => {
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    httpGet.mockRejectedValueOnce({});
    await wrapper.vm.getCarePlanHistory();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('getCarePlanHistory() does nothing if no clientId', async () => {
    await wrapper.setProps({ clientId: null });
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    await wrapper.vm.getCarePlanHistory();
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpGet).toHaveBeenCalledTimes(0);
  });

  it('customSort() sorts carePlans by store date value descending', () => {
    const items = [{ id: 1 }, { id: 9 }];
    const sortedCarePlans = wrapper.vm.customSort(items, 'date', false);
    expect(sortedCarePlans).toStrictEqual([{ id: 9 }, { id: 1 }]);
  });

  it('customSort() sorts carePlans by store date value ascending', () => {
    const items = [{ id: 1 }, { id: 9 }];
    const sortedCarePlans = wrapper.vm.customSort(items, 'date', true);
    expect(sortedCarePlans).toStrictEqual([{ id: 1 }, { id: 9 }]);
  });

  it('customSort() sorts carePlans alphabetically if no date value', () => {
    const items = [
      { id: 1, comment: 'b' },
      { id: 4, comment: 'd' },
      { id: 5, comment: 'c' },
      { id: 6, comment: 'a' }
    ];
    const sortedCarePlans = wrapper.vm.customSort(items, 'comment', false);
    expect(sortedCarePlans).toStrictEqual([
      { id: 6, comment: 'a' },
      { id: 1, comment: 'b' },
      { id: 5, comment: 'c' },
      { id: 4, comment: 'd' }
    ]);
  });

  it('customSort() sorts carePlans alphabetically descending if no date value', () => {
    const items = [
      { id: 1, comment: 'b' },
      { id: 4, comment: 'd' },
      { id: 5, comment: 'c' },
      { id: 6, comment: 'a' }
    ];
    const sortedCarePlans = wrapper.vm.customSort(items, 'comment', true);
    expect(sortedCarePlans).toStrictEqual([
      { id: 4, comment: 'd' },
      { id: 5, comment: 'c' },
      { id: 1, comment: 'b' },
      { id: 6, comment: 'a' }
    ]);
  });
});
