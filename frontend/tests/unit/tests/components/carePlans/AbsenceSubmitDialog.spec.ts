import { shallowMount } from '@vue/test-utils';
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import AbsenceSubmitDialog from 'src/components/carePlans/AbsenceSubmitDialog.vue';
import setup from 'tests/unit/functions/setup';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import {
  mockCarePlans,
  mockCentres,
  mockClients,
  mockUsers
} from '../../../mocks';

setup();

describe('AbsenceSubmitDialog.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockCentres();
    mockClients();
    mockUsers();
    mockCarePlans();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    wrapper = shallowMount(AbsenceSubmitDialog, {
      props: {
        clientId: 4,
        date: '2011-11-18',
        centres: [2, 3, 99]
      }
    });
    wrapper.vm.userStore.user.centres = [2, 3];
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = undefined;
  });

  it('clientName returns the client name', () => {
    expect(wrapper.vm.clientName).toStrictEqual('test name 4');
  });

  it("clientName returns empty string if client doesn't exist", async () => {
    await wrapper.setProps({
      clientId: 799
    });
    expect(wrapper.vm.clientName).toStrictEqual('');
  });

  it('closeDialog() closes dialog', () => {
    const emit = vi.spyOn(wrapper.vm.context, 'emit');
    wrapper.vm.closeDialog();
    expect(emit).toHaveBeenCalledTimes(1);
    expect(emit).toHaveBeenCalledWith('close-dialog');
  });

  it('submitAbsences submits an absence per selected centre', async () => {
    wrapper.vm.selectedCentres = [
      { text: 'Example Centre 2', value: 2 },
      { text: 'Example Centre 4', value: 4 }
    ];
    wrapper.vm.reasonForAbsences = {
      2: 'reason for absence 2',
      4: 'reason for absence 4'
    };
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockResolvedValueOnce('');
    await wrapper.vm.submitAbsences();
    expect(httpPost).toHaveBeenCalledTimes(1);
    expect(httpPost).toHaveBeenCalledWith('attendanceRecord/', [
      {
        centre: 2,
        client: 4,
        date: '2011-11-18',
        reasonForAbsence: 'reason for absence 2'
      },
      {
        centre: 4,
        client: 4,
        date: '2011-11-18',
        reasonForAbsence: 'reason for absence 4'
      }
    ]);
  });

  it('submitAbsences calls axiosFailed on fail', async () => {
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockRejectedValueOnce('');
    await wrapper.vm.submitAbsences();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('changing props sets selectedCentres to empty array', async () => {
    wrapper.vm.selectedCentres = [
      { text: 'Example Centre 2', value: 2 },
      { text: 'Example Centre 4', value: 4 }
    ];
    wrapper.vm.reasonForAbsences = { '1': 'Example' };
    await wrapper.setProps({
      clientId: 799
    });
    expect(wrapper.vm.selectedCentres).toStrictEqual([]);
    expect(wrapper.vm.reasonForAbsences).toStrictEqual({});
  });

  it('changing props sets selectedCentres to selected centre if only one', async () => {
    wrapper.vm.selectedCentres = [
      { text: 'Example Centre 2', value: 2 },
      { text: 'Example Centre 4', value: 4 }
    ];
    wrapper.vm.centres.splice(1, 1);
    wrapper.vm.centres.splice(1, 1);
    await wrapper.setProps({ date: '2022-01-01' });
    expect(wrapper.vm.selectedCentres).toStrictEqual(wrapper.vm.centreNames);
  });
});
