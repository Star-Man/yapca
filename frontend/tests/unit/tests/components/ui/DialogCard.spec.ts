import { mount } from '@vue/test-utils';
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import DialogCard from 'src/components/ui/DialogCard.vue';
import setup from 'tests/unit/functions/setup';
import { nextTick } from 'vue';
import { getRouter } from 'vue-router-mock';
import type { SpyInstance } from 'vitest';

setup();

describe('CardWithHeader.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let emit: SpyInstance;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    wrapper = mount(DialogCard, {
      props: { cardTitle: 'Example Title', propShowDialog: true }
    });
    emit = vi.spyOn(wrapper.vm, 'emit');
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = undefined;
  });

  it('header card closing emits set-dialog false', async () => {
    wrapper.vm.$q.screen.xs = true;
    wrapper.vm.showDialog = true;
    await nextTick();
    wrapper.vm.$refs.cardWithHeaderRef.$emit('close-dialog');
    expect(emit).toHaveBeenCalledWith('set-dialog', false);
  });

  it('setShowDialog shows or hides dialog', async () => {
    await getRouter().setHash('#my-hash');
    wrapper.vm.setShowDialog(false);
    expect(wrapper.vm.showDialog).toStrictEqual(false);

    await wrapper.setProps({
      dialogHash: 'my-hash'
    });
    await getRouter().setHash('#different-hash');
    wrapper.vm.setShowDialog(true);
    await nextTick();
    expect(wrapper.vm.showDialog).toStrictEqual(true);
    expect(wrapper.router.push).toHaveBeenCalledWith('#different-hash&my-hash');
  });
});
