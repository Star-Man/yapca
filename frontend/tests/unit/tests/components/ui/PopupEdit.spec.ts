import { mount } from '@vue/test-utils';
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import PopupEdit from 'src/components/ui/PopupEdit.vue';
import setup from 'tests/unit/functions/setup';
import type { SpyInstance } from 'vitest';

setup();

describe('PopupEdit.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let emit: SpyInstance;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    wrapper = mount(PopupEdit, {
      props: {
        modelId: 5,
        fieldName: 'surname',
        model: 'examplesurname'
      }
    });
    emit = vi.spyOn(wrapper.vm.context, 'emit');
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = undefined;
  });

  it('save data calls emit', async () => {
    wrapper.vm.popupEdit.hide = vi.fn();
    wrapper.vm.value = 'newSurname';
    await wrapper.vm.saveDataOnHide();
    expect(emit).toHaveBeenCalledWith('save-data', 5, 'surname', 'newSurname');
    expect(wrapper.vm.popupEdit.hide).toHaveBeenCalledOnce();
  });

  it("saveDataOnHide doesn't emit if savebutton is true", async () => {
    await wrapper.setProps({ saveButton: true });
    await wrapper.vm.saveDataOnHide();
    expect(emit).toHaveBeenCalledTimes(0);
  });

  it('changing props updates value', async () => {
    await wrapper.setProps({ model: 'updatedSurname' });
    expect(wrapper.vm.value).toStrictEqual('updatedSurname');
  });

  it('showEmptyText updates with value', async () => {
    await wrapper.setProps({ showEmptyText: true });
    wrapper.vm.value = 'newValue';
    expect(wrapper.vm.showEmptyText).toStrictEqual(false);
    wrapper.vm.value = '';
    expect(wrapper.vm.showEmptyText).toStrictEqual(true);
  });

  it('setNewValue sets value', async () => {
    await wrapper.vm.setNewValue('test-value');
    expect(wrapper.vm.value).toStrictEqual('test-value');
    expect(emit).toHaveBeenCalledTimes(0);
  });

  it('setNewValue closes dialog if closeOnSet is true', async () => {
    await wrapper.setProps({ closeOnSet: true });
    await wrapper.vm.setNewValue('test-value');
    expect(emit).toHaveBeenCalledWith('save-data', 5, 'surname', 'test-value');
  });

  it('can set width with prop', async () => {
    expect(wrapper.vm.popupWidth).toStrictEqual('');
    await wrapper.setProps({ popupWidth: 250 });
    expect(wrapper.vm.popupWidth).toStrictEqual('width: 250px');
  });
});
