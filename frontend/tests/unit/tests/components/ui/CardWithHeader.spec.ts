import { mount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import CardWithHeader from 'src/components/ui/CardWithHeader.vue';
import setup from 'tests/unit/functions/setup';
import { nextTick } from 'vue';
import type { SpyInstance } from 'vitest';

setup();

describe('CardWithHeader.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let emit: SpyInstance;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    wrapper = mount(CardWithHeader, { props: { cardTitle: 'Example Title' } });
    emit = vi.spyOn(wrapper.vm, 'emit');
  });

  it('has correct class on xs viewpoints', async () => {
    wrapper.vm.$q.screen.xs = false;
    await nextTick();
    const card = wrapper.find('.q-card');
    expect(card.classes()).toContain('q-ma-md');
    wrapper.vm.$q.screen.xs = true;
    await nextTick();
    expect(card.classes()).not.toContain('q-ma-md');
  });

  it('header menu button is hidden if dialog ', async () => {
    wrapper.vm.$q.screen.xs = true;
    await wrapper.setProps({ dialog: false });
    await nextTick();
    const menuButton = wrapper.find('#headerMenuButton');
    expect(menuButton.classes()).not.toContain('hidden-button');
    await wrapper.setProps({ dialog: true });
    expect(menuButton.classes()).toContain('hidden-button');
  });

  it('header close dialog button is shown if dialog ', async () => {
    wrapper.vm.$q.screen.xs = true;
    await wrapper.setProps({ dialog: false });
    await nextTick();
    const closeDialogButton = wrapper.find('#headerCloseDialogButton');
    expect(closeDialogButton.classes()).toContain('hidden-button');
    await wrapper.setProps({ dialog: true });
    expect(closeDialogButton.classes()).not.toContain('hidden-button');
  });

  it('clicking header menu button toggles nav bar', async () => {
    wrapper.vm.basicStore.navDrawer = false;
    wrapper.vm.$q.screen.xs = true;
    await wrapper.setProps({ dialog: false });
    const menuButton = wrapper.find('#headerMenuButton');
    await menuButton.trigger('click');
    expect(wrapper.vm.basicStore.navDrawer).toStrictEqual(true);
    await menuButton.trigger('click');
    expect(wrapper.vm.basicStore.navDrawer).toStrictEqual(false);
  });

  it('clicking header close dialog button emits close-dialog event', async () => {
    wrapper.vm.$q.screen.xs = true;
    await wrapper.setProps({ dialog: true });
    const closeDialogButton = wrapper.find('#headerCloseDialogButton');
    await closeDialogButton.trigger('click');
    expect(emit).toHaveBeenCalledWith('close-dialog');
  });

  it('clicking toolbar close dialog button emits close-dialog event', async () => {
    wrapper.vm.$q.screen.xs = false;
    await wrapper.setProps({ dialog: true });
    const closeDialogButton = wrapper.find('#closeDialogButton');
    await closeDialogButton.trigger('click');
    expect(emit).toHaveBeenCalledWith('close-dialog');
  });

  it('closeDialogButtonClass is hidden when persistent', async () => {
    await wrapper.setProps({ persistent: true });
    expect(wrapper.vm.closeDialogButtonClass).toStrictEqual('hidden-button');
    await wrapper.setProps({ persistent: false });
    expect(wrapper.vm.closeDialogButtonClass).toStrictEqual('');
  });
});
