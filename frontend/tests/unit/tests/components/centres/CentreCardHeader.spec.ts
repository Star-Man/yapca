/* eslint-disable @typescript-eslint/no-explicit-any */
import { mount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import CentreCardHeader from 'src/components/centres/CentreCardHeader.vue';
import setup from 'tests/unit/functions/setup';
import { useUserStore } from 'src/stores/userStore';
import updateCentre from 'src/functions/centre/updateCentre';
import { nextTick } from 'vue';
import { mockCentres } from '../../../mocks';

setup();

describe('CentreCardHeader.vue', () => {
  let wrapper: any;
  let userStore: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    vi.mock('src/functions/centre/updateCentre');
    mockCentres();
    wrapper = mount(CentreCardHeader, {
      props: {
        selectedCentreId: 4
      }
    });
    userStore = useUserStore();
  });

  it('updateCentreNameEditingStatus() changes a centres edit value if admin', () => {
    userStore.user.group = 'admin';
    wrapper.vm.centreStore.centres[4].editingCentreName = false;
    wrapper.vm.setCentreNameEditingStatusTrue();
    expect(wrapper.vm.centreStore.centres[4].editingCentreName).toStrictEqual(
      true
    );
  });

  it('updateCentreNameEditingStatus() does not change if nurse', () => {
    userStore.user.group = 'nurse';
    const centre = { editingCentreName: false };
    wrapper.vm.updateCentreNameEditingStatus(centre, true);
    expect(centre).toStrictEqual({ editingCentreName: false });
  });

  it('uniqueRule() returns error string when duplicate in array', () => {
    wrapper.vm.centreStore.centres = {
      2: { id: 2, name: 'Centre 2', setupComplete: true },
      3: { id: 3, name: 'Centre 3', setupComplete: true },
      4: { id: 4, name: 'Centre 4', setupComplete: true }
    };
    const ruleResponse = wrapper.vm.uniqueRule('Centre 3');
    expect(ruleResponse).toStrictEqual('Centre name must be unique');
  });

  it('uniqueRule() returns false when no centre exists', async () => {
    await wrapper.setProps({ selectedCentreId: null });
    wrapper.vm.centreStore.centres = {
      2: { id: 2, name: 'Centre 2', setupComplete: true },
      3: { id: 3, name: 'Centre 3', setupComplete: true },
      4: { id: 4, name: 'Centre 4', setupComplete: true }
    };
    const ruleResponse = wrapper.vm.uniqueRule('Centre 3');
    expect(ruleResponse).toStrictEqual(false);
  });

  it('uniqueRule() returns true when no duplicate in array', () => {
    wrapper.vm.centreStore.centres = {
      2: { id: 2, name: 'Centre 2', setupComplete: true },
      3: { id: 3, name: 'Centre 3', setupComplete: true },
      4: { id: 3, name: 'Centre 4', setupComplete: true }
    };
    const ruleResponse = wrapper.vm.uniqueRule('New Centre Name');
    expect(ruleResponse).toStrictEqual(true);
  });

  it('validateCentre() does not call updateCentre() if invalid', () => {
    const centre = { id: 2, name: 'Centre 2', nameValid: false };
    wrapper.vm.validateCentre(centre);
    expect(updateCentre).toHaveBeenCalledTimes(0);
  });

  it('validateCentre() calls updateCentreName() if valid', async () => {
    const updateCentreNameEditingStatus = vi.spyOn(
      wrapper.vm.context,
      'updateCentreNameEditingStatus'
    );
    userStore.user.group = 'admin';
    wrapper.vm.setCentreNameEditingStatusTrue();
    await nextTick();
    await wrapper.find('#centreHeaderNameInput').setValue('new unique name');
    await wrapper.vm.validateCentre();
    expect(updateCentre).toHaveBeenCalledWith(
      wrapper.vm.centreStore.centres[4]
    );
    expect(updateCentreNameEditingStatus).toHaveBeenCalledWith(false);
  });

  it('centre name rules handle empty value', async () => {
    userStore.user.group = 'admin';
    wrapper.vm.setCentreNameEditingStatusTrue();
    await nextTick();
    const centreNameInput = wrapper.find('#centreHeaderNameInput');
    await centreNameInput.setValue('');
    expect(
      wrapper.vm.centreNameRules[1](centreNameInput.element.value)
    ).toStrictEqual("Centre name can't be blank");
    await centreNameInput.setValue('test');
    expect(
      wrapper.vm.centreNameRules[1](centreNameInput.element.value)
    ).toStrictEqual(true);
  });
});
