import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import CreateCloseEventDialog from 'src/components/centres/calendar/CreateCloseEventDialog.vue';
import setup from 'tests/unit/functions/setup';
import { mockCentres } from 'tests/unit/mocks';
import { nextTick } from 'vue';
import { formatISO } from 'date-fns';
import type { SpyInstance } from 'vitest';
setup();

describe('CreateCloseEventDialog.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let httpPost: SpyInstance;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockCentres();
    wrapper = shallowMount(CreateCloseEventDialog, {
      props: {
        centreId: 2,
        propDate: '2021-04-04'
      }
    });
    httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
  });

  it('formattedStartDate gets formatted start date', () => {
    expect(wrapper.vm.formattedStartDate).toStrictEqual('04/04/21');
  });

  it('formattedEndDate gets formatted end date', () => {
    expect(wrapper.vm.formattedEndDate).toStrictEqual('04/04/21');
  });

  it('createEventValid is true when all values exist', () => {
    expect(wrapper.vm.createEventValid).toStrictEqual(false);
    wrapper.vm.closeReason = 'test';
    expect(wrapper.vm.createEventValid).toStrictEqual(true);
    wrapper.vm.closeReason = '';
    expect(wrapper.vm.createEventValid).toStrictEqual(false);
  });

  it('maxDate is startDate + 5 when weekly', () => {
    wrapper.vm.selectedRecurrenceOption = {
      label: 'Weekly',
      value: 'weekly'
    };
    expect(wrapper.vm.maxDate).toStrictEqual('2021/04/09');
  });

  it('maxDate is startDate + 20 when monthly', () => {
    wrapper.vm.selectedRecurrenceOption = {
      label: 'Monthly',
      value: 'monthly'
    };
    expect(wrapper.vm.maxDate).toStrictEqual('2021/04/24');
  });

  it('maxDate is startDate + 90 when yearly', async () => {
    wrapper.vm.endDate = '2023/04/04';
    wrapper.vm.selectedRecurrenceOption = {
      label: 'Yearly',
      value: 'yearly'
    };
    expect(wrapper.vm.maxDate).toStrictEqual('2021/07/03');
    await nextTick();
    expect(wrapper.vm.endDate).toStrictEqual('2021/07/03');
  });

  it('maxDate is way in future if no recurrence', () => {
    wrapper.vm.selectedRecurrenceOption = {
      label: 'Never',
      value: 'never'
    };
    expect(wrapper.vm.maxDate).toStrictEqual('2500/12/31');
  });

  it('moving start date past end date updates end date', async () => {
    wrapper.vm.endDate = '2015/04/04';
    wrapper.vm.startDate = '2016/04/04';
    await nextTick();
    expect(wrapper.vm.endDate).toStrictEqual('2016/04/04');
  });

  it('createEvent sends get request to server', async () => {
    httpPost.mockResolvedValueOnce(true);
    wrapper.vm.closeReason = 'example close reason';
    await wrapper.vm.createEvent();
    expect(httpPost).toHaveBeenCalledWith('events/', {
      allDay: true,
      centre: 2,
      eventType: 'User Close Event',
      name: 'example close reason',
      start: formatISO(new Date('2021-04-04T00:00')),
      timePeriod: 86400
    });
  });

  it('createEvent does nothing without propDate', async () => {
    await wrapper.setProps({ propDate: undefined });
    wrapper.vm.closeReason = 'example close reason';
    await wrapper.vm.createEvent();
    expect(httpPost).toHaveBeenCalledTimes(0);
  });

  it('createEvent sets recurrence options', async () => {
    httpPost.mockResolvedValueOnce(true);
    wrapper.vm.selectedRecurrenceOption = {
      label: 'Weekly',
      value: 'weekly'
    };
    await wrapper.vm.createEvent();
    expect(httpPost).toHaveBeenCalledWith('events/', {
      allDay: true,
      centre: 2,
      eventType: 'User Close Event',
      name: '',
      recurrences: 'weekly',
      start: formatISO(new Date('2021-04-04T00:00')),
      timePeriod: 86400
    });
  });

  it('createEvent handles setting day based recurrence', async () => {
    httpPost.mockResolvedValueOnce(true);
    wrapper.vm.selectedRecurrenceOption = {
      label: 'Monthly',
      value: 'monthly'
    };
    wrapper.vm.isDate = false;
    await wrapper.vm.createEvent();
    expect(httpPost).toHaveBeenCalledWith('events/', {
      allDay: true,
      centre: 2,
      eventType: 'User Close Event',
      name: '',
      recurrences: 'monthly',
      start: formatISO(new Date('2021-04-04T00:00')),
      recurrenceWeek: 1,
      timePeriod: 86400
    });
  });

  it('formattedWeekOfMonth returns week of month', () => {
    wrapper.vm.selectedWeekOfMonth = 3;
    expect(wrapper.vm.formattedWeekOfMonth).toStrictEqual('3rd');
  });

  it('formattedWeekOfMonth returns week of month', () => {
    wrapper.vm.selectedWeekOfMonth = 3;
    expect(wrapper.vm.formattedWeekOfMonth).toStrictEqual('3rd');
  });

  it('startDay returns the day startDate falls on', () => {
    wrapper.vm.startDate = '2023/01/08';
    expect(wrapper.vm.startDay).toStrictEqual('Sunday');
  });

  it('updating start date updates selectedWeekOfMonth', async () => {
    wrapper.vm.startDate = '2022/04/28';
    await nextTick();
    expect(wrapper.vm.selectedWeekOfMonth).toStrictEqual(4);
  });
});
