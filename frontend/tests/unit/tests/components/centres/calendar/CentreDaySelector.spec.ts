/* eslint-disable @typescript-eslint/no-explicit-any */
import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import CentreDaySelector from 'src/components/centres/calendar/CentreDaySelector.vue';
import setup from 'tests/unit/functions/setup';
import { mockCentres, mockClients } from 'tests/unit/mocks';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { useUserStore } from 'src/stores/userStore';

setup();

describe('CentreDaySelector.vue', () => {
  let wrapper: any;
  let userStore: any;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockCentres();
    mockClients();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    userStore = useUserStore();
    wrapper = shallowMount(CentreDaySelector, {
      props: {
        centreId: 1
      }
    });
  });

  it('closeDialog() closes dialog', async () => {
    wrapper.vm.showCentreClientDayWarningDialog = true;
    const updateDays = vi.spyOn(wrapper.vm.context, 'updateDays');
    await wrapper.vm.closeDialog(1);
    expect(updateDays).toHaveBeenCalledWith(1);
    expect(wrapper.vm.showCentreClientDayWarningDialog).toStrictEqual(false);
  });

  it('updateDays() sends a patch request to server', async () => {
    wrapper.vm.loaded = true;
    userStore.user.group = 'admin';
    wrapper.vm.selectedDays = [2, 3];
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce({});
    await wrapper.vm.updateDays(4);
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPatch).toHaveBeenCalledWith('centres/1/update/', {
      openingDays: [3, 4, 5]
    });
  });

  it('updateDays() does not call patch if not loaded', async () => {
    wrapper.vm.loaded = false;
    userStore.user.group = 'admin';
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    await wrapper.vm.updateDays();
    expect(httpPatch).toHaveBeenCalledTimes(0);
  });

  it('updateDays() does not call patch if not admin', async () => {
    wrapper.vm.loaded = true;
    userStore.user.group = 'nurse';
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    await wrapper.vm.updateDays();
    expect(httpPatch).toHaveBeenCalledTimes(0);
  });

  it('updateDays() does not call patch if not conflictingClients', async () => {
    const conflictingClients = vi.spyOn(
      wrapper.vm.context,
      'conflictingClients'
    );
    conflictingClients.mockReturnValueOnce(true);
    wrapper.vm.loaded = true;
    userStore.user.group = 'admin';
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    await wrapper.vm.updateDays();
    expect(httpPatch).toHaveBeenCalledTimes(0);
  });

  it('updateDays() calls axiosFailed() on failure', async () => {
    wrapper.vm.loaded = true;
    userStore.user.group = 'admin';
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockRejectedValueOnce({});
    await wrapper.vm.updateDays();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('getVisitingClients() creates list of clients visiting on a day', () => {
    wrapper.vm.clientStore.clients = {
      1: {
        visitingDays: [
          { day: 1, centre: 1 },
          { day: 3, centre: 1 },
          { day: 5, centre: 1 }
        ],
        isActive: true
      },
      2: {
        visitingDays: [
          { day: 1, centre: 1 },
          { day: 2, centre: 1 }
        ],
        isActive: true
      },
      3: { visitingDays: [{ day: 3, centre: 1 }], isActive: true },
      4: { visitingDays: [{ day: 6, centre: 1 }], isActive: true },
      5: {
        visitingDays: [
          { day: 1, centre: 1 },
          { day: 3, centre: 1 },
          { day: 5, centre: 1 }
        ],
        isActive: false
      }
    };
    const visitingClients = wrapper.vm.getVisitingClients(2);
    expect(visitingClients).toStrictEqual([1, 3]);
  });

  it('conflictingClients() returns true if clients visit on day', () => {
    wrapper.vm.selectedDays = [2, 3];
    wrapper.vm.clientStore.clients = {
      1: {
        visitingDays: [
          { day: 1, centre: 1 },
          { day: 3, centre: 1 },
          { day: 5, centre: 1 }
        ],
        isActive: true
      },
      2: {
        visitingDays: [
          { day: 1, centre: 1 },
          { day: 2, centre: 1 }
        ],
        isActive: true
      },
      3: { visitingDays: [{ day: 3, centre: 1 }], isActive: true },
      4: { visitingDays: [{ day: 6, centre: 1 }], isActive: true },
      5: {
        visitingDays: [
          { day: 1, centre: 1 },
          { day: 3, centre: 1 },
          { day: 5, centre: 1 }
        ],
        isActive: false
      }
    };
    const conflictingClients = wrapper.vm.conflictingClients(2);
    expect(conflictingClients).toStrictEqual(true);
    expect(wrapper.vm.dayConflictDay).toStrictEqual(2);
    expect(wrapper.vm.dayConflictClients).toStrictEqual([1, 3]);
    expect(wrapper.vm.showCentreClientDayWarningDialog).toStrictEqual(true);
  });

  it('conflictingClients() returns false if selectedDays has no day', () => {
    wrapper.vm.selectedDays = [3];
    const conflictingClients = wrapper.vm.conflictingClients(2);
    expect(conflictingClients).toStrictEqual(false);
    expect(wrapper.vm.selectedDays).toStrictEqual([3, 2]);
  });

  it('conflictingClients() returns false if no visitingClients', () => {
    wrapper.vm.selectedDays = [2, 3];
    wrapper.vm.clientStore.clients = {};
    const conflictingClients = wrapper.vm.conflictingClients(2);
    expect(conflictingClients).toStrictEqual(false);
    expect(wrapper.vm.selectedDays).toStrictEqual([3]);
  });

  it('updateSelectedDays() does not update selectedDays if no days length', () => {
    wrapper.vm.centreStore.centres[1] = { openingDays: [] };
    expect(wrapper.vm.selectedDays).toStrictEqual([1, 3, 5]);
  });

  it('updateSelectedDays() updates selectedDays if days length', async () => {
    wrapper.vm.centreStore.centres[1] = { openingDays: [1, 5, 6] };
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.selectedDays).toStrictEqual([0, 4, 5]);
  });

  it('days() is an empty array if centreId is undefined', async () => {
    await wrapper.setProps({ centreId: null });
    expect(wrapper.vm.days).toStrictEqual([]);
  });

  it('setCentreClientDayWarningDialog shows or hides dialog', () => {
    wrapper.vm.setCentreClientDayWarningDialog(false);
    expect(wrapper.vm.showCentreClientDayWarningDialog).toStrictEqual(false);

    wrapper.vm.setCentreClientDayWarningDialog(true);
    expect(wrapper.vm.showCentreClientDayWarningDialog).toStrictEqual(true);
  });
});
