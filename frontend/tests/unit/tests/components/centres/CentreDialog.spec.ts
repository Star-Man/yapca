import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import CentreDialog from 'src/components/centres/CentreDialog.vue';
import setup from 'tests/unit/functions/setup';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { Notify } from 'quasar';
import updateCentre from 'src/functions/centre/updateCentre';
import { mockCentres } from '../../../mocks';
import type { SpyInstance } from 'vitest';

setup();

describe('CentreDialog.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let notifySpy: SpyInstance;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    vi.mock('src/functions/centre/updateCentre');

    mockCentres();
    wrapper = shallowMount(CentreDialog, {
      props: {
        centreId: 4
      }
    });
    notifySpy = vi.spyOn(Notify, 'create');
  });

  it('subscribedLabel() returns sub text if user is subbed to centre', async () => {
    wrapper.vm.userStore.user.centres = [1, 3, 4];
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.subscribed).toStrictEqual(true);
    expect(wrapper.vm.subscribedLabel).toStrictEqual(
      'You are subscribed to this centre'
    );
  });

  it('subscribed() returns true if user is not subbed to centre', () => {
    wrapper.vm.userStore.user.centres = [1, 3, 4];
    expect(wrapper.vm.subscribed).toStrictEqual(true);
  });

  it('subscribed() returns false if user is not subbed to centre', () => {
    wrapper.vm.userStore.user.centres = [2, 3];
    expect(wrapper.vm.subscribed).toStrictEqual(false);
  });

  it('subscribed() returns false if user does not exist', async () => {
    delete wrapper.vm.userStore.user.centres;
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.subscribed).toStrictEqual(false);
  });

  it('closeDialog() closes dialog', () => {
    wrapper.vm.showDeleteCentreConfirmDialog = true;
    wrapper.vm.closeDialog();
    expect(wrapper.vm.showDeleteCentreConfirmDialog).toStrictEqual(false);
  });

  it('subscribedLabel() returns not subbed text if user not subbed', () => {
    wrapper.vm.userStore.user.centres = [2, 3];
    expect(wrapper.vm.subscribedLabel).toStrictEqual(
      'You are not subscribed to this centre'
    );
  });

  it('changeSubscription() sends post request to server when adding', async () => {
    wrapper.vm.userStore.user.id = 5;
    wrapper.vm.userStore.user.group = 'admin';
    wrapper.vm.userStore.user.centres = [2, 3, 4];
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockResolvedValueOnce({});
    await wrapper.vm.changeSubscription(6, true);
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPost).toHaveBeenCalledWith('users/centres/update/', {
      centre: 6,
      user: 5
    });
  });

  it('changeSubscription() sends delete request to server when removing', async () => {
    wrapper.vm.userStore.user.id = 8;
    wrapper.vm.userStore.user.group = 'admin';
    wrapper.vm.userStore.user.centres = [2, 3, 4];
    const httpDelete = vi.spyOn(wrapper.vm.basicStore.http, 'delete');
    httpDelete.mockResolvedValueOnce({});
    await wrapper.vm.changeSubscription(2, false);
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpDelete).toHaveBeenCalledWith('users/centres/update/', {
      data: {
        centre: 2,
        user: 8
      }
    });
  });

  it('changeSubscription() calls axiosFailed() on failure', async () => {
    wrapper.vm.userStore.user.id = 5;
    wrapper.vm.userStore.user.group = 'admin';
    wrapper.vm.userStore.user.centres = [1, 3, 4];
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockRejectedValueOnce({});
    await wrapper.vm.changeSubscription(3, true);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('validateChangeSubscription() shows error when remove last centre', () => {
    wrapper.vm.userStore.user.centres = [3];
    const changeSubscription = vi.spyOn(wrapper.vm, 'changeSubscription');
    wrapper.vm.validateChangeSubscription(false);
    expect(notifySpy).toHaveBeenCalledWith({
      message: 'You must be subscribed to at least one centre',
      type: 'info'
    });
    expect(changeSubscription).toHaveBeenCalledTimes(0);
  });

  it('validateChangeSubscription() calls changeSubscription', () => {
    wrapper.vm.userStore.user.centres = [1, 3, 4];
    const changeSubscription = vi.spyOn(
      wrapper.vm.context,
      'changeSubscription'
    );
    wrapper.vm.validateChangeSubscription(false);
    expect(changeSubscription).toHaveBeenCalledWith(4, false);
    wrapper.vm.validateChangeSubscription(true);
    expect(changeSubscription).toHaveBeenCalledWith(4, true);
  });

  it('updateSelectedColor() calls updateCentre', () => {
    wrapper.vm.userStore.user.centres = [1, 3, 4];
    wrapper.vm.updateSelectedColor('#FFFFFF', 4);
    expect(updateCentre).toHaveBeenCalledWith({ id: 4, color: '#FFFFFF' });
  });

  it('centre() is null when no centreId', async () => {
    await wrapper.setProps({ centreId: null });
    expect(wrapper.vm.centre).toStrictEqual(null);
  });

  it('totalCentres() returns total centres', () => {
    expect(wrapper.vm.totalCentres).toStrictEqual(10);
  });

  it('setDeleteCentreConfirmDialog() can set modal', () => {
    wrapper.vm.setDeleteCentreConfirmDialog(false);
    expect(wrapper.vm.showDeleteCentreConfirmDialog).toStrictEqual(false);
    wrapper.vm.setDeleteCentreConfirmDialog(true);
    expect(wrapper.vm.showDeleteCentreConfirmDialog).toStrictEqual(true);
  });
});
