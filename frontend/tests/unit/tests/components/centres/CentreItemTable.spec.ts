import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import CentreItemTable from 'src/components/centres/CentreItemTable.vue';
import setup from 'tests/unit/functions/setup';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import getCentreRelatedData from 'src/functions/centre/getCentreRelatedData';
import { mockCentres } from '../../../mocks';

setup();

describe('CentreItemTable.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    vi.mock('src/functions/centre/getCentreRelatedData', () => ({
      default: vi.fn(() => Promise.resolve())
    }));
    mockCentres();
    wrapper = shallowMount(CentreItemTable, {
      props: {
        centreId: 3,
        itemName: 'need',
        itemPlural: 'needs'
      }
    });
  });

  it('openItemCreationDialog() does that', () => {
    wrapper.vm.newItemDescription = 'test';
    wrapper.vm.showCreateItemDialog = false;
    wrapper.vm.openItemCreationDialog();
    expect(wrapper.vm.newItemDescription).toStrictEqual('');
    expect(wrapper.vm.showCreateItemDialog).toStrictEqual(true);
  });

  it('items() returns empty array if no centre items in store', () => {
    wrapper.vm.centreStore.centres['3'].needs = [];
    expect(wrapper.vm.items).toStrictEqual([]);
  });

  it('items() returns store items if it exists', () => {
    expect(wrapper.vm.items).toStrictEqual([
      { id: 1, description: 'Test Need 1', valid: true },
      { id: 3, description: 'Test Need 3', valid: true },
      { id: 2, description: 'Test Need 4', valid: true }
    ]);
  });

  it('deleteItem() removes item from array by index number and updates', async () => {
    const updateCentre = vi.spyOn(wrapper.vm.context, 'updateCentre');
    await wrapper.vm.deleteItem({
      id: 2,
      description: 'Test Need 2',
      valid: true
    });
    expect(updateCentre).toHaveBeenCalledTimes(1);
    expect(updateCentre).toHaveBeenCalledWith([
      { id: 1, description: 'Test Need 1', valid: true },
      { id: 3, description: 'Test Need 3', valid: true }
    ]);
  });

  it('upsertItem() sends patch when passed item has id', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce({});
    await wrapper.vm.upsertItem({
      id: 8,
      description: 'test description'
    });
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPatch).toHaveBeenCalledWith('needs/8/update/', {
      description: 'test description'
    });
  });

  it('upsertItem() sends post when passed item has no id', async () => {
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockResolvedValueOnce({
      data: [{ id: 9, description: 'test' }]
    });
    wrapper.vm.context.updateCentre = vi.fn();
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    await wrapper.vm.upsertItem({ description: 'test description' });

    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPost).toHaveBeenCalledWith('needs/', [
      { description: 'test description' }
    ]);
  });

  it('upsertItem() calls axiosFailed() on failure with id', async () => {
    wrapper.vm.context.updateCentre = vi.fn();
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockRejectedValueOnce({});
    await wrapper.vm.upsertItem({
      id: 2,
      description: 'test description'
    });
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('upsertItem() calls axiosFailed() on failure without id', async () => {
    wrapper.vm.context.updateCentre = vi.fn();
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockRejectedValueOnce({});
    await wrapper.vm.upsertItem({ description: 'test description' });
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('upsertItem() does nothing if unique rule fails for creation', async () => {
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPost.mockRejectedValue(true);
    httpPatch.mockRejectedValue(true);
    await wrapper.vm.upsertItem({ description: 'Test Need 3' });
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPost).toHaveBeenCalledTimes(0);
    expect(httpPatch).toHaveBeenCalledTimes(0);
  });

  it('upsertItem() does nothing if unique rule fails for editing', async () => {
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPost.mockRejectedValue(true);
    httpPatch.mockRejectedValue(true);
    wrapper.vm.items.push({ id: 7, description: 'Test Need 3' });
    await wrapper.vm.updateItemDescription(7, 'Test Need 3');
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPost).toHaveBeenCalledTimes(0);
    expect(httpPatch).toHaveBeenCalledTimes(0);
  });

  it('updateCentre() sends patch to server with passed items', async () => {
    const items = [
      { id: 1, description: 'Test Item 1' },
      { description: 'Test Item 2' },
      { id: 3, description: 'Test Item 3' }
    ];
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce({});
    await wrapper.vm.updateCentre(items);
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPatch).toHaveBeenCalledWith('centres/3/update/', {
      needs: [1, 3]
    });
  });

  it('updateCentre() calls axiosFailed() on failure', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockRejectedValueOnce({});
    await wrapper.vm.updateCentre([
      {
        id: 2,
        description: 'test description'
      }
    ]);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('addNewCentreItem() calls updateCentre() with item array', async () => {
    const item = { id: 6, description: 'Test Item 6' };
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce({});
    await wrapper.vm.addNewCentreItem(item);
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPatch).toHaveBeenCalledWith('centres/3/update/', {
      needs: [1, 3, 2, 6]
    });
  });

  it('validate returns error string when duplicate in array', () => {
    const validation = wrapper.vm.validate('Test Need 1');
    expect(validation).toStrictEqual(false);
  });

  it('validate returns error string when empty', () => {
    const validation = wrapper.vm.validate('');
    expect(validation).toStrictEqual(false);
  });

  it('validate returns true when value exists and unique', () => {
    const validation = wrapper.vm.validate('Test Need 100');
    expect(validation).toStrictEqual(true);
  });

  it('uniqueRule() returns true when no duplicate in array', () => {
    const ruleResponse = wrapper.vm.uniqueRule('example', [
      { description: 'example' }
    ]);
    expect(ruleResponse).toStrictEqual(true);
  });

  it('itemNames() returns variations of item names', async () => {
    expect(wrapper.vm.itemNames).toStrictEqual({
      capital: 'Need',
      lowercase: 'need',
      pluralCaptial: 'Needs',
      plural: 'needs'
    });
    await wrapper.setProps({ itemPlural: 'policies', itemName: 'policy' });
    expect(wrapper.vm.itemNames).toStrictEqual({
      capital: 'Policy',
      lowercase: 'policy',
      pluralCaptial: 'Policies',
      plural: 'policies'
    });
  });

  it('updateCentre() does not send patch to server when no centreId', async () => {
    wrapper.vm.getCentreRelatedData = vi.fn();
    await wrapper.setProps({
      itemName: 'need',
      centreId: null
    });
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    wrapper.vm.updateCentre([]);
    expect(httpPatch).toHaveBeenCalledTimes(0);
  });

  it('getCentreRelatedData() is called on load', async () => {
    expect(getCentreRelatedData).toHaveBeenCalledTimes(1);
    await wrapper.vm.created();
    expect(getCentreRelatedData).toHaveBeenCalledTimes(2);
  });

  it('getCentreRelatedData() is not called on load when no centreId', async () => {
    await wrapper.setProps({ centreId: null });
    expect(getCentreRelatedData).toHaveBeenCalledTimes(1);
    await wrapper.vm.created();
    expect(getCentreRelatedData).toHaveBeenCalledTimes(1);
  });

  it('items is empty if centreId not in store', async () => {
    await wrapper.setProps({ centreId: 600 });
    expect(wrapper.vm.items).toStrictEqual([]);
  });

  it('setShowCreateItemDialog() can set dialog', () => {
    wrapper.vm.setShowCreateItemDialog(false);
    expect(wrapper.vm.showCreateItemDialog).toStrictEqual(false);
    wrapper.vm.setShowCreateItemDialog(true);
    expect(wrapper.vm.showCreateItemDialog).toStrictEqual(true);
  });
});
