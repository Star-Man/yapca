import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import CentreMatchingColorDialog from 'src/components/centres/CentreMatchingColorDialog.vue';
import setup from 'tests/unit/functions/setup';
import type { SpyInstance } from 'vitest';

setup();

describe('CentreMatchingColorDialog.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let emit: SpyInstance;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed');
    wrapper = shallowMount(CentreMatchingColorDialog, {
      props: {
        centres: ['Centre 1']
      }
    });
    emit = vi.spyOn(wrapper.vm.context, 'emit');
  });

  it('centrePlural() returns singular words when one centre exists', () => {
    expect(wrapper.vm.centrePlural).toStrictEqual(['centre', 'has']);
  });

  it('centrePlural() returns plural words when multiple centres', async () => {
    await wrapper.setProps({ centres: ['Centre 1', 'Centre 2'] });
    expect(wrapper.vm.centrePlural).toStrictEqual(['centres', 'have']);
  });

  it('closeDialog does that', () => {
    wrapper.vm.closeDialog();
    expect(emit).toHaveBeenCalledWith('close-dialog');
  });

  it('updateColor does that', () => {
    wrapper.vm.updateColor();
    expect(emit).toHaveBeenCalledWith('update');
  });
});
