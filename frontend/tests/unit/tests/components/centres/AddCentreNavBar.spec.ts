import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import AddCentreNavBar from 'src/components/centres/AddCentreNavBar.vue';
import setup from 'tests/unit/functions/setup';
import { nextTick } from 'vue';
import { Screen } from 'quasar';
import { mockCentres } from '../../../mocks';

setup();

describe('AddCentreNavBar.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockCentres();
    vi.mock('src/functions/proxy/AxiosFailed');
    wrapper = shallowMount(AddCentreNavBar);
    Screen.width = 400;
    wrapper.vm.centreStore.centres = {
      '4': {
        id: 4,
        name: 'Test Centre 4',
        created: 'before',
        openingDaysFirstSetupComplete: true,
        policies: [],
        policiesFirstSetupComplete: true,
        needs: [],
        needsFirstSetupComplete: true,
        setupComplete: true,
        openingDays: [2, 4, 6],
        editingCentreName: false,
        nameValid: false,
        color: '#FFFFFF',
        closedOnPublicHolidays: true,
        country: 'AU',
        state: 'QLD'
      },
      '5': {
        id: 5,
        name: 'Test Centre 5',
        created: 'before',
        openingDaysFirstSetupComplete: true,
        policies: [],
        policiesFirstSetupComplete: true,
        needs: [],
        needsFirstSetupComplete: true,
        setupComplete: false,
        openingDays: [2, 4, 6],
        editingCentreName: false,
        nameValid: false,
        color: '#FFFFFF',
        closedOnPublicHolidays: true,
        country: 'AU',
        state: 'QLD'
      },
      '6': {
        id: 6,
        name: 'Test Centre 6',
        created: 'before',
        openingDaysFirstSetupComplete: true,
        policies: [],
        policiesFirstSetupComplete: true,
        needs: [],
        needsFirstSetupComplete: true,
        setupComplete: false,
        openingDays: [2, 4, 6],
        editingCentreName: false,
        nameValid: false,
        color: '#FFFFFF',
        closedOnPublicHolidays: true,
        country: 'AU',
        state: 'QLD'
      }
    };
  });

  it('closeDialog() closes dialog', () => {
    wrapper.vm.showDeleteCentreConfirmDialog = true;
    wrapper.vm.closeDialog();
    expect(wrapper.vm.showDeleteCentreConfirmDialog).toStrictEqual(false);
  });

  it('incompleteCentres() returns incomplete store centres', () => {
    expect(wrapper.vm.incompleteCentres.length).toStrictEqual(2);
    expect(wrapper.vm.incompleteCentres[0].id).toStrictEqual(5);
    expect(wrapper.vm.incompleteCentres[1].id).toStrictEqual(6);
  });

  it('goToCentreSetup() calls SETUP_CENTRE store function with passed centre', async () => {
    const setSetupCentre = vi.spyOn(wrapper.vm.centreStore, 'setSetupCentre');
    await wrapper.vm.goToCentreSetup({ id: 8 });
    expect(setSetupCentre).toHaveBeenCalledWith({ id: 8 });
  });

  it('goToCentreSetup() calls router push', async () => {
    const routerPush = vi.spyOn(wrapper.vm.router, 'push');
    routerPush.mockResolvedValueOnce(true);
    await wrapper.vm.goToCentreSetup({ id: 8 });
    expect(routerPush).toHaveBeenCalledWith({
      name: 'centreSetup',
      query: { nextUrl: '/' }
    });
  });

  it('sets nav bar clip when not xs', async () => {
    expect(wrapper.vm.clipClass).toStrictEqual('');
    Screen.width = 800;
    await nextTick();
    expect(wrapper.vm.clipClass).toStrictEqual('sidebarPush');
  });

  it('setDeleteCentreConfirmDialog() can set centre confim dialog', () => {
    wrapper.vm.showDeleteCentreConfirmDialog = true;
    wrapper.vm.setDeleteCentreConfirmDialog(false);
    expect(wrapper.vm.showDeleteCentreConfirmDialog).toStrictEqual(false);
    wrapper.vm.setDeleteCentreConfirmDialog(true);
    expect(wrapper.vm.showDeleteCentreConfirmDialog).toStrictEqual(true);
  });
});
