import { mount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import OtpInputModal from 'src/components/auth/OtpInputModal.vue';
import setup from 'tests/unit/functions/setup';
import { nextTick } from 'vue';
import type { SpyInstance } from 'vitest';

setup();

describe('OtpInputModal.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let emit: SpyInstance;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed');
    wrapper = mount(OtpInputModal);
    emit = vi.spyOn(wrapper.vm.context, 'emit');
  });

  it('updating otpCode resets otp errors', () => {
    wrapper.vm.updateOtpCode(['2', '3', '4']);
    expect(emit).toHaveBeenCalledWith('reset-otp-errors');
  });

  it('updating calls set-otp-code if finished', () => {
    wrapper.vm.resetOtpInput = 0;
    wrapper.vm.updateOtpCode(['2', '3', '4']);
    expect(emit).not.toHaveBeenCalledWith('set-otp-code', '3333');
    expect(wrapper.vm.resetOtpInput).toStrictEqual(0);

    wrapper.vm.updateOtpCode(['2', '3', '4', '5', '6', '7']);
    expect(emit).toHaveBeenCalledWith('set-otp-code', '234567');
    expect(wrapper.vm.resetOtpInput).toStrictEqual(1);
  });

  it('closeDialog does that', () => {
    wrapper.vm.closeDialog();
    expect(emit).toHaveBeenCalledWith('close-dialog');
  });

  it('goToOtpTempCodePage does that', () => {
    wrapper.vm.goToOtpTempCodePage();
    expect(emit).toHaveBeenCalledWith('otp-reset-temp-code');
  });

  it('otpIncorrectMessage is only visible when errors exist', async () => {
    expect(wrapper.find('#otpIncorrectMessage').exists()).toStrictEqual(false);
    await wrapper.setProps({ otpErrors: ['example error '] });
    await nextTick();
    expect(wrapper.find('#otpIncorrectMessage').exists()).toStrictEqual(true);
  });
});
