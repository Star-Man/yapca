import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import OtpInput from 'src/components/auth/OtpInput.vue';
import setup from 'tests/unit/functions/setup';
import type { DOMWrapper } from '@vue/test-utils';

setup();

describe('OtpInput.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  const inputs = {} as Record<string, DOMWrapper<HTMLInputElement>>;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed');
    wrapper = shallowMount(OtpInput);

    Array.from({ length: 6 }).forEach((_, index: number) => {
      inputs[index] = wrapper.find(`#otpInput--${index}`);
    });
  });

  it('updating resetInput prop resets inputs', async () => {
    await inputs[3].setValue('3');
    await wrapper.setProps({ resetInput: 9 });
    expect(inputs[3].element.value).toStrictEqual('');
  });

  it('isNumber checks element input if number', () => {
    const inputEvent = {
      currentTarget: { value: '' },
      key: '7',
      preventDefault: vi.fn()
    };
    wrapper.vm.isNumber(inputEvent);
    expect(inputEvent.preventDefault).toHaveBeenCalledTimes(0);

    inputEvent.key = 'a';
    wrapper.vm.isNumber(inputEvent);
    expect(inputEvent.preventDefault).toHaveBeenCalledTimes(1);
  });

  it('onWheel prevents default', () => {
    const wheelEvent = {
      preventDefault: vi.fn()
    };
    wrapper.vm.onWheel(wheelEvent);
    expect(wheelEvent.preventDefault).toHaveBeenCalledTimes(1);
  });

  it('handleInput focuses on next input', () => {
    const inputEvent = {
      inputType: 'insertText',
      target: {
        nextElementSibling: {
          focus: vi.fn()
        }
      }
    };
    wrapper.vm.handleInput(inputEvent);
    expect(inputEvent.target.nextElementSibling.focus).toHaveBeenCalledTimes(1);
  });

  it('handleInput can set all inputs when pasted', () => {
    const inputEvent = {
      inputType: 'insertFromPaste',
      target: {
        nextElementSibling: {
          focus: vi.fn(),
          id: 'otpInput--2',
          nextElementSibling: {
            focus: vi.fn(),
            id: 'otpInput--3',
            nextElementSibling: {
              focus: vi.fn(),
              id: 'otpInput--4',
              nextElementSibling: {
                focus: vi.fn(),
                id: 'otpInput--5',
                nextElementSibling: {
                  focus: vi.fn(),
                  id: 'otpInput--6'
                }
              }
            }
          }
        },
        id: 'otpInput--1'
      }
    };
    wrapper.vm.dataFromPaste = ['2', '3', '4', '5', '6', '7'];
    wrapper.vm.handleInput(inputEvent);
    expect(
      inputEvent.target.nextElementSibling.nextElementSibling.focus
    ).toHaveBeenCalledTimes(1);
    expect(
      inputEvent.target.nextElementSibling.nextElementSibling.nextElementSibling
        .focus
    ).toHaveBeenCalledTimes(1);
    expect(
      inputEvent.target.nextElementSibling.nextElementSibling.nextElementSibling
        .nextElementSibling.focus
    ).toHaveBeenCalledTimes(2);
  });

  it('handleDelete focuses on previous element if no value', () => {
    const inputEvent = {
      target: {
        value: '',
        previousElementSibling: {
          focus: vi.fn()
        }
      }
    };
    wrapper.vm.handleDelete(inputEvent);
    expect(
      inputEvent.target.previousElementSibling.focus
    ).toHaveBeenCalledTimes(1);
  });

  it('handleDelete does not focus on previous element if value', () => {
    const inputEvent = {
      target: {
        value: '3',
        previousElementSibling: {
          focus: vi.fn()
        }
      }
    };
    wrapper.vm.handleDelete(inputEvent);
    expect(
      inputEvent.target.previousElementSibling.focus
    ).toHaveBeenCalledTimes(0);
  });

  it('onPaste trims pasted clipboard data', () => {
    const inputEvent = {
      clipboardData: {
        getData: () => '372942'
      },
      preventDefault: vi.fn()
    };
    wrapper.vm.onPaste(inputEvent);
    expect(wrapper.vm.dataFromPaste).toStrictEqual([
      '3',
      '7',
      '2',
      '9',
      '4',
      '2'
    ]);
    expect(inputEvent.preventDefault).toHaveBeenCalledTimes(0);
  });

  it('onPaste prevents default if not number', () => {
    const inputEvent = {
      clipboardData: {
        getData: () => '37a942'
      },
      preventDefault: vi.fn()
    };
    wrapper.vm.onPaste(inputEvent);
    expect(wrapper.vm.dataFromPaste).toStrictEqual([
      '3',
      '7',
      'a',
      '9',
      '4',
      '2'
    ]);
    expect(inputEvent.preventDefault).toHaveBeenCalledTimes(1);
  });

  it('input class adjusts to quasar dark setting', () => {
    wrapper.vm.$q.dark.isActive = true;
    expect(wrapper.vm.inputClass).toStrictEqual('dark-number-input');
    wrapper.vm.$q.dark.isActive = false;
    expect(wrapper.vm.inputClass).toStrictEqual('light-number-input');
  });
});
