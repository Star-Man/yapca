import { mount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import ForgotPasswordModal from 'src/components/auth/ForgotPasswordModal.vue';
import setup from 'tests/unit/functions/setup';
import { Notify } from 'quasar';
import { nextTick } from 'vue';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import type { SpyInstance } from 'vitest';

setup();

describe('ForgotPasswordModal.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let notifySpy: SpyInstance;
  let httpGet: SpyInstance;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    wrapper = mount(ForgotPasswordModal);
    notifySpy = vi.spyOn(Notify, 'create');
    httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
  });

  it('updateEmailErrors resets errors on change', async () => {
    wrapper.vm.emailErrors = ['example errors'];
    const emailInput = wrapper.find('#forgotEmailInput');
    await emailInput.setValue('newEmail');
    await nextTick();
    expect(wrapper.vm.emailErrors).toStrictEqual([]);
  });

  it('closeDialog does that', () => {
    const emit = vi.spyOn(wrapper.vm.context, 'emit');
    wrapper.vm.closeDialog();
    expect(emit).toHaveBeenCalledWith('close-dialog');
    expect(wrapper.vm.email).toStrictEqual('');
  });

  it('validate() calls resetRequest on success', async () => {
    const emailInput = wrapper.find('#forgotEmailInput');
    await emailInput.setValue('test@test.test');
    await nextTick();
    wrapper.vm.context.requestPasswordResetEmail = vi.fn();
    wrapper.vm.validate();
    expect(wrapper.vm.context.requestPasswordResetEmail).toHaveBeenCalledTimes(
      1
    );
  });

  it('validate() does not call resetRequest on fail', async () => {
    wrapper.vm.email = 'testtest.test';
    await nextTick();
    wrapper.vm.requestPasswordResetEmail = vi.fn();
    wrapper.vm.validate();
    expect(wrapper.vm.requestPasswordResetEmail).toHaveBeenCalledTimes(0);
  });

  it('requestPasswordResetEmail sends get request', async () => {
    const emit = vi.spyOn(wrapper.vm.context, 'emit');
    httpGet.mockResolvedValueOnce(true);
    await wrapper.vm.requestPasswordResetEmail();
    expect(emit).toHaveBeenCalledWith('go-to-temp-code-page');
    expect(wrapper.vm.email).toStrictEqual('');
    expect(notifySpy).toHaveBeenCalledWith({
      message: 'Request submitted successfully, check your inbox',
      type: 'positive'
    });
  });

  it('requestPasswordResetEmail calls axiosFailed on fail', async () => {
    httpGet.mockRejectedValueOnce(false);
    wrapper.vm.closeDialog = vi.fn();
    await wrapper.vm.requestPasswordResetEmail();
    expect(wrapper.vm.closeDialog).toHaveBeenCalledTimes(0);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });
});
