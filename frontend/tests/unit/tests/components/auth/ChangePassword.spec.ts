import { mount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import ChangePassword from 'src/components/auth/ChangePassword.vue';
import setup from 'tests/unit/functions/setup';
import { Notify } from 'quasar';
import { nextTick } from 'vue';
import axiosFailed from '@/functions/proxy/AxiosFailed';
import type { SpyInstance } from 'vitest';

setup();

describe('ChangePassword.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let notifySpy: SpyInstance;
  let httpPut: SpyInstance;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    wrapper = mount(ChangePassword);
    notifySpy = vi.spyOn(Notify, 'create');
    httpPut = vi.spyOn(wrapper.vm.basicStore.http, 'put');
    vi.mock('src/functions/proxy/AxiosFailed');
    vi.spyOn(console, 'warn').mockImplementation(() => undefined);
  });

  it('changing password clears existing errors', async () => {
    wrapper.vm.newPasswordErrors = [1, 2, 3];
    wrapper.vm.oldPasswordErrors = [4, 5, 6];
    await wrapper.find('#confirmPasswordInput').setValue('newValue');
    await nextTick();
    expect(wrapper.vm.newPasswordErrors).toStrictEqual([]);
    expect(wrapper.vm.oldPasswordErrors).toStrictEqual([]);
  });

  it('test passwordRules()', async () => {
    expect(wrapper.vm.passwordRules).toStrictEqual([]);
    await wrapper.find('#confirmPasswordInput').setValue('test');
    expect(typeof wrapper.vm.passwordRules).toStrictEqual('object');
    expect(typeof wrapper.vm.passwordRules[0]).toStrictEqual('function');
    expect(wrapper.vm.passwordRules[0]()).toStrictEqual('Values do not match');
  });

  it('test changePassword()', async () => {
    const changePasswordSuccess = vi.spyOn(
      wrapper.vm.context,
      'changePasswordSuccess'
    );
    const changePasswordFailed = vi.spyOn(
      wrapper.vm.context,
      'changePasswordFailed'
    );
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    await wrapper.find('#passwordInput').setValue('newPassword');
    await wrapper.find('#oldPasswordInput').setValue('oldPassword');
    httpPost.mockRejectedValueOnce('Example Error');
    await wrapper.vm.changePassword();
    expect(httpPost).toHaveBeenCalledWith('change-password/', {
      new_password1: 'newPassword',
      new_password2: 'newPassword',
      old_password: 'oldPassword'
    });
    expect(changePasswordFailed).toHaveBeenCalledWith('Example Error');
    expect(changePasswordSuccess).toHaveBeenCalledTimes(0);

    wrapper.vm.context.changePasswordSuccess.mockResolvedValue();
    httpPost.mockResolvedValueOnce({ data: {} });
    await wrapper.vm.changePassword();
    expect(changePasswordFailed).toHaveBeenCalledTimes(1);
    expect(changePasswordSuccess).toHaveBeenCalledTimes(1);
  });

  it('changePassword() as temp code', async () => {
    await wrapper.setProps({
      tempCode: 'Y789B629G897R236F7U24F23F223R33S',
      username: 'user1'
    });
    wrapper.vm.newPassword = 'password12345';
    const changePasswordFailed = vi.spyOn(
      wrapper.vm.context,
      'changePasswordFailed'
    );
    wrapper.vm.context.changePasswordSuccess = vi.fn();
    httpPut.mockResolvedValueOnce(true);
    await wrapper.vm.changePassword();
    expect(httpPut).toHaveBeenCalledWith(
      'temp-code/reset-password/',
      {
        username: 'user1',
        password: 'password12345',
        code: 'Y789B629G897R236F7U24F23F223R33S'
      },
      {
        headers: {
          Authorization: ''
        }
      }
    );
    expect(changePasswordFailed).toHaveBeenCalledTimes(0);
    expect(wrapper.vm.context.changePasswordSuccess).toHaveBeenCalledTimes(1);
  });

  it('changePasswordSuccess() resets inputs', () => {
    wrapper.vm.oldPassword = 'test';
    wrapper.vm.newPassword = 'test';
    wrapper.vm.confirmPassword = 'test';
    wrapper.vm.changePasswordSuccess();
    expect(wrapper.vm.oldPassword).toStrictEqual('');
    expect(wrapper.vm.newPassword).toStrictEqual('');
    expect(wrapper.vm.confirmPassword).toStrictEqual('');
  });

  it('changePasswordSuccess() calls snackbar', () => {
    wrapper.vm.changePasswordSuccess();
    expect(notifySpy).toHaveBeenCalledWith({
      message: 'Password Changed!',
      type: 'positive'
    });
  });

  it('clicking old password eye icon changes password type', async () => {
    const oldPasswordInput = wrapper.find('#oldPasswordInput');
    const oldPasswordEyeIcon = wrapper.find('#oldPasswordEyeIcon');
    expect(wrapper.vm.showOldPassword).toStrictEqual(false);
    expect(oldPasswordInput.attributes().type).toStrictEqual('password');

    await oldPasswordEyeIcon.trigger('click');
    expect(wrapper.vm.showOldPassword).toStrictEqual(true);
    expect(oldPasswordInput.attributes().type).toStrictEqual('text');
  });

  it('clicking new password eye icon changes password type', async () => {
    const newPasswordInput = wrapper.find('#passwordInput');
    const newPasswordEyeIcon = wrapper.find('#newPasswordEyeIcon');
    expect(wrapper.vm.showNewPassword).toStrictEqual(false);
    expect(newPasswordInput.attributes().type).toStrictEqual('password');

    await newPasswordEyeIcon.trigger('click');
    expect(wrapper.vm.showNewPassword).toStrictEqual(true);
    expect(newPasswordInput.attributes().type).toStrictEqual('text');
  });

  it('clicking confirm password eye icon changes password type', async () => {
    const confirmPasswordInput = wrapper.find('#confirmPasswordInput');
    const confirmPasswordEyeIcon = wrapper.find('#confirmPasswordEyeIcon');
    expect(wrapper.vm.showConfirmPassword).toStrictEqual(false);
    expect(confirmPasswordInput.attributes().type).toStrictEqual('password');

    await confirmPasswordEyeIcon.trigger('click');
    expect(wrapper.vm.showConfirmPassword).toStrictEqual(true);
    expect(confirmPasswordInput.attributes().type).toStrictEqual('text');
  });

  it('changePasswordSuccess() closes dialog', () => {
    const emit = vi.spyOn(wrapper.vm.context, 'emit');
    wrapper.vm.changePasswordSuccess();
    expect(emit).toHaveBeenCalledWith('close-dialog');
  });

  it('changePasswordFailed() handles field errors', () => {
    const err = {
      response: {
        data: {
          old_password: ['old password error'],
          new_password: ['new password error']
        }
      }
    };
    wrapper.vm.changePasswordFailed(err);
    expect(wrapper.vm.newPasswordErrors).toStrictEqual(['new password error']);
    expect(wrapper.vm.oldPasswordErrors).toStrictEqual(['old password error']);
  });

  it('changePasswordFailed() handles non field errors', () => {
    const expectedError = 'non field error';

    const nonFieldErr = {
      response: {
        data: {
          non_field_errors: [expectedError]
        }
      }
    };
    wrapper.vm.changePasswordFailed(nonFieldErr);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('test changePasswordFailed() with no response error', () => {
    const noResponseErr = {};
    wrapper.vm.changePasswordFailed(noResponseErr);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('isValid is true when passwords match and ar 6 chars', async () => {
    await wrapper.setProps({ tempCode: '1234' });
    expect(wrapper.vm.isValid).toStrictEqual(false);
    wrapper.vm.confirmPassword = 'test';
    expect(wrapper.vm.isValid).toStrictEqual(false);
    wrapper.vm.newPassword = 'test';
    expect(wrapper.vm.isValid).toStrictEqual(false);
    wrapper.vm.newPassword = 'test12';
    wrapper.vm.confirmPassword = 'test21';
    expect(wrapper.vm.isValid).toStrictEqual(false);
    wrapper.vm.confirmPassword = 'test12';
    expect(wrapper.vm.isValid).toStrictEqual(true);
  });

  it('isValid requires oldPassword when not tempcode', async () => {
    await wrapper.setProps({ tempCode: '' });
    wrapper.vm.newPassword = 'test12';
    wrapper.vm.confirmPassword = 'test12';
    expect(wrapper.vm.isValid).toStrictEqual(false);
    wrapper.vm.oldPassword = 'test';
    expect(wrapper.vm.isValid).toStrictEqual(false);
    wrapper.vm.oldPassword = 'test41';
    expect(wrapper.vm.isValid).toStrictEqual(true);
  });
});
