import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import OtpSetup from 'src/components/auth/OtpSetup.vue';
import setup from 'tests/unit/functions/setup';

setup();

describe('OtpSetup.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed');
    wrapper = shallowMount(OtpSetup);
  });

  it('updates qrUrl on mount', () => {
    expect(wrapper.vm.qrUrl).toContain(
      'otpauth://totp/Yapca:YAPCA%20TOTP%20Authentication%20Code?issuer=Yapca&secret='
    );
    expect(wrapper.vm.qrUrl).toContain('&algorithm=SHA1&digits=6&period=30');
  });

  it('resets codeIncorrect error on change otpCode', async () => {
    wrapper.vm.codeIncorrect = true;
    await wrapper.vm.updateOtpCode(['1', '2']);
    expect(wrapper.vm.codeIncorrect).toStrictEqual(false);
  });

  it('updateOtpCode does not reset codeIncorrect if otpCode empty', async () => {
    wrapper.vm.codeIncorrect = true;
    await wrapper.vm.updateOtpCode([]);
    expect(wrapper.vm.codeIncorrect).toStrictEqual(true);
  });

  it('updateOtpCode is successful for valid token', async () => {
    const emit = vi.spyOn(wrapper.vm.context, 'emit');
    const token = wrapper.vm.totp.generate();
    await wrapper.vm.updateOtpCode(token.split(''));
    expect(wrapper.vm.codeIncorrect).toStrictEqual(false);
    expect(wrapper.vm.codeValid).toStrictEqual(true);
    expect(emit).toHaveBeenCalledWith(
      'otp-setup-complete',
      wrapper.vm.otpSecret
    );
  });

  it('updateOtpCode is successful for previous valid token', async () => {
    wrapper.vm.resetOtpInput = 0;
    const emit = vi.spyOn(wrapper.vm.context, 'emit');
    const token = wrapper.vm.totp.generate({
      timestamp: Date.now() - 30000
    });
    await wrapper.vm.updateOtpCode(token.split(''));
    expect(wrapper.vm.codeIncorrect).toStrictEqual(false);
    expect(wrapper.vm.codeValid).toStrictEqual(true);
    expect(wrapper.vm.resetOtpInput).toStrictEqual(0);
    expect(emit).toHaveBeenCalledWith(
      'otp-setup-complete',
      wrapper.vm.otpSecret
    );
  });

  it('updateOtpCode fails for invalid code', async () => {
    wrapper.vm.resetOtpInput = 0;
    const emit = vi.spyOn(wrapper.vm.context, 'emit');
    await wrapper.vm.updateOtpCode(['1', '2', '3', '4', '5', '6']);
    expect(wrapper.vm.codeIncorrect).toStrictEqual(true);
    expect(wrapper.vm.codeValid).toStrictEqual(false);
    expect(wrapper.vm.resetOtpInput).toStrictEqual(1);
    expect(emit).toHaveBeenCalledTimes(0);
  });

  it('sets qr code size to 80% of width when less than 350px wide', () => {
    wrapper.vm.$q.screen.width = 200;
    expect(wrapper.vm.qrCodeSize).toStrictEqual(160);
    wrapper.vm.$q.screen.width = 2000;
    expect(wrapper.vm.qrCodeSize).toStrictEqual(350);
  });

  it('shows different info text when registering', async () => {
    const otpSetupInfoText = await wrapper.find('#otpSetupInfoText');
    expect(otpSetupInfoText.element.textContent).toContain(
      'To continue with registration, enter the 6 digit code'
    );
    expect(otpSetupInfoText.element.textContent).not.toContain(
      'To reset your TOTP code, enter the 6 digit code displayed'
    );
  });

  it('shows different info text when not registering', async () => {
    await wrapper.setProps({ registering: false, isClient: true });
    const otpSetupInfoText = await wrapper.find('#otpSetupInfoText');
    expect(otpSetupInfoText.element.textContent).not.toContain(
      'To continue with registration, enter the 6 digit code'
    );
    expect(otpSetupInfoText.element.textContent).toContain(
      'To reset your TOTP code, enter the 6 digit code displayed'
    );
  });

  it('otpSetupInfoText is visible when no needs', async () => {
    expect(wrapper.text()).not.toContain('To reset your TOTP code');
    await wrapper.setProps({ registering: false });
    expect(wrapper.text()).toContain('To reset your TOTP code');
  });
});
