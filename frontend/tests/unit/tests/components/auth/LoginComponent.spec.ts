import { mount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import LoginComponent from 'src/components/auth/LoginComponent.vue';
import setup from 'tests/unit/functions/setup';
import { nextTick } from 'vue';
import loginSuccess from 'src/functions/auth/loginSuccess';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { add, differenceInSeconds, sub } from 'date-fns';

setup();

describe('LoginComponent.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed');
    wrapper = mount(LoginComponent);
  });

  it('usernameErrors reset on username change', async () => {
    wrapper.vm.userNameErrors = ['test1', 'test2'];
    expect(wrapper.vm.userNameErrors).toStrictEqual(['test1', 'test2']);
    wrapper.vm.username = 'newUsername';
    await nextTick();
    expect(wrapper.vm.userNameErrors).toStrictEqual([]);
  });

  it('username rules handle empty value', async () => {
    const loginUsernameInput = wrapper.find('#loginUsernameInput');
    await loginUsernameInput.setValue('test');
    expect(
      wrapper.vm.usernameRules[0](loginUsernameInput.element.value)
    ).toStrictEqual(true);
    await loginUsernameInput.setValue('');
    expect(
      wrapper.vm.usernameRules[0](loginUsernameInput.element.value)
    ).toStrictEqual('Username is required');
  });

  it('password rules handle empty value and too short password', async () => {
    const loginPasswordInput = wrapper.find('#loginPasswordInput');

    await loginPasswordInput.setValue('');
    expect(
      wrapper.vm.passwordRules[0](loginPasswordInput.element.value)
    ).toStrictEqual('Password is required');

    await loginPasswordInput.setValue('test');

    expect(
      wrapper.vm.passwordRules[1](loginPasswordInput.element.value)
    ).toStrictEqual('Password must be at least 6 characters');
  });

  it('clicking eye icon shows password', async () => {
    const passwordEyeIcon = wrapper.find('#loginPasswordEyeIcon');
    expect(wrapper.vm.showPassword).toStrictEqual(false);
    await passwordEyeIcon.trigger('click');
    expect(wrapper.vm.showPassword).toStrictEqual(true);
  });

  it('passwordErrors reset on password change', async () => {
    wrapper.vm.passwordErrors = ['test1', 'test2'];
    expect(wrapper.vm.passwordErrors).toStrictEqual(['test1', 'test2']);
    wrapper.vm.password = 'newPassword';
    await nextTick();
    expect(wrapper.vm.passwordErrors).toStrictEqual([]);
  });

  it('test loginFailed()', () => {
    const err = {
      response: {
        data: {
          username: ['test username error'],
          password: ['test password error'],
          otpCode: ['test otp error']
        }
      }
    };
    wrapper.vm.otpErrors = ['example error'];
    wrapper.vm.loginFailed(err);
    expect(wrapper.vm.userNameErrors).toStrictEqual(['test username error']);
    expect(wrapper.vm.passwordErrors).toStrictEqual(['test password error']);
    expect(wrapper.vm.otpErrors).toStrictEqual(['test otp error']);

    expect(axiosFailed).toHaveBeenCalledTimes(1);
    const expectedError = 'non field error';
    const nonFieldErr = {
      response: {
        data: {
          non_field_errors: [expectedError]
        }
      }
    };
    wrapper.vm.loginFailed(nonFieldErr);
    expect(axiosFailed).toHaveBeenCalledTimes(2);
    const noResponseErr = {};
    wrapper.vm.loginFailed(noResponseErr);
    expect(axiosFailed).toHaveBeenCalledTimes(3);
  });

  it('test login failure', async () => {
    vi.mock('src/functions/auth/loginSuccess', () => ({
      default: vi.fn()
    }));
    wrapper.vm.username = 'testUser';
    wrapper.vm.password = 'testPass';
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockRejectedValueOnce('example error');
    await wrapper.vm.login();
    expect(httpPost).toHaveBeenCalledTimes(1);
    expect(httpPost).toHaveBeenCalledWith('login/', {
      password: 'testPass',
      username: 'testUser',
      otpCode: ''
    });
    expect(loginSuccess).toHaveBeenCalledTimes(0);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
    expect(axiosFailed).toHaveBeenCalledWith('example error');
  });

  it('test login success', async () => {
    vi.mock('src/functions/auth/loginSuccess', () => ({
      default: vi.fn()
    }));
    wrapper.vm.username = 'testUser';
    wrapper.vm.password = 'testPass';

    vi.mock('src/functions/proxy/AxiosFailed');
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockResolvedValueOnce('example success');
    await wrapper.vm.login();
    expect(loginSuccess).toHaveBeenCalledTimes(1);
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(loginSuccess).toHaveBeenCalledWith(
      'example success',
      expect.any(Object)
    );
  });

  it('focuses on otpInput when modal is opened', async () => {
    vi.useFakeTimers();
    wrapper.vm.showOtpInputModal = true;
    await nextTick();
    const otpInputModal = wrapper.findComponent({
      ref: 'otpInput'
    }).wrapperElement;
    const otpFocus = vi.spyOn(
      otpInputModal.children[1].children[1].children[0].children[0].children[0]
        .children[0],
      'focus'
    );
    vi.runAllTimers();
    expect(otpFocus).toHaveBeenCalledTimes(1);
    vi.useFakeTimers();
    wrapper.vm.showOtpInputModal = false;
    vi.runAllTimers();
    expect(
      otpInputModal.children[1].children[1].children[0].children[0].children[0]
        .children[0].focus
    ).toHaveBeenCalledTimes(1);
  });

  it('focuses on forgotPasswordModal when modal is opened', async () => {
    vi.useFakeTimers();
    wrapper.vm.showForgotPasswordModal = true;
    await nextTick();
    const forgotPasswordModal = wrapper.findComponent({
      ref: 'forgotPasswordModalRef'
    }).wrapperElement;
    const forgotPasswordFocus = vi.spyOn(
      forgotPasswordModal.children[1].children[2].children[0].children[0]
        .children[0].children[0].children[0],
      'focus'
    );
    vi.runAllTimers();
    expect(forgotPasswordFocus).toHaveBeenCalledTimes(1);
    vi.useFakeTimers();
    wrapper.vm.showForgotPasswordModal = false;
    vi.runAllTimers();
    expect(forgotPasswordFocus).toHaveBeenCalledTimes(1);
  });

  it('updateOtpCode runs login', async () => {
    const loginSpy = vi.spyOn(wrapper.vm.context, 'login');
    await wrapper.vm.updateOtpCode('123456');
    expect(wrapper.vm.otpCode).toStrictEqual('123456');
    expect(loginSpy).toHaveBeenCalledTimes(1);
  });

  it('loginFailed() sets OTP settings if errors was no code', () => {
    const err = {
      response: {
        data: {
          username: [],
          password: [],
          otpCode: ['You must provide an OTP code']
        }
      }
    };
    wrapper.vm.showOtpInputModal = false;
    wrapper.vm.otpCode = '223412';
    wrapper.vm.loginFailed(err);
    expect(wrapper.vm.otpErrors).toStrictEqual([]);
    expect(wrapper.vm.showOtpInputModal).toStrictEqual(true);
    expect(wrapper.vm.otpCode).toStrictEqual('');
  });

  it('closeOtpDialog() does that', () => {
    wrapper.vm.showOtpInputModal = true;
    wrapper.vm.otpCode = '223412';
    wrapper.vm.closeOtpDialog();
    expect(wrapper.vm.showOtpInputModal).toStrictEqual(false);
    expect(wrapper.vm.otpCode).toStrictEqual('');
  });

  it('ForgotPasswordModal emits close dialog', async () => {
    const forgotPasswordButton = wrapper.find('#forgotPasswordButton');
    await forgotPasswordButton.trigger('click');
    expect(wrapper.vm.showForgotPasswordModal).toStrictEqual(true);
    const forgotPasswordModal = wrapper.findComponent({
      ref: 'forgotPasswordModalRef'
    });
    await forgotPasswordModal.vm.$emit('close-dialog');
    expect(wrapper.vm.showForgotPasswordModal).toStrictEqual(false);
  });

  it('OtpInputModal emits reset-otp-errors', async () => {
    wrapper.vm.showOtpInputModal = true;
    await nextTick();
    wrapper.vm.otpErrors = ['test', 'test2'];
    const otpInputModal = wrapper.findComponent({
      ref: 'otpInput'
    });
    await otpInputModal.vm.$emit('reset-otp-errors');
    expect(wrapper.vm.otpErrors).toStrictEqual([]);
  });

  it('OtpInputModal emits otp-reset-temp-code', async () => {
    const routerPush = vi.spyOn(wrapper.vm.$router, 'push');

    wrapper.vm.showOtpInputModal = true;
    await nextTick();
    const otpInputModal = wrapper.findComponent({
      ref: 'otpInput'
    });
    await otpInputModal.vm.$emit('otp-reset-temp-code');
    expect(routerPush).toHaveBeenCalledWith({
      name: 'tempCode',
      query: { action: 'reset-totp', username: '' }
    });
  });

  it('username calls validate on enter', async () => {
    const login = vi.spyOn(wrapper.vm.context, 'login');
    await wrapper.find('#loginUsernameInput').trigger('keyup.enter');
    expect(login).toHaveBeenCalledTimes(0);

    wrapper.vm.username = 'a';
    wrapper.vm.password = 'b';
    await wrapper.find('#loginUsernameInput').trigger('keyup.enter');
    expect(login).toHaveBeenCalledTimes(0);

    wrapper.vm.password = 'bbbbbb';
    await wrapper.find('#loginUsernameInput').trigger('keyup.enter');
    expect(login).toHaveBeenCalledTimes(1);
  });

  it('goToTempCodePage() does that', async () => {
    const routerPush = vi.spyOn(wrapper.vm.$router, 'push');
    wrapper.vm.username = '1234';
    const forgotPasswordButton = wrapper.find('#forgotPasswordButton');
    await forgotPasswordButton.trigger('click');
    const forgotPasswordModal = wrapper.findComponent({
      ref: 'forgotPasswordModalRef'
    });
    await forgotPasswordModal.vm.$emit('go-to-temp-code-page');
    expect(routerPush).toHaveBeenCalledWith({
      name: 'tempCode',
      query: { action: 'reset-password', username: '1234' }
    });
  });

  it('goToTempCodePage() catches error when router.push fails', async () => {
    const routerPush = vi.spyOn(wrapper.vm.$router, 'push');
    routerPush.mockRejectedValueOnce('Example Error');
    const registerButton = wrapper.find('#registerButton');
    await registerButton.trigger('click');
    expect(routerPush).toHaveBeenCalledWith({
      name: 'tempCode',
      query: { action: 'registration', username: '' }
    });
  });

  it('starts setting the "now" value on creates', () => {
    vi.useFakeTimers();
    vi.runOnlyPendingTimers();
    expect(differenceInSeconds(new Date(), wrapper.vm.now)).toStrictEqual(0);
  });

  it('lockoutEndTime returns value from store', () => {
    const newDate = new Date();
    wrapper.vm.userStore.lockoutEndTime = newDate;
    expect(wrapper.vm.lockoutEndTime).toStrictEqual(newDate);

    wrapper.vm.userStore.lockoutEndTime = '2022-03-05T16:38:23.820Z';
    expect(wrapper.vm.lockoutEndTime).toEqual(
      new Date(2022, 2, 5, 16, 38, 23, 820)
    );
  });

  it('lockoutRemainingTime calculates time between now and endTime', () => {
    wrapper.vm.userStore.lockoutEndTime = add(new Date(), {
      minutes: 41
    });
    expect(wrapper.vm.lockoutRemainingTime).toStrictEqual(2460);

    wrapper.vm.userStore.lockoutEndTime = sub(new Date(), {
      minutes: 5
    });
    expect(wrapper.vm.lockoutRemainingTime).toStrictEqual(0);
  });

  it('lockoutRemainingTimeFormatted gets a fomatted remaining time', () => {
    wrapper.vm.userStore.lockoutEndTime = add(new Date(), {
      minutes: 4,
      seconds: 6
    });
    expect(wrapper.vm.lockoutRemainingTimeFormatted).toStrictEqual('4:06');

    wrapper.vm.userStore.lockoutEndTime = add(new Date(), {
      minutes: 0,
      seconds: 55
    });
    expect(wrapper.vm.lockoutRemainingTimeFormatted).toStrictEqual('0:55');
  });

  it('loginFailed sets lockout end time on error', () => {
    const err = {
      response: { data: { detail: 'Too many failed login attempts' } }
    };
    wrapper.vm.loginFailed(err);
    expect(wrapper.vm.lockoutRemainingTimeFormatted).toStrictEqual('10:00');
  });

  it('getNow sets current date', () => {
    vi.useFakeTimers();
    wrapper.vm.getNow();
    vi.advanceTimersByTime(1000);
    expect(differenceInSeconds(wrapper.vm.now, Date.now())).toBeLessThanOrEqual(
      1
    );
  });
});
