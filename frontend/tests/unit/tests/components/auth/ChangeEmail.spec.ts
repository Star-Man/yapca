import { mount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import ChangeEmail from 'src/components/auth/ChangeEmail.vue';
import setup from 'tests/unit/functions/setup';
import { Notify } from 'quasar';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { mockUsers } from 'tests/unit/mocks';
import type { SpyInstance } from 'vitest';

setup();

describe('ChangeEmail.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let notifySpy: SpyInstance;
  let httpPatch: SpyInstance;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed');
    mockUsers();
    wrapper = mount(ChangeEmail, {
      props: {
        userId: 3,
        admin: false
      }
    });
    notifySpy = vi.spyOn(Notify, 'create');
    httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
  });

  it('changing email clears existing errors', async () => {
    wrapper.vm.newEmailErrors = [1, 2, 3];
    await wrapper.find('#emailInput').setValue('newValue');
    expect(wrapper.vm.newEmailErrors).toStrictEqual([]);
  });

  it('changeEmail() sends patch request', async () => {
    httpPatch.mockResolvedValueOnce(true);
    wrapper.vm.newEmail = 'examplenewmail@test.com';
    await wrapper.vm.changeEmail();
    expect(httpPatch).toHaveBeenCalledWith('users/3/update/', {
      email: 'examplenewmail@test.com'
    });
    expect(axiosFailed).toHaveBeenCalledTimes(0);
  });

  it('changeEmail() calls AxiosFailed on failure', async () => {
    wrapper.vm.newEmail = 'examplenewmail@test.com';

    const err = {
      response: {
        data: {
          error: [
            'Cannot Change Email: Email was changed within the last 12 hours'
          ]
        }
      }
    };
    httpPatch.mockRejectedValueOnce(err);
    await wrapper.vm.changeEmail();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('changeEmailSuccess() closes dialog', () => {
    const emit = vi.spyOn(wrapper.vm.context, 'emit');
    wrapper.vm.changeEmailSuccess();
    expect(emit).toHaveBeenCalledWith('close-dialog');
  });

  it('changeEmailSuccess() sets snackbar if admin', async () => {
    await wrapper.setProps({ admin: true });
    wrapper.vm.changeEmailSuccess();
    expect(notifySpy).toHaveBeenCalledWith({
      message: 'Email Changed!',
      type: 'positive'
    });
  });

  it('userEmail is blank if no userId', async () => {
    await wrapper.setProps({ userId: null });
    expect(wrapper.vm.userEmail).toStrictEqual('');
  });

  it('userEmail is blank if no userId', async () => {
    await wrapper.setProps({ userId: null });
    expect(wrapper.vm.userEmail).toStrictEqual('');
  });
});
