import { mount, shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import AdditionalInfo from 'src/components/clientCreation/AdditionalInfo.vue';
import setup from 'tests/unit/functions/setup';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { mockClients } from '../../../mocks';

setup();

describe('AdditionalInfo.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockClients();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    wrapper = shallowMount(AdditionalInfo, { props: { clientId: 4 } });
  });

  it('formattedDateOfBirth() returns dates formatted in IST', () => {
    expect(wrapper.vm.formattedDateOfBirth).toStrictEqual('15/07/91');
  });

  it('formattedDateOfBirth() returns empty string if no dateOfBirth', () => {
    wrapper.vm.clientStore.clients[4].dateOfBirth = undefined;
    expect(wrapper.vm.formattedDateOfBirth).toStrictEqual('');
  });

  it('clientValid() returns true when next of kin and all data entered', () => {
    expect(wrapper.vm.clientValid).toStrictEqual(true);
  });

  it('clientValid() returns false when no next of kin', () => {
    wrapper.vm.clientStore.clients[4].nextOfKin = [];
    expect(wrapper.vm.clientValid).toStrictEqual(false);
  });

  it('clientValid() returns false when no address', () => {
    wrapper.vm.clientStore.clients[4].address = undefined;
    expect(wrapper.vm.clientValid).toStrictEqual(false);
  });

  it('finishAdditionalSetup() sends patch to server', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce(null);
    await wrapper.vm.finishAdditionalSetup();
    expect(httpPatch).toHaveBeenCalledTimes(1);
    expect(httpPatch).toHaveBeenCalledWith('clients/4/update/', {
      additionalInfoSetup: true
    });
  });

  it('finishAdditionalSetup() calls axiosFailed on server error', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockRejectedValueOnce(null);
    await wrapper.vm.finishAdditionalSetup();
    expect(httpPatch).toHaveBeenCalledTimes(1);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('clientValid() returns false when no clientId', async () => {
    await wrapper.setProps({ clientId: null });
    expect(wrapper.vm.clientValid).toStrictEqual(false);
  });

  it('finishAdditionalSetup() returns null when no clientId', async () => {
    await wrapper.setProps({ clientId: null });
    expect(wrapper.vm.finishAdditionalSetup()).toStrictEqual(null);
  });

  it('maxDateToday() returns false for date before 1910', () => {
    expect(wrapper.vm.maxDateToday('1909/01/03')).toStrictEqual(false);
  });

  it('maxDateToday() returns true for valid date', () => {
    expect(wrapper.vm.maxDateToday('2022/01/03')).toStrictEqual(true);
  });

  it('maxDateToday() returns false for date after today', () => {
    expect(wrapper.vm.maxDateToday('2100/01/03')).toStrictEqual(false);
  });

  it('updateFieldFromRow() returns null if no value', async () => {
    wrapper.vm.clientStore.clients[4].firstName = '';
    const response = await wrapper.vm.updateFieldFromRow('firstName');
    expect(response).toStrictEqual(null);
  });

  it('updateFieldFromRow() sets value from store', async () => {
    wrapper.vm.clientStore.clients[4].phoneNumber = '0118999';
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce(null);
    await wrapper.vm.updateFieldFromRow('phoneNumber');
    expect(httpPatch).toHaveBeenCalledWith('clients/4/update/', {
      phoneNumber: '0118999'
    });
  });

  it('setting gender updates field', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce(null);
    await wrapper.findComponent('#genderInput').setValue('Male');
    await wrapper.vm.updateFieldFromRow('gender');
    expect(httpPatch).toHaveBeenCalledWith('clients/4/update/', {
      gender: 'Male'
    });
  });

  it('phoneNumber handles empty input', () => {
    expect(wrapper.vm.clientData[2].rules[0]('')).toStrictEqual(
      'Phone Number is required'
    );
    expect(wrapper.vm.clientData[2].rules[0]('1111')).toStrictEqual(true);
  });

  it('address handles empty input', () => {
    expect(wrapper.vm.clientData[0].rules[0]('')).toStrictEqual(
      'Address is required'
    );
    expect(wrapper.vm.clientData[0].rules[0]('1111')).toStrictEqual(true);
  });

  it('address handles empty input', () => {
    expect(wrapper.vm.clientData[0].rules[0]('')).toStrictEqual(
      'Address is required'
    );
    expect(wrapper.vm.clientData[0].rules[0]('1111')).toStrictEqual(true);
  });

  it('updateFieldFromRow does nothing if no clientId', async () => {
    await wrapper.setProps({ clientId: undefined });
    const updateFieldSpy = vi.spyOn(wrapper.vm.context, 'updateField');
    await wrapper.vm.updateFieldFromRow('mobility');
    expect(updateFieldSpy).toHaveBeenCalledTimes(0);
  });
});

describe('AdditionalInfo.vue full mount', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockClients();
    vi.mock('src/functions/proxy/AxiosFailed');
    wrapper = mount(AdditionalInfo, { props: { clientId: 4 } });
  });

  it('sets address from input', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce(null);
    await wrapper.find('#addressInput').setValue('new address');
    await wrapper.vm.updateFieldFromRowBlur({ target: { id: 'addressInput' } });
    expect(httpPatch).toHaveBeenCalledWith('clients/4/update/', {
      address: 'new address'
    });
  });
});
