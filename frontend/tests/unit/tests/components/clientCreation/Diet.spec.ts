import { mount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import Diet from 'src/components/clientCreation/Diet.vue';
import setup from 'tests/unit/functions/setup';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { nextTick } from 'vue';
import { mockClients } from '../../../mocks';

setup();

describe('Diet.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockClients();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    vi.mock('src/functions/centre/getCentreRelatedData');
    wrapper = mount(Diet, { props: { clientId: 4 } });
  });

  it('finishDietSetup() sends patch to server', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce(null);
    await wrapper.vm.finishDietSetup();
    expect(httpPatch).toHaveBeenCalledTimes(1);
    expect(httpPatch).toHaveBeenCalledWith('clients/4/update/', {
      dietSetup: true
    });
  });

  it('finishDietSetup() calls axiosFailed on server error', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockRejectedValueOnce(null);
    await wrapper.vm.finishDietSetup();
    expect(httpPatch).toHaveBeenCalledTimes(1);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('updateFeedingRisksModel() updates the model for feeding risks', async () => {
    expect(wrapper.vm.feedingRisks).toStrictEqual(['Poor Appetite', 'Chewing']);
    wrapper.vm.clientStore.clients['4'].feedingRisks = [
      {
        id: 1,
        feedingRisk: 'Swallowing'
      }
    ];
    await nextTick();
    expect(wrapper.vm.feedingRisks).toStrictEqual(['Swallowing']);
    wrapper.vm.clientStore.clients['4'].feedingRisks = undefined;
    await nextTick();
    expect(wrapper.vm.feedingRisks).toStrictEqual([]);
  });

  it('updateFeedingRisks() sends post request to server', async () => {
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockResolvedValueOnce({});
    await wrapper.vm.updateFeedingRisks(['Chewing', 'Poor Appetite']);
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPost).toHaveBeenCalledWith('feedingRisks/4/', [
      { client: 4, feedingRisk: 'Chewing' },
      { client: 4, feedingRisk: 'Poor Appetite' }
    ]);
    expect(axiosFailed).toHaveBeenCalledTimes(0);
  });

  it('updateFeedingRisks() calls axiosFailed() on failure', async () => {
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockRejectedValueOnce({});
    await wrapper.vm.updateFeedingRisks(['Chewing']);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('updateFeedingRisks() does nothing when no clientId', async () => {
    await wrapper.setProps({ clientId: undefined });
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    await wrapper.vm.updateFeedingRisks(['Chewing']);
    expect(httpPost).toHaveBeenCalledTimes(0);
  });

  it('finishDietSetup() returns null when no clientId', async () => {
    await wrapper.setProps({ clientId: undefined });
    expect(wrapper.vm.finishDietSetup()).toStrictEqual(null);
  });

  it('storeFeedingRisks() is an empty array if clientId is undefined', async () => {
    await wrapper.setProps({ clientId: null });
    expect(wrapper.vm.storeFeedingRisks).toStrictEqual([]);
  });

  it("feedingRisksModel() doesn't call updateFeedingRisksModel if no clientId", async () => {
    await wrapper.setProps({ clientId: null });
    const updateFeedingRisksModel = vi.spyOn(
      wrapper.vm,
      'updateFeedingRisksModel'
    );
    wrapper.vm.feedingRisksModel();
    expect(updateFeedingRisksModel).toHaveBeenCalledTimes(0);
  });

  it('sets allergies from input', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce(null);
    await wrapper.find('#allergiesInput').setValue('new allergies');
    await wrapper.vm.updateFieldFromRowBlur({
      target: { id: 'allergiesInput' }
    });
    expect(httpPatch).toHaveBeenCalledWith('clients/4/update/', {
      allergies: 'new allergies'
    });
  });

  it('requiresAssistanceEating checkbox calls updateField', async () => {
    const updateFieldSpy = vi.spyOn(wrapper.vm.context, 'updateField');
    await wrapper.find('#requiresAssistanceEatingInput').trigger('click');
    expect(updateFieldSpy).toHaveBeenCalledWith(
      false,
      'requiresAssistanceEating',
      4
    );
  });

  it('updateSlider calls updateField', async () => {
    const updateFieldSpy = vi.spyOn(wrapper.vm.context, 'updateField');
    await wrapper.vm.updateSlider('thicknerGrade');
    expect(updateFieldSpy).toHaveBeenCalledWith(2, 'thicknerGrade', 4);
  });

  it('slider updates thickner grade', async () => {
    await wrapper.findComponent('#thicknerGradeInput').setValue('3');
    expect(wrapper.vm.clientStore.clients[4].thicknerGrade).toStrictEqual('3');
  });

  it('updateCheckboxFromRow does nothing without row value', async () => {
    const updateFieldSpy = vi.spyOn(wrapper.vm.context, 'updateField');

    await wrapper.vm.updateCheckboxFromRow(true, undefined);
    expect(updateFieldSpy).toHaveBeenCalledTimes(0);
  });

  it('clientFirstName returns clients firstName', async () => {
    expect(await wrapper.vm.clientFirstName).toStrictEqual('test name 4');
    await wrapper.setProps({ clientId: undefined });
    expect(await wrapper.vm.clientFirstName).toStrictEqual('');
  });

  it('updateFieldFromRowBlur does nothing if no clientId', async () => {
    const updateFieldSpy = vi.spyOn(wrapper.vm.context, 'updateField');
    await wrapper.setProps({ clientId: undefined });
    await wrapper.vm.updateFieldFromRowBlur({
      target: { id: 'caseFormulationInput' }
    });
    expect(updateFieldSpy).toHaveBeenCalledTimes(0);
  });

  it('updateFieldFromRow does nothing if no clientId', async () => {
    const updateFieldSpy = vi.spyOn(wrapper.vm.context, 'updateField');

    await wrapper.setProps({ clientId: undefined });
    await wrapper.vm.updateFieldFromRow('mobility');
    expect(updateFieldSpy).toHaveBeenCalledTimes(0);
  });

  it('updateSlider does nothing if no clientId', async () => {
    const updateFieldSpy = vi.spyOn(wrapper.vm.context, 'updateField');

    await wrapper.setProps({ clientId: undefined });
    await wrapper.vm.updateSlider('thicknerGrade');
    expect(updateFieldSpy).toHaveBeenCalledTimes(0);
  });
});
