import { flushPromises, mount, shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import Ability from 'src/components/clientCreation/Ability.vue';
import setup from 'tests/unit/functions/setup';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import getCentreRelatedData from 'src/functions/centre/getCentreRelatedData';
import updateField from 'src/functions/client/updateField';
import { mockCentres, mockClients } from '../../../mocks';

setup();

describe('Ability.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockClients();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    vi.mock('src/functions/centre/getCentreRelatedData');
    wrapper = shallowMount(Ability, { props: { clientId: 4 } });
  });

  it('finishAbilitySetup() sends patch to server', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce(null);
    await wrapper.vm.finishAbilitySetup();
    expect(httpPatch).toHaveBeenCalledTimes(1);
    expect(httpPatch).toHaveBeenCalledWith('clients/4/update/', {
      abilitySetup: true
    });
  });

  it('finishAbilitySetup() calls axiosFailed on server error', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockRejectedValueOnce(null);
    await wrapper.vm.finishAbilitySetup();
    expect(httpPatch).toHaveBeenCalledTimes(1);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('subscribedCentres() returns clients centres', async () => {
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    httpGet.mockResolvedValueOnce({ data: [] });
    wrapper.vm.getCentreRelatedData = vi.fn();
    mockCentres();
    expect(wrapper.vm.subscribedCentres).toStrictEqual([
      {
        color: '#FFFFFF',
        created: 'before',
        editingCentreName: false,
        id: 3,
        name: 'Test Centre 3',
        nameValid: false,
        needs: [1, 2, 3],
        needsFirstSetupComplete: true,
        openingDays: [2, 4, 6],
        openingDaysFirstSetupComplete: true,
        closingDaysFirstSetupComplete: true,
        policies: [],
        policiesFirstSetupComplete: true,
        setupComplete: true,
        closedOnPublicHolidays: false,
        country: 'US',
        state: 'TX'
      }
    ]);
    await flushPromises();
    expect(getCentreRelatedData).toHaveBeenCalledTimes(3);
    expect(getCentreRelatedData).toHaveBeenCalledWith(3, 'needs');
  });

  it('finishAbilitySetup() returns null when no clientId', async () => {
    await wrapper.setProps({ clientId: null });
    expect(wrapper.vm.finishAbilitySetup()).toStrictEqual(null);
  });

  it('subscribedCentres() returns empty array when no clientId', async () => {
    await wrapper.setProps({ clientId: null });
    expect(wrapper.vm.subscribedCentres).toStrictEqual([]);
  });

  it('created() calls updateClientData()', async () => {
    const getClientSpy = vi.spyOn(wrapper.vm.context, 'getClient');
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    httpGet.mockResolvedValueOnce({ data: { id: 2 } });
    httpGet.mockResolvedValueOnce({ data: [] });
    await wrapper.vm.created();
    await flushPromises();
    expect(httpGet).toHaveBeenCalledTimes(2);
    expect(httpGet).toHaveBeenCalledWith('clients/4/');
    expect(httpGet).toHaveBeenCalledWith('clients/4/clientNeeds');
    expect(wrapper.vm.clientStore.clients[2]).toStrictEqual({ id: 2 });
    expect(getClientSpy).toHaveBeenCalledTimes(1);
  });

  it('created() does nothing with no clientId', async () => {
    const getClientSpy = vi.spyOn(wrapper.vm.context, 'getClient');
    wrapper.vm.updateClientData = vi.fn();
    await wrapper.setProps({ clientId: null });
    await wrapper.vm.created();
    await flushPromises();
    expect(getClientSpy).toHaveBeenCalledTimes(0);
  });

  it('updateFieldFromRow calls updateField', async () => {
    await wrapper.findComponent('#mobilityInput').setValue('Walks With Stick');
    expect(updateField).toHaveBeenCalledWith('Walks With Stick', 'mobility', 4);
  });

  it('updateFieldFromRow does nothing if no clientId', async () => {
    await wrapper.setProps({ clientId: undefined });
    await wrapper.vm.updateFieldFromRow('mobility');
    expect(updateField).toHaveBeenCalledTimes(0);
  });

  it('updateCheckboxFromRow does nothing if no clientId', async () => {
    await wrapper.setProps({ clientId: undefined });
    expect(
      await wrapper.vm.updateCheckboxFromRow(false, 'isActive')
    ).toStrictEqual(null);
    expect(updateField).toHaveBeenCalledTimes(0);
  });

  it('clientFirstName returns clients firstName', async () => {
    expect(await wrapper.vm.clientFirstName).toStrictEqual('test name 4');
    await wrapper.setProps({ clientId: undefined });
    expect(await wrapper.vm.clientFirstName).toStrictEqual('');
  });
});

describe('Ability.vue full mount', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockClients();
    vi.mock('src/functions/proxy/AxiosFailed');
    vi.mock('src/functions/centre/getCentreRelatedData');
    vi.mock('src/functions/client/updateField');
    wrapper = mount(Ability, { props: { clientId: 4 } });
  });

  it('updateFieldFromRowBlur calls updateField', async () => {
    await wrapper
      .find('#caseFormulationInput')
      .setValue('new case formulation');
    await wrapper.vm.updateFieldFromRowBlur({
      target: { id: 'caseFormulationInput' }
    });
    expect(updateField).toHaveBeenCalledWith(
      'new case formulation',
      'caseFormulation',
      4
    );
  });

  it('updateFieldFromRowBlur does nothing if no clientId', async () => {
    await wrapper.setProps({ clientId: undefined });
    await wrapper.vm.updateFieldFromRowBlur({
      target: { id: 'caseFormulationInput' }
    });
    expect(updateField).toHaveBeenCalledTimes(0);
  });

  it('updateFieldFromRow calls updateField', async () => {
    await wrapper.find('#usesDenturesInput').trigger('click');
    expect(updateField).toHaveBeenCalledWith(false, 'usesDentures', 4);
  });
});
