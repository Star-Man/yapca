import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import BasicInfo from 'src/components/clientCreation/BasicInfo.vue';
import setup from 'tests/unit/functions/setup';
import { nextTick } from 'vue';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { mockClients } from '../../../mocks';

setup();

describe('BasicInfo.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockClients();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    wrapper = shallowMount(BasicInfo);
  });

  it('validateCentre() calls createClient() if isValid passes', () => {
    const createClient = vi.spyOn(wrapper.vm.context, 'createClient');

    wrapper.vm.selectedCentres = [2, 3, 5];
    wrapper.vm.firstName = 'newFirstName';
    wrapper.vm.surname = 'newSurname';

    expect(wrapper.vm.isValid).toStrictEqual(true);
    wrapper.vm.validateClient();
    expect(createClient).toHaveBeenCalledTimes(1);
  });

  it('validateCentre() does not call createClient() if isValid fails', () => {
    wrapper.vm.selectedCentres = [];
    wrapper.vm.firstName = 'newFirstName';
    wrapper.vm.surname = 'newSurname';
    expect(wrapper.vm.isValid).toStrictEqual(false);

    const createClient = vi.spyOn(wrapper.vm, 'createClient');
    wrapper.vm.validateClient();
    expect(createClient).not.toHaveBeenCalled();
  });

  it('createClient() sends post request to create client', async () => {
    wrapper.vm.firstName = 'Johnny';
    wrapper.vm.surname = 'Bravo';
    wrapper.vm.selectedCentres = [2, 3];
    const upsertClientSuccessful = vi.spyOn(
      wrapper.vm.context,
      'upsertClientSuccessful'
    );
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockResolvedValueOnce(true);
    await wrapper.vm.createClient();
    expect(httpPost).toHaveBeenCalledWith('clients/', {
      basicInfoSetup: true,
      centres: [2, 3],
      firstName: 'Johnny',
      surname: 'Bravo'
    });
    expect(upsertClientSuccessful).toHaveBeenCalledTimes(1);
  });

  it('createClient() calls upsertFailed on failure', async () => {
    wrapper.vm.firstName = 'Johnny';
    wrapper.vm.surname = 'Bravo';
    const upsertFailed = vi.spyOn(wrapper.vm.context, 'upsertFailed');
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockRejectedValueOnce(true);
    upsertFailed.mockResolvedValueOnce(true);
    await wrapper.vm.createClient();
    expect(upsertFailed).toHaveBeenCalledTimes(1);
  });

  it('upsertClientSuccessful() updates client and emits to parent', () => {
    const emit = vi.spyOn(wrapper.vm.context, 'emit');
    const updateClient = vi.spyOn(wrapper.vm.clientStore, 'updateClient');
    const setSetupClient = vi.spyOn(wrapper.vm.clientStore, 'setSetupClient');
    wrapper.vm.upsertClientSuccessful({ data: { id: 701 } });
    expect(emit).toHaveBeenCalledTimes(2);
    expect(emit).toHaveBeenCalledWith('update-client-id', 701);
    expect(emit).toHaveBeenCalledWith('update-step', 2);
    expect(updateClient).toHaveBeenCalledTimes(1);
    expect(updateClient).toHaveBeenCalledWith({ id: 701 });
    expect(setSetupClient).toHaveBeenCalledTimes(1);
    expect(setSetupClient).toHaveBeenCalledWith({ id: 701 });
  });

  it('upsertFailed() calls axiosFailed()', () => {
    wrapper.vm.upsertFailed({});
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('upsertFailed() sets first and surname errors', () => {
    wrapper.vm.firstNameErrors = [];
    wrapper.vm.surnameErrors = [];
    const clientError = {
      response: {
        data: {
          firstName: ['Example First Name Error'],
          surname: ['Example Surname Error']
        }
      }
    };
    wrapper.vm.upsertFailed(clientError);
    expect(wrapper.vm.firstNameErrors).toStrictEqual([
      'Example First Name Error'
    ]);
    expect(wrapper.vm.surnameErrors).toStrictEqual(['Example Surname Error']);
  });

  it('upsertFailed() sets empty array if no name errors', () => {
    wrapper.vm.firstNameErrors = ['test'];
    wrapper.vm.surnameErrors = ['test'];
    const clientError = {
      response: {
        data: {}
      }
    };
    wrapper.vm.upsertFailed(clientError);
    expect(wrapper.vm.firstNameErrors).toStrictEqual([]);
    expect(wrapper.vm.surnameErrors).toStrictEqual([]);
  });
  it('firstName errors reset on firstName change', async () => {
    wrapper.vm.firstNameErrors = ['Example First Name Error'];
    wrapper.vm.firstName = 'New Name';
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.firstNameErrors).toStrictEqual([]);
  });

  it('surname errors reset on surname change', async () => {
    wrapper.vm.surnameErrors = ['Example Surname Error'];
    wrapper.vm.surname = 'New Surname';
    await nextTick();
    expect(wrapper.vm.surnameErrors).toStrictEqual([]);
  });

  it('centres() gets state centres as array', () => {
    wrapper.vm.userStore.user.centres = [3, 4];
    wrapper.vm.centreStore.centres = {
      2: { id: 2, setupComplete: true },
      3: { id: 3, setupComplete: false },
      4: { id: 4, setupComplete: true }
    };
    expect(wrapper.vm.centres).toStrictEqual([{ id: 4, setupComplete: true }]);
  });
});
