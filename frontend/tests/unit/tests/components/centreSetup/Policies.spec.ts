import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import Policies from 'src/components/centreSetup/Policies.vue';
import setup from 'tests/unit/functions/setup';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { mockCentres } from 'tests/unit/mocks';

setup();

describe('Policies.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockCentres();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    wrapper = shallowMount(Policies, {
      props: {
        centres: ['Centre 2']
      }
    });
  });

  it('test addPolicyField()', () => {
    wrapper.vm.policyInputs = [{ description: 'test 1' }];
    wrapper.vm.addPolicyField();
    expect(wrapper.vm.policyInputs).toStrictEqual([
      { description: 'test 1' },
      { description: '' }
    ]);
  });

  it('test removePolicyField()', () => {
    wrapper.vm.policyInputs = [
      { description: 'test 1' },
      { description: 'test 2' },
      { description: 'test 3' }
    ];
    wrapper.vm.removePolicyField(1);
    expect(wrapper.vm.policyInputs).toStrictEqual([
      { description: 'test 1' },
      { description: 'test 3' }
    ]);
  });

  it('test validatePolicies()', async () => {
    wrapper.vm.policyInputs = [
      { description: 'test 1' },
      { description: '' },
      { description: 'test 3' }
    ];
    wrapper.vm.noPolicyConfirmDialog = false;
    const createPolicies = vi.spyOn(wrapper.vm.context, 'createPolicies');

    await wrapper.vm.validatePolicies();
    expect(createPolicies).toHaveBeenCalledTimes(1);
    expect(wrapper.vm.noPolicyConfirmDialog).toStrictEqual(false);
    expect(wrapper.vm.policies).toStrictEqual([
      { description: 'test 1' },
      { description: 'test 3' }
    ]);

    wrapper.vm.policyInputs = [
      { description: '' },
      { description: '' },
      { description: '' }
    ];

    wrapper.vm.validatePolicies();
    expect(wrapper.vm.noPolicyConfirmDialog).toStrictEqual(true);
    expect(createPolicies).toHaveBeenCalledTimes(1);
    expect(wrapper.vm.policies).toStrictEqual([]);
  });

  it('test createPolicies()', async () => {
    wrapper.vm.policies = [
      { description: 'test 1' },
      { description: 'test 3' }
    ];
    const createPoliciesSuccessful = vi.spyOn(
      wrapper.vm.context,
      'createPoliciesSuccessful'
    );

    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockRejectedValueOnce('Example Error');
    await wrapper.vm.createPolicies();
    expect(httpPost).toHaveBeenCalledWith('policies/', [
      { description: 'test 1' },
      { description: 'test 3' }
    ]);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
    expect(axiosFailed).toHaveBeenCalledWith('Example Error');
    expect(createPoliciesSuccessful).toHaveBeenCalledTimes(0);

    createPoliciesSuccessful.mockResolvedValue({});
    httpPost.mockResolvedValue({ data: {} });
    await wrapper.vm.createPolicies();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
    expect(createPoliciesSuccessful).toHaveBeenCalledWith({});
  });

  it('test createPoliciesSuccessful()', () => {
    wrapper.vm.centreStore.setupCentre.openingDays = [2, 3, 4];
    wrapper.vm.centreStore.setupCentre.policies = [2, 3, 4];

    const data = [
      { id: 1, description: 'Test Policy 1' },
      { id: 2, description: 'Test Policy 2' },
      { id: 3, description: 'Test Policy 3' }
    ];
    const emit = vi.spyOn(wrapper.vm.context, 'emit');
    wrapper.vm.createPoliciesSuccessful(data);
    expect(emit).toHaveBeenCalledWith('update-centre', {
      id: 2,
      name: 'Test Centre',
      policies: [1, 2, 3],
      needsFirstSetupComplete: false,
      policiesFirstSetupComplete: true,
      setupComplete: false,
      openingDaysFirstSetupComplete: false,
      closingDaysFirstSetupComplete: false,
      nameValid: true,
      editingCentreName: false,
      created: '',
      color: '#FFFFFF',
      closedOnPublicHolidays: true,
      country: 'AU',
      state: 'QLD'
    });
  });
});
