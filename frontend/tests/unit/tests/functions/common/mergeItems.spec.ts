import { beforeEach, describe, expect, it } from 'vitest';
import mergeItems from 'src/functions/common/mergeItems';
import { useCentreStore } from 'src/stores/centresStore';
import { createPinia, setActivePinia } from 'pinia';
import { useClientStore } from 'src/stores/clientsStore';

describe('mergeItems', () => {
  beforeEach(() => {
    setActivePinia(createPinia());
  });

  it('can merge policies for centre', () => {
    const centreStore = useCentreStore();
    centreStore.policies[2] = { id: 2, description: 'policy 2' };
    mergeItems(
      [
        { id: 3, description: 'policy 3' },
        { id: 4, description: 'policy 4' }
      ],
      'policies'
    );
    expect(centreStore.policies).toStrictEqual({
      '2': { id: 2, description: 'policy 2' },
      '3': { id: 3, description: 'policy 3' },
      '4': { id: 4, description: 'policy 4' }
    });
  });

  it('handles missing ids', () => {
    const clientStore = useClientStore();
    mergeItems(
      [{ firstName: 'newFirstName 1' }, { id: 3, firstName: 'newFirstName 3' }],
      'clients'
    );
    expect(clientStore.clients).toStrictEqual({
      '3': { id: 3, firstName: 'newFirstName 3' }
    });
  });
});
