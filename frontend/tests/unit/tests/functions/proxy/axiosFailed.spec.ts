import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import App from 'src/App.vue';
import setup from 'tests/unit/functions/setup';
import { Notify } from 'quasar';
import type { SpyInstance } from 'vitest';

setup();

describe('axiosFailed', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let notifySpy: SpyInstance;

  beforeEach(() => {
    vi.resetAllMocks();
    wrapper = shallowMount(App);
    notifySpy = vi.spyOn(Notify, 'create');
  });

  it('handles array of errors', () => {
    wrapper.vm.context.axiosFailed({
      response: {
        data: {
          error: ['example error 1']
        }
      }
    });
    expect(notifySpy).toHaveBeenCalledWith({
      message: 'example error 1',
      type: 'negative'
    });
  });

  it('handles string errors', () => {
    wrapper.vm.context.axiosFailed({
      response: {
        data: {
          error: 'example error 2'
        }
      }
    });
    expect(notifySpy).toHaveBeenCalledWith({
      message: 'example error 2',
      type: 'negative'
    });
  });
});
