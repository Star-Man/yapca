import { beforeEach, describe, expect, it, vi } from 'vitest';
import goToNextPage from 'src/functions/proxy/goToNextPage';
import type { SpyInstance } from 'vitest';
import type { Router } from 'vue-router';

describe('goToNextPage', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let router: Router;
  let routerPush: SpyInstance;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    router = {
      push: vi.fn().mockResolvedValue(true),
      currentRoute: {
        value: {
          query: {
            nextUrl: 'example-next-url'
          }
        }
      }
    } as unknown as Router;
    routerPush = vi.spyOn(router, 'push');
  });

  it('goes to next url when passed', () => {
    goToNextPage(router);
    expect(routerPush).toHaveBeenCalledWith('example-next-url');
  });

  it('goes to clients page when nexturl is /', () => {
    router.currentRoute.value.query.nextUrl = '/';
    goToNextPage(router);
    expect(routerPush).toHaveBeenCalledWith({ name: 'Clients' });
  });

  it('goes to clients page when nexturl is empty', () => {
    router.currentRoute.value.query.nextUrl = '';
    goToNextPage(router);
    expect(routerPush).toHaveBeenCalledWith({ name: 'Clients' });
  });
});
