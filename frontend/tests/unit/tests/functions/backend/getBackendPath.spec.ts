import getBackendPath from 'src/functions/backend/getBackendPath';
import { describe, expect, it } from 'vitest';

describe('getBackendPath.ts', () => {
  it('gets backend path with env set', () => {
    process.env.VUE_APP_API_HOST = 'examplehost';
    process.env.VUE_APP_API_PORT = '1234';
    process.env.VUE_APP_API_PATH = 'api-page';

    expect(getBackendPath()).toStrictEqual('examplehost:1234/api-page');
  });

  it('gets backend path without env set', () => {
    process.env.VUE_APP_API_HOST = undefined;
    process.env.VUE_APP_API_PORT = undefined;
    process.env.VUE_APP_API_PATH = undefined;
    expect(getBackendPath()).toStrictEqual(`${window.location.hostname}:`);
  });
});
