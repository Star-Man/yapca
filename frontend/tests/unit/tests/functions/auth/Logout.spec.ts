import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import App from 'src/App.vue';
import setup from 'tests/unit/functions/setup';

setup();

describe('Logout.ts', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed');
    wrapper = shallowMount(App);
  });

  it('test logout deletes user data from store', async () => {
    wrapper.vm.userStore.user = {
      username: 'testUser',
      id: 3,
      groups: [{ id: 3, name: 'admin' }],
      group: 'admin',
      accentColor: '#FFFFFF',
      darkTheme: false
    };
    wrapper.vm.basicStore.token = 'test-token';
    await wrapper.vm.context.logout(wrapper.vm.router);
    expect(wrapper.vm.basicStore.token).toStrictEqual('');
    expect(wrapper.vm.userStore.user).toStrictEqual({
      username: null,
      id: null,
      groups: [],
      group: '',
      accentColor: null,
      darkTheme: null,
      displayName: '',
      isActive: false,
      permissions: [],
      centres: [],
      userClientTableHiddenRows: [],
      email: ''
    });
  });
});
