import { beforeEach, describe, expect, it, vi } from 'vitest';
import updateCentre from 'src/functions/centre/updateCentre';
import { useBasicStore } from 'src/stores/basicStore';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { createPinia, setActivePinia } from 'pinia';
import type { SpyInstance } from 'vitest';

describe('updateCentre', () => {
  let httpPatch: SpyInstance;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let basicStore: any;
  beforeEach(() => {
    vi.resetAllMocks();

    vi.mock('src/functions/proxy/AxiosFailed');
    setActivePinia(createPinia());
    basicStore = useBasicStore();
    httpPatch = vi.spyOn(basicStore.http, 'patch');
  });

  it('updateCentre() sends patch request to server', async () => {
    httpPatch.mockResolvedValueOnce(true);
    await updateCentre({ id: 3, name: 'new centre name' });
    expect(httpPatch).toHaveBeenCalledWith('centres/3/update/', {
      id: 3,
      name: 'new centre name'
    });
    expect(axiosFailed).toHaveBeenCalledTimes(0);
  });

  it('updateCentre() calls axiosFailed on failure', async () => {
    httpPatch.mockRejectedValueOnce(false);
    await updateCentre({ id: 3, name: 'new centre name' });
    expect(axiosFailed).toHaveBeenCalledOnce();
  });

  it('updateCentre() returns null when no id', async () => {
    await updateCentre({ name: 'new centre name' });
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPatch).toHaveBeenCalledTimes(0);
  });
});
