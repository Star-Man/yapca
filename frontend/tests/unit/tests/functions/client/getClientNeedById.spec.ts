/* eslint-disable @typescript-eslint/no-explicit-any */
import { beforeEach, describe, expect, it, vi } from 'vitest';
import { createPinia, setActivePinia } from 'pinia';
import getClientNeedById from 'src/functions/client/getClientNeedById';
import { mockCentres, mockClients } from 'tests/unit/mocks';

describe('getClientNeedById', () => {
  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    setActivePinia(createPinia());
    mockCentres();
    mockClients();
  });

  it('getClientNeedById gets clientNeed data based on Id', () => {
    expect(getClientNeedById(3)).toStrictEqual({
      plan: 'Example Plan 3',
      needDescription: 'Test Need 3',
      centre: 'Test Centre 3'
    });
  });
});
