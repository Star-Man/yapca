/* eslint-disable @typescript-eslint/no-explicit-any */
import { beforeEach, describe, expect, it, vi } from 'vitest';
import { createPinia, setActivePinia } from 'pinia';
import { useBasicStore } from 'src/stores/basicStore';
import getClientNeedsForClient from 'src/functions/client/getClientNeedsForClient';
import { useCentreStore } from 'src/stores/centresStore';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { useClientStore } from 'src/stores/clientsStore';

describe('getClientNeedsForClient', () => {
  let basicStore: any;
  let centreStore: any;
  let clientStore: any;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));

    setActivePinia(createPinia());
    basicStore = useBasicStore();
    centreStore = useCentreStore();
    clientStore = useClientStore();
  });

  it('getClientNeedsForClient gets client related centre needs and merges to store', async () => {
    const httpGet = vi.spyOn(basicStore.http, 'get');
    httpGet.mockResolvedValueOnce({
      data: [{ id: 1 }, { id: 2 }, { id: 3 }]
    });
    centreStore.needs = {
      '1': { id: 1, description: 'example need 1' },
      '3': { id: 3, description: 'example need 3' }
    };
    await getClientNeedsForClient(3);
    expect(httpGet).toHaveBeenCalledWith('clients/3/clientNeeds');
    expect(clientStore.clientNeeds).toStrictEqual({
      '1': { id: 1 },
      '2': { id: 2 },
      '3': { id: 3 }
    });
  });

  it('getClientNeedsForClient() calls axiosFailed() on failure ', async () => {
    const httpGet = vi.spyOn(basicStore.http, 'get');
    httpGet.mockRejectedValueOnce({});
    await getClientNeedsForClient(4);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });
});
