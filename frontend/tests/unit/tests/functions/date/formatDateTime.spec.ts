import { describe, expect, it } from 'vitest';
import formatDateTime from 'src/functions/date/formatDateTime';
import { formatISO, startOfYesterday } from 'date-fns';

describe('formatDateTime', () => {
  it('formatDateTime() returns date and time if boolean is set', () => {
    const formattedDateTime = formatDateTime('2020-10-12T16:36:37', true);
    expect(formattedDateTime).toStrictEqual('12/10/20 16:36');
  });

  it('formatDateTime() can return relative date', () => {
    const yesterday = formatISO(startOfYesterday());
    const formattedDateTime = formatDateTime(yesterday, false, true);
    expect(formattedDateTime).toStrictEqual('Yesterday');
  });
});
