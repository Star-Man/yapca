import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import DefaultLayout from 'src/layouts/DefaultLayout.vue';
import setup from 'tests/unit/functions/setup';
import { mockClients, mockUsers } from 'tests/unit/mocks';
import { Notify } from 'quasar';
import { nextTick } from 'vue';
import logout from 'src/functions/auth/logout';
import type { SpyInstance } from 'vitest';

setup();

describe('DefaultLayout.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let notifySpy: SpyInstance;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockUsers();
    vi.mock('src/functions/proxy/AxiosFailed');
    vi.mock('src/functions/auth/logout');
    wrapper = shallowMount(DefaultLayout);
    notifySpy = vi.spyOn(Notify, 'create');
  });

  it('test userLetter()', () => {
    const letterT = wrapper.vm.userLetter;
    expect(letterT).toStrictEqual('T');
    wrapper.vm.userStore.user.displayName = 'example';
    const letterE = wrapper.vm.userLetter;
    expect(letterE).toStrictEqual('E');
    wrapper.vm.userStore.user.displayName = null;
    const blankLetter = wrapper.vm.userLetter;
    expect(blankLetter).toStrictEqual('');
  });

  it('test updateAdminPage() updates admin to nurse', async () => {
    expect(wrapper.vm.items.length).toStrictEqual(4);
    wrapper.vm.userStore.user.group = 'nurse';
    wrapper.vm.updateAdminPage('nurse');
    await nextTick();
    expect(wrapper.vm.items.length).toStrictEqual(3);
    expect(notifySpy).toHaveBeenCalledWith({
      message: 'An admin has changed your group to nurse',
      type: 'info'
    });
  });

  it('test updateAdminPage() updates nurse to admin', async () => {
    wrapper.vm.userStore.user.group = 'nurse';
    await nextTick();

    expect(wrapper.vm.items.length).toStrictEqual(3);
    wrapper.vm.updateAdminPage('admin');
    wrapper.vm.userStore.user.group = 'admin';
    await nextTick();
    expect(wrapper.vm.items.length).toStrictEqual(4);
    expect(notifySpy).toHaveBeenCalledWith({
      message: 'An admin has changed your group to admin',
      type: 'info'
    });
  });

  it('test updateCalendarPage() creates calendar option', async () => {
    expect(wrapper.vm.items.length).toStrictEqual(4);
    wrapper.vm.userStore.user.centres = [1, 2, 3];
    mockClients();
    await nextTick();
    expect(wrapper.vm.items.length).toStrictEqual(5);
  });

  it('test updateCalendarPage() removes calendar option', async () => {
    wrapper.vm.userStore.user.centres = [1, 2, 3, 4];
    mockClients();
    await nextTick();
    expect(wrapper.vm.items[0].title).toStrictEqual('Calendar');
    expect(wrapper.vm.items.length).toStrictEqual(5);
    wrapper.vm.clientStore.clients = [];
    await nextTick();
    expect(wrapper.vm.items.length).toStrictEqual(4);
    expect(notifySpy).toHaveBeenCalledWith({
      message: 'The only client was removed',
      type: 'info'
    });
  });

  it('test updateAdminPage() redirects page if current is admin', async () => {
    wrapper.vm.router.currentRoute.value.name = 'admin';
    await wrapper.vm.updateAdminPage('nurse', 'admin');
    expect(notifySpy).toHaveBeenCalledWith({
      message: 'An admin has changed your group to nurse',
      type: 'info'
    });
    expect(wrapper.vm.router.push).toHaveBeenCalledWith({ name: 'Clients' });
  });

  it('test updateAdminPage() does nothing when group same', () => {
    expect(wrapper.vm.items.length).toStrictEqual(4);
    wrapper.vm.updateAdminPage('admin');
    expect(wrapper.vm.items.length).toStrictEqual(4);
  });

  it('logoutUser does that', () => {
    wrapper.vm.logoutUser();
    expect(logout).toHaveBeenCalledWith(wrapper.vm.router);
  });

  it('changing screen size opens and closes app drawer', async () => {
    wrapper.vm.$q.screen.xs = false;
    await nextTick();
    expect(wrapper.vm.basicStore.navDrawer).toStrictEqual(true);
    wrapper.vm.$q.screen.xs = true;
    await nextTick();
    expect(wrapper.vm.basicStore.navDrawer).toStrictEqual(false);

    wrapper.vm.$q.screen.xs = false;
    await nextTick();
    expect(wrapper.vm.basicStore.navDrawer).toStrictEqual(true);
  });

  it('sidebar border changes in darkmode', async () => {
    expect(wrapper.vm.sideBorder).toStrictEqual('border-right: 1px solid #ddd');
    wrapper.vm.$q.dark.isActive = true;
    await nextTick();
    expect(wrapper.vm.sideBorder).toStrictEqual('');
  });

  it('pages are different for client user', async () => {
    mockClients();
    wrapper.vm.userStore.user.group = 'client';
    await nextTick();
    expect(wrapper.vm.items.length).toStrictEqual(0);
    wrapper.vm.userStore.user.permissions = ['client_can_view_own_data'];
    expect(wrapper.vm.items.length).toStrictEqual(1);
    expect(wrapper.vm.items[0].title).toStrictEqual('My Medical Info');
    wrapper.vm.userStore.user.permissions = ['client_can_view_calendar'];
    expect(wrapper.vm.items.length).toStrictEqual(1);
    expect(wrapper.vm.items[0].title).toStrictEqual('Calendar');
    wrapper.vm.userStore.user.permissions = [
      'client_can_view_own_data',
      'client_can_view_calendar'
    ];
    expect(wrapper.vm.items.length).toStrictEqual(2);
  });

  it('displayName is set from store', async () => {
    mockClients();
    wrapper.vm.clientStore.activeClient = wrapper.vm.clientStore.clients[3];
    expect(wrapper.vm.displayName).toStrictEqual('Test User');
    wrapper.vm.userStore.user.group = 'client';
    wrapper.vm.userStore.user.displayName = null;
    await nextTick();
    expect(wrapper.vm.displayName).toStrictEqual('test name 1');
  });

  it('removing calendar permission for client changes page', async () => {
    mockClients();
    wrapper.vm.userStore.user.group = 'client';
    wrapper.vm.router.currentRoute.value.name = 'calendar';
    wrapper.vm.clientStore.activeClient = wrapper.vm.clientStore.clients[3];
    wrapper.vm.userStore.user.permissions = ['client_can_view_own_data'];

    await nextTick();
    expect(notifySpy).toHaveBeenCalledWith({
      message: 'Your access to the calendar has been restricted by an admin.',
      type: 'info'
    });
    expect(wrapper.vm.router.push).toHaveBeenCalledWith({
      name: 'My Medical Info'
    });
  });

  it('removing own data permission for client changes page', async () => {
    mockClients();
    wrapper.vm.userStore.user.group = 'client';
    wrapper.vm.router.currentRoute.value.name = 'My Medical Info';
    wrapper.vm.clientStore.activeClient = wrapper.vm.clientStore.clients[3];
    wrapper.vm.userStore.user.permissions = ['client_can_view_calendar'];

    await nextTick();
    expect(notifySpy).toHaveBeenCalledWith({
      message:
        'Your access to your medical info has been restricted by an admin.',
      type: 'info'
    });
    expect(wrapper.vm.router.push).toHaveBeenCalledWith({
      name: 'calendar'
    });
  });

  it('removing own data permission for nurse does nothing', async () => {
    mockClients();
    wrapper.vm.userStore.user.group = 'nurse';
    wrapper.vm.router.currentRoute.value.name = 'My Medical Info';
    wrapper.vm.clientStore.activeClient = wrapper.vm.clientStore.clients[3];
    wrapper.vm.userStore.user.permissions = ['client_can_view_calendar'];

    await nextTick();
    expect(notifySpy).not.toHaveBeenCalledWith({
      message:
        'Your access to your medical info has been restricted by an admin.',
      type: 'info'
    });
    expect(wrapper.vm.router.push).not.toHaveBeenCalled();
  });
});
