import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import CalendarPage from 'src/pages/CalendarPage.vue';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import setup from 'tests/unit/functions/setup';
import { addDays, format, startOfWeek } from 'date-fns';
import { nextTick } from 'vue';
import { parseDate } from '@quasar/quasar-ui-qcalendar';
import { mockCentres, mockClients } from '../../mocks';

setup();

describe('CalendarPage.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.clearAllMocks();
    mockCentres();
    mockClients();
    vi.mock('src/functions/proxy/AxiosFailed');
    wrapper = shallowMount(CalendarPage);
    wrapper.vm.centreStore.centres[1].openingDays = [2, 6];
    wrapper.vm.centreStore.centres[2].openingDays = [1, 7, 5, 6];
    wrapper.vm.centreStore.centres[2].setupComplete = true;
    wrapper.vm.centreStore.centres[3].openingDays = [2, 3, 4];
    wrapper.vm.userStore.users = {
      1: {
        id: 1,
        username: 'testUser',
        displayName: 'Test User',
        accentColor: '#FFFFFF',
        darkTheme: false,
        group: 'admin',
        groups: [{ id: 4, name: 'admin' }],
        isActive: true,
        centres: [1, 2],
        userClientTableHiddenRows: [],
        email: 'test@test.test'
      }
    };
    wrapper.vm.userStore.user = {
      id: 1,
      username: 'testUser',
      displayName: 'Test User',
      accentColor: '#FFFFFF',
      darkTheme: false,
      group: 'admin',
      groups: [{ id: 4, name: 'admin' }],
      isActive: true,
      centres: [1, 2],
      userClientTableHiddenRows: [],
      email: 'test@test.test'
    };
    wrapper.vm.clientStore.clients[4].centres = [2, 3, 4];
    wrapper.vm.clientStore.clients[4].visitingDays = [
      { day: 4, centre: 4, client: 3 },
      { day: 2, centre: 2, client: 3 },
      { day: 3, centre: 3, client: 3 },
      { day: 4, centre: 2, client: 3 },
      { day: 1, centre: 2, client: 3 }
    ];
    wrapper.vm.clientStore.clients[4].setupComplete = true;
    wrapper.vm.centreStore.events = [
      {
        id: 4,
        name: 'Example Event 4',
        start: '2020-03-12',
        end: '2020-03-15',
        color: '#CCCCCC',
        eventType: 'Public Holiday',
        centre: 2
      },
      {
        id: 6,
        name: 'Example Event 6',
        start: '2020-05-12',
        end: '2020-03-12',
        color: '#CCCCCC',
        eventType: 'Public Holiday',
        centre: 1
      },
      {
        id: 7,
        name: 'Example Event 7',
        start: '2020-05-10',
        end: '2020-03-19',
        color: '#CCCCCC',
        eventType: 'Public Holiday'
      },
      {
        id: 8,
        name: 'Example Event 8',
        start: '2020-03-18',
        end: format(
          addDays(startOfWeek(Date.now(), { weekStartsOn: 1 }), 1),
          'yyyy-MM-dd'
        ),
        color: '#CCCCCC',
        eventType: 'User Close Event',
        centre: 2
      }
    ];
  });

  it('updateCloseEventsOnWSUpdate() calls getEvents() on change', async () => {
    wrapper.vm.context.getEvents = vi.fn();
    wrapper.vm.centreStore.eventsNeedUpdating = true;
    await nextTick();
    expect(wrapper.vm.context.getEvents).toHaveBeenCalledTimes(1);
  });

  it('updateCloseEventsOnWSUpdate() does nothing if false', async () => {
    wrapper.vm.context.getEvents = vi.fn();
    wrapper.vm.centreStore.eventsNeedUpdating = false;
    await nextTick();
    expect(wrapper.vm.context.getEvents).toHaveBeenCalledTimes(0);
  });

  it('weekdays() gets all weekdays of users subscribed centres', () => {
    expect(wrapper.vm.weekdays).toStrictEqual([1, 2, 5, 6, 0]);
  });

  it('weekdaysFormatted() gets weekdays readable', () => {
    expect(wrapper.vm.weekdaysFormatted).toStrictEqual([1, 2, 3, 6, 0]);
  });

  it('start() returns start of week based on weekday', () => {
    expect(wrapper.vm.start).toStrictEqual(
      startOfWeek(Date.now(), { weekStartsOn: 1 })
    );
  });

  it('events() returns array of events based on visiting days', () => {
    expect(wrapper.vm.events).toStrictEqual([
      {
        id: wrapper.vm.events[0].id,
        name: 'test name 3 test surname 3',
        color: '#FFFFFF',
        start: format(
          addDays(startOfWeek(Date.now(), { weekStartsOn: 1 }), 3),
          'yyyy-MM-dd'
        ),
        allDay: true,
        centreId: 2
      },
      {
        color: '#FFFFFF',
        id: wrapper.vm.events[1].id,
        name: 'test name 4 test surname 4',
        start: format(
          addDays(startOfWeek(Date.now(), { weekStartsOn: 1 }), 3),
          'yyyy-MM-dd'
        ),
        allDay: true,
        centreId: 2
      }
    ]);
  });

  it('clients() gets state clients as array', () => {
    expect(wrapper.vm.clients.length).toStrictEqual(12);
    expect(wrapper.vm.clients[0].id).toStrictEqual(1);
    expect(wrapper.vm.clients[1].id).toStrictEqual(3);
  });

  it('storeEvents returns closeEvents from store', async () => {
    await nextTick();
    expect(wrapper.vm.centreEvents).toStrictEqual([
      {
        id: 4,
        name: 'Example Event 4',
        start: '2020-03-12',
        end: '2020-03-15',
        color: '#B00020',
        eventType: 'Public Holiday',
        centre: 2
      },
      {
        id: 8,
        name: 'Example Event 8',
        start: '2020-03-18',
        end: format(
          addDays(startOfWeek(Date.now(), { weekStartsOn: 1 }), 1),
          'yyyy-MM-dd'
        ),
        color: '#4CAF50',
        eventType: 'User Close Event',
        centre: 2
      }
    ]);
  });

  it('getEvents() sends get to server', async () => {
    const firstWeekDay = format(
      startOfWeek(Date.now(), { weekStartsOn: 1 }),
      'yyyy-MM-dd'
    );
    const lastWeekDay = format(
      addDays(startOfWeek(Date.now(), { weekStartsOn: 1 }), 6),
      'yyyy-MM-dd'
    );
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    httpGet.mockResolvedValue({ data: [] });
    await wrapper.vm.getEvents();
    expect(httpGet).toHaveBeenCalledWith(
      `events/${firstWeekDay}/${lastWeekDay}/`
    );
  });

  it('axiosFailed is called on getEvents if server failure', async () => {
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    httpGet.mockRejectedValue({});
    await wrapper.vm.getEvents();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('eventsByDate filters by day', () => {
    expect(
      wrapper.vm.eventsByDate[
        format(
          addDays(startOfWeek(Date.now(), { weekStartsOn: 1 }), 3),
          'yyyy-MM-dd'
        )
      ]['2'].length
    ).toStrictEqual(2);

    expect(wrapper.vm.eventsByDate['2022-01-01']).toStrictEqual(undefined);
  });

  it('selectedVisitingCentreClients is empty array when no values set', () => {
    wrapper.vm.selectedVisitingCentre = undefined;
    expect(wrapper.vm.selectedVisitingCentreClients).toStrictEqual([]);
  });

  it('openCentreVisitorsTodayModal gets clients visiting centre on day', () => {
    wrapper.vm.openCentreVisitorsTodayModal(
      format(
        addDays(startOfWeek(Date.now(), { weekStartsOn: 1 }), 3),
        'yyyy-MM-dd'
      ),
      2
    );
    expect(wrapper.vm.selectedVisitingCentreClients).toStrictEqual([3, 4]);
  });

  it('setCentreVisitorsTodayDialog does that', () => {
    wrapper.vm.showCentreVisitorsTodayModal = false;
    wrapper.vm.setCentreVisitorsTodayDialog(true);
    expect(wrapper.vm.showCentreVisitorsTodayModal).toStrictEqual(true);
  });

  it('rendering calendar gets current time', async () => {
    vi.useFakeTimers();
    const timeStartPos = vi.fn();
    wrapper.vm.calendar = { timeStartPos };
    await nextTick();
    vi.runOnlyPendingTimers();
    expect(timeStartPos).toHaveBeenCalledWith(
      parseDate(new Date())?.time,
      false
    );
  });

  it('nowLineStyle gets style based on accent colour', () => {
    wrapper.vm.userStore.user.accentColor = '#fafafa';
    wrapper.vm.timeStartPos = 280;
    expect(wrapper.vm.nowLineStyle).toStrictEqual({
      top: '280px',
      'border-top': '#fafafa85 2px solid',
      'background-color': '#fafafa'
    });

    wrapper.vm.userStore.user.accentColor = undefined;
    wrapper.vm.timeStartPos = undefined;
    expect(wrapper.vm.nowLineStyle).toStrictEqual({
      top: '0px',
      'border-top': '#0606f785 2px solid',
      'background-color': '#0606f7'
    });
  });
});
