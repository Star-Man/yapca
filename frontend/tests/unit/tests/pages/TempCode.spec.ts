import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import TempCode from 'src/pages/TempCode.vue';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import setup from 'tests/unit/functions/setup';
import { getRouter } from 'vue-router-mock';
import logout from 'src/functions/auth/logout';
import { Notify } from 'quasar';
import type { SpyInstance } from 'vitest';

setup();

describe('TempCode.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let routerPush: SpyInstance;
  let notifySpy: SpyInstance;

  beforeEach(() => {
    vi.clearAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed');
    vi.mock('src/functions/auth/logout', () => ({
      default: vi.fn().mockImplementation(() => Promise.resolve({}))
    }));
    wrapper = shallowMount(TempCode);
    routerPush = vi.spyOn(wrapper.vm.router, 'push');
    notifySpy = vi.spyOn(Notify, 'create');
  });

  it('sets action if value passed as query', async () => {
    await getRouter().setQuery({ action: 'new-action' });
    expect(wrapper.vm.query.action).toStrictEqual('new-action');
  });

  it('displayName is undefined if no query', () => {
    expect(wrapper.vm.query.displayName).toStrictEqual(undefined);
  });

  it('sets tempCode if value passed as query', async () => {
    const validateTempCode = vi.spyOn(wrapper.vm, 'validateTempCode');
    wrapper.vm.tempCodeInput = '54321';
    await getRouter().setQuery({ code: '12345' });
    expect(wrapper.vm.tempCodeInput).toStrictEqual('12345');
    expect(validateTempCode).toHaveBeenCalledTimes(0);
  });

  it('sets username if value passed as query', async () => {
    const validateTempCode = vi.spyOn(wrapper.vm, 'validateTempCode');
    const validateInput = vi.spyOn(wrapper.vm.context, 'validateInput');
    wrapper.vm.usernameInput = 'oldName';
    await getRouter().setQuery({ username: 'newName' });
    expect(wrapper.vm.usernameInput).toStrictEqual('newName');
    expect(validateTempCode).toHaveBeenCalledTimes(0);
    expect(validateInput).toHaveBeenCalledTimes(1);
  });

  it('sets email if value passed as query', async () => {
    const validateTempCode = vi.spyOn(wrapper.vm, 'validateTempCode');
    const validateInput = vi.spyOn(wrapper.vm.context, 'validateInput');
    wrapper.vm.emailInput = 'oldemail@test.com';
    await getRouter().setQuery({ email: 'newemail@test.com' });
    expect(wrapper.vm.emailInput).toStrictEqual('newemail@test.com');
    expect(validateTempCode).toHaveBeenCalledTimes(0);
    expect(validateInput).toHaveBeenCalledTimes(1);
  });

  it('sets displayName if value passed as query', async () => {
    const validateTempCode = vi.spyOn(wrapper.vm, 'validateTempCode');
    await getRouter().setQuery({ displayName: 'new display name' });
    expect(wrapper.vm.query.displayName).toStrictEqual('new display name');
    expect(validateTempCode).toHaveBeenCalledTimes(0);
  });

  it('sends temp code if both username and code passed in query', async () => {
    const validateTempCode = vi.spyOn(wrapper.vm.context, 'validateTempCode');
    await getRouter().setQuery({
      username: 'newName',
      code: 'Y789B629G897R236F7U24F23F223R33S',
      action: 'test'
    });
    expect(wrapper.vm.isValid).toStrictEqual(true);
    expect(validateTempCode).toHaveBeenCalledTimes(1);
  });

  it('validateInput allows input validation', async () => {
    const validateTempCode = vi.spyOn(wrapper.vm.context, 'validateTempCode');
    await getRouter().setQuery({
      username: 'newName',
      code: '',
      action: 'test'
    });
    wrapper.vm.tempCodeInput = 'Y789B629G897R236F7U24F23F223R33Q';
    expect(wrapper.vm.isValid).toStrictEqual(true);
    await wrapper.vm.validateInput();
    expect(validateTempCode).toHaveBeenCalledTimes(1);
  });

  it('sends temp code if both email and code passed in query', async () => {
    const validateTempCode = vi.spyOn(wrapper.vm.context, 'validateTempCode');
    await getRouter().setQuery({
      email: 'newemail@test.com',
      code: 'Y789B629G897R236F7U24F23F223R33S',
      action: 'registration'
    });
    expect(wrapper.vm.isValid).toStrictEqual(true);
    expect(validateTempCode).toHaveBeenCalledTimes(1);
  });

  it('axiosFailed is called on send temp code fail', async () => {
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockRejectedValueOnce({ data: {} });
    await wrapper.vm.validateTempCode();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
    expect(wrapper.vm.checkingTempCode).toStrictEqual(false);
  });

  it('sets code to valid is axios success', async () => {
    wrapper.vm.codeValid = false;
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockResolvedValueOnce({ status: 200 });
    await wrapper.vm.validateTempCode();
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(wrapper.vm.codeValid).toStrictEqual(true);
  });

  it('failed otp setup shows error', async () => {
    const error = {
      response: { data: { error: 'Code/User is invalid or outdated' } }
    };
    const httpPut = vi.spyOn(wrapper.vm.basicStore.http, 'put');
    httpPut.mockRejectedValueOnce(error);
    await wrapper.vm.otpSetupComplete('12345');
    expect(axiosFailed).toHaveBeenCalledTimes(1);
    expect(wrapper.vm.resettingOtpSecret).toStrictEqual(false);
    expect(logout).toHaveBeenCalledTimes(1);
  });

  it('successful otp setup logs out user and shows snackbar message', async () => {
    const httpPut = vi.spyOn(wrapper.vm.basicStore.http, 'put');
    httpPut.mockResolvedValueOnce({});
    await wrapper.vm.otpSetupComplete('12345');
    expect(wrapper.vm.logout).toHaveBeenCalledTimes(2);
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(notifySpy).toHaveBeenCalledWith({
      message: 'TOTP Device Successfully Reset',
      type: 'positive'
    });
  });

  it('returns to login if action is login', async () => {
    await getRouter().setQuery({
      action: 'login'
    });
    routerPush.mockResolvedValueOnce(true);
    expect(routerPush).toHaveBeenCalledTimes(1);
    expect(routerPush).toHaveBeenCalledWith({ name: 'auth' });
  });
});
