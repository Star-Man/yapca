import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import CarePlansPage from 'src/pages/CarePlansPage.vue';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import setup from 'tests/unit/functions/setup';
import { formatISO, startOfToday, startOfYesterday } from 'date-fns';
import { mockCarePlans, mockCentres, mockClients } from '../../mocks';

setup();

describe('CarePlansPage.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.clearAllMocks();
    mockCentres();
    mockClients();
    mockCarePlans();
    vi.mock('src/functions/proxy/AxiosFailed');
    wrapper = shallowMount(CarePlansPage);
  });

  it('carePlans returns aggregation of total care plans', () => {
    expect(wrapper.vm.carePlans.length).toStrictEqual(12);
    expect(wrapper.vm.carePlans[0]).toStrictEqual({
      totalNeeds: 3,
      date: formatISO(startOfToday()),
      clientId: 1,
      clientName: 'test name 1 test surname 1'
    });
    expect(wrapper.vm.carePlans[1]).toStrictEqual({
      totalNeeds: 2,
      date: formatISO(startOfYesterday()),
      clientId: 1,
      clientName: 'test name 1 test surname 1'
    });
    expect(wrapper.vm.carePlans[2]).toStrictEqual({
      totalNeeds: 2,
      date: '2019-11-20',
      clientId: 2,
      clientName: 'test name 2 test surname 2'
    });
  });

  it('carePlans filers by active client needs', () => {
    wrapper.vm.clientStore.clientNeeds[2].isActive = false;
    expect(wrapper.vm.carePlans[0]).toStrictEqual({
      totalNeeds: 2,
      date: formatISO(startOfToday()),
      clientId: 1,
      clientName: 'test name 1 test surname 1'
    });
    expect(wrapper.vm.carePlans[1]).toStrictEqual({
      totalNeeds: 1,
      date: formatISO(startOfYesterday()),
      clientId: 1,
      clientName: 'test name 1 test surname 1'
    });
    expect(wrapper.vm.carePlans[2]).toStrictEqual({
      totalNeeds: 1,
      date: '2019-11-20',
      clientId: 2,
      clientName: 'test name 2 test surname 2'
    });
  });

  it('gets carePlans from server', async () => {
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');

    httpGet.mockResolvedValue({
      data: [
        {
          id: 10,
          date: '2011-12-12',
          need: 2,
          client: 4,
          inputted: true,
          comment: 'example comment 10',
          completed: false,
          clientNeed: {
            id: 3,
            need: 2,
            plan: 'test plan 3'
          }
        },
        {
          id: 11,
          date: '2011-12-14',
          need: 3,
          client: 4,
          inputted: true,
          comment: 'example comment 11',
          completed: false
        },
        {
          id: 12,
          date: '2011-12-12',
          need: 2,
          client: 4,
          inputted: true,
          comment: 'example comment 12',
          completed: false
        }
      ]
    });
    await wrapper.vm.getCarePlans();
    expect(httpGet).toHaveBeenCalledWith('carePlans/');
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(wrapper.vm.clientStore.carePlans).toStrictEqual({
      '10': {
        id: 10,
        date: '2011-12-12',
        need: 2,
        client: 4,
        inputted: true,
        comment: 'example comment 10',
        completed: false
      },
      '11': {
        id: 11,
        date: '2011-12-14',
        need: 3,
        client: 4,
        inputted: true,
        comment: 'example comment 11',
        completed: false
      },
      '12': {
        id: 12,
        date: '2011-12-12',
        need: 2,
        client: 4,
        inputted: true,
        comment: 'example comment 12',
        completed: false
      }
    });
    expect(wrapper.vm.clientStore.clientNeeds[3]).toStrictEqual({
      id: 3,
      need: 2,
      plan: 'test plan 3'
    });
  });

  it('getCarePlans functions throws axios error on get fail', async () => {
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    httpGet.mockRejectedValueOnce(null);
    await wrapper.vm.getCarePlans();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('openCarePlanModal does that', () => {
    wrapper.vm.showCarePlanDialog = false;
    wrapper.vm.selectedCarePlanAggregate = null;
    wrapper.vm.openCarePlanModal({
      clientId: 2,
      date: '2011-11-18',
      totalNeeds: 4
    });
    expect(wrapper.vm.selectedCarePlanAggregate).toStrictEqual({
      clientId: 2,
      date: '2011-11-18',
      totalNeeds: 4
    });
    expect(wrapper.vm.showCarePlanDialog).toStrictEqual(true);
  });

  it('closeDialog() closes dialog', () => {
    wrapper.vm.showCarePlanDialog = true;
    wrapper.vm.closeDialog();
    expect(wrapper.vm.showCarePlanDialog).toStrictEqual(false);
  });

  it('computes care plan title', () => {
    expect(wrapper.vm.carePlanTitle).toStrictEqual('Care Plans');
    wrapper.vm.openCarePlanModal({
      clientId: 2,
      date: '2011-11-18',
      totalNeeds: 4
    });
    expect(wrapper.vm.carePlanTitle).toStrictEqual(
      "test name 2's Care Plans For 18/11/11"
    );
  });
});
