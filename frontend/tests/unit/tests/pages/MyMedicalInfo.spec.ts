/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import MyMedicalInfo from 'src/pages/MyMedicalInfo.vue';
import setup from 'tests/unit/functions/setup';
import { nextTick } from 'vue';
import { mockClients } from '../../mocks';

setup();

describe('MyMedicalInfo.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  beforeEach(() => {
    vi.clearAllMocks();
    mockClients();
    wrapper = shallowMount(MyMedicalInfo);
  });

  it('dialog not visible with no activeClient', async () => {
    expect(await wrapper.find('#myMedicalInfoClientDialog').exists()).toBe(
      false
    );
    wrapper.vm.clientStore.activeClient = wrapper.vm.clientStore.clients[1];
    await nextTick();
    expect(await wrapper.find('#myMedicalInfoClientDialog').exists()).toBe(
      true
    );
  });
});
