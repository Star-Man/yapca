import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import ClientCreation from 'src/pages/ClientCreation.vue';
import setup from 'tests/unit/functions/setup';
import goToNextPage from 'src/functions/proxy/goToNextPage';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { nextTick } from 'vue';
import getCentreDataForClient from 'src/functions/proxy/getCentreDataForClient';
import { mockCentres, mockClients } from '../../mocks';
import type { SpyInstance } from 'vitest';

setup();

describe('ClientCreation.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let httpPatch: SpyInstance;

  beforeEach(() => {
    vi.clearAllMocks();
    mockCentres();
    mockClients();
    vi.mock('src/functions/proxy/AxiosFailed');
    vi.mock('src/functions/proxy/goToNextPage');
    vi.mock('src/functions/proxy/getCentreDataForClient');

    wrapper = shallowMount(ClientCreation);
    wrapper.vm.clientStore.setupClient.setupComplete = false;
    wrapper.vm.clientStore.setupClient.additionalInfoSetup = false;
    wrapper.vm.clientStore.setupClient.servicesSetup = false;
    wrapper.vm.clientStore.setupClient.visitingDaysSetup = false;
    wrapper.vm.clientStore.setupClient.abilitySetup = false;
    wrapper.vm.clientStore.setupClient.dietSetup = false;
    httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
  });

  it('updateStep() changes step value to passed value', () => {
    wrapper.vm.step = 1;
    wrapper.vm.updateStep(2);
    expect(wrapper.vm.step).toStrictEqual(2);
    expect(goToNextPage).toHaveBeenCalledTimes(0);
  });

  it('updateStep() calls getPolicies on step 7', () => {
    wrapper.vm.step = 0;
    wrapper.vm.context.getPolicies = vi.fn();
    wrapper.vm.updateStep(7);
    expect(wrapper.vm.context.getPolicies).toHaveBeenCalledTimes(1);
  });

  it('updateStep() goes to next page on step 8', async () => {
    wrapper.vm.step = 0;
    await wrapper.vm.updateStep(8);
    expect(goToNextPage).toHaveBeenCalledTimes(1);
  });

  it('updateStep() updates client on step 8', async () => {
    wrapper.vm.clientId = 3;
    wrapper.vm.step = 0;
    httpPatch.mockResolvedValueOnce(null);
    await wrapper.vm.updateStep(8);
    expect(httpPatch).toHaveBeenCalledTimes(1);
    expect(httpPatch).toHaveBeenCalledWith('clients/3/update/', {
      setupComplete: true
    });
  });

  it('updateStep() calls axiosFailed on server error', async () => {
    wrapper.vm.step = 0;
    httpPatch.mockRejectedValueOnce(null);
    await wrapper.vm.updateStep(8);
    expect(httpPatch).toHaveBeenCalledTimes(1);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('updateClientId() changes clientId value to passed value', () => {
    wrapper.vm.clientId = 3;
    wrapper.vm.updateClientId(1);
    expect(wrapper.vm.clientId).toStrictEqual(1);
  });

  it('clientName() returns client first and last name when clientId exists', () => {
    wrapper.vm.clientId = 4;
    expect(wrapper.vm.clientName).toStrictEqual('test name 4 test surname 4');
  });

  it("clientName() returns 'this client' when no clientId exists", () => {
    wrapper.vm.clientId = null;
    expect(wrapper.vm.clientName).toStrictEqual('this client');
  });

  it('clientVisitingDaysSet() returns false when no clientId exists', () => {
    wrapper.vm.clientId = null;
    expect(wrapper.vm.clientVisitingDaysSet).toStrictEqual(false);
  });

  it('clientVisitingDaysSet() returns true when all subbed centres have days', () => {
    wrapper.vm.clientId = 2;
    wrapper.vm.clientStore.clients[2].centres = [1, 2];
    wrapper.vm.clientStore.clients[2].visitingDays = [
      { centre: 1, day: 2 },
      { centre: 1, day: 4 },
      { centre: 2, day: 5 },
      { centre: 3, day: 5 }
    ];
    expect(wrapper.vm.clientVisitingDaysSet).toStrictEqual(true);
  });

  it('clientVisitingDaysSet() returns false when some subbed centres have no days', () => {
    wrapper.vm.clientId = 2;
    wrapper.vm.clientStore.clients[2].centres = [1, 2];
    wrapper.vm.clientStore.clients[2].visitingDays = [
      { centre: 1, day: 2 },
      { centre: 1, day: 4 },
      { centre: 3, day: 5 }
    ];
    expect(wrapper.vm.clientVisitingDaysSet).toStrictEqual(false);
  });

  it('updatePolicies() sends patch to server', async () => {
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    httpGet.mockResolvedValueOnce({});
    await wrapper.vm.context.updatePolicies(1);
    expect(httpGet).toHaveBeenCalledWith('clients/1/policies/agree/');
  });

  it('setVisitingDays() sends patch to server', async () => {
    wrapper.vm.clientId = 2;
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce(null);
    await wrapper.vm.setVisitingDays();
    expect(httpPatch).toHaveBeenCalledTimes(1);
    expect(httpPatch).toHaveBeenCalledWith('clients/2/update/', {
      visitingDaysSetup: true
    });
  });

  it('setVisitingDays() calls axiosFailed on server error', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockRejectedValueOnce(null);
    await wrapper.vm.setVisitingDays();
    expect(httpPatch).toHaveBeenCalledTimes(1);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('unagreedPolicies() returns list of unagreed policies', async () => {
    wrapper.vm.clientId = 2;
    wrapper.vm.centreStore.centres[1].policies = [3, 2, 600];
    wrapper.vm.clientStore.clients[2].centres = [1, 2];
    await nextTick();
    expect(wrapper.vm.unagreedPolicies).toStrictEqual([
      {
        centre: 1,
        unagreedPolicies: [
          { description: 'Test Policy 2', id: 2 },
          { description: 'Test Policy 3', id: 3 }
        ]
      }
    ]);
  });

  it('unagreedPolicies() returns empty array when no client', () => {
    wrapper.vm.clientId = 200;
    expect(wrapper.vm.unagreedPolicies).toStrictEqual([]);
  });

  it('acceptPolicies() calls updateStep', async () => {
    const updateStep = vi.spyOn(wrapper.vm.context, 'updateStep');
    const updatePolicies = vi.spyOn(wrapper.vm.context, 'updatePolicies');
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    httpGet.mockResolvedValueOnce(null);
    await wrapper.vm.acceptPolicies(1);
    expect(updateStep).toHaveBeenCalledWith(8);
    expect(updatePolicies).toHaveBeenCalledWith(1);
  });

  it('created() sets step to 1 by default', async () => {
    wrapper.vm.clientStore.setupClient = null;
    await wrapper.vm.created();
    expect(wrapper.vm.step).toStrictEqual(1);
  });

  it('created() sets step to 2 if setupClient exists', async () => {
    await wrapper.vm.created();
    expect(wrapper.vm.step).toStrictEqual(2);
    expect(wrapper.vm.clientId).toStrictEqual(1);
  });

  it('created() sets step to 3 if additionalInfoSetup is true', async () => {
    wrapper.vm.clientStore.setupClient.additionalInfoSetup = true;
    await wrapper.vm.created();
    expect(wrapper.vm.step).toStrictEqual(3);
    expect(wrapper.vm.clientId).toStrictEqual(1);
  });

  it('created() sets step to 4 if servicesSetup is true', async () => {
    wrapper.vm.clientStore.setupClient.servicesSetup = true;
    await wrapper.vm.created();
    expect(wrapper.vm.step).toStrictEqual(4);
    expect(wrapper.vm.clientId).toStrictEqual(1);
  });

  it('created() sets step to 5 if abilitySetup is true', async () => {
    wrapper.vm.clientStore.setupClient.abilitySetup = true;
    await wrapper.vm.created();
    expect(wrapper.vm.step).toStrictEqual(5);
    expect(wrapper.vm.clientId).toStrictEqual(1);
  });

  it('created() sets step to 6 if dietSetup is true', async () => {
    wrapper.vm.clientStore.setupClient.dietSetup = true;
    await wrapper.vm.created();
    expect(wrapper.vm.step).toStrictEqual(6);
    expect(wrapper.vm.clientId).toStrictEqual(1);
  });

  it('created() sets step to 7 if visitingDaysSetup is true', async () => {
    wrapper.vm.context.getPolicies = vi.fn();
    wrapper.vm.clientStore.setupClient.servicesSetup = true;
    wrapper.vm.clientStore.setupClient.visitingDaysSetup = true;
    await wrapper.vm.created();
    expect(wrapper.vm.step).toStrictEqual(7);
    expect(wrapper.vm.clientId).toStrictEqual(1);
    expect(wrapper.vm.context.getPolicies).toHaveBeenCalledTimes(1);
  });

  it('created() catches axios error', async () => {
    wrapper.vm.clientStore.setupClient.servicesSetup = true;
    wrapper.vm.clientStore.setupClient.visitingDaysSetup = true;
    wrapper.vm.context.getPolicies = vi
      .fn()
      .mockImplementation(() => Promise.reject({}));
    await wrapper.vm.created();
  });

  it('getPolicies() requests policies', () => {
    wrapper.vm.getPolicies();
    expect(getCentreDataForClient).toHaveBeenCalledTimes(2);
    expect(getCentreDataForClient).toHaveBeenCalledWith(1, 'policies');
  });

  it('getPolicies() does not request policies when no clientId', () => {
    wrapper.vm.clientId = null;
    wrapper.vm.getPolicies();
    expect(getCentreDataForClient).toHaveBeenCalledTimes(1);
  });

  it('setVisitingDaysAndSetStep7() sets step to 7', async () => {
    wrapper.vm.step = 1;
    const setVisitingDays = vi.spyOn(wrapper.vm.context, 'setVisitingDays');
    await wrapper.vm.setVisitingDaysAndSetStep7();
    expect(wrapper.vm.step).toStrictEqual(7);
    expect(setVisitingDays).toHaveBeenCalledTimes(1);
  });

  it('setVisitingDays returns null if no clientId', () => {
    wrapper.vm.clientId = null;
    expect(wrapper.vm.setVisitingDays()).toStrictEqual(null);
    expect(httpPatch).toHaveBeenCalledTimes(0);
  });
});
