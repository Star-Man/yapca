/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import ErrorPage from 'src/pages/ErrorPage.vue';
import logout from 'src/functions/auth/logout';
import setup from 'tests/unit/functions/setup';
import { mockUsers } from '../../mocks';

setup();

describe('ErrorPage.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  (window.history as { state: { errorMessage: string | undefined } }).state = {
    errorMessage: 'example error'
  };

  beforeEach(() => {
    vi.clearAllMocks();
    mockUsers();
    vi.mock('src/functions/auth/logout');
  });

  it('clicking button logs out user', async () => {
    wrapper = shallowMount(ErrorPage);
    wrapper.vm.loading = false;
    const logoutButton = wrapper.find('#logoutButton');
    await logoutButton.trigger('click', logoutButton);
    expect(wrapper.vm.loading).toStrictEqual(true);
    expect(logout).toHaveBeenCalledTimes(1);
  });

  it('error message is set from state', () => {
    wrapper = shallowMount(ErrorPage);
    expect(wrapper.find('#errorText').element.textContent).toStrictEqual(
      'example error'
    );
  });

  it('error message handles blank', () => {
    window.history.state.errorMessage = undefined;
    wrapper = shallowMount(ErrorPage);
    expect(wrapper.find('#errorText').element.textContent).toStrictEqual('');
  });
});
