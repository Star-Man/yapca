import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import Clients from 'src/pages/ClientsPage.vue';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import setup from 'tests/unit/functions/setup';
import { differenceInYears } from 'date-fns';
import { mockClients, mockUsers } from '../../mocks';
import type { SpyInstance } from 'vitest';

setup();

describe('ClientsPage.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let httpPatch: SpyInstance;

  beforeEach(() => {
    vi.clearAllMocks();
    mockUsers();
    mockClients();
    vi.mock('src/functions/proxy/AxiosFailed');
    wrapper = shallowMount(Clients);
    httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
  });

  it('clients() gets state clients as array', () => {
    expect(wrapper.vm.clients.length).toStrictEqual(10);
    expect(wrapper.vm.clients[0]).toStrictEqual({
      id: 1,
      admittedBy: 'Test User',
      isActive: true,
      gender: 'Female',
      createdDate: '26/06/20',
      agreedPolicies: [],
      centres: [1],
      visitingDays: [],
      firstName: 'test name 1',
      surname: 'test surname 1',
      basicInfoSetup: true,
      additionalInfoSetup: true,
      servicesSetup: true,
      setupComplete: true,
      abilitySetup: true,
      address: 'Test Address 1',
      visitingDaysSetup: true,
      phoneNumber: '11111111',
      dateOfBirth: `10/11/11 (${differenceInYears(
        new Date(),
        new Date(1911, 10, 11)
      )})`,
      nextOfKin: [],
      publicHealthNurseName: 'PHN 1',
      publicHealthNursePhoneNumber: 'PHN 1111111',
      homeHelpName: 'HH 1',
      homeHelpPhoneNumber: 'HH 1111111',
      gpName: 'GP 1',
      gpPhoneNumber: 'GP 1111111',
      chemistName: 'C 1',
      chemistPhoneNumber: 'C 1111111',
      medicalHistory: 'Client 1 Medical History',
      surgicalHistory: 'Client 1 Surgical History',
      medications: [],
      mobility: 'Walks Unaided',
      toileting: 'Incontinent',
      hygiene: 'Self',
      sight: 'Glasses',
      hearing: 'Normal',
      usesDentures: true,
      caseFormulation: 'Example Case Formulation 1',
      dietSetup: true,
      allergies: 'Allergies List 1',
      intolerances: 'Intolerances List 1',
      requiresAssistanceEating: true,
      thicknerGrade: 3,
      feedingRisks: [
        {
          id: 1,
          feedingRisk: 'Chewing'
        }
      ],
      needs: [],
      documents: [],
      storageSize: 60,
      reasonForInactivity: 'Deceased',
      accountStatus: 'No Account'
    });
  });

  it('clients() shows inactive users when toggled', () => {
    wrapper.vm.inActiveClientsShown = true;
    expect(wrapper.vm.clients.length).toStrictEqual(11);
  });

  it('clients() handles undefined centres', () => {
    wrapper.vm.clientStore.clients[8].centres = undefined;
    expect(wrapper.vm.clients.length).toStrictEqual(9);
  });

  it('clients() are reduced with less user centres', () => {
    wrapper.vm.userStore.user.centres = [];
    expect(wrapper.vm.clients.length).toStrictEqual(0);
  });

  it('updateIncludedHeaders() sets included headers to reverse of excluded', async () => {
    wrapper.vm.userStore.user.userClientTableHiddenRows = ['Date Added'];
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.includedHeaders.length).toStrictEqual(22);
  });

  it('updateIncludedHeaders() does nothing when no excluded headers', async () => {
    wrapper.vm.userStore.user.userClientTableHiddenRows = null;
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.includedHeaders.length).toStrictEqual(23);
  });

  it('createFilteredColumns() calls patch to server', async () => {
    wrapper.vm.userStore.user.userClientTableHiddenRows = ['Date Added'];
    httpPatch.mockResolvedValueOnce({});
    await wrapper.vm.createFilteredColumns(['Date Added']);
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPatch).toHaveBeenCalledWith('users/1/update/', {
      userClientTableHiddenRows:
        '["First Name","Surname","Admitted By","Gender","Date Added","Date Of Birth","Address","Phone Number","Home Help","Home Help Number","Public Health Nurse","Public Health Nurse Number","GP","GP Number","Chemist","Chemist Number","Mobility","Toileting","Hygiene","Sight","Hearing","Thickner Grade","Account Status"]'
    });
  });

  it('createFilteredColumns() calls axiosFailed() on failure', async () => {
    wrapper.vm.userStore.user.userClientTableHiddenRows = ['Date Added'];
    httpPatch.mockRejectedValueOnce({});
    await wrapper.vm.createFilteredColumns(['Date Added']);
    expect(httpPatch).toHaveBeenCalledTimes(1);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('createFilteredColumns() does nothing if no id', async () => {
    wrapper.vm.userStore.user.id = undefined;
    wrapper.vm.userStore.user.userClientTableHiddenRows = ['Address'];
    httpPatch.mockResolvedValueOnce({});
    await wrapper.vm.createFilteredColumns(['Address']);
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPatch).toHaveBeenCalledTimes(0);
  });

  it('openClientDialog() opens dialog and sets client id', async () => {
    wrapper.vm.selectedClientId = 1;
    wrapper.vm.showClientDialog = false;
    await wrapper.vm.openClientDialog({ id: 3 });
    expect(wrapper.vm.selectedClientId).toStrictEqual(3);
    expect(wrapper.vm.showClientDialog).toStrictEqual(true);
  });

  it('closeClientDialog() closes dialog', () => {
    wrapper.vm.showClientDialog = true;
    wrapper.vm.closeClientDialog();
    expect(wrapper.vm.showClientDialog).toStrictEqual(false);
  });

  it('customSort() sorts clients by store date value descending', () => {
    const items = [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }];
    const index = 'dateOfBirth';
    const sortedClients = wrapper.vm.customSort(items, index, false);
    expect(sortedClients).toStrictEqual([
      { id: 3 },
      { id: 1 },
      { id: 2 },
      { id: 4 }
    ]);
  });

  it('customSort() sorts clients by store date value ascending', () => {
    const items = [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }];
    const index = 'dateOfBirth';
    const sortedClients = wrapper.vm.customSort(items, index, true);
    expect(sortedClients).toStrictEqual([
      { id: 4 },
      { id: 2 },
      { id: 1 },
      { id: 3 }
    ]);
  });

  it('customSort() sorts clients alphabetically if no date value', () => {
    const items = [
      { id: 1, address: 'b' },
      { id: 2, address: 'd' },
      { id: 3, address: 'c' },
      { id: 4, address: 'a' }
    ];
    const index = 'address';
    const sortedClients = wrapper.vm.customSort(items, index, false);
    expect(sortedClients).toStrictEqual([
      { id: 4, address: 'a' },
      { id: 1, address: 'b' },
      { id: 3, address: 'c' },
      { id: 2, address: 'd' }
    ]);
  });

  it('customSort() sorts clients alphabetically descending if no date value', () => {
    const items = [
      { id: 1, address: 'b' },
      { id: 2, address: 'd' },
      { id: 3, address: 'c' },
      { id: 4, address: 'a' }
    ];
    const index = 'address';
    const sortedClients = wrapper.vm.customSort(items, index, true);
    expect(sortedClients).toStrictEqual([
      { id: 2, address: 'd' },
      { id: 3, address: 'c' },
      { id: 1, address: 'b' },
      { id: 4, address: 'a' }
    ]);
  });

  it('noDataMessage creates message when no clients', () => {
    wrapper.vm.clientStore.clients = {};
    expect(wrapper.vm.noDataMessage).toStrictEqual(
      'No clients have been added to your subscribed centres'
    );
  });

  it('noDataMessage creates message when clients filtered', () => {
    wrapper.vm.ageFilter = [900, 902];
    expect(wrapper.vm.noDataMessage).toStrictEqual(
      'No clients visible due to filters'
    );
  });

  it('setClientDialog shows or hides dialog', () => {
    wrapper.vm.setClientDialog(false);
    expect(wrapper.vm.showClientDialog).toStrictEqual(false);

    wrapper.vm.setClientDialog(true);
    expect(wrapper.vm.showClientDialog).toStrictEqual(true);
  });

  it('client header color changes based on client active', () => {
    wrapper.vm.selectedClientId = 4;
    expect(wrapper.vm.selectedClientHeaderColor).toStrictEqual('primary');
    wrapper.vm.clientStore.clients[4].isActive = false;
    expect(wrapper.vm.selectedClientHeaderColor).toStrictEqual('grey');
  });
});
