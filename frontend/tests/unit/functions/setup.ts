import { Notify } from 'quasar';
import { vi } from 'vitest';
import { WebSocket } from 'mock-socket';
import { installQuasarPlugin } from '@quasar/quasar-app-extension-testing-unit-vitest';
import { installPinia } from './installPinia';
import { installRouter } from './installRouter';
global.WebSocket = WebSocket;

export default () => {
  installQuasarPlugin({ plugins: { Notify } });
  installRouter({
    spy: {
      create: fn => vi.fn(fn),
      reset: spy => spy.mockReset()
    }
  });
  installPinia({ stubActions: false, createSpy: vi.fn });
};
