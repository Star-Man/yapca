import {
  editPlanDescriptionField,
  needsList,
  planEditButton
} from '../generic-selectors/Needs';

// Doing this because the e2e tests break multi selects for some illogical reason
const clickPlanEditButton = async (browser: TestController, index: number) => {
  let descriptionVisible = false;
  while (!descriptionVisible) {
    if (!(await needsList.visible)) {
      await browser.click(needsList);
    }
    await browser.click(planEditButton.nth(index));
    descriptionVisible = await editPlanDescriptionField.visible;
  }
};
export default clickPlanEditButton;
