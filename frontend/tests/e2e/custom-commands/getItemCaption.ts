export default (listItem: Selector): Promise<string> => {
  return listItem.find('.q-item__label--caption').textContent;
};
