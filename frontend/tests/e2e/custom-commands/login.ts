import { ClientFunction, t } from 'testcafe';
import { baseUrl } from '../globals';
import loginRequest from '../generic-functions/loginRequest';
import { usernameInput } from '../generic-selectors/Login';
import type Group from 'src/types/Group';
import type User from 'src/types/User';
import type LoginResponse from 'src/types/axios/LoginResponse';
import type ClientApp from '../types/ClientApp';

const backendDataLoaded = ClientFunction(() => {
  const app = document.querySelector('#q-app') as unknown as ClientApp;
  const globalProperties = app.__vue_app__!.config.globalProperties;
  const store = globalProperties.$pinia.state.value;
  return store.basic.backendDataLoaded;
});

const appLoaded = ClientFunction(() => {
  const app = document.querySelector('#q-app') as unknown as ClientApp;
  return !!app.__vue_app__;
});

const setStoreValues = ClientFunction(
  (loginData: { key: string; user: User }) => {
    const app = document.querySelector('#q-app') as unknown as ClientApp;
    const vueApp = app.__vue_app__!;
    const globalProperties = vueApp.config.globalProperties;
    const store = globalProperties.$pinia.state.value;
    store.user.user = loginData.user;
    const group = loginData.user.groups![0] as Group;
    store.user.user.group = group.name;
    store.basic.token = loginData.key;
    return globalProperties.$router.currentRoute.value.query.nextUrl || '/';
  }
);

const login = async (username: string): Promise<TestController> => {
  const loginResponse: LoginResponse = await loginRequest(
    username,
    'testPassword1'
  );
  while (!(await appLoaded())) {
    await new Promise(r => setTimeout(r, 50));
  }
  const nextUrl = await setStoreValues(loginResponse.data);
  while (!(await backendDataLoaded())) {
    await new Promise(r => setTimeout(r, 50));
  }
  return t
    .navigateTo(`${baseUrl}${nextUrl}`)
    .expect(usernameInput.visible)
    .notOk();
};

export const nurse = async (): Promise<TestController> => login('testNurse');

export const admin = async (): Promise<TestController> => login('testAdmin');

export const clientUser = async (): Promise<TestController> =>
  login('clientUser');
