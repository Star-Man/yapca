import { Selector } from 'testcafe';

export const subscribeToCentreDialog = Selector('#subscribeToCentreDialog');
export const firstNeedInput = Selector('#needInput-0');
export const needsSubmitButton = Selector('#needsSubmitButton');
