import { Selector } from 'testcafe';

export const mySettingsLink = Selector('#sidebar-title-user-settings');
export const logoutIcon = Selector('#sidebar-icon-logout');
export const topNavBarDrawerOpenButton = Selector('#headerMenuButton');
export const sidebar = Selector('#sidebar');
export const closeDialogButton = Selector('#closeDialogButton').filterVisible();
export const confirmButton = Selector('#confirmButton');
export const goBackButton = Selector('#goBackButton');
export const tableHeaderExpander = Selector(
  '.tableHeaderExpander'
).filterVisible();
export const accentColorSelectors = Selector('#colorInput').find(
  '.accentSelectButton'
);
export const responsiveTableHeaders = Selector(
  '.responsiveTableHeader'
).filterVisible();
export const selectorMenu = Selector('.q-menu').find('.q-item').filterVisible();
export const dialogSelectorMenu = Selector('.q-select__dialog')
  .find('.q-item')
  .filterVisible();
export const selectedStepHighlight = Selector('.q-stepper__tab--active').child(
  '.q-stepper__dot'
);
export const datePickerDay = Selector('.q-date__calendar-days').find('button');
export const datePickerNextMonth = Selector('.q-date__arrow')
  .find('button')
  .nth(1);
export const datePickerTitle = Selector('.q-date__view').find('.block').nth(0);
const datePickerNavOptions = Selector('.q-date__navigation')
  .filterVisible()
  .child('div');
export const datePickerYearOption = datePickerNavOptions.nth(4);
export const datePickerMonthOption = datePickerNavOptions.nth(1);
export const popupEditSaveButton = Selector('#popupEditSaveButton');
export const deleteConfirmButton = Selector('#deleteConfirmButton');
export const deleteConfirmDialog = Selector('#deleteConfirmDialog');
export const cardTitle = Selector('.cardTitle');
export const menuSelector = Selector('.q-menu').find('.q-item');
