import { Selector } from 'testcafe';

export const firstNameInput = Selector('#firstNameInput');
export const surnameInput = Selector('#surnameInput');
export const clientBasicInfoSubmit = Selector('#clientBasicInfoSubmit');
export const clientAdditionalInfoSubmit = Selector(
  '#clientAdditionalInfoSubmit'
);
export const centresList = Selector('#centresList').find('.v-list-item__title');
export const addressInput = Selector('#addressInput');
export const phoneNumberInput = Selector('#phoneNumberInput');
export const dateOfBirthInput = Selector('#dateOfBirthInput');
export const clientServicesSubmit = Selector('#clientServicesSubmit');
export const clientAbilitySubmit = Selector('#clientAbilitySubmit');
export const clientDietSubmit = Selector('#clientDietSubmit');
export const genderInput = Selector('#genderInput');
export const datePickerYearOptions = Selector('.v-date-picker-years').child(
  'li'
);
export const createNextOfKinSubmitButton = Selector(
  '#createNextOfKinSubmitButton'
);
