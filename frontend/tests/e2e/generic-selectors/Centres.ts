import { Selector } from 'testcafe';

export const deleteConfirmDialog = Selector('#deleteConfirmDocumentDialog');
export const deleteConfirmDialogGoBackButton = Selector(
  '#deleteConfirmDialogGoBackButton'
);
export const deleteConfirmDialogContinueButton = Selector(
  '#deleteConfirmButton'
);
export const centreMatchingColorDialog = Selector('#centreMatchingColorDialog');
export const centresTable = Selector('#centreListTable');
export const centresTableRows = centresTable.find('tbody').find('tr');
export const centresTableRow1 = centresTableRows.nth(0);
export const centresTableRow2 = centresTableRows.nth(1);
export const centresTableRow3 = centresTableRows.nth(2);
export const centresTableRow4 = centresTableRows.nth(3);
export const centresTableRow5 = centresTableRows.nth(4);
export const countrySelector = Selector('#countrySelector');
export const stateSelector = Selector('#stateSelector');
export const centreOpeningDaysButton = Selector('#centreOpeningDaysButton');
export const dayLabels = Selector('.q-calendar-month__day--label');
export const day2 = dayLabels.nth(7);
export const day3 = dayLabels.nth(8);
export const day5 = dayLabels.nth(10);
export const closeReasonInput = Selector('#closeReasonInput');
export const calendarPreviousButton = Selector('#calendarPreviousButton');
export const eventDeleteButton = Selector('.eventDeleteButton');
export const centreSelectButton = Selector('#centreSelectButton');
export const centresList = Selector('#centresList').find('.centreName');
export const closeModalButton = Selector('#centresCloseModalIcon');
export const centreCards = Selector('#centreDialog');
export const cardTitle = Selector('.cardTitle');
export const closeDialogIcon = Selector('#closeDialogButton').filterVisible();
export const centreSubSwitch = Selector('.subscribeSwitch');
export const centreExpanderColorSelect = Selector('#centreExpanderColorSelect');
export const centreExpanderOpeningDays = Selector('#centreExpanderOpeningDays');
export const needsExpander = Selector('#needsExpander');
export const needsTable = Selector('#needsTable');
export const policiesExpander = Selector('#policiesExpander');
export const policiesTable = Selector('#policiesTable');
export const centreExpanderLocationSelect = Selector(
  '#centreExpanderLocationSelect'
);
export const calendarTitle = Selector('#calendarTitle');
export const selectorMenu = Selector('.q-menu').find('.q-item');
export const events = Selector('.my-event');
