import { Selector } from 'testcafe';

export const carePlanDialog = Selector('#carePlanDialog');
export const aggregateCarePlansTable = Selector('#aggregateCarePlansTable');
export const aggregateCarePlansRows = aggregateCarePlansTable
  .find('tbody')
  .find('tr');
export const carePlanHistoryDialog = Selector('#carePlanHistoryDialog');
export const absenceHistoryDialog = Selector('#absenceHistoryDialog');
export const historyButton = Selector('#carePlanHistoryButton');
export const historyTable = Selector('#carePlanHistoryTable');
export const carePlanMenuIcon = Selector('#carePlanMenuIcon');
export const absenceButton = Selector('#absenceHistoryButton');
