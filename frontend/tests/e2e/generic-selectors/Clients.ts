import { Selector } from 'testcafe';

export const clientTableFilterColumnInput = Selector(
  '#clientTableFilterColumnInput'
);
export const clientsTableHeaders = Selector('#clientListTable').find('th');
export const clientsTableHeader1 = clientsTableHeaders.nth(0).child('span');
export const clientsTableHeader2 = clientsTableHeaders.nth(1).child('span');
export const clientsTableHeader3 = clientsTableHeaders.nth(2).child('span');
export const clientsTable = Selector('#clientListTable');
export const clientsTableRows = clientsTable.find('tbody').find('tr');
export const clientsGridRows = clientsTable.find('.responsiveTableGridCard');
export const clientsTableRow1 = clientsTableRows.nth(0);
export const clientsTableRow2 = clientsTableRows.nth(1);
export const clientsTableRow3 = clientsTableRows.nth(2);
export const clientsTableRow4 = clientsTableRows.nth(3);
export const visitingCentresEditIcon = Selector('#visitingCentresEditIcon');
export const clientCentreCards = Selector('.client-centre-card');
export const clientUnagreedPoliciesDialog = Selector(
  '#clientUnagreedPoliciesDialog'
);
export const daySelector = Selector('.daySelector').nth(0);
export const daySelectorTwo = Selector('.daySelector').nth(1);
export const daySelectorTue = daySelector.find('button').nth(0);
export const daySelectorWed = daySelector.find('button').nth(1);
export const daySelectorFri = daySelector.find('button').nth(2);
export const daySelectorTwoMon = daySelectorTwo.find('button').nth(0);
export const finishClientButtons = Selector('.finishClientButton');
export const clientDialogHeader = Selector('#clientDialog').find('.cardTitle');
export const createNextOfKinSubmitButton = Selector(
  '#createNextOfKinSubmitButton'
);
export const addNextOfKinButton = Selector('#addNextOfKinButton');
export const nextOfKinCreateNameField = Selector('#nextOfKinCreateNameField');
export const nextOfKinCreatePhoneNumberField = Selector(
  '#nextOfKinCreatePhoneNumberField'
);
export const medicationCreateNameField = Selector('#medicationCreateNameField');
export const medicationCreateDosageField = Selector(
  '#medicationCreateDosageField'
);
export const medicationCreateFrequencyField = Selector(
  '#medicationCreateFrequencyField'
);
export const dateFieldButtons = Selector('.v-date-picker-table').find('button');
export const addMedicationButton = Selector('#addMedicationButton');
export const createMedicationSubmitButton = Selector(
  '#createMedicationSubmitButton'
);
export const medicationsTable = Selector('#medicationsTable');
export const clientVisitingCentreHeaders = Selector(
  '#clientVisitingCentresExpander'
).find('.q-item');
export const deleteClientButton = Selector('#deleteClientButton');
export const noClientsMessage = clientsTable.find('.q-table__bottom');
export const clientHamburgerMenuIcon = Selector('#clientHamburgerMenuIcon');
export const clientFilterButton = Selector('#clientFilterButton');
export const clientUserPermissionsButton = Selector(
  '#clientUserPermissionsButton'
);
