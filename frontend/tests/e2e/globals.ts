/* eslint-disable @typescript-eslint/no-var-requires */
// eslint-disable-next-line unicorn/prefer-node-protocol
import { resolve } from 'path';
import { config } from 'dotenv';
import * as knex from 'knex';
import knexCleaner from 'knex-cleaner';
import createClientDocument from './generic-functions/createClientDocument';
import type { KnexCleanerOptions } from 'knex-cleaner';
import type { Knex } from 'knex';

config({
  path: resolve(
    __dirname,
    `../../../config/${process.env.YAPCA_ENV ?? 'local'}.env`
  )
});

let db: Knex | undefined = undefined;
const createClientDocuments = async () => {
  const documentUploads = [];
  const fileTypes = ['csv', 'png', 'pdf'];
  for (let i = 1; i <= 3; i += 1) {
    documentUploads.push(
      createClientDocument(
        4,
        `example_file_${i}.${fileTypes[i - 1]}`,
        `Example Document ${i}`,
        'a,b,c,d,e'.repeat(5 - i)
      ),
      createClientDocument(
        3,
        `example_file_${i}.${fileTypes[i - 1]}`,
        `Test Document ${i}`,
        'a,b'.repeat(5 - i)
      )
    );
  }
  return Promise.all(documentUploads);
};

export const seedDb = async (sections = ['base']): Promise<void> => {
  if (sections[0] === 'base') {
    if (!db?.client.pool) {
      db = knex.default({
        client: 'postgres',
        connection: {
          host: process.env.POSTGRES_HOST,
          user: process.env.POSTGRES_USER,
          password: process.env.POSTGRES_PASSWORD,
          database: process.env.POSTGRES_DB
        }
      });
    }
    const ignoreTables = [
      'auth_group',
      'centre_day',
      'authentication_userpermission'
    ];
    const cleanerOptions: KnexCleanerOptions = {
      mode: 'truncate',
      restartIdentity: true, // Used to tell PostgresSQL to reset the ID counter
      ignoreTables
    };
    await knexCleaner.clean(db, cleanerOptions);
  }
  const sectionPaths: string[] = [];
  sections.forEach(section => {
    if (section !== 'documents') {
      sectionPaths.push(`./tests/e2e/seeds/${section}`);
    }
  });
  await db!.seed.run({ directory: sectionPaths, sortDirsSeparately: true });
  if (sections.includes('documents')) {
    await createClientDocuments();
  }
};

export const destroyDb = async (): Promise<void> => db!.destroy();
export const database = (): Knex => db!;
export const baseUrl = `http://${process.env.FRONTEND_HOST ?? ''}:${
  process.env.FRONTEND_PORT ?? ''
}`;
export const backendUrl = `http://${process.env.VUE_APP_API_HOST ?? ''}:${
  process.env.VUE_APP_API_PORT ?? ''
}`;
