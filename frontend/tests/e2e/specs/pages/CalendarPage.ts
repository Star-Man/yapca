import { Selector } from 'testcafe';
import { addDays, format, startOfWeek } from 'date-fns';
import { baseUrl, destroyDb, seedDb } from '../../globals';
import {
  createCentreEvent,
  deleteEvent,
  explicitErrorHandler,
  getCentreByName,
  removeUserPermission,
  updateCentre,
  updateClient
} from '../../generic-functions';
import { clientUser, nurse, url } from '../../custom-commands';
import { logoutIcon } from '../../generic-selectors/Layout';
import {
  headerEvents,
  tueHeaderEvents
} from '../../generic-selectors/Calendar';

const daySectionHeaders = Selector('.q-calendar-day__head--weekday');
const daySections = Selector('.q-calendar-day__day');
const monHeaderEvents = headerEvents.nth(0).find('.centre-visitors-banner');
const wedHeaderEvents = headerEvents.nth(2).find('.centre-visitors-banner');
const thuHeaderEvents = headerEvents.nth(3).find('.centre-visitors-banner');
const friHeaderEvents = headerEvents.nth(4).find('.centre-visitors-banner');
const satHeaderEvents = headerEvents.nth(5).find('.centre-visitors-banner');

fixture`pages/Calendar`
  .clientScripts({ content: `(${explicitErrorHandler.toString()})()` })
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.navigateTo(`${baseUrl}/calendar`).resizeWindow(1280, 720);
    await nurse();
  })
  .after(destroyDb);

test('page can be accessed by clientUser', async browser => {
  await browser.click(logoutIcon);
  await clientUser();
  return browser
    .navigateTo(`${baseUrl}/calendar`)
    .expect(url())
    .eql(`${baseUrl}/calendar`);
});

test('page cannot be accessed by clientUser without permission', async browser => {
  await removeUserPermission('clientUser', 'client_can_view_calendar');

  await browser.click(logoutIcon);
  await clientUser();
  return browser
    .navigateTo(`${baseUrl}/calendar`)
    .expect(url())
    .eql(`${baseUrl}/my-medical-info`);
});

test('shows visiting users in banner on day', browser =>
  browser
    .expect(monHeaderEvents.count)
    .eql(1)
    .expect(monHeaderEvents.textContent)
    .contains('Example Centre 2 |')
    .expect(monHeaderEvents.textContent)
    .contains('1 visitor')
    .expect(monHeaderEvents.getStyleProperty('background-color'))
    .eql('rgb(211, 47, 47)')
    .expect(tueHeaderEvents.count)
    .eql(1)
    .expect(tueHeaderEvents.textContent)
    .contains('Example Centre |')
    .expect(tueHeaderEvents.textContent)
    .contains('2 visitors')
    .expect(tueHeaderEvents.getStyleProperty('background-color'))
    .eql('rgb(0, 151, 167)')
    .expect(wedHeaderEvents.count)
    .eql(1)
    .expect(wedHeaderEvents.textContent)
    .contains('Example Centre |')
    .expect(wedHeaderEvents.textContent)
    .contains('2 visitors')
    .expect(wedHeaderEvents.getStyleProperty('background-color'))
    .eql('rgb(0, 151, 167)')
    .expect(wedHeaderEvents.count)
    .eql(1)
    .expect(thuHeaderEvents.count)
    .eql(1)
    .expect(friHeaderEvents.count)
    .eql(1)
    .expect(friHeaderEvents.textContent)
    .contains('Example Centre |')
    .expect(friHeaderEvents.textContent)
    .contains('1 visitor')
    .expect(friHeaderEvents.getStyleProperty('background-color'))
    .eql('rgb(0, 151, 167)')
    .expect(satHeaderEvents.count)
    .eql(0));

test('calendar only shows days that are part of users centres', async browser =>
  browser
    .expect(daySections.count)
    .eql(6)
    .expect(daySectionHeaders.count)
    .eql(6)
    .expect(daySectionHeaders.nth(0).textContent)
    .eql('Monday')
    .expect(daySectionHeaders.nth(1).textContent)
    .eql('Tuesday')
    .expect(daySectionHeaders.nth(2).textContent)
    .eql('Wednesday')
    .expect(daySectionHeaders.nth(3).textContent)
    .eql('Thursday')
    .expect(daySectionHeaders.nth(4).textContent)
    .eql('Friday')
    .expect(daySectionHeaders.nth(5).textContent)
    .eql('Saturday'));

test('calendar days update when changed elsewhere', async browser => {
  await updateCentre('Example Centre 2', { openingDays: [2, 5] });
  return browser
    .expect(daySections.count)
    .eql(4)
    .expect(daySectionHeaders.count)
    .eql(4)
    .expect(daySectionHeaders.nth(0).textContent)
    .eql('Tuesday')
    .expect(daySectionHeaders.nth(1).textContent)
    .eql('Wednesday')
    .expect(daySectionHeaders.nth(2).textContent)
    .eql('Friday')
    .expect(daySectionHeaders.nth(3).textContent)
    .eql('Saturday');
});

test('visiting days update when changed elsewhere', async browser => {
  const [centre1] = await getCentreByName('Example Centre');
  await updateClient('1', {
    visitingDays: [{ centre: centre1.id, day: 5 }]
  });
  return browser.expect(friHeaderEvents.textContent).contains('2 visitors');
});

test('calendar color updates when changed elsewhere', async browser => {
  await updateCentre('Example Centre', {
    color: '#B0BEC5'
  });
  return browser
    .expect(await tueHeaderEvents.nth(0).getStyleProperty('background-color'))
    .eql('rgb(176, 190, 197)');
});

test('visiting days change on close event creation', async browser => {
  await updateCentre('Example Centre 2', { openingDays: [1, 4, 5, 6] });
  const [centre2] = await getCentreByName('Example Centre 2');
  await updateClient(
    '5',
    {
      visitingDays: [{ centre: centre2.id, day: 5 }]
    },
    'testNurse'
  );
  await browser.expect(friHeaderEvents.count).eql(2);

  const friday = format(
    addDays(startOfWeek(Date.now(), { weekStartsOn: 1 }), 4),
    "yyyy-MM-dd'T'HH:mm:ss'Z'"
  );
  await createCentreEvent({
    centre: centre2.id,
    name: 'Example Close Event 1',
    start: friday,
    timePeriod: 86400,
    recurrences: 'yearly',
    eventType: 'User Close Event',
    allDay: true
  });
  await browser.expect(friHeaderEvents.count).eql(1);
  await deleteEvent('Example Close Event 1');
  return browser.expect(friHeaderEvents.count).eql(2);
});
