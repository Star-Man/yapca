import { Selector } from 'testcafe';
import { logoutIcon } from '../../generic-selectors/Layout';
import { baseUrl, database, destroyDb, seedDb } from '../../globals';
import { inputValidationMessage, url } from '../../custom-commands';
import {
  changeClientType,
  changeRegisteringAbility,
  setupTOTP
} from '../../generic-functions';
import {
  confirmPasswordInput,
  displayNameInput,
  emailInput,
  loginForm,
  passwordInput,
  registerButton,
  returnToServerSetupButton,
  returnToServerSetupDialog,
  usernameInput
} from '../../generic-selectors/Auth';
import {
  centreSelectButton,
  centresList
} from '../../generic-selectors/Centres';
import { clientsTable } from '../../generic-selectors/Clients';

const registerTab = Selector('#registerTab');
const loginTab = Selector('#loginTab');

const registerForm = Selector('#registerForm');

fixture`pages/AuthPage Setup Database`
  .beforeEach(async browser => {
    await seedDb();
    await browser.navigateTo(`${baseUrl}/auth`);
  })
  .after(destroyDb);

test('non registered urls are directed to main page when logged out', browser =>
  browser.navigateTo(`${baseUrl}/fakeurl`).expect(loginTab.visible).ok());

test('register form is removed when registerAbility disabled', async browser => {
  await changeRegisteringAbility(true);
  await browser.eval(() => location.reload());
  await browser
    .expect(registerTab.visible)
    .ok()
    .expect(registerForm.visible)
    .ok()
    .expect(loginTab.visible)
    .notOk()
    .expect(loginForm.visible)
    .notOk();
  await changeRegisteringAbility(false);
  await browser.eval(() => location.reload());
  await browser
    .expect(registerTab.exists)
    .notOk()
    .expect(registerForm.exists)
    .notOk()
    .expect(loginTab.visible)
    .ok()
    .expect(loginForm.visible)
    .ok();
});

test('returnToServerSetupButton button not visible for non-app user', async browser =>
  browser.expect(returnToServerSetupButton.visible).notOk());

test('returnToServerSetupButton button visible for electron user', async browser => {
  await browser.expect(loginTab.visible).ok();
  await changeClientType('electron');
  return browser.expect(returnToServerSetupButton.visible).ok();
});

test('returnToServerSetupButton button visible for android user', async browser => {
  await changeClientType('mobile');
  return browser.expect(returnToServerSetupButton.visible).ok();
});

test('clicking returnToServerSetupButton opens dialog', async browser => {
  await changeClientType('electron');
  return browser
    .expect(returnToServerSetupDialog.visible)
    .notOk()
    .click(returnToServerSetupButton)
    .expect(returnToServerSetupDialog.visible)
    .ok();
});

test('email input shows error when email taken', async browser => {
  await changeRegisteringAbility(true);
  await browser.eval(() => location.reload());
  await browser
    .typeText(usernameInput, 'newUser')
    .expect(emailInput.hasAttribute('disabled'))
    .notOk()
    .typeText(emailInput, 'testemail@test.com')
    .typeText(displayNameInput, 'New User')
    .typeText(passwordInput, 'testPassword1')
    .typeText(confirmPasswordInput, 'testPassword1');
  await setupTOTP(browser);
  return browser
    .click(registerButton)
    .expect(inputValidationMessage(emailInput))
    .eql('A user is already registered with this e-mail address.');
});

test('email input shows error when invalid email', async browser => {
  await changeRegisteringAbility(true);
  await browser.eval(() => location.reload());
  return browser
    .typeText(emailInput, 'notanemail')
    .expect(inputValidationMessage(emailInput))
    .eql('Invalid Email Address');
});

test('email input shows error missing', async browser => {
  await changeRegisteringAbility(true);
  await browser.eval(() => location.reload());
  return browser
    .typeText(emailInput, 'testemail@test.com')
    .selectText(emailInput)
    .pressKey('delete')
    .expect(inputValidationMessage(emailInput))
    .eql('Email is required');
});

test('registering first user disables registration from auth page', async browser => {
  await changeRegisteringAbility(true);
  await seedDb(['centres']);
  await database().raw('TRUNCATE TABLE authentication_customuser CASCADE');
  await browser.eval(() => location.reload());
  await browser
    .typeText(usernameInput, 'newUser')
    .typeText(emailInput, 'test@test.com')
    .typeText(displayNameInput, 'New User')
    .typeText(passwordInput, 'testPassword1')
    .typeText(confirmPasswordInput, 'testPassword1');
  await setupTOTP(browser);
  await browser
    .click(registerButton)
    .click(centresList.nth(2))
    .click(centreSelectButton);
  return browser
    .expect(clientsTable.visible)
    .ok()
    .expect(url())
    .eql(`${baseUrl}/clients`)
    .click(logoutIcon)
    .expect(registerForm.exists)
    .notOk()
    .expect(loginForm.visible)
    .ok();
});
