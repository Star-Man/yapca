import { formatISO, startOfToday } from 'date-fns';
import { baseUrl, destroyDb, seedDb } from '../../globals';
import {
  createCarePlans,
  deleteClient,
  getClientByName,
  getNeedByDescription,
  goBack,
  goForward,
  removeUserCentre,
  updateCarePlan,
  updateCentre,
  updateClientNeed
} from '../../generic-functions';
import {
  aggregateCarePlansRows,
  aggregateCarePlansTable,
  carePlanDialog
} from '../../generic-selectors/CarePlans';
import { clientUser, nurse, url } from '../../custom-commands';
import { cardTitle, logoutIcon } from '../../generic-selectors/Layout';

const headers = aggregateCarePlansTable.find('th');
const noDataLabel = aggregateCarePlansTable.find('.q-table__bottom');

fixture`pages/CarePlansPage`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.navigateTo(baseUrl);
    return nurse();
  })
  .after(destroyDb);

test('page cannot be accessed by clientUser', async browser => {
  await browser.click(logoutIcon);
  await clientUser();
  return browser
    .navigateTo(`${baseUrl}/carePlans`)
    .expect(url())
    .eql(`${baseUrl}/my-medical-info`);
});

test('title is correct', async browser =>
  browser
    .navigateTo(`${baseUrl}/carePlans`)
    .expect(cardTitle.textContent)
    .eql('Care Plans'));

test('table has correct headers', async browser =>
  browser
    .navigateTo(`${baseUrl}/carePlans`)
    .expect(headers.count)
    .eql(3)
    .expect(headers.nth(0).textContent)
    .eql('Date')
    .expect(headers.nth(1).textContent)
    .eql('Client')
    .expect(headers.nth(2).textContent)
    .eql('Needs Remaining'));

test('table has correct data', async browser => {
  await createCarePlans();
  return browser
    .navigateTo(`${baseUrl}/carePlans`)
    .expect(aggregateCarePlansRows.count)
    .eql(6)
    .expect(aggregateCarePlansRows.nth(0).find('td').nth(0).textContent)
    .eql('Today')
    .expect(aggregateCarePlansRows.nth(0).find('td').nth(1).textContent)
    .eql('Test Client 4')
    .expect(aggregateCarePlansRows.nth(0).find('td').nth(2).textContent)
    .eql('2')
    .expect(aggregateCarePlansRows.nth(1).find('td').nth(0).textContent)
    .eql('Yesterday')
    .expect(aggregateCarePlansRows.nth(1).find('td').nth(2).textContent)
    .eql('1')
    .expect(aggregateCarePlansRows.nth(3).find('td').nth(0).textContent)
    .eql('20/11/19')
    .expect(aggregateCarePlansRows.nth(3).find('td').nth(2).textContent)
    .eql('2')
    .expect(aggregateCarePlansRows.nth(4).find('td').nth(0).textContent)
    .eql('18/11/19')
    .expect(aggregateCarePlansRows.nth(4).find('td').nth(1).textContent)
    .eql('Test Client 5')
    .expect(aggregateCarePlansRows.nth(4).find('td').nth(2).textContent)
    .eql('3');
});

test('table can be sorted by date', async browser => {
  await createCarePlans();
  return browser
    .navigateTo(`${baseUrl}/carePlans`)
    .click(headers.nth(0))
    .click(headers.nth(0))
    .expect(aggregateCarePlansRows.nth(0).find('td').nth(0).textContent)
    .eql('18/11/18')
    .expect(aggregateCarePlansRows.nth(5).find('td').nth(0).textContent)
    .eql('Today');
});

test('table can be sorted by client name', async browser => {
  await createCarePlans();
  return browser
    .navigateTo(`${baseUrl}/carePlans`)
    .click(headers.nth(1))
    .expect(aggregateCarePlansRows.nth(0).find('td').nth(1).textContent)
    .eql('Test Client 4')
    .expect(aggregateCarePlansRows.nth(3).find('td').nth(1).textContent)
    .eql('Test Client 5')
    .click(headers.nth(1))
    .expect(aggregateCarePlansRows.nth(0).find('td').nth(1).textContent)
    .eql('Test Client 5')
    .expect(aggregateCarePlansRows.nth(3).find('td').nth(1).textContent)
    .eql('Test Client 4');
});

test('table can be sorted by remaining needs', async browser => {
  await createCarePlans();
  return browser
    .navigateTo(`${baseUrl}/carePlans`)
    .click(headers.nth(2))
    .expect(aggregateCarePlansRows.nth(0).find('td').nth(2).textContent)
    .eql('1')
    .expect(aggregateCarePlansRows.nth(5).find('td').nth(2).textContent)
    .eql('3')
    .click(headers.nth(2))
    .expect(aggregateCarePlansRows.nth(0).find('td').nth(2).textContent)
    .eql('3')
    .expect(aggregateCarePlansRows.nth(5).find('td').nth(2).textContent)
    .eql('1');
});

test('table shows message when no data', async browser =>
  browser
    .navigateTo(`${baseUrl}/carePlans`)
    .expect(noDataLabel.textContent)
    .eql('There are no care plans left to complete!'));

test('clicking row opens care plan', async browser => {
  await createCarePlans();
  return browser
    .navigateTo(`${baseUrl}/carePlans`)
    .expect(carePlanDialog.visible)
    .notOk()
    .click(aggregateCarePlansRows.nth(1))
    .expect(carePlanDialog.visible)
    .ok();
});

test('deleting client removes rows', async browser => {
  await createCarePlans();
  await browser.navigateTo(`${baseUrl}/carePlans`);
  await deleteClient('4');
  return browser
    .expect(aggregateCarePlansRows.count)
    .eql(3)
    .expect(aggregateCarePlansRows.nth(0).find('td').nth(1).textContent)
    .eql('Test Client 5');
});

test('value updates when inputted set to true for care plan', async browser => {
  await createCarePlans();
  await browser.navigateTo(`${baseUrl}/carePlans`);
  const clientData = await getClientByName('4');
  const { clientNeedId } = clientData.find(
    (client: { need: string }) => client.need === 'Example Need 1'
  )!;
  await updateCarePlan(clientNeedId, formatISO(startOfToday()), {
    inputted: true
  });
  return browser
    .expect(aggregateCarePlansRows.nth(0).find('td').nth(2).textContent)
    .eql('1');
});

test('value updates when inputted set to false for care plan', async browser => {
  await createCarePlans();
  await browser.navigateTo(`${baseUrl}/carePlans`);
  const clientData = await getClientByName('4');
  const clientNeed1Id = clientData.find(
    (client: { need: string }) => client.need === 'Example Need 1'
  )!.clientNeedId;
  const clientNeed3Id = clientData.find(
    (client: { need: string }) => client.need === 'Example Need 3'
  )!.clientNeedId;
  await updateCarePlan(clientNeed1Id, formatISO(startOfToday()), {
    inputted: true
  });
  await browser.expect(aggregateCarePlansRows.count).eql(6);
  await updateCarePlan(clientNeed3Id, formatISO(startOfToday()), {
    inputted: true
  });
  return browser
    .expect(aggregateCarePlansRows.count)
    .eql(5)
    .expect(aggregateCarePlansRows.nth(0).find('td').nth(0).textContent)
    .eql('Yesterday');
});

test('need disappears when deleted from centre', async browser => {
  await createCarePlans();
  await browser.navigateTo(`${baseUrl}/carePlans`);
  const [exampleNeed3] = await getNeedByDescription('Example Need 3');
  await updateCentre('Example Centre', { needs: [exampleNeed3.id] });
  return browser
    .expect(aggregateCarePlansRows.nth(0).find('td').nth(2).textContent)
    .eql('1');
});

test('needs update when client need removed', async browser => {
  await createCarePlans();
  await browser
    .navigateTo(`${baseUrl}/carePlans`)
    .expect(aggregateCarePlansRows.nth(0).find('td').nth(2).textContent)
    .eql('2');
  await updateClientNeed('Example Plan 1', '4', { isActive: false });
  return browser
    .expect(aggregateCarePlansRows.nth(0).find('td').nth(2).textContent)
    .eql('1');
});

test('needs update when client need readded', async browser => {
  await createCarePlans();
  await browser.navigateTo(`${baseUrl}/carePlans`);
  await updateClientNeed('Example Plan 1', '4', { isActive: false });
  await updateClientNeed('Example Plan 1', '4', { isActive: true });
  return browser
    .expect(aggregateCarePlansRows.nth(0).find('td').nth(2).textContent)
    .eql('2');
});

test('pressing back while in care plan dialog closes dialog', async browser => {
  await createCarePlans();
  await browser
    .navigateTo(`${baseUrl}/carePlans`)
    .click(aggregateCarePlansRows.nth(1));
  await goBack();
  await browser.expect(carePlanDialog.visible).notOk();
  await goForward();
  return browser.expect(carePlanDialog.visible).ok();
});

test('care plans aggregate rows disappear when user unsubs from centre', async browser => {
  await createCarePlans();
  await browser.navigateTo(`${baseUrl}/carePlans`);
  await removeUserCentre('testNurse', 'Example Centre');
  await browser.expect(aggregateCarePlansRows.count).eql(3);
  await removeUserCentre('testNurse', 'Example Centre 4');
  return browser.expect(aggregateCarePlansRows.count).eql(1);
});
