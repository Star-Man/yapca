import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../globals';
import { getUserByName, setupTOTP, updateUser } from '../../generic-functions';
import {
  closeDialogButton,
  mySettingsLink
} from '../../generic-selectors/Layout';
import {
  admin,
  clientUser,
  inputLabel,
  inputValidationMessage,
  nurse
} from '../../custom-commands';
import {
  emailChangeForm,
  newEmailInput,
  openChangeEmailDialogButton,
  openChangePasswordDialogButton,
  openDisableTOTPDialog,
  otpTitleRow,
  passwordChangeForm
} from '../../generic-selectors/Auth';
import { newPasswordInput } from '../../generic-selectors/ChangePassword';

const sidebar = Selector('#sidebar');
const clientsLink = Selector('#sidebar-title-clients');
const colorThemeOptions =
  Selector('#themeColorInput').find('.themeSelectButton');
const lightThemeOption = colorThemeOptions.nth(0);
const darkThemeOption = colorThemeOptions.nth(1);
const accentOptions = Selector('#colorInput').find('.accentSelectButton');
const purpleAccentOption = accentOptions.nth(4);
const orangeAccentOption = accentOptions.nth(13);
const mySettingsIcon = Selector('#sidebar-icon-user-settings');
const selectedTheme = Selector('#themeColorInput')
  .find('span.q-icon')
  .parent()
  .parent();
const selectedAccentColor = Selector('#colorInput')
  .find('span.q-icon')
  .parent()
  .parent();
const displayNameInput = Selector('#displayNameInput');
const window = Selector('body');
const openSetupTOTPDialog = Selector('#openSetupTOTPDialog');

fixture`views/User`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres']);
    await browser
      .navigateTo(`${baseUrl}/user-settings`)
      .resizeWindow(1280, 720);
  })
  .after(destroyDb);

test('sidebar is visible', async browser => {
  await admin();
  return browser.expect(sidebar.visible).ok();
});

test('theme options are visible', async browser => {
  await admin();
  return browser.expect(colorThemeOptions.count).eql(2);
});

test('accent options are visible', async browser => {
  await admin();
  return browser.expect(accentOptions.count).eql(26);
});

test('page theme changes when dark theme is selected', async browser => {
  await admin();
  return browser
    .click(darkThemeOption)
    .expect(window.getStyleProperty('background-color'))
    .eql('rgb(18, 18, 18)');
});

test('page theme changes when light theme is selected', async browser => {
  await admin();
  return browser
    .click(darkThemeOption)
    .click(lightThemeOption)
    .expect(window.getStyleProperty('background-color'))
    .eql('rgba(0, 0, 0, 0)');
});

test('text color changes when light theme is selected', async browser => {
  await admin();
  return browser
    .click(darkThemeOption)
    .click(lightThemeOption)
    .expect(clientsLink.getStyleProperty('color'))
    .eql('rgb(0, 0, 0)');
});

test('text color changes when dark theme is selected', async browser => {
  await admin();
  return browser
    .click(darkThemeOption)
    .expect(clientsLink.getStyleProperty('color'))
    .eql('rgb(255, 255, 255)');
});

test('avatar color changes when accent is selected', async browser => {
  await nurse();
  return browser
    .click(purpleAccentOption)
    .expect(mySettingsIcon.getStyleProperty('background-color'))
    .eql('rgb(81, 45, 168)')
    .click(orangeAccentOption)
    .expect(mySettingsIcon.getStyleProperty('background-color'))
    .eql('rgb(230, 74, 25)');
});

test('correct theme is selected on load', async browser => {
  await admin();
  return browser
    .expect(selectedTheme.getStyleProperty('background-color'))
    .eql('rgb(255, 255, 255)');
});

test('correct accent is selected on load', async browser => {
  await admin();
  return browser
    .expect(selectedAccentColor.getStyleProperty('background-color'))
    .eql('rgb(211, 47, 47)');
});

test('updated theme is reflected in database', async browser => {
  await admin();
  return browser
    .click(darkThemeOption)
    .then(() =>
      getUserByName('testAdmin').then(user =>
        browser.expect(user[0].darkTheme).eql(true)
      )
    );
});

test('updated accent color is reflected in database', async browser => {
  await admin();
  return browser
    .click(orangeAccentOption)
    .then(() =>
      getUserByName('testAdmin').then(user =>
        browser.expect(user[0].accentColor).eql('#E64A19')
      )
    );
});

test('page updates when theme changed elsewhere', async browser => {
  await admin();
  await updateUser('testAdmin', { darkTheme: true });
  await browser
    .expect(window.getStyleProperty('background-color'))
    .eql('rgb(18, 18, 18)');
});

test('selected theme updates when updated elsewhere', async browser => {
  await admin();
  await browser
    .navigateTo(`${baseUrl}/user-settings`)
    .expect(selectedTheme.visible)
    .ok();
  await updateUser('testAdmin', { darkTheme: true });
  await browser
    .expect(selectedTheme.getStyleProperty('background-color'))
    .eql('rgb(30, 30, 30)');
});

test('selected accent color updates when updated elsewhere', async browser => {
  await admin();
  await updateUser('testAdmin', { accentColor: '#388E3C' });
  await browser
    .expect(selectedAccentColor.getStyleProperty('background-color'))
    .eql('rgb(56, 142, 60)');
});

test('display name input is visible', async browser => {
  await nurse();
  return browser
    .expect(displayNameInput.visible)
    .ok()
    .expect(inputLabel(displayNameInput))
    .eql('Display Name');
});

test('display name input updates database on enter key', async browser => {
  await nurse();
  return browser
    .selectText(displayNameInput)
    .pressKey('delete')
    .typeText(displayNameInput, 'New Display Name')
    .pressKey('enter')
    .then(() =>
      getUserByName('testNurse').then(user =>
        browser.expect(user[0].displayName).eql('New Display Name')
      )
    );
});

test('display name input updates database on unfocus', async browser => {
  await nurse();
  return browser
    .selectText(displayNameInput)
    .pressKey('delete')
    .typeText(displayNameInput, 'New Display Name 1')
    .click(selectedTheme)
    .then(() =>
      getUserByName('testNurse').then(user =>
        browser.expect(user[0].displayName).eql('New Display Name 1')
      )
    );
});

test('display name input shows current user display name on load', async browser => {
  await nurse();
  return browser.expect(displayNameInput.value).eql('Test Nurse');
});

test('updating display name updates name in menu', async browser => {
  await nurse();
  return browser
    .selectText(displayNameInput)
    .pressKey('delete')
    .typeText(displayNameInput, 'New Display Name 2')
    .pressKey('enter')
    .expect(mySettingsLink.textContent)
    .eql('New Display Name 2');
});

test('display name input shows warning on empty', async browser => {
  await nurse();
  return browser
    .selectText(displayNameInput)
    .pressKey('delete')
    .expect(inputValidationMessage(displayNameInput))
    .eql("Display Name can't be blank");
});

test("display name input doesn't update name when empty", async browser => {
  await nurse();
  return browser
    .selectText(displayNameInput)
    .pressKey('delete')
    .pressKey('enter')
    .then(() =>
      getUserByName('testNurse').then(user =>
        browser.expect(user[0].displayName).eql('Test Nurse')
      )
    );
});

test('display name input updates when user updated elsewhere', async browser => {
  await nurse();
  await updateUser('testNurse', { displayName: 'New Display Name 5' });
  await browser.expect(displayNameInput.value).eql('New Display Name 5');
});

test('can open change email dialog', async browser => {
  await nurse();
  await browser
    .click(openChangeEmailDialogButton)
    .expect(emailChangeForm.visible)
    .ok()
    .typeText(newEmailInput, 'a')
    .click(closeDialogButton)
    .expect(emailChangeForm.visible)
    .notOk();
});

test('can open change password dialog', async browser => {
  await nurse();
  await browser
    .click(openChangePasswordDialogButton)
    .expect(passwordChangeForm.visible)
    .ok()
    .typeText(newPasswordInput, 'a')
    .click(closeDialogButton)
    .expect(passwordChangeForm.visible)
    .notOk();
});

test('totp buttons are not visible for nurse', async browser => {
  await nurse();
  await browser
    .expect(openDisableTOTPDialog.visible)
    .notOk()
    .expect(openSetupTOTPDialog.visible)
    .notOk();
});

test('client user can setup otp', async browser => {
  await seedDb(['clients']);
  await clientUser();
  await browser
    .click(openSetupTOTPDialog)
    .expect(otpTitleRow.textContent)
    .notContains(
      "YAPCA requires that users with access to multiple clients' data"
    )
    .expect(otpTitleRow.textContent)
    .contains('YAPCA supports optional multi-factor authentication');
  await setupTOTP(browser);
  const user = (await getUserByName('clientUser'))[0];
  return browser.expect(user.otpDeviceKey?.length).eql(32);
});
