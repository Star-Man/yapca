import { Selector } from 'testcafe';
import { selectorMenu } from '../../../generic-selectors/Layout';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  getUserByName,
  setClientHiddenHeaders
} from '../../../generic-functions';
import { inputHint, inputLabel, nurse } from '../../../custom-commands';
import {
  clientFilterButton,
  clientTableFilterColumnInput,
  clientsTableHeaders
} from '../../../generic-selectors/Clients';
import setSelector from '../../../custom-commands/setSelectorMenuValue';
const menuSelector = Selector('.q-menu').find('.q-item');

fixture`pages/clients Header Filter Setup Database`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.resizeWindow(1280, 720).navigateTo(`${baseUrl}/clients`);
    await nurse();
  })
  .after(destroyDb);

test('filter button is visible', async browser =>
  browser
    .click(clientFilterButton)
    .expect(clientTableFilterColumnInput.visible)
    .ok()
    .expect(clientTableFilterColumnInput.textContent)
    .eql('First Name, Surname, Address')
    .expect(inputLabel(clientTableFilterColumnInput))
    .eql('Filter Columns')
    .expect(inputHint(clientTableFilterColumnInput))
    .eql('3 out of 6 selected'));

test('filter input opens list of headers', async browser =>
  browser
    .click(clientFilterButton)
    .click(clientTableFilterColumnInput)
    .expect(selectorMenu.count)
    .eql(23)
    .expect(selectorMenu.nth(0).textContent)
    .eql('First Name')
    .expect(selectorMenu.nth(1).textContent)
    .eql('Surname')
    .expect(selectorMenu.nth(2).textContent)
    .eql('Admitted By')
    .expect(selectorMenu.nth(3).textContent)
    .eql('Gender')
    .expect(selectorMenu.nth(4).textContent)
    .eql('Date Added')
    .expect(selectorMenu.nth(5).textContent)
    .eql('Date Of Birth')
    .expect(selectorMenu.nth(6).textContent)
    .eql('Address')
    .expect(selectorMenu.nth(7).textContent)
    .eql('Phone Number')
    .expect(selectorMenu.nth(8).textContent)
    .eql('Home Help')
    .expect(selectorMenu.nth(9).textContent)
    .eql('Home Help Number')
    .expect(selectorMenu.nth(10).textContent)
    .eql('Public Health Nurse')
    .expect(selectorMenu.nth(11).textContent)
    .eql('Public Health Nurse Number')
    .expect(selectorMenu.nth(12).textContent)
    .eql('GP')
    .expect(selectorMenu.nth(13).textContent)
    .eql('GP Number')
    .expect(selectorMenu.nth(14).textContent)
    .eql('Chemist')
    .expect(selectorMenu.nth(15).textContent)
    .eql('Chemist Number')
    .expect(selectorMenu.nth(16).textContent)
    .eql('Mobility')
    .expect(selectorMenu.nth(17).textContent)
    .eql('Toileting')
    .expect(selectorMenu.nth(18).textContent)
    .eql('Hygiene')
    .expect(selectorMenu.nth(19).textContent)
    .eql('Sight')
    .expect(selectorMenu.nth(20).textContent)
    .eql('Hearing')
    .expect(selectorMenu.nth(21).textContent)
    .eql('Thickner Grade')
    .expect(selectorMenu.nth(22).textContent)
    .eql('Account Status'));

test('3 filter options are selected by default', async browser => {
  await browser.click(clientFilterButton).click(clientTableFilterColumnInput);
  for await (const i of Array.from(Array.from({ length: 21 }).keys())) {
    if ([0, 1, 6].includes(i)) {
      await browser
        .expect(selectorMenu.nth(i).getAttribute('aria-selected'))
        .eql('true');
    } else {
      await browser
        .expect(selectorMenu.nth(i).getAttribute('aria-selected'))
        .eql('false');
    }
  }
});

test('clicking filter options changes their selected value', async browser => {
  await browser
    .click(clientFilterButton)
    .click(clientTableFilterColumnInput)
    .click(menuSelector.nth(0))
    .expect(selectorMenu.nth(0).getAttribute('aria-selected'))
    .eql('false');
  await setSelector(0);
  return browser
    .expect(selectorMenu.nth(0).getAttribute('aria-selected'))
    .eql('true')
    .expect(clientTableFilterColumnInput.textContent)
    .eql('First Name, Surname, Address');
});

test('clicking filter options removes and adds headers', async browser => {
  await browser
    .click(clientFilterButton)
    .click(clientTableFilterColumnInput)
    .click(menuSelector.nth(0))
    .expect(clientsTableHeaders.count)
    .eql(2)
    .expect(clientsTableHeaders.nth(0).textContent)
    .eql('Surname');
  await setSelector(0);
  return browser.expect(clientsTableHeaders.count).eql(3);
});

test('cannot select less than one header', async browser => {
  await browser.click(clientFilterButton).click(clientTableFilterColumnInput);
  await setSelector(5);
  await setSelector(1);
  await setSelector(0);
  await setSelector(0);
  return browser
    .expect(selectorMenu.nth(0).getAttribute('aria-selected'))
    .eql('true');
});

test('clicking filter options updates database', async browser => {
  await browser
    .click(clientFilterButton)
    .click(clientTableFilterColumnInput)
    .click(menuSelector.nth(9));
  const user = await getUserByName('testNurse');
  return browser
    .expect(user[0].userClientTableHiddenRows)
    .eql(
      JSON.stringify([
        'Admitted By',
        'Gender',
        'Date Added',
        'Date Of Birth',
        'Phone Number',
        'Home Help',
        'Public Health Nurse',
        'Public Health Nurse Number',
        'GP',
        'GP Number',
        'Chemist',
        'Chemist Number',
        'Mobility',
        'Toileting',
        'Hygiene',
        'Sight',
        'Hearing',
        'Thickner Grade',
        'Account Status'
      ])
    );
});

test('filtered headers update when updated elsewhere', async browser => {
  await setClientHiddenHeaders('testNurse', ['Date Added', 'Home Help']);
  await browser
    .expect(clientsTableHeaders.count)
    .eql(21)
    .expect(clientsTableHeaders.nth(0).textContent)
    .eql('First Name')
    .expect(clientsTableHeaders.nth(1).textContent)
    .eql('Surname')
    .expect(clientsTableHeaders.nth(2).textContent)
    .eql('Admitted By');
  await setClientHiddenHeaders('testNurse', ['Admitted By']);
  await browser
    .expect(clientsTableHeaders.count)
    .eql(22)
    .expect(clientsTableHeaders.nth(0).textContent)
    .eql('First Name')
    .expect(clientsTableHeaders.nth(1).textContent)
    .eql('Surname')
    .expect(clientsTableHeaders.nth(2).textContent)
    .eql('Gender');
});
