import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../globals';
import { admin, clientUser, nurse, url } from '../../custom-commands';

const sidebar = Selector('#sidebar');

fixture`views/Admin`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres']);
    await browser.navigateTo(baseUrl).resizeWindow(1280, 720);
  })
  .after(destroyDb);

test('page cannot be accessed by nurse', async browser => {
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/admin`)
    .expect(url())
    .eql(`${baseUrl}/clients`);
});

test('page cannot be accessed by clientUser', async browser => {
  await seedDb(['clients']);
  await clientUser();
  return browser
    .navigateTo(`${baseUrl}/admin`)
    .expect(url())
    .eql(`${baseUrl}/my-medical-info`);
});

test('sidebar is visible', async browser => {
  await admin();
  return browser.expect(sidebar.visible).ok();
});
