import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../globals';
import { inputValidationMessage, nurse, url } from '../../custom-commands';
import { infoSnackbar, snackbarText } from '../../generic-selectors/Auth';

import { changeClientType, changeClientVersion } from '../../generic-functions';

const serverAddressInput = Selector('#serverAddressInput');
const serverConnectButton = Selector('#serverConnectButton');
const serverSelectForm = Selector('#serverSelectForm');

fixture`pages/ServerSelect`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres']);
    return browser.resizeWindow(1280, 720).navigateTo(baseUrl);
  })
  .after(destroyDb);

test('page does not open on web version', async browser => {
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/clients`)
    .expect(url())
    .eql(`${baseUrl}/clients`);
});

test('page does open on electron version', async browser => {
  await nurse();
  await changeClientType('electron');
  return browser
    .navigateTo(`${baseUrl}/temp-code`)
    .expect(serverSelectForm.visible)
    .ok()
    .expect(url())
    .eql(`${baseUrl}/server-select?nextUrl=/temp-code`);
});

test('cannot press submit if invalid domain', async browser => {
  await nurse();
  await changeClientType('electron');
  return browser
    .navigateTo(`${baseUrl}/server-select`)
    .typeText(serverAddressInput, 'test')
    .expect(inputValidationMessage(serverAddressInput))
    .eql('Must be valid URL')
    .expect(serverConnectButton.hasAttribute('disabled'))
    .ok()
    .selectText(serverAddressInput)
    .pressKey('delete')
    .expect(inputValidationMessage(serverAddressInput))
    .eql('Server Address is required')
    .expect(serverConnectButton.hasAttribute('disabled'))
    .ok()
    .typeText(serverAddressInput, 'https://exampledomain.com')
    .expect(serverConnectButton.hasAttribute('disabled'))
    .notOk();
});

test('submitting goes to nexturl', async browser => {
  await nurse();
  await changeClientType('electron');
  return browser
    .navigateTo(`${baseUrl}/server-select?nextUrl=/temp-code`)
    .typeText(
      serverAddressInput,
      `${process.env.VUE_APP_API_PROTOCOL ?? ''}://${
        process.env.VUE_APP_API_HOST ?? ''
      }:${process.env.VUE_APP_API_PORT ?? ''}`
    )
    .click(serverConnectButton)
    .expect(url())
    .eql(`${baseUrl}/temp-code`);
});

test('fail to connect to domain shows error', async browser => {
  await nurse();
  await changeClientVersion('99999.99.99');
  await changeClientType('electron');
  return browser
    .navigateTo(`${baseUrl}/server-select?nextUrl=/temp-code`)
    .typeText(serverAddressInput, 'http://not.a.real.website.yes')
    .click(serverConnectButton)
    .expect(url())
    .eql(`${baseUrl}/server-select?nextUrl=/temp-code`)
    .expect(infoSnackbar.exists)
    .ok()
    .expect(snackbarText.textContent)
    .contains('There was a problem connecting to the backend server');
});

test('can submit by pressing enter', async browser => {
  await nurse();
  await changeClientVersion('99999.99.99');
  await changeClientType('electron');
  return browser
    .navigateTo(`${baseUrl}/server-select?nextUrl=/temp-code`)
    .typeText(serverAddressInput, 'http://not.a.real.website.yes')
    .pressKey('enter')
    .expect(infoSnackbar.exists)
    .ok();
});

test('pressing enter to submit does not work when data is invalid', async browser => {
  await nurse();
  await changeClientVersion('99999.99.99');
  await changeClientType('electron');
  return browser
    .navigateTo(`${baseUrl}/server-select?nextUrl=/temp-code`)
    .typeText(serverAddressInput, 'notvalidtext')
    .pressKey('enter')
    .expect(infoSnackbar.exists)
    .notOk();
});
