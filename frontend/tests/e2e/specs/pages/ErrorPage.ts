import { Selector } from 'testcafe';
import { admin, nurse, url } from '../../custom-commands';
import { baseUrl, destroyDb, seedDb } from '../../globals';
import { infoSnackbar } from '../../generic-selectors/Auth';

const errorText = Selector('#errorText');
const logoutButtonText = Selector('#logoutButton > .q-btn__content');
const errorPage = Selector('#errorPage');

fixture`views/ErrorPage Setup Database`
  .beforeEach(async browser => {
    await seedDb();
    return browser.navigateTo(baseUrl);
  })
  .after(destroyDb);

test('page can not be accessed by a non-logged in user', async browser =>
  browser
    .navigateTo(`${baseUrl}/error`)
    .expect(url())
    .eql(`${baseUrl}/auth?nextUrl=/error`));

test('page can be accessed without parameters', async browser => {
  await admin();
  return browser
    .navigateTo(`${baseUrl}/error`)
    .expect(url())
    .eql(`${baseUrl}/error`);
});

test('page can be accessed by nurse', async browser => {
  await nurse();
  return browser.expect(url()).eql(`${baseUrl}/error`);
});

test('page shows text provided in parameters', async browser => {
  await nurse();
  return browser
    .expect(errorText.textContent)
    .eql('No centre has been setup, please request an admin to set one up');
});

test('page does not show notification on load', async browser => {
  await nurse();
  return browser.expect(infoSnackbar.exists).notOk();
});

test('background color is correct accent color', async browser => {
  await nurse();
  return browser
    .expect(errorPage.getStyleProperty('background-color'))
    .eql('rgb(211, 47, 47)');
});

test('text color is correct accent color', async browser => {
  await nurse();
  return browser
    .expect(logoutButtonText.getStyleProperty('color'))
    .eql('rgb(211, 47, 47)');
});

test('logout button logs user out', async browser => {
  await nurse();
  return browser.click(logoutButtonText).expect(url()).eql(`${baseUrl}/auth`);
});
