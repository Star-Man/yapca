import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  inputLabel,
  inputValidationMessage,
  nurse
} from '../../../custom-commands';
import {
  clientBasicInfoSubmit,
  firstNameInput,
  surnameInput
} from '../../../generic-selectors/ClientCreation';
import { getCentreByName, getClientByName } from '../../../generic-functions';
import { centresList } from '../../../generic-selectors/Centres';

fixture`components/clientCreation/BasicInfo`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres']);
    await browser
      .resizeWindow(1280, 720)
      .navigateTo(`${baseUrl}/client-creation`);
    return nurse();
  })
  .after(destroyDb);

test('first name field should be focused', async browser =>
  browser.pressKey('t e s t').expect(firstNameInput.value).eql('test'));

test('continue button only enabled when first name, surname and centres selected', async browser =>
  browser
    .expect(clientBasicInfoSubmit.hasAttribute('disabled'))
    .ok()
    .typeText(firstNameInput, 'Ace')
    .expect(clientBasicInfoSubmit.hasAttribute('disabled'))
    .ok()
    .typeText(surnameInput, 'Ventura')
    .expect(clientBasicInfoSubmit.hasAttribute('disabled'))
    .ok()
    .click(centresList.nth(0))
    .expect(clientBasicInfoSubmit.hasAttribute('disabled'))
    .notOk()
    .selectText(surnameInput)
    .pressKey('delete')
    .expect(clientBasicInfoSubmit.hasAttribute('disabled'))
    .ok());

test('subscribed centres list shows list of centres user is subbed to', browser =>
  browser
    .expect(centresList.count)
    .eql(3)
    .expect(centresList.nth(0).textContent)
    .eql('Example Centre')
    .expect(centresList.nth(1).textContent)
    .eql('Example Centre 2')
    .expect(centresList.nth(2).textContent)
    .eql('Example Centre 4'));

test('all text is correctly rendered', browser =>
  browser
    .expect(inputLabel(firstNameInput))
    .eql('First Name')
    .expect(inputLabel(surnameInput))
    .eql('Surname'));

test('surname input shows error when no input', browser =>
  browser
    .typeText(surnameInput, '1')
    .selectText(surnameInput)
    .pressKey('delete')
    .expect(inputValidationMessage(surnameInput))
    .eql('Surname is required'));

test('first name input shows error when no input', browser =>
  browser
    .typeText(firstNameInput, '1')
    .selectText(firstNameInput)
    .pressKey('delete')
    .expect(inputValidationMessage(firstNameInput))
    .eql('First Name is required'));

test('inputting basic info saves client to database', async browser => {
  await browser
    .typeText(firstNameInput, 'Ace')
    .typeText(surnameInput, 'Ventura')
    .click(centresList.nth(1))
    .click(centresList.nth(0))
    .click(centresList.nth(2))
    .click(clientBasicInfoSubmit);
  const centre2Id = (await getCentreByName('Example Centre 2'))[0].id;
  const centre4Id = (await getCentreByName('Example Centre 4'))[0].id;
  const clients = await getClientByName('Ventura');
  return browser
    .expect(
      clients.filter(
        (client: { basicInfoSetup: boolean }) => client.basicInfoSetup
      ).length
    )
    .gte(1)
    .expect(
      clients.filter(
        (client: { surname: string }) => client.surname === 'Ventura'
      ).length
    )
    .gte(1)
    .expect(
      clients.filter(
        (client: { firstName: string }) => client.firstName === 'Ace'
      ).length
    )
    .gte(1)
    .expect(
      clients.filter(
        (client: { centreId: number }) => client.centreId === centre2Id
      ).length
    )
    .gte(1)
    .expect(
      clients.filter(
        (client: { centreId: number }) => client.centreId === centre4Id
      ).length
    )
    .gte(1);
});
