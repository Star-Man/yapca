import { Selector } from 'testcafe';
import { format } from 'date-fns';
import setSelector from '../../../custom-commands/setSelectorMenuValue';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  inputLabel,
  inputValidationMessage,
  nurse
} from '../../../custom-commands';
import {
  addressInput,
  clientAdditionalInfoSubmit,
  clientBasicInfoSubmit,
  createNextOfKinSubmitButton,
  dateOfBirthInput,
  firstNameInput,
  genderInput,
  phoneNumberInput,
  surnameInput
} from '../../../generic-selectors/ClientCreation';
import {
  addNextOfKinButton,
  nextOfKinCreateNameField,
  nextOfKinCreatePhoneNumberField
} from '../../../generic-selectors/Clients';
import { getClientByName, updateClient } from '../../../generic-functions';
import { centresList } from '../../../generic-selectors/Centres';
import {
  datePickerDay,
  datePickerMonthOption,
  datePickerYearOption
} from '../../../generic-selectors/Layout';

const option2020 = Selector('.q-date__years-item').nth(0);
const febButton = Selector('.q-date__months-item').nth(1);

fixture`components/ClientCreation/AdditionalInfo`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser
      .resizeWindow(1280, 720)
      .navigateTo(`${baseUrl}/client-creation`);
    await nurse();
    return browser
      .typeText(firstNameInput, 'First Name')
      .typeText(surnameInput, 'Surname')
      .click(centresList.nth(2))
      .click(clientBasicInfoSubmit);
  })
  .after(destroyDb);

test('address field should be focused', async browser =>
  browser.pressKey('t e s t').expect(addressInput.value).eql('test'));

test('continue button only enabled when all data entered', async browser =>
  browser
    .expect(clientAdditionalInfoSubmit.hasAttribute('disabled'))
    .ok()
    .typeText(addressInput, 'Example Address')
    .expect(clientAdditionalInfoSubmit.hasAttribute('disabled'))
    .ok()
    .typeText(phoneNumberInput, '01111111')
    .expect(clientAdditionalInfoSubmit.hasAttribute('disabled'))
    .ok()
    .click(dateOfBirthInput)
    .expect(datePickerDay.nth(0).textContent)
    .eql('1')
    .click(datePickerDay.nth(0)) // Day
    .pressKey('esc')
    .expect(clientAdditionalInfoSubmit.hasAttribute('disabled'))
    .ok()
    .click(addNextOfKinButton)
    .typeText(nextOfKinCreateNameField, 'Example Name')
    .typeText(nextOfKinCreatePhoneNumberField, '1234567')
    .click(createNextOfKinSubmitButton)
    .expect(clientAdditionalInfoSubmit.hasAttribute('disabled'))
    .notOk()
    .selectText(addressInput)
    .pressKey('delete')
    .expect(clientAdditionalInfoSubmit.hasAttribute('disabled'))
    .ok());

test('all text is correctly rendered', browser =>
  browser
    .expect(inputLabel(addressInput))
    .eql('Address')
    .expect(inputLabel(phoneNumberInput))
    .eql('Phone Number')
    .expect(inputLabel(dateOfBirthInput))
    .eql('Date Of Birth')
    .expect(inputLabel(genderInput))
    .eql('Gender'));

test('address input shows error when no input', browser =>
  browser
    .typeText(addressInput, 'Example Address')
    .selectText(addressInput)
    .pressKey('delete')
    .expect(inputValidationMessage(addressInput))
    .eql('Address is required'));

test('phone number input shows error when no input', browser =>
  browser
    .typeText(phoneNumberInput, '1')
    .selectText(phoneNumberInput)
    .pressKey('delete')
    .expect(inputValidationMessage(phoneNumberInput))
    .eql('Phone Number is required'));

test('inputting additional info saves client to database', async browser => {
  await browser
    .typeText(addressInput, 'Example Address')
    .typeText(phoneNumberInput, '01111111')
    .click(dateOfBirthInput)
    .click(datePickerYearOption)
    .click(option2020)
    .click(datePickerMonthOption)
    .click(febButton)
    .click(datePickerDay.nth(14))
    .pressKey('esc')
    .click(addNextOfKinButton)
    .typeText(nextOfKinCreateNameField, 'Example Name')
    .typeText(nextOfKinCreatePhoneNumberField, '1234567')
    .click(createNextOfKinSubmitButton)
    .click(genderInput);
  await setSelector(1);
  return browser.click(clientAdditionalInfoSubmit);
  const clients = await getClientByName('Surname');
  return browser
    .expect(
      clients.filter((client: { dateOfBirth: Date }) => {
        return (
          format(client.dateOfBirth, 'dd-MM-yyyy').toString() === '15-02-2020'
        );
      }).length
    )
    .gte(1)
    .expect(
      clients.filter(
        (client: { address: string }) => client.address === 'Example Address'
      ).length
    )
    .gte(1)
    .expect(
      clients.filter(
        (client: { nextOfKinPhoneNumber: string }) =>
          client.nextOfKinPhoneNumber === '1234567'
      ).length
    )
    .gte(1)
    .expect(
      clients.filter(
        (client: { nextOfKinName: string }) =>
          client.nextOfKinName === 'Example Name'
      ).length
    )
    .gte(1)
    .expect(
      clients.filter(
        (client: { additionalInfoSetup: boolean }) => client.additionalInfoSetup
      ).length
    )
    .gte(1)
    .expect(
      clients.filter((client: { gender: string }) => client.gender === 'Male')
        .length
    )
    .gte(1);
});

test('data appears if set elsewhere', async browser => {
  await updateClient(
    'Surname',
    {
      address: 'New Address',
      phoneNumber: '0193791',
      dateOfBirth: '2011-10-09',
      gender: 'Male'
    },
    'testNurse'
  );
  return browser
    .expect(addressInput.value)
    .eql('New Address')
    .expect(phoneNumberInput.value)
    .eql('0193791')
    .expect(dateOfBirthInput.value)
    .eql('09/10/11')
    .expect(genderInput.child('span').textContent)
    .eql('Male');
});
