import { Selector } from 'testcafe';
import { selectorMenu } from '../../../generic-selectors/Centres';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  getItemCaption,
  getItemLabel,
  nurse,
  selectorInputLabel,
  textAreaLabel
} from '../../../custom-commands';
import { finishClientButtons } from '../../../generic-selectors/Clients';
import { clientDietSubmit } from '../../../generic-selectors/ClientCreation';

import {
  createClientAbilitySetup,
  getClientByName
} from '../../../generic-functions';
import { selectedStepHighlight } from '../../../generic-selectors/Layout';
import setSelector from '../../../custom-commands/setSelectorMenuValue';

const clientFieldEdit = Selector('.clientFieldEdit');
const feedingRisksChips = Selector('.q-chip__content');
const slider = clientFieldEdit.nth(4).find('.q-slider__thumb');

fixture`components/clientCreation/Diet`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres']);
    await createClientAbilitySetup();
    await browser.resizeWindow(1280, 720).navigateTo(`${baseUrl}/clients`);
    await nurse();
    return browser.click(finishClientButtons.nth(0));
  })
  .after(destroyDb);

test('continue button should be enabled', async browser =>
  browser.expect(clientDietSubmit.hasAttribute('disabled')).notOk());

test('all text is correctly rendered', browser =>
  browser
    .expect(clientFieldEdit.count)
    .eql(5)
    .expect(selectorInputLabel(clientFieldEdit.nth(0)))
    .eql('Feeding Risks')
    .expect(getItemLabel(clientFieldEdit.nth(1)))
    .eql('Requires Assistance Eating')
    .expect(getItemCaption(clientFieldEdit.nth(1)))
    .eql('Does Test Client require assistance eating?')
    .expect(textAreaLabel(clientFieldEdit.nth(2)))
    .eql('Allergies')
    .expect(textAreaLabel(clientFieldEdit.nth(3)))
    .eql('Intolerances')
    .expect(getItemLabel(clientFieldEdit.nth(4)))
    .eql('Thickner Grade'));

test('inputting diet info saves client to database', async browser => {
  await browser.click(clientFieldEdit.nth(0));
  await setSelector(1);
  await browser
    .pressKey('esc')
    .typeText(clientFieldEdit.nth(2).find('textarea'), 'Allergies')
    .click(clientFieldEdit.nth(2))
    .typeText(clientFieldEdit.nth(3).find('textarea'), 'Intolerances')
    .click(clientFieldEdit.nth(1))
    .drag(slider, 400, 0)
    .click(clientDietSubmit)
    .expect(selectedStepHighlight.textContent)
    .eql('6');
  const clients = await getClientByName('12');
  return browser
    .expect(
      clients.filter(
        (client: { intolerances: string }) =>
          client.intolerances === 'Intolerances'
      ).length
    )
    .gte(1)
    .expect(
      clients.filter(
        (client: { allergies: string }) => client.allergies === 'Allergies'
      ).length
    )
    .gte(1)
    .expect(
      clients.filter(
        (client: { requiresAssistanceEating: boolean }) =>
          client.requiresAssistanceEating
      ).length
    )
    .gte(1)
    .expect(
      clients.filter(
        (client: { thicknerGrade: number }) => client.thicknerGrade === 2
      ).length
    )
    .gte(1)
    .expect(
      clients.filter(
        (client: { feedingRisk: string }) => client.feedingRisk === 'Swallowing'
      ).length
    )
    .gte(1);
});

test('feeding risk select menu has correct text', browser =>
  browser
    .click(clientFieldEdit.nth(0))
    .expect(selectorMenu.count)
    .eql(3)
    .expect(selectorMenu.nth(0).textContent)
    .eql('Chewing')
    .expect(selectorMenu.nth(1).textContent)
    .eql('Swallowing')
    .expect(selectorMenu.nth(2).textContent)
    .eql('Poor Appetite'));

test('selecting feeding risk value updates database', async browser => {
  await browser.click(clientFieldEdit.nth(0));
  await setSelector(1);
  const clients = await getClientByName('12');
  return browser
    .expect(
      clients.filter(
        (client: { feedingRisk: string }) => client.feedingRisk === 'Swallowing'
      ).length
    )
    .gt(0);
});

test('selecting value updates feedingRisksChips', async browser => {
  await browser.click(clientFieldEdit.nth(0));
  await setSelector(1);
  return browser
    .expect(feedingRisksChips.count)
    .eql(1)
    .expect(feedingRisksChips.nth(0).textContent)
    .eql('Swallowing');
});

test('unselecting value updates feedingRisksChips', async browser => {
  await browser.click(clientFieldEdit.nth(0));
  await setSelector(1);
  await setSelector(1);
  return browser.expect(feedingRisksChips.count).eql(0);
});
