import { Selector } from 'testcafe';
import {
  centreSelectListItem,
  userTableRow3
} from '../../../generic-selectors/Admin';
import { admin } from '../../../custom-commands';
import { baseUrl, destroyDb, seedDb } from '../../../globals';

import {
  addUserCentre,
  getUserByName,
  removeUserCentre
} from '../../../generic-functions';
import { confirmButton } from '../../../generic-selectors/Layout';

const warningDialog = Selector('#userChangeCentreWarningDialog');

const userChangeCentreWarningDialogText = Selector(
  '#userChangeCentreWarningDialogText'
);
const userChangeCentreWarningHeader = warningDialog.find('.q-toolbar__title');

fixture`components/admin/CentreSelect`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres']);
    await addUserCentre('testAdmin', 'Example Centre 3');
    await addUserCentre('testAdmin', 'Example Centre 4');
    await browser.navigateTo(baseUrl);
    await admin();
    return browser.navigateTo(`${baseUrl}/admin`).click(userTableRow3);
  })
  .after(destroyDb);

test('list of centres is visible', browser =>
  // test container visible, centre name, and correct error shown
  browser
    .expect(centreSelectListItem.count)
    .eql(3)
    .expect(centreSelectListItem.nth(0).textContent)
    .eql('Example Centre')
    .expect(centreSelectListItem.nth(2).textContent)
    .eql('Example Centre 4')
    .expect(
      centreSelectListItem
        .nth(0)
        .find('.q-checkbox__inner')
        .hasClass('q-checkbox__inner--truthy')
    )
    .ok()
    .expect(
      centreSelectListItem
        .nth(1)
        .find('.q-checkbox__inner')
        .hasClass('q-checkbox__inner--truthy')
    )
    .notOk()
    .expect(
      centreSelectListItem
        .nth(2)
        .find('.q-checkbox__inner')
        .hasClass('q-checkbox__inner--truthy')
    )
    .ok());

test('clicking currently active centre shows warning', browser =>
  // Header is correct, text is correct, and selected list item does not
  // change until submit is clicked
  browser
    .expect(userChangeCentreWarningDialogText.visible)
    .notOk()
    .click(centreSelectListItem.nth(0))
    .expect(userChangeCentreWarningDialogText.textContent)
    .eql(
      'Unsubscribing Test Nurse from Example Centre means ' +
        'they will lose access to clients and care plans' +
        ' relating to Example Centre.Are you sure you wi' +
        'sh to continue? '
    )
    .expect(userChangeCentreWarningHeader.textContent)
    .eql('Unsubscribe Test Nurse from Example Centre?')
    .expect(
      centreSelectListItem
        .nth(0)
        .find('.q-checkbox__inner')
        .hasClass('q-checkbox__inner--truthy')
    )
    .ok());

test('clicking currently inactive centre shows warning', browser =>
  // Header is correct, text is correct, and selected list item does not
  // change until submit is clicked
  browser
    .expect(userChangeCentreWarningDialogText.visible)
    .notOk()
    .click(centreSelectListItem.nth(1))
    .expect(userChangeCentreWarningDialogText.textContent)
    .eql(
      'Subscribing Test Nurse to Example Centre 3 will all' +
        'ow them to view and modify every client in the ' +
        'centre. Only press the accept if you are sure y' +
        'ou want to give Test Nurse access to these sens' +
        'itive medical records.'
    )
    .expect(userChangeCentreWarningHeader.textContent)
    .eql('Subscribe Test Nurse to Example Centre 3?')
    .expect(
      centreSelectListItem
        .nth(1)
        .find('.q-checkbox__inner')
        .hasClass('q-checkbox__inner--truthy')
    )
    .notOk());

test('clicking ok on remove centre warning dialog updates list and saves to db', async browser => {
  await browser
    .click(centreSelectListItem.nth(0))
    .click(confirmButton)
    .expect(userChangeCentreWarningDialogText.visible)
    .notOk()
    .expect(
      centreSelectListItem
        .nth(0)
        .find('.q-checkbox__inner')
        .hasClass('q-checkbox__inner--truthy')
    )
    .notOk();
  const user = await getUserByName('testNurse');
  return browser
    .expect(
      user.filter(
        (userData: { centreName: string }) =>
          userData.centreName === 'Example Centre'
      ).length
    )
    .eql(0);
});

test('clicking ok on add centre warning dialog updates list and saves to db', async browser => {
  await browser
    .click(centreSelectListItem.nth(1))
    .click(confirmButton)
    .expect(
      centreSelectListItem
        .nth(1)
        .find('.q-checkbox__inner')
        .hasClass('q-checkbox__inner--truthy')
    )
    .ok();
  const user = await getUserByName('testNurse');
  return browser
    .expect(
      user.filter(
        (userData: { centreName: string }) =>
          userData.centreName === 'Example Centre 3'
      ).length
    )
    .gte(1);
});

test('centres update when changed elsewhere', async browser => {
  await addUserCentre('testNurse', 'Example Centre 3');
  await removeUserCentre('testNurse', 'Example Centre');
  await removeUserCentre('testNurse', 'Example Centre 4');
  return browser
    .expect(
      centreSelectListItem
        .nth(0)
        .find('.q-checkbox__inner')
        .hasClass('q-checkbox__inner--truthy')
    )
    .notOk()
    .expect(
      centreSelectListItem
        .nth(1)
        .find('.q-checkbox__inner')
        .hasClass('q-checkbox__inner--truthy')
    )
    .ok()
    .expect(
      centreSelectListItem
        .nth(2)
        .find('.q-checkbox__inner')
        .hasClass('q-checkbox__inner--truthy')
    )
    .notOk();
});
