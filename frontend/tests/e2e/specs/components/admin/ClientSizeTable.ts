import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import { admin } from '../../../custom-commands';
import {
  createClientDocument,
  deleteClient,
  deleteClientDocument,
  getCentreByName,
  getClientByName,
  updateClient,
  updateClientCentre
} from '../../../generic-functions';
import { snackbarText } from '../../../generic-selectors/Auth';

const table = Selector('#clientSizeTable');
const rows = table.find('tbody').find('tr');
const gridRows = table.find('.responsiveTableGridCard');
const headers = table.find('th');
const progressBarText = Selector('#storageUsageLabel');
const clientDialog = Selector('#clientDialog');
const noDataLabel = table.find('.q-table__bottom');

fixture`components/Admin/ClientSizeTable Setup Database`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients', 'documents']);
    const centre1Id = (await getCentreByName('Example Centre'))[0].id;
    await updateClientCentre('testNurse', '3', centre1Id, false);
    await browser.navigateTo(`${baseUrl}/admin`).resizeWindow(1280, 720);
    return admin();
  })
  .after(destroyDb);

test('table has correct headers', browser =>
  browser
    .expect(headers.count)
    .eql(3)
    .expect(headers.nth(0).textContent)
    .eql('First Name')
    .expect(headers.nth(1).textContent)
    .eql('Surname')
    .expect(headers.nth(2).textContent)
    .eql('Storage Usage'));

test('table shows clients with files', browser =>
  browser
    .expect(rows.count)
    .eql(2)
    .expect(rows.nth(0).find('td').nth(0).textContent)
    .eql('Test Client')
    .expect(rows.nth(0).find('td').nth(1).textContent)
    .eql('4')
    .expect(rows.nth(0).find('td').nth(2).textContent)
    .eql('81 B')
    .expect(rows.nth(1).find('td').nth(0).textContent)
    .eql('Test Client')
    .expect(rows.nth(1).find('td').nth(1).textContent)
    .eql('3')
    .expect(rows.nth(1).find('td').nth(2).textContent)
    .eql('27 B'));

test('table can be sorted by surname', browser =>
  browser
    .click(headers.nth(1))
    .expect(rows.nth(0).find('td').nth(1).textContent)
    .eql('3')
    .expect(rows.nth(1).find('td').nth(1).textContent)
    .eql('4')
    .click(headers.nth(1))
    .expect(rows.nth(0).find('td').nth(1).textContent)
    .eql('4')
    .expect(rows.nth(1).find('td').nth(1).textContent)
    .eql('3'));

test('table can be sorted by size', browser =>
  browser
    .click(headers.nth(2))
    .expect(rows.nth(0).find('td').nth(1).textContent)
    .eql('3')
    .expect(rows.nth(1).find('td').nth(1).textContent)
    .eql('4')
    .click(headers.nth(2))
    .click(headers.nth(2))
    .expect(rows.nth(0).find('td').nth(1).textContent)
    .eql('4')
    .expect(rows.nth(1).find('td').nth(1).textContent)
    .eql('3'));

test('deleting file updates clients size', async browser => {
  await deleteClientDocument('Example Document 1');
  return browser.expect(rows.nth(0).find('td').nth(2).textContent).eql('45 B');
});

test('adding file updates clients size', async browser => {
  const [client4] = await getClientByName('4');
  await createClientDocument(
    client4.id,
    `example_file_100`,
    `Example Document 100`,
    'a,b,c'
  );
  return browser.expect(rows.nth(0).find('td').nth(2).textContent).eql('86 B');
});

test('client is added to table if file is added', async browser => {
  const [client6] = await getClientByName('6');
  await createClientDocument(
    client6.id,
    `example_file_100`,
    `Example Document 100`,
    'a,b,c'
  );
  return browser
    .expect(rows.count)
    .eql(3)
    .expect(rows.nth(2).find('td').nth(1).textContent)
    .eql('6')
    .expect(rows.nth(2).find('td').nth(2).textContent)
    .eql('5 B');
});

test('client is removed from table if file is removed', async browser => {
  const [client2] = await getClientByName('2');
  await createClientDocument(
    client2.id,
    `example_file_100`,
    `Example Document 100`,
    'a,b,c'
  );
  await deleteClientDocument('Example Document 100');
  return browser.expect(rows.count).eql(2);
});

test('progress bar shows file usage percentage', browser =>
  browser.expect(progressBarText.textContent).eql('85.0%'));

test('clicking file shows file size more exactly', browser =>
  browser
    .expect(headers.count)
    .eql(3)
    .click(progressBarText)
    .expect(progressBarText.textContent)
    .eql('108 B/127 B'));

test('client dialog opens on row click', async browser => {
  await browser
    .resizeWindow(480, 720)
    .expect(clientDialog.visible)
    .notOk()
    .click(gridRows.nth(0))
    .click(gridRows.nth(0)) // Damn you chrome
    .expect(clientDialog.visible)
    .ok();
});

test('client is removed from table if client is removed', async browser => {
  await deleteClient('3');
  await deleteClient('4');
  return browser
    .expect(noDataLabel.textContent)
    .eql('Clients have no uploaded documents');
});

test('client name updates when changed elsewhere', async browser => {
  await updateClient('4', { surname: 'Test' });
  await updateClient('3', { firstName: 'New First Name' }, 'testNurse');
  return browser
    .expect(rows.nth(0).find('td').nth(0).textContent)
    .eql('Test Client')
    .expect(rows.nth(0).find('td').nth(1).textContent)
    .eql('Test')
    .expect(rows.nth(1).find('td').nth(0).textContent)
    .eql('New First Name')
    .expect(rows.nth(1).find('td').nth(1).textContent)
    .eql('3');
});

test('shows warning if user not subbed to same centres as client', browser =>
  browser
    .resizeWindow(480, 720)
    .click(gridRows.nth(1))
    .click(gridRows.nth(1)) // Damn you chrome
    .expect(snackbarText.textContent)
    .contains(
      'You cannot view this client, they are not in any of your centres'
    )
    .expect(clientDialog.visible)
    .notOk());
