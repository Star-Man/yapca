import { Selector } from 'testcafe';
import { centreSelectListItem } from '../../../generic-selectors/Admin';
import { admin, inputValidationMessage } from '../../../custom-commands';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import { snackbarText } from '../../../generic-selectors/Auth';

import {
  addUserCentre,
  getTempCodesByAction
} from '../../../generic-functions';
import { confirmButton } from '../../../generic-selectors/Layout';

const addNewUserButton = Selector('#addNewUserButton');
const addNewUserCard = Selector('#addNewUserDialog');
const emailInput = Selector('#addNewUserEmailInput');
const addNewUserWarning = Selector('#addNewUserWarning');
const displayNameInput = Selector('#addNewUserDisplayNameInput');
const addUserCentresList = Selector('#addUserCentresList');
const addUserCentreSelectErrorCode = Selector('#addUserCentreSelectErrorCode');

fixture`components/admin/AddNewUserDialog`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres']);
    await addUserCentre('testAdmin', 'Example Centre 2');
    await addUserCentre('testAdmin', 'Example Centre 4');
    await browser.navigateTo(`${baseUrl}/admin`);
    await admin();
    return browser.click(addNewUserButton);
  })
  .after(destroyDb);

test('input shows error when invalid email', browser =>
  browser
    .typeText(emailInput, 'invalidemail')
    .selectText(emailInput)
    .expect(inputValidationMessage(emailInput))
    .eql('Invalid Email Address'));

test('shows snackbar and closes dialog on success', browser =>
  browser
    .expect(addNewUserCard.visible)
    .ok()
    .typeText(displayNameInput, 'Example Display Name')
    .typeText(emailInput, 'testemail2@test.com')
    .click(centreSelectListItem.nth(2))
    .click(confirmButton)
    .expect(snackbarText.textContent)
    .contains('Registration Email Sent!')
    .expect(addNewUserCard.visible)
    .notOk());

test('warning text should be visible', browser =>
  browser
    .expect(addNewUserWarning.textContent)
    .contains('Please make sure you enter the correct email address'));

test('pressing enter to submit does not work when data is invalid', browser =>
  browser
    .typeText(displayNameInput, 'Example Display Name')
    .typeText(emailInput, 'testemail2@test.com')
    .click(centreSelectListItem.nth(2))
    .selectText(displayNameInput)
    .pressKey('delete')
    .pressKey('enter')
    .expect(addNewUserCard.visible)
    .ok());

test('can submit new user by pressing enter', browser =>
  browser
    .typeText(displayNameInput, 'Example Display Name')
    .click(centreSelectListItem.nth(2))
    .typeText(emailInput, 'testemail2@test.com')
    .pressKey('enter')
    .expect(addNewUserCard.visible)
    .notOk());

test('submit button is disabled until all data is entered', browser =>
  browser
    .expect(confirmButton.hasAttribute('disabled'))
    .ok()
    .typeText(displayNameInput, 'Example Display Name')
    .click(centreSelectListItem.nth(2))
    .expect(confirmButton.hasAttribute('disabled'))
    .ok()
    .typeText(emailInput, 'testemail')
    .expect(confirmButton.hasAttribute('disabled'))
    .ok()
    .typeText(emailInput, '@asdasjn.asdyv')
    .expect(confirmButton.hasAttribute('disabled'))
    .notOk()
    .click(centreSelectListItem.nth(2))
    .expect(confirmButton.hasAttribute('disabled'))
    .ok());

test('submitting adds new user tempCode to database', async browser => {
  await browser
    .typeText(displayNameInput, 'Example Display Name')
    .typeText(emailInput, 'testemail2@test.com')
    .click(centreSelectListItem.nth(0))
    .click(centreSelectListItem.nth(2))
    .click(confirmButton)
    .expect(addNewUserCard.visible)
    .notOk();
  const registrationTempCodes = await getTempCodesByAction('registration');
  return browser
    .expect(registrationTempCodes.length)
    .eql(2)
    .expect(registrationTempCodes[0].email)
    .eql('testemail2@test.com')
    .expect(registrationTempCodes[0].associatedUserId)
    .eql(null)
    .expect(registrationTempCodes[0].centreName)
    .eql('Example Centre')
    .expect(registrationTempCodes[1].centreName)
    .eql('Example Centre 4');
});

test('input shows error when email taken by user', browser =>
  browser
    .typeText(displayNameInput, 'Example Display Name')
    .typeText(emailInput, 'testemail@test.com')
    .click(centreSelectListItem.nth(2))
    .click(confirmButton)
    .expect(inputValidationMessage(emailInput))
    .eql('Email is already in use.')
    .expect(addNewUserCard.visible)
    .ok());

test('input shows error when email already sent', browser =>
  browser
    .typeText(displayNameInput, 'Example Display Name')
    .typeText(emailInput, 'testemail2@test.com')
    .click(centreSelectListItem.nth(2))
    .click(confirmButton)
    .expect(addNewUserCard.visible)
    .notOk()
    .click(addNewUserButton)
    .typeText(displayNameInput, 'Example Display Name')
    .typeText(emailInput, 'testemail2@test.com')
    .click(centreSelectListItem.nth(2))
    .click(confirmButton)
    .expect(inputValidationMessage(emailInput))
    .eql(
      'Registration was already requested for this email address in the last 12 hours'
    )
    .expect(addNewUserCard.visible)
    .ok());

test('list of centres is visible in add user dialog', browser =>
  // test container visible, centre name, and correct error shown
  browser
    .expect(addUserCentresList.visible)
    .notOk()
    .typeText(displayNameInput, '1')
    .expect(addUserCentresList.visible)
    .ok()
    .selectText(displayNameInput)
    .pressKey('delete')
    .expect(addUserCentresList.visible)
    .notOk()
    .typeText(displayNameInput, '1')
    .expect(centreSelectListItem.count)
    .eql(3)
    .expect(centreSelectListItem.nth(0).textContent)
    .eql('Example Centre')
    .expect(centreSelectListItem.nth(2).textContent)
    .eql('Example Centre 4')
    .expect(addUserCentreSelectErrorCode.textContent)
    .eql('At least one centre must be selected')
    .click(centreSelectListItem.nth(1))
    .expect(addUserCentreSelectErrorCode.textContent)
    .eql('')
    .click(centreSelectListItem.nth(1))
    .expect(addUserCentreSelectErrorCode.textContent)
    .eql('At least one centre must be selected'));
