import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import { clientUser } from '../../../custom-commands';

import { getUserByName, updateUser } from '../../../generic-functions';
import { openDisableTOTPDialog } from '../../../generic-selectors/Auth';

const disableTOTPConfirmButton = Selector('#disableTOTPConfirmButton');
const disableTOTPDialog = Selector('#disableTOTPDialog');

fixture`components/auth/DisableTOTP`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser
      .resizeWindow(1280, 720)
      .navigateTo(`${baseUrl}/user-settings`);
    await clientUser();
    await updateUser(
      'clientUser',
      { otpDeviceKey: 'I4ZG3S4SVU3TKKQUA3QKRL7FL4XHPHKB' },
      'clientUser'
    );
  })
  .after(destroyDb);

test('can disable otp device key', async browser => {
  await browser
    .expect(openDisableTOTPDialog.textContent)
    .eql('Disable Multi Factor Authentication')
    .expect(disableTOTPDialog.visible)
    .notOk()
    .click(openDisableTOTPDialog)
    .expect(disableTOTPDialog.visible)
    .ok()
    .click(disableTOTPConfirmButton)
    .expect(disableTOTPDialog.visible)
    .notOk();
  const user = (await getUserByName('clientUser'))[0];
  return browser.expect(user.otpDeviceKey).eql(null);
});
