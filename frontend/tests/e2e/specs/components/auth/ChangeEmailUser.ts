import {
  inputLabel,
  inputValidationMessage,
  nurse
} from '../../../custom-commands';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  changeEmailButton,
  emailChangeForm,
  infoSnackbar,
  newEmailInput,
  openChangeEmailDialogButton,
  snackbarText
} from '../../../generic-selectors/Auth';

import { getUserByName } from '../../../generic-functions';

fixture`components/Auth/ChangeEmail`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres']);
    await browser.navigateTo(baseUrl);
    await nurse();
    return browser.navigateTo(`${baseUrl}/user-settings`);
  })
  .after(destroyDb);

test('change email form opens on button click', browser =>
  browser
    .expect(emailChangeForm.exists)
    .notOk()
    .click(openChangeEmailDialogButton)
    .expect(emailChangeForm.visible)
    .ok());

test('change email dialog closes by pressing escape', browser =>
  browser
    .click(openChangeEmailDialogButton)
    .pressKey('esc')
    .expect(emailChangeForm.visible)
    .notOk());

test('all text is correctly rendered', browser =>
  browser
    .click(openChangeEmailDialogButton)
    .expect(inputLabel(newEmailInput))
    .eql('New Email'));

test('changing email updates a users email', async browser => {
  await browser
    .click(openChangeEmailDialogButton)
    .selectText(newEmailInput)
    .pressKey('delete')
    .typeText(newEmailInput, 'newemail@test.com')
    .click(changeEmailButton)
    .expect(emailChangeForm.visible)
    .notOk();
  const { email } = (await getUserByName('testNurse'))[0];
  return browser.expect(email).eql('newemail@test.com');
});

test('new email field should be auto focused', browser => {
  return browser
    .click(openChangeEmailDialogButton)
    .expect(newEmailInput.focused)
    .ok()
    .pressKey('t e s t')
    .expect(newEmailInput.value)
    .eql('testemail@test.comtest');
});

test('new email input shows error when no email', browser =>
  browser
    .click(openChangeEmailDialogButton)
    .typeText(newEmailInput, '1515123')
    .selectText(newEmailInput)
    .pressKey('delete')
    .expect(inputValidationMessage(newEmailInput))
    .eql('Email is required'));

test('new email input shows error when invalid email', browser =>
  browser
    .click(openChangeEmailDialogButton)
    .selectText(newEmailInput)
    .pressKey('delete')
    .typeText(newEmailInput, 'notanemail')
    .expect(inputValidationMessage(newEmailInput))
    .eql('Invalid Email Address'));

test('new email input shows error when using current email', browser =>
  browser
    .click(openChangeEmailDialogButton)
    .selectText(newEmailInput)
    .pressKey('delete')
    .typeText(newEmailInput, 'testemail@test.com')
    .expect(inputValidationMessage(newEmailInput))
    .eql('Email Must Be New'));

test('input shows current email by default', browser =>
  browser
    .click(openChangeEmailDialogButton)
    .expect(newEmailInput.value)
    .eql('testemail@test.com'));

test('opening email change form after save shows new email', browser =>
  browser
    .click(openChangeEmailDialogButton)
    .selectText(newEmailInput)
    .pressKey('delete')
    .typeText(newEmailInput, 'newemail@test.com')
    .click(changeEmailButton)
    .click(openChangeEmailDialogButton)
    .expect(newEmailInput.value)
    .eql('newemail@test.com'));

test('opening email change form after save will reset validation', browser =>
  browser
    .click(openChangeEmailDialogButton)
    .selectText(newEmailInput)
    .pressKey('delete')
    .typeText(newEmailInput, 'newemail@test.com')
    .click(changeEmailButton)
    .click(openChangeEmailDialogButton)
    .expect(
      newEmailInput.parent().parent().parent().find('.v-messages__message')
        .exists
    )
    .notOk());

test('snackbar should appear on successful email change', browser =>
  browser
    .click(openChangeEmailDialogButton)
    .selectText(newEmailInput)
    .pressKey('delete')
    .typeText(newEmailInput, 'newemail@test.com')
    .click(changeEmailButton)
    .expect(infoSnackbar.exists)
    .ok()
    .expect(snackbarText.textContent)
    .contains(
      "Your email was changed! You'll receive an email at your new address shortly."
    ));

test('change email button is disabled until all data is entered', browser =>
  browser
    .click(openChangeEmailDialogButton)
    .selectText(newEmailInput)
    .pressKey('delete')
    .expect(changeEmailButton.hasAttribute('disabled'))
    .ok()
    .typeText(newEmailInput, 'newemail@test.com')
    .expect(changeEmailButton.hasAttribute('disabled'))
    .notOk());

test('change email button disables after data becoming invalid', browser =>
  browser
    .click(openChangeEmailDialogButton)
    .selectText(newEmailInput)
    .pressKey('delete')
    .typeText(newEmailInput, 'newemail@test.com')
    .expect(changeEmailButton.hasAttribute('disabled'))
    .notOk()
    .selectText(newEmailInput)
    .pressKey('delete')
    .expect(changeEmailButton.hasAttribute('disabled'))
    .ok());

test('change email can be done by pressing enter', browser =>
  browser
    .click(openChangeEmailDialogButton)
    .selectText(newEmailInput)
    .pressKey('delete')
    .typeText(newEmailInput, 'newemail@test.com')
    .pressKey('enter')
    .expect(emailChangeForm.visible)
    .notOk());

test('pressing enter to submit does not work when data is invalid', browser =>
  browser
    .click(openChangeEmailDialogButton)
    .selectText(newEmailInput)
    .pressKey('delete')
    .typeText(newEmailInput, 'newemail')
    .selectText(newEmailInput)
    .pressKey('enter')
    .expect(emailChangeForm.visible)
    .ok());

test('admin warning text should not be visible', browser =>
  browser
    .click(openChangeEmailDialogButton)
    .expect(emailChangeForm.textContent)
    .notContains('WARNING'));

test('error is shown when trying to change user email too much', browser =>
  browser
    .click(openChangeEmailDialogButton)
    .selectText(newEmailInput)
    .pressKey('delete')
    .typeText(newEmailInput, 'newemail1111111@test.com')
    .click(changeEmailButton)
    .click(openChangeEmailDialogButton)
    .selectText(newEmailInput)
    .pressKey('delete')
    .typeText(newEmailInput, 'newemail2222222@test.com')
    .click(changeEmailButton)
    .expect(emailChangeForm.visible)
    .ok()
    .expect(snackbarText.nth(1).textContent)
    .contains(
      'Cannot Change Email: Email was changed within the last 12 hours'
    ));
