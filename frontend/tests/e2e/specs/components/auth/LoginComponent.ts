import sub from 'date-fns/sub';
import { Selector } from 'testcafe';
import { baseUrl, database, destroyDb, seedDb } from '../../../globals';
import {
  infoSnackbar,
  snackbarCountBadge,
  snackbarText
} from '../../../generic-selectors/Auth';
import {
  loginButton,
  passwordInput,
  usernameInput
} from '../../../generic-selectors/Login';
import { resetLockoutTime } from '../../../generic-functions';

import {
  buttonLabel,
  inputLabel,
  inputValidationMessage,
  url
} from '../../../custom-commands';

const registerButton = Selector('#registerButton');

fixture`components/auth/LoginComponent Setup Database`
  .beforeEach(() => seedDb())
  .beforeEach(async browser => {
    await seedDb();
    return browser
      .navigateTo(baseUrl)
      .expect(usernameInput.visible)
      .ok({ timeout: 10000 });
  })
  .after(destroyDb);

const lockOutUser = async (browser: TestController) => {
  return browser
    .typeText(usernameInput, '123')
    .typeText(passwordInput, '456789')
    .selectText(passwordInput)
    .pressKey('enter')
    .pressKey('enter')
    .pressKey('enter')
    .pressKey('enter')
    .pressKey('enter')
    .pressKey('enter')
    .pressKey('enter')
    .pressKey('enter');
};

test('logging in as admin should redirect to centre setup', browser =>
  browser
    .expect(usernameInput.exists)
    .ok()
    .typeText(usernameInput, 'testAdmin')
    .typeText(passwordInput, 'testPassword1')
    .click(loginButton)
    .expect(url())
    .eql(`${baseUrl}/centre-setup?nextUrl=/clients`));

test('logging in as admin when centre exists redirects to main page', browser =>
  database()
    .table('centre_centre')
    .insert({
      name: 'testCentre',
      openingDaysFirstSetupComplete: true,
      closingDaysFirstSetupComplete: true,
      policiesFirstSetupComplete: true,
      needsFirstSetupComplete: true,
      setupComplete: true,
      color: '#FFFFFF',
      created: '2020-02-26 21:17:56',
      closedOnPublicHolidays: true,
      country: 'JP',
      state: ''
    })
    .then(() =>
      browser
        .typeText(usernameInput, 'testAdmin')
        .typeText(passwordInput, 'testPassword1')
        .click(loginButton)
        .expect(url())
        .eql(`${baseUrl}/clients`)
    ));

test('logging in as nurse should redirect to error page when no centres exist', browser =>
  browser
    .typeText(usernameInput, 'testNurse')
    .typeText(passwordInput, 'testPassword1')
    .click(loginButton)
    .expect(url())
    .eql(`${baseUrl}/error`));

test('logging in as nurse should redirect to clients page when centres exist', async browser => {
  await seedDb(['centres']);
  return browser
    .typeText(usernameInput, 'testNurse')
    .typeText(passwordInput, 'testPassword1')
    .click(loginButton)
    .expect(loginButton.visible)
    .notOk()
    .expect(url())
    .eql(`${baseUrl}/clients`);
});

test('login with wrong password should display notification', browser =>
  browser
    .typeText(usernameInput, 'testIncorrectUser')
    .typeText(passwordInput, 'testIncorrectPassword')
    .click(loginButton)
    .expect(infoSnackbar.exists)
    .ok()
    .expect(snackbarText.textContent)
    .contains('Unable to log in with provided credentials.'));

test('all text is correctly rendered', browser =>
  browser
    .expect(inputLabel(usernameInput))
    .eql('Username')
    .expect(inputLabel(passwordInput))
    .eql('Password')
    .expect(buttonLabel(loginButton))
    .eql('Login'));

test('login button only appears when data has been entered', browser =>
  browser
    .expect(loginButton.hasAttribute('disabled'))
    .ok()
    .typeText(usernameInput, 'testUser1')
    .typeText(passwordInput, 'testPassword1')
    .expect(loginButton.hasAttribute('disabled'))
    .notOk()
    .selectText(passwordInput)
    .pressKey('delete')
    .expect(loginButton.hasAttribute('disabled'))
    .ok());

test('pressing enter to login only works when data has been entered', browser =>
  browser
    .selectText(passwordInput)
    .pressKey('enter')
    .expect(infoSnackbar.exists)
    .notOk()
    .typeText(usernameInput, 'testUser1')
    .typeText(passwordInput, 'testPassword1')
    .selectText(passwordInput)
    .pressKey('enter')
    .expect(infoSnackbar.exists)
    .ok());

test('username input shows error when no username', browser =>
  browser
    .typeText(usernameInput, 'testUser1')
    .selectText(usernameInput)
    .pressKey('delete')
    .expect(inputValidationMessage(usernameInput))
    .eql('Username is required'));

test('password input shows error when no password', browser =>
  browser
    .typeText(passwordInput, 'testUser1')
    .selectText(passwordInput)
    .pressKey('delete')
    .expect(inputValidationMessage(passwordInput))
    .eql('Password is required'));

test('password input shows error when password too short', browser =>
  browser
    .typeText(passwordInput, 'test')
    .expect(inputValidationMessage(passwordInput))
    .eql('Password must be at least 6 characters'));

test('login button is disabled when password too short', browser =>
  browser
    .typeText(usernameInput, 'testUser1')
    .typeText(passwordInput, 'test')
    .expect(loginButton.hasAttribute('disabled'))
    .ok()
    .typeText(passwordInput, '12')
    .expect(loginButton.hasAttribute('disabled'))
    .notOk());

test('pressing enter to submit is disabled when password too short', browser =>
  browser
    .typeText(usernameInput, 'testUser1')
    .typeText(passwordInput, 'test')
    .selectText(passwordInput)
    .pressKey('enter')
    .expect(infoSnackbar.exists)
    .notOk()
    .typeText(passwordInput, '12')
    .selectText(passwordInput)
    .pressKey('enter')
    .expect(url())
    .eql(`${baseUrl}/auth?nextUrl=/clients`));

test('username field should be auto focused', browser =>
  browser
    .expect(usernameInput.visible)
    .ok()
    .pressKey('t e s t')
    .expect(usernameInput.value)
    .eql('test'));

test('failing to login multiple times shows lockout error in snackbar', async browser => {
  await lockOutUser(browser);
  return browser
    .expect(infoSnackbar.count)
    .eql(2)
    .expect(snackbarCountBadge.innerText)
    .eql('7')
    .expect(infoSnackbar.nth(1).textContent)
    .contains('Too many failed login attempts')
    .expect(loginButton.hasAttribute('disabled'))
    .ok();
});

test('user can log in after lockout expires', async browser => {
  await lockOutUser(browser);
  await database()
    .table('axes_accessattempt')
    .update({
      attempt_time: sub(new Date(), { minutes: 11 })
    });
  await resetLockoutTime();
  return browser
    .expect(loginButton.innerText)
    .eql('LOGIN')
    .expect(loginButton.hasAttribute('disabled'))
    .notOk();
});

test('pressing enter to submit is disabled while locked out', async browser => {
  await lockOutUser(browser);
  return browser
    .typeText(usernameInput, 'testAdmin')
    .typeText(passwordInput, 'testPassword1')
    .selectText(passwordInput)
    .pressKey('enter')
    .expect(url())
    .eql(`${baseUrl}/auth?nextUrl=/clients`);
});

test('login button shows remaining lockout time', async browser => {
  await lockOutUser(browser);
  return browser.expect(loginButton.innerText).eql('LOCKED OUT FOR: 9:58');
});

test('login button shows remaining lockout time', async browser => {
  await lockOutUser(browser);
  return browser.expect(loginButton.innerText).eql('LOCKED OUT FOR: 9:58');
});

test('clicking register button goes to tempCode page', browser =>
  browser
    .click(registerButton)
    .expect(url())
    .eql(`${baseUrl}/temp-code?action=registration&username=`));
