import { Selector } from 'testcafe';
import { infoSnackbar, snackbarText } from '../../../generic-selectors/Auth';
import { inputValidationMessage, url } from '../../../custom-commands';
import { baseUrl, destroyDb, seedDb } from '../../../globals';

const forgotPasswordButton = Selector('#forgotPasswordButton');
const forgotEmailInput = Selector('#forgotEmailInput');
const submitForgotEmailButton = Selector('#submitForgotEmailButton');
const cancelForgotEmailButton = Selector('#cancelForgotEmailButton');
const forgotEmailModal = Selector('#forgotEmailModal');

fixture`components/Auth/ForgotPasswordModal`
  .beforeEach(async browser => {
    await seedDb();
    return browser
      .resizeWindow(1280, 720)
      .navigateTo(baseUrl)
      .click(forgotPasswordButton);
  })
  .after(destroyDb);

test('clicking reset redirects to tempcode page', browser =>
  browser
    .typeText(forgotEmailInput, 'testuseremail@test.com')
    .click(submitForgotEmailButton)
    .expect(forgotEmailModal.visible)
    .notOk()
    .expect(infoSnackbar.exists)
    .ok()
    .expect(snackbarText.textContent)
    .contains('Request submitted successfully, check your inbox')
    .expect(url())
    .eql(`${baseUrl}/temp-code?action=reset-password&username=`));

test('can press enter to submit', browser =>
  browser
    .typeText(forgotEmailInput, 'testuseremail@test.com')
    .pressKey('enter')
    .expect(forgotEmailModal.visible)
    .notOk());

test('cannot press enter if invalid', browser =>
  browser
    .typeText(forgotEmailInput, 'invalidemail')
    .pressKey('enter')
    .expect(forgotEmailModal.visible)
    .ok());

test('input shows error when invalid email', browser =>
  browser
    .typeText(forgotEmailInput, 'invalidemail')
    .selectText(forgotEmailInput)
    .expect(inputValidationMessage(forgotEmailInput))
    .eql('Invalid Email Address'));

test('input shows error when no email', browser =>
  browser
    .typeText(forgotEmailInput, 'testuseremail@test.com')
    .selectText(forgotEmailInput)
    .pressKey('delete')
    .expect(inputValidationMessage(forgotEmailInput))
    .eql('Email is required'));

test('forgot email input is auto-focused', browser =>
  browser
    .expect(forgotEmailInput.focused)
    .ok()
    .pressKey('t e s t')
    .expect(forgotEmailInput.value)
    .eql('test'));

test('submit button is disabled until correct email is entered', browser =>
  browser
    .expect(submitForgotEmailButton.hasAttribute('disabled'))
    .ok()
    .typeText(forgotEmailInput, 'email')
    .expect(submitForgotEmailButton.hasAttribute('disabled'))
    .ok()
    .typeText(forgotEmailInput, '@test.com')
    .expect(submitForgotEmailButton.hasAttribute('disabled'))
    .notOk()
    .selectText(forgotEmailInput)
    .pressKey('delete')
    .expect(submitForgotEmailButton.hasAttribute('disabled'))
    .ok());

test('cancel button closes modal', browser =>
  browser
    .expect(forgotEmailModal.visible)
    .ok()
    .click(cancelForgotEmailButton)
    .expect(forgotEmailModal.visible)
    .notOk()
    .expect(url())
    .eql(`${baseUrl}/auth?nextUrl=/clients`));
