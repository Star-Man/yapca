import { userTableRow3 } from '../../../generic-selectors/Admin';
import { admin, inputValidationMessage } from '../../../custom-commands';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  changeEmailButton,
  emailChangeForm,
  emailWarningText,
  infoSnackbar,
  newEmailInput,
  openChangeEmailDialogButton,
  snackbarText
} from '../../../generic-selectors/Auth';

import { getUserByName } from '../../../generic-functions';

fixture`components/auth/ChangeEmail Admin`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres']);
    await browser.navigateTo(`${baseUrl}/admin`).resizeWindow(1280, 720);
    await admin();
    return browser.click(userTableRow3).click(openChangeEmailDialogButton);
  })
  .after(destroyDb);

test('changing email updates a users email', async browser => {
  await browser
    .selectText(newEmailInput)
    .pressKey('delete')
    .typeText(newEmailInput, 'newemail@test.com')
    .click(changeEmailButton)
    .expect(emailChangeForm.visible)
    .notOk();
  const { email } = (await getUserByName('testNurse'))[0];
  return browser.expect(email).eql('newemail@test.com');
});

test('snackbar should appear on successful email change', browser =>
  browser
    .selectText(newEmailInput)
    .pressKey('delete')
    .typeText(newEmailInput, 'newemail@test.com')
    .click(changeEmailButton)
    .expect(infoSnackbar.exists)
    .ok()
    .expect(snackbarText.textContent)
    .contains('Email Changed!'));

test('admin warning text should be visible', browser =>
  browser.expect(emailWarningText.textContent).contains('WARNING'));

test('input shows current email by default', browser =>
  browser.expect(newEmailInput.value).eql('testemail@test.com'));

test('new email input shows error when using current email', browser =>
  browser
    .selectText(newEmailInput)
    .pressKey('delete')
    .typeText(newEmailInput, 'testemail@test.com')
    .expect(inputValidationMessage(newEmailInput))
    .eql('Email Must Be New'));

test('error is shown when trying to change email too much', browser =>
  browser
    .selectText(newEmailInput)
    .pressKey('delete')
    .typeText(newEmailInput, 'newemail1111111@test.com')
    .click(changeEmailButton)
    .expect(infoSnackbar.exists)
    .ok()
    .click(openChangeEmailDialogButton)
    .selectText(newEmailInput)
    .pressKey('delete')
    .typeText(newEmailInput, 'newemail2222222@test.com')
    .click(changeEmailButton)
    .expect(emailChangeForm.visible)
    .ok()
    .expect(infoSnackbar.nth(1).exists)
    .ok()
    .expect(snackbarText.nth(1).textContent)
    .contains(
      'Cannot Change Email: Email was changed within the last 12 hours'
    ));
