import { Selector } from 'testcafe';
import { baseUrl, database, destroyDb, seedDb } from '../../../globals';
import {
  confirmPasswordInput,
  displayNameInput,
  emailInput,
  passwordInput,
  registerButton,
  usernameInput
} from '../../../generic-selectors/Auth';
import {
  buttonLabel,
  inputHint,
  inputLabel,
  inputValidationMessage,
  url
} from '../../../custom-commands';
import {
  createRegistrationTempCode,
  getTempCodesByAction,
  getUserByName,
  setupTOTP
} from '../../../generic-functions';
import { accentColorSelectors } from '../../../generic-selectors/Layout';

const colorThemeOptions =
  Selector('#themeColorInput').find('.themeSelectButton');
const window = Selector('body');
const passwordInputLabel = passwordInput.parent().child('.q-field__label');
const lightThemeOption = colorThemeOptions.nth(0);
const darkThemeOption = colorThemeOptions.nth(1);
const registrationTempCode = 'ci6TbI7VDKXA1wIIRAtyyPUc9JT7lrq5';

fixture`components/auth/Register`
  .beforeEach(async browser => {
    await seedDb();
    await createRegistrationTempCode(registrationTempCode);
    return browser
      .navigateTo(
        `${baseUrl}/temp-code?action=registration&email=newuseremail@test.com&displayName=newname&code=${registrationTempCode}`
      )
      .expect(usernameInput.visible)
      .ok({ timeout: 10000 })
      .resizeWindow(1280, 720);
  })
  .after(destroyDb);

test('registering as admin should redirect to centre setup', async browser => {
  await database().raw('TRUNCATE TABLE authentication_customuser CASCADE');
  await createRegistrationTempCode(registrationTempCode);
  await browser.eval(() => location.reload());
  await browser
    .typeText(usernameInput, 'testAdmin')
    .typeText(displayNameInput, 'Test Admin')
    .typeText(passwordInput, 'testPassword1')
    .typeText(confirmPasswordInput, 'testPassword1');
  await setupTOTP(browser);
  return browser
    .expect(registerButton.hasAttribute('disabled'))
    .notOk()
    .click(registerButton)
    .expect(registerButton.visible)
    .notOk()
    .expect(url())
    .eql(`${baseUrl}/centre-setup?nextUrl=/clients`);
});

test('registering as nurse should redirect to error page when no centres exist', async browser => {
  await browser
    .typeText(usernameInput, 'testNurse1')
    .typeText(displayNameInput, 'Test Nurse')
    .typeText(passwordInput, 'testPassword1')
    .typeText(confirmPasswordInput, 'testPassword1');
  await setupTOTP(browser);
  return browser.click(registerButton).expect(url()).eql(`${baseUrl}/error`);
});

test('registering as nurse should go to error page when centres exist but not subbed', async browser => {
  await seedDb(['centres']);

  await setupTOTP(browser);
  await browser
    .typeText(usernameInput, 'testNurse1')
    .selectText(displayNameInput)
    .pressKey('delete')
    .typeText(displayNameInput, 'Test Nurse')
    .typeText(passwordInput, 'testPassword1')
    .typeText(confirmPasswordInput, 'testPassword1')
    .click(registerButton)
    .expect(url())
    .eql(`${baseUrl}/error`);
  const user = (await getUserByName('testNurse1'))[0];
  return browser
    .expect(user.username)
    .eql('testNurse1')
    .expect(user.email)
    .eql('newuseremail@test.com')
    .expect(user.displayName)
    .eql('Test Nurse');
});

test('validation error should occur when attempting to register as existing user', async browser => {
  await setupTOTP(browser);
  return browser
    .typeText(usernameInput, 'testNurse')
    .typeText(displayNameInput, 'Test Nurse')
    .typeText(passwordInput, 'testPassword1')
    .typeText(confirmPasswordInput, 'testPassword1')
    .click(registerButton)
    .expect(inputValidationMessage(usernameInput))
    .eql('A user with that username already exists.');
});

test('all text is correctly rendered', browser =>
  browser
    .expect(inputLabel(usernameInput))
    .eql('Username')
    .expect(inputLabel(emailInput))
    .eql('Email')
    .expect(inputLabel(displayNameInput))
    .eql('Display Name')
    .expect(inputHint(displayNameInput))
    .eql('Your name as others will see it')
    .expect(inputLabel(passwordInput))
    .eql('Password')
    .expect(inputLabel(confirmPasswordInput))
    .eql('Confirm Password')
    .expect(buttonLabel(registerButton))
    .eql('Register'));

test('theme options are visible', browser =>
  browser.expect(colorThemeOptions.count).eql(2));

test('accent options are visible', browser =>
  browser.expect(accentColorSelectors.count).eql(26));

test('page theme changes when dark theme is selected', browser =>
  browser
    .click(darkThemeOption)
    .expect(window.getStyleProperty('background-color'))
    .eql('rgb(18, 18, 18)'));

test('text color changes when dark theme is selected', browser =>
  browser
    .click(darkThemeOption)
    .expect(passwordInputLabel.getStyleProperty('color'))
    .eql('rgba(255, 255, 255, 0.7)'));

test('page theme changes when light theme is selected', browser =>
  browser
    .click(darkThemeOption)
    .click(lightThemeOption)
    .expect(window.getStyleProperty('background-color'))
    .eql('rgba(0, 0, 0, 0)'));

test('text color changes when light theme is selected', browser =>
  browser
    .click(darkThemeOption)
    .click(lightThemeOption)
    .expect(passwordInputLabel.getStyleProperty('color'))
    .eql('rgba(0, 0, 0, 0.6)'));

test('register button is disabled until all valid data is entered', async browser => {
  await browser
    .expect(registerButton.hasAttribute('disabled'))
    .ok()
    .typeText(usernameInput, 'testUser')
    .expect(registerButton.hasAttribute('disabled'))
    .ok()
    .typeText(displayNameInput, 'Test User')
    .expect(registerButton.hasAttribute('disabled'))
    .ok()
    .typeText(passwordInput, 'testPassword')
    .expect(registerButton.hasAttribute('disabled'))
    .ok()
    .typeText(confirmPasswordInput, 'testPassword')
    .expect(registerButton.hasAttribute('disabled'))
    .ok();
  await setupTOTP(browser);
  return browser.expect(registerButton.hasAttribute('disabled')).notOk();
});

test('register button disables after being enabled if data becomes invalid', async browser => {
  await setupTOTP(browser);
  await browser
    .typeText(usernameInput, 'testUser')
    .typeText(displayNameInput, 'Test User')
    .typeText(passwordInput, 'testPassword')
    .typeText(confirmPasswordInput, 'testPassword')
    .expect(registerButton.hasAttribute('disabled'))
    .notOk()
    .selectText(usernameInput)
    .pressKey('delete')
    .expect(registerButton.hasAttribute('disabled'))
    .ok();
});

test('register username input shows error when no username', browser =>
  browser
    .typeText(usernameInput, 'testUser1')
    .selectText(usernameInput)
    .pressKey('delete')
    .expect(inputValidationMessage(usernameInput))
    .eql('Username is required'));

test('display name input shows error when no username', browser =>
  browser
    .typeText(displayNameInput, 'Test User')
    .selectText(displayNameInput)
    .pressKey('delete')
    .expect(inputValidationMessage(displayNameInput))
    .eql('Display name is required'));

test('register password input shows error when no password', browser =>
  browser
    .typeText(passwordInput, 'testUser1')
    .selectText(passwordInput)
    .pressKey('delete')
    .expect(inputValidationMessage(passwordInput))
    .eql('Password is required'));

test('password input shows error when passwords do not match', browser =>
  browser
    .typeText(usernameInput, 'testUser1')
    .typeText(passwordInput, 'password')
    .typeText(confirmPasswordInput, 'differentPassword')
    .expect(inputValidationMessage(passwordInput))
    .eql('Values do not match'));

test('password input shows error when passwords too short', browser =>
  browser
    .typeText(passwordInput, 'test')
    .typeText(confirmPasswordInput, 'test')
    .expect(inputValidationMessage(passwordInput))
    .eql('Password must be at least 6 characters'));

test('login button is disabled when password too short', async browser => {
  await setupTOTP(browser);
  return browser
    .typeText(usernameInput, 'testUser1')
    .typeText(displayNameInput, 'Test User')
    .typeText(passwordInput, 'test')
    .typeText(confirmPasswordInput, 'test')
    .expect(registerButton.hasAttribute('disabled'))
    .ok()
    .typeText(passwordInput, '12')
    .typeText(confirmPasswordInput, '12')
    .expect(registerButton.hasAttribute('disabled'))
    .notOk();
});

test('username field should be auto focused', browser => {
  return browser
    .expect(usernameInput.visible)
    .ok()
    .pressKey('t e s t')
    .expect(usernameInput.value)
    .eql('test');
});

test('displayName field should be auto filled if passed in query', browser => {
  return browser.expect(displayNameInput.value).eql('newname');
});

test('finishing registration deletes related tempCode', async browser => {
  const previousRegistrationTempCodes = await getTempCodesByAction(
    'registration'
  );
  await browser
    .expect(previousRegistrationTempCodes.length)
    .eql(1)
    .typeText(usernameInput, 'testNurse1')
    .typeText(displayNameInput, 'Test Nurse')
    .typeText(passwordInput, 'testPassword1')
    .typeText(confirmPasswordInput, 'testPassword1');
  await setupTOTP(browser);
  await browser.click(registerButton).expect(url()).eql(`${baseUrl}/error`);
  const newRegistrationTempCodes = await getTempCodesByAction('registration');
  return browser.expect(newRegistrationTempCodes.length).eql(0);
});

test('email input is disabled', browser => {
  return browser.expect(emailInput.hasAttribute('disabled')).ok();
});
