/* eslint-disable camelcase */
/* eslint-disable no-await-in-loop */
import { Selector } from 'testcafe';
import parse from 'postgres-interval';
import { baseUrl, destroyDb, seedDb } from '../../../../globals';
import { admin } from '../../../../custom-commands';
import {
  createCentreWithOpeningDaysSetup,
  getCentreByName,
  getCloseEventByName,
  getUserByName
} from '../../../../generic-functions';
import {
  calendarPreviousButton,
  calendarTitle,
  cardTitle,
  closeDialogIcon,
  closeReasonInput,
  day2,
  day5,
  selectorMenu
} from '../../../../generic-selectors/Centres';
import {
  closeDialogButton,
  confirmButton,
  datePickerDay,
  datePickerNextMonth,
  datePickerTitle
} from '../../../../generic-selectors/Layout';
import setSelector from '../../../../custom-commands/setSelectorMenuValue';

const recurrenceSelector = Selector('#recurrenceSelector').parent('label');
const recurrenceSelectorText = recurrenceSelector
  .find('.q-field__native')
  .child('span');

const weekOfMonthSelector = Selector('#weekOfMonthSelector').parent('label');
const weekOfMonthSelectorText = weekOfMonthSelector
  .find('.q-field__native')
  .child('span');

const isDateToggle = Selector('input[name="isDateToggle"]')
  .parent('.q-btn-group')
  .child('button');
const startDateField = Selector('#startDateField');
const endDateField = Selector('#endDateField');

fixture`components/centres/calendar/CreateCloseEventDialog`
  .skipJsErrors({
    message: /.*resizeobserver.*/gi
  })
  .beforeEach(async browser => {
    await seedDb();
    await createCentreWithOpeningDaysSetup();
    await browser.navigateTo(baseUrl).resizeWindow(1280, 720);
    await admin();

    let titleText = '';
    while (titleText !== 'October 2022') {
      await browser.click(calendarPreviousButton);
      titleText = await calendarTitle.textContent;
    }
    return browser.click(day2);
  })
  .after(destroyDb);

test('submit button is disabled while no close reason', async browser =>
  browser
    .expect(confirmButton.hasAttribute('disabled'))
    .ok()
    .typeText(closeReasonInput, 'Test Close')
    .expect(confirmButton.hasAttribute('disabled'))
    .notOk()
    .selectText(closeReasonInput)
    .pressKey('delete')
    .expect(confirmButton.hasAttribute('disabled'))
    .ok());

test('start and end dates are the same as the clicked date', async browser =>
  browser
    .expect(startDateField.value)
    .eql('02/10/22')
    .expect(endDateField.value)
    .eql('02/10/22'));

test('recurrences options are correct', async browser =>
  browser
    .click(recurrenceSelector)
    .expect(recurrenceSelectorText.textContent)
    .eql('Never')
    .expect(selectorMenu.count)
    .eql(4)
    .expect(selectorMenu.nth(0).textContent)
    .eql('Never')
    .expect(selectorMenu.nth(1).textContent)
    .eql('Weekly')
    .expect(selectorMenu.nth(2).textContent)
    .eql('Monthly')
    .expect(selectorMenu.nth(3).textContent)
    .eql('Yearly'));

test('modal resets on selecting different date', async browser => {
  await browser
    .typeText(closeReasonInput, 'Test Close')
    .click(recurrenceSelector);
  await setSelector(1);
  await browser
    .expect(recurrenceSelectorText.textContent)
    .eql('Weekly')
    .click(closeDialogButton)
    .click(day5)
    .expect(closeReasonInput.value)
    .eql('')
    .expect(recurrenceSelectorText.textContent)
    .eql('Never');
});

test('clicking submit closes modal', async browser =>
  browser
    .typeText(closeReasonInput, 'Test Close')
    .click(confirmButton)
    .expect(closeReasonInput.visible)
    .notOk());

test('clicking submit saves to database', async browser => {
  const [centre] = await getCentreByName('Example Centre OD Setup');
  const [user] = await getUserByName('testAdmin');

  await browser
    .typeText(closeReasonInput, 'Test Close')
    .click(recurrenceSelector);
  await setSelector(1);
  await browser.click(confirmButton);
  const closeEventData = await getCloseEventByName('Test Close');
  return browser.expect(closeEventData).eql([
    {
      allDay: true,
      centre_id: centre.id,
      createdBy_id: user.id,
      description: '',
      eventType: 'User Close Event',
      id: closeEventData[0].id,
      link: '',
      name: 'Test Close',
      recurrences: 'RRULE:FREQ=WEEKLY',
      start: closeEventData[0].start,
      timePeriod: parse('1 days')
    }
  ]);
});

test('endDate is limited to startDate +5 if weekly', async browser => {
  await browser
    .click(endDateField)
    .expect(datePickerDay.count)
    .eql(30)
    .expect(datePickerDay.nth(0).textContent)
    .eql('2')
    .expect(datePickerDay.nth(29).textContent)
    .eql('31')
    .click(datePickerDay.nth(4))
    .expect(endDateField.value)
    .eql('06/10/22')
    .pressKey('esc')
    .click(recurrenceSelector);
  await setSelector(1);
  return browser
    .click(endDateField)
    .expect(datePickerDay.count)
    .eql(6)
    .expect(datePickerDay.nth(0).textContent)
    .eql('2')
    .expect(datePickerDay.nth(5).textContent)
    .eql('7');
});

test('endDate is limited to startDate +20 if monthly', async browser => {
  await browser
    .click(endDateField)
    .click(datePickerDay.nth(4))
    .expect(endDateField.value)
    .eql('06/10/22')
    .pressKey('esc')
    .click(recurrenceSelector);
  await setSelector(2);
  return browser
    .click(endDateField)
    .expect(datePickerDay.count)
    .eql(21)
    .expect(datePickerDay.nth(20).textContent)
    .eql('22');
});

test('endDate is limited to startDate +90 if yearly', async browser => {
  await browser
    .pressKey('esc')
    .click(day5)
    .click(endDateField)
    .expect(datePickerTitle.textContent)
    .eql('October')
    .click(datePickerNextMonth)
    .expect(datePickerTitle.textContent)
    .eql('November')
    .click(datePickerNextMonth)
    .expect(datePickerTitle.textContent)
    .eql('December')
    .click(datePickerNextMonth)
    .expect(datePickerTitle.textContent)
    .eql('January')
    .click(datePickerNextMonth)
    .expect(datePickerTitle.textContent)
    .eql('February')
    .expect(datePickerDay.count)
    .eql(28)
    .expect(datePickerDay.nth(0).textContent)
    .eql('1')
    .expect(datePickerDay.nth(27).textContent)
    .eql('28')
    .click(datePickerDay.nth(0))
    .expect(endDateField.value)
    .eql('01/02/23')
    .pressKey('esc')
    .click(recurrenceSelector);
  await setSelector(3);
  return browser
    .click(endDateField)
    .expect(datePickerDay.count)
    .eql(3)
    .expect(datePickerDay.nth(0).textContent)
    .eql('1')
    .expect(datePickerDay.nth(2).textContent)
    .eql('3')
    .expect(endDateField.value)
    .eql('03/01/23');
});

test('endDate resets to max if change recurrence value and over max', async browser => {
  await browser
    .click(endDateField)
    .expect(datePickerTitle.textContent)
    .eql('October')
    .click(datePickerDay.nth(29))
    .expect(endDateField.value)
    .eql('31/10/22')
    .pressKey('esc')
    .click(recurrenceSelector);
  await setSelector(1);
  return browser.expect(endDateField.value).eql('07/10/22');
});

test('endDate resets to startDate if startDate moved past endDate', async browser =>
  browser
    .expect(endDateField.value)
    .eql('02/10/22')
    .click(startDateField)
    .expect(datePickerTitle.textContent)
    .eql('October')
    .click(datePickerDay.nth(20))
    .expect(endDateField.value)
    .eql('21/10/22'));

test('creating event resets data', async browser =>
  browser
    .typeText(closeReasonInput, 'Test Close')
    .click(confirmButton)
    .click(day2)
    .expect(closeReasonInput.value)
    .eql(''));

test('date on modal is correct', async browser =>
  browser.expect(cardTitle.innerText).eql('Create close event for 02/10/22'));

test('clicking close button closes dialog', async browser =>
  browser.click(closeDialogIcon).expect(closeReasonInput.visible).notOk());

test('can set an event to be on a specific weekday in the month', async browser => {
  await browser
    .typeText(closeReasonInput, 'Test Close')
    .expect(isDateToggle.exists)
    .notOk()
    .click(recurrenceSelector);

  await setSelector(2);
  await browser
    .expect(isDateToggle.visible)
    .ok()
    .expect(isDateToggle.nth(0).textContent)
    .eql('Repeat on date (02/10/22)')
    .expect(isDateToggle.nth(1).textContent)
    .eql('Repeat on day')
    .expect(weekOfMonthSelector.exists)
    .notOk()
    .click(isDateToggle.nth(1))
    .expect(weekOfMonthSelector.visible)
    .ok()
    .expect(weekOfMonthSelectorText.textContent)
    .eql('1st')
    .click(weekOfMonthSelector)
    .expect(selectorMenu.count)
    .eql(4)
    .expect(selectorMenu.nth(0).textContent)
    .eql('1st')
    .expect(selectorMenu.nth(1).textContent)
    .eql('2nd')
    .expect(selectorMenu.nth(2).textContent)
    .eql('3rd')
    .expect(selectorMenu.nth(3).textContent)
    .eql('4th');
  await setSelector(2);

  await browser.click(confirmButton);
  const [centre] = await getCentreByName('Example Centre OD Setup');
  const [user] = await getUserByName('testAdmin');
  const closeEventData = await getCloseEventByName('Test Close');
  return browser.expect(closeEventData).eql([
    {
      allDay: true,
      centre_id: centre.id,
      createdBy_id: user.id,
      description: '',
      eventType: 'User Close Event',
      id: closeEventData[0].id,
      link: '',
      name: 'Test Close',
      recurrences: 'RRULE:FREQ=MONTHLY;BYDAY=SU;BYSETPOS=3',
      start: closeEventData[0].start,
      timePeriod: parse('1 days')
    }
  ]);
});
