import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../../../globals';
import {
  addUserCentre,
  createCentreWithOpeningDaysSetup,
  getCentreByName,
  updateCentre
} from '../../../../generic-functions';
import {
  centreExpanderOpeningDays,
  centresTableRow1,
  centresTableRow4,
  centresTableRow5
} from '../../../../generic-selectors/Centres';

import { admin, nurse } from '../../../../custom-commands';

const daySelector = Selector('.daySelector').nth(0);
const daySelectorMon = daySelector.find('button').nth(0);
const daySelectorTue = daySelector.find('button').nth(1);
const daySelectorWed = daySelector.find('button').nth(2);
const daySelectorThu = daySelector.find('button').nth(3);
const daySelectorFri = daySelector.find('button').nth(4);
const daySelectorSat = daySelector.find('button').nth(5);
const daySelectorSun = daySelector.find('button').nth(6);
const centreClientDayWarningDialog = Selector('#centreClientDayWarningDialog');

fixture`components/Centres/Calendar/DaySelector`
  .page(`${baseUrl}/centres`)
  .beforeEach(async browser => {
    await seedDb(['base', 'centres']);
    return browser.navigateTo(`${baseUrl}/centres`).resizeWindow(1280, 720);
  })
  .after(destroyDb);

test('centres card day selector is visible', async browser => {
  await admin();
  return browser
    .click(centresTableRow1)
    .click(centreExpanderOpeningDays)
    .expect(daySelector.visible)
    .ok();
});

test('day selector shows correct days', async browser => {
  await admin();
  return browser
    .click(centresTableRow1)
    .click(centreExpanderOpeningDays)
    .expect(daySelectorMon.getStyleProperty('background-color'))
    .eql('rgba(0, 0, 0, 0)')
    .expect(daySelectorTue.getStyleProperty('background-color'))
    .eql('rgb(211, 47, 47)')
    .expect(daySelectorWed.getStyleProperty('background-color'))
    .eql('rgb(211, 47, 47)')
    .expect(daySelectorThu.getStyleProperty('background-color'))
    .eql('rgba(0, 0, 0, 0)')
    .expect(daySelectorFri.getStyleProperty('background-color'))
    .eql('rgb(211, 47, 47)')
    .expect(daySelectorSat.getStyleProperty('background-color'))
    .eql('rgba(0, 0, 0, 0)')
    .expect(daySelectorSun.getStyleProperty('background-color'))
    .eql('rgba(0, 0, 0, 0)');
});

test('selecting a day as admin changes it in database', async browser => {
  await admin();
  return browser
    .click(centresTableRow1)
    .click(centreExpanderOpeningDays)
    .click(daySelectorSun)
    .then(() =>
      getCentreByName('Example Centre').then(centres =>
        browser
          .expect(
            centres.filter((centre: { dayId: number }) => centre.dayId === 7)
              .length
          )
          .gt(1)
      )
    );
});

test('nurse cannot select day buttons', async browser => {
  await nurse();
  return browser
    .click(centresTableRow1)
    .click(centreExpanderOpeningDays)
    .click(daySelectorSun)
    .expect(daySelectorSun.hasClass('v-btn--active'))
    .notOk();
});

test('cannot select less than one day', async browser => {
  await admin();
  return browser
    .click(centresTableRow1)
    .click(centreExpanderOpeningDays)
    .click(daySelectorTue)
    .click(daySelectorWed)
    .click(daySelectorFri)
    .expect(daySelectorTue.getStyleProperty('background-color'))
    .eql('rgba(0, 0, 0, 0)')
    .expect(daySelectorWed.getStyleProperty('background-color'))
    .eql('rgba(0, 0, 0, 0)')
    .expect(daySelectorFri.getStyleProperty('background-color'))
    .eql('rgb(211, 47, 47)');
});

test('selected days update in real time', async browser => {
  await nurse();
  await browser.click(centresTableRow1).click(centreExpanderOpeningDays);
  await updateCentre('Example Centre', { openingDays: [2, 5] });
  await browser
    .expect(daySelectorMon.getStyleProperty('background-color'))
    .eql('rgba(0, 0, 0, 0)')
    .expect(daySelectorTue.getStyleProperty('background-color'))
    .eql('rgb(211, 47, 47)')
    .expect(daySelectorWed.getStyleProperty('background-color'))
    .eql('rgba(0, 0, 0, 0)')
    .expect(daySelectorThu.getStyleProperty('background-color'))
    .eql('rgba(0, 0, 0, 0)')
    .expect(daySelectorFri.getStyleProperty('background-color'))
    .eql('rgb(211, 47, 47)')
    .expect(daySelectorSat.getStyleProperty('background-color'))
    .eql('rgba(0, 0, 0, 0)')
    .expect(daySelectorSun.getStyleProperty('background-color'))
    .eql('rgba(0, 0, 0, 0)');
});

test('days are full on normal breakpoints', async browser => {
  await nurse();
  await browser
    .click(centresTableRow1)
    .click(centreExpanderOpeningDays)
    .expect(daySelectorMon.textContent)
    .eql('Monday')
    .expect(daySelectorTue.textContent)
    .eql('Tuesday')
    .expect(daySelectorWed.textContent)
    .eql('Wednesday')
    .expect(daySelectorThu.textContent)
    .eql('Thursday')
    .expect(daySelectorFri.textContent)
    .eql('Friday')
    .expect(daySelectorSat.textContent)
    .eql('Saturday')
    .expect(daySelectorSun.textContent)
    .eql('Sunday');
});

test('day selector shows correct days for newly created centre', async browser => {
  await createCentreWithOpeningDaysSetup();
  await admin();
  await updateCentre('Example Centre OD Setup', { setupComplete: true });
  await addUserCentre('testAdmin', 'Example Centre OD Setup');
  return browser
    .click(centresTableRow5)
    .click(centreExpanderOpeningDays)
    .expect(daySelectorMon.getStyleProperty('background-color'))
    .eql('rgba(0, 0, 0, 0)')
    .expect(daySelectorTue.getStyleProperty('background-color'))
    .eql('rgba(0, 0, 0, 0)')
    .expect(daySelectorWed.getStyleProperty('background-color'))
    .eql('rgba(0, 0, 0, 0)')
    .expect(daySelectorThu.getStyleProperty('background-color'))
    .eql('rgb(211, 47, 47)')
    .expect(daySelectorFri.getStyleProperty('background-color'))
    .eql('rgba(0, 0, 0, 0)')
    .expect(daySelectorSat.getStyleProperty('background-color'))
    .eql('rgba(0, 0, 0, 0)')
    .expect(daySelectorSun.getStyleProperty('background-color'))
    .eql('rgba(0, 0, 0, 0)');
});

test('warning does not appear when no clients visit that day', async browser => {
  await updateCentre('Example Centre', { openingDays: [2, 3, 5, 6] });
  await seedDb(['clients']);
  await admin();
  await browser
    .click(centresTableRow1)
    .click(centreExpanderOpeningDays)
    .click(daySelectorSat)
    .expect(centreClientDayWarningDialog.visible)
    .notOk();
});

test('warning does not appear when client visits but is inactive', async browser => {
  await seedDb(['clients']);
  await admin();
  await addUserCentre('testAdmin', 'Example Centre 4');
  await browser
    .click(centresTableRow4)
    .click(centreExpanderOpeningDays)
    .click(daySelectorTue)
    .expect(centreClientDayWarningDialog.visible)
    .notOk();
});

test('warning appears when clients visit on this day', async browser => {
  await seedDb(['clients']);
  await admin();
  await browser
    .click(centresTableRow1)
    .click(centreExpanderOpeningDays)
    .click(daySelectorFri)
    .expect(centreClientDayWarningDialog.visible)
    .ok();
});
