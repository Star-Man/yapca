/* eslint-disable no-await-in-loop */
import { Selector } from 'testcafe';
import { format } from 'date-fns';
import { baseUrl, destroyDb, seedDb } from '../../../../globals';
import { admin } from '../../../../custom-commands';
import {
  createCentreEvent,
  createCentreWithOpeningDaysSetup,
  deleteEvent,
  getCentreByName,
  getCloseEventByName,
  updateCentre
} from '../../../../generic-functions';
import {
  calendarPreviousButton,
  calendarTitle,
  countrySelector,
  dayLabels,
  eventDeleteButton,
  events,
  stateSelector
} from '../../../../generic-selectors/Centres';
import setSelector from '../../../../custom-commands/setSelectorMenuValue';

const calendarNextButton = Selector('#calendarNextButton');
const day25 = dayLabels.nth(30);
const setTodayButton = Selector('#setTodayButton');
const closedOnPublicHolidaysOptions = Selector(
  '#closedOnPublicHolidaysSwitch'
).find('.q-btn');
const header = Selector('#centreCalendar').child('.q-toolbar');

fixture`components/centres/calendar/CentreCalendar`
  .skipJsErrors({
    message: /.*resizeobserver.*/gi
  })
  .beforeEach(async browser => {
    await seedDb();
    await createCentreWithOpeningDaysSetup();
    await browser.navigateTo(baseUrl).resizeWindow(1280, 720);
    await admin();
    let titleText = '';
    while (titleText !== 'October 2022') {
      await browser.click(calendarPreviousButton);
      titleText = await calendarTitle.textContent;
    }
  })
  .after(destroyDb);

test('changing month updates title', async browser =>
  browser
    .expect(calendarTitle.textContent)
    .eql('October 2022')
    .click(calendarNextButton)
    .click(calendarNextButton)
    .click(calendarNextButton)
    .expect(calendarTitle.textContent)
    .eql('January 2023')
    .click(calendarPreviousButton)
    .expect(calendarTitle.textContent)
    .eql('December 2022'));

test('events are visible', browser =>
  browser
    .expect(events.count)
    .eql(1)
    .expect(day25.textContent)
    .eql('25')
    .expect(events.nth(0).textContent)
    .eql('October Bank Holiday')
    .expect(events.nth(0).getStyleProperty('background-color'))
    .eql('rgb(176, 0, 32)'));

test('events update when added elsewhere', async browser => {
  const [centre] = await getCentreByName('Example Centre OD Setup');
  await createCentreEvent({
    centre: centre.id,
    name: 'Example Close Event 1',
    start: '2010-10-03T00:00:00.000Z',
    timePeriod: 172800,
    recurrences: 'yearly',
    eventType: 'User Close Event',
    allDay: true
  });
  return browser
    .expect(events.count)
    .eql(2)
    .expect(events.nth(0).textContent)
    .eql('Example Close Event 1')
    .expect(events.nth(1).getStyleProperty('background-color'))
    .eql('rgb(176, 0, 32)');
});

test('events update when country is updated', async browser => {
  await browser.click(countrySelector);
  await setSelector(5);
  return browser
    .expect(events.count)
    .eql(2)
    .expect(events.nth(0).textContent)
    .eql('Nossa Senhora Aparecida')
    .expect(events.nth(1).textContent)
    .eql('Finados');
});

test('events update when state is updated', async browser => {
  await browser.click(countrySelector);
  await setSelector(5);
  await browser.click(stateSelector);
  await setSelector(2);
  return browser
    .expect(events.count)
    .eql(3)
    .expect(events.nth(0).textContent)
    .eql('Criação do estado');
});

test('events update when changing month', async browser =>
  browser
    .click(calendarNextButton)
    .click(calendarNextButton)
    .expect(events.count)
    .eql(2));

test('set today button updates title', async browser =>
  browser
    .click(setTodayButton)
    .expect(calendarTitle.textContent)
    .eql(format(new Date(), 'MMMM yyyy')));

test('clicking closed on public holiday switch updates database', async browser => {
  await browser
    .expect(closedOnPublicHolidaysOptions.nth(0).getAttribute('aria-pressed'))
    .eql('true')
    .expect(closedOnPublicHolidaysOptions.nth(1).getAttribute('aria-pressed'))
    .eql('false')
    .click(closedOnPublicHolidaysOptions.nth(1))
    .expect(closedOnPublicHolidaysOptions.nth(0).getAttribute('aria-pressed'))
    .eql('false')
    .expect(closedOnPublicHolidaysOptions.nth(1).getAttribute('aria-pressed'))
    .eql('true');
  const centreData = await getCentreByName('Example Centre OD Setup');
  return browser
    .expect(
      centreData.filter(
        (centre: { closedOnPublicHolidays: boolean }) =>
          !centre.closedOnPublicHolidays
      ).length
    )
    .gt(0);
});

test('closed on public holiday switch changes when updated elsewhere', async browser => {
  await updateCentre('Example Centre OD Setup', {
    closedOnPublicHolidays: false
  });
  return browser
    .expect(closedOnPublicHolidaysOptions.nth(0).getAttribute('aria-pressed'))
    .eql('false')
    .expect(closedOnPublicHolidaysOptions.nth(1).getAttribute('aria-pressed'))
    .eql('true');
});

test('closed on public holiday switch hides public holidays', async browser =>
  browser
    .click(closedOnPublicHolidaysOptions.nth(1))
    .expect(events.count)
    .eql(0));

test('closed on public holiday switch does not hide user holidays', async browser => {
  const [centre] = await getCentreByName('Example Centre OD Setup');
  await createCentreEvent({
    centre: centre.id,
    name: 'Example Close Event 1',
    start: '2010-10-03T00:00:00.000Z',
    timePeriod: 172800,
    recurrences: 'yearly',
    eventType: 'User Close Event',
    allDay: true
  });
  return browser
    .click(closedOnPublicHolidaysOptions.nth(1))
    .expect(events.count)
    .eql(1);
});

test('public holidays hide when updated elsewhere', async browser => {
  await updateCentre('Example Centre OD Setup', {
    closedOnPublicHolidays: false
  });
  return browser.expect(events.count).eql(0);
});

test('event can be deleted on click', async browser => {
  const [centre] = await getCentreByName('Example Centre OD Setup');
  await createCentreEvent({
    centre: centre.id,
    name: 'Example Close Event 1',
    start: '2010-10-03T00:00:00.000Z',
    timePeriod: 172800,
    recurrences: 'yearly',
    eventType: 'User Close Event',
    allDay: true
  });
  return browser
    .expect(events.count)
    .eql(2)
    .click(events.nth(0))
    .click(eventDeleteButton)
    .expect(events.count)
    .eql(1);
});

test('event deletion updates database', async browser => {
  const [centre] = await getCentreByName('Example Centre OD Setup');
  await createCentreEvent({
    centre: centre.id,
    name: 'Example Close Event 1',
    start: '2010-10-03T00:00:00.000Z',
    timePeriod: 172800,
    recurrences: 'yearly',
    eventType: 'User Close Event',
    allDay: true
  });
  await browser
    .click(events.withText('Example Close Event 1'))
    .click(eventDeleteButton);
  const closeEventData = await getCloseEventByName('Example Close Event 1');
  return browser.expect(closeEventData).eql([]);
});

test('events update when deleted elsewhere', async browser => {
  const [centre] = await getCentreByName('Example Centre OD Setup');
  await createCentreEvent({
    centre: centre.id,
    name: 'Example Close Event 1',
    start: '2010-10-03T00:00:00.000Z',
    timePeriod: 172800,
    recurrences: 'yearly',
    eventType: 'User Close Event',
    allDay: true
  });
  await deleteEvent('Example Close Event 1');
  return browser.expect(events.count).eql(1);
});

test('calendar header has centre color', browser =>
  browser
    .expect(header.getStyleProperty('background-color'))
    .eql('rgb(104, 159, 56)'));
