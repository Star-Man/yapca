import {
  createIncompleteCentres,
  getCentreByName,
  updateCentre
} from '../../../../generic-functions';
import { baseUrl, destroyDb, seedDb } from '../../../../globals';
import { admin, url } from '../../../../custom-commands';
import {
  countrySelector,
  selectorMenu,
  stateSelector
} from '../../../../generic-selectors/Centres';
import setSelector from '../../../../custom-commands/setSelectorMenuValue';

fixture`components/Centres/Calendar/LocationSelect`
  .page(baseUrl)
  .skipJsErrors({
    message: /.*resizeobserver.*/gi
  })
  .beforeEach(async browser => {
    await seedDb();
    await createIncompleteCentres();
    await browser.navigateTo(baseUrl).resizeWindow(1280, 720);
    return admin();
  })
  .after(destroyDb);

test('country selector has correct countries', async browser =>
  browser
    .click(countrySelector)
    .expect(selectorMenu.count)
    .eql(65)
    .expect(selectorMenu.nth(0).textContent)
    .eql('Angola')
    .expect(selectorMenu.nth(64).textContent)
    .eql('香港'));

test('selecting country with states shows state selector', async browser => {
  await browser
    .expect(stateSelector.exists)
    .notOk()
    .click(countrySelector)
    .hover(selectorMenu.nth(0));
  await setSelector(3);
  return browser.expect(stateSelector.visible).ok();
});

test('selecting country without states hides state selector', async browser => {
  await browser.click(countrySelector).hover(selectorMenu.nth(0));
  await setSelector(15);
  return browser.expect(stateSelector.exists).notOk();
});

test('selecting country updates database', async browser => {
  await browser.click(countrySelector).hover(selectorMenu.nth(0));
  await setSelector(7);
  const centres = await getCentreByName('Example Centre 5');
  return browser
    .expect(
      centres.filter((centre: { country: string }) => centre.country === 'CZ')
        .length
    )
    .gte(1);
});

test('selected country updates when changed elsewhere', async browser => {
  await updateCentre('Example Centre 5', { country: 'DE' });
  return browser
    .expect(countrySelector.find('span').textContent)
    .eql('Deutschland');
});

test('selected country is correct on load', async browser => {
  await updateCentre('Example Centre 5', { country: 'DE' });
  await browser.eval(() => document.location.reload());
  return browser
    .expect(countrySelector.find('span').textContent)
    .eql('Deutschland');
});

test('state selector has correct countries', async browser => {
  await updateCentre('Example Centre 5', { country: 'DE' });
  return browser
    .click(stateSelector)
    .expect(selectorMenu.count)
    .eql(16)
    .expect(selectorMenu.nth(4).textContent)
    .eql('Hansestadt Bremen')
    .expect(selectorMenu.nth(13).textContent)
    .eql('Sachsen-Anhalt');
});

test('selecting state updates database', async browser => {
  await updateCentre('Example Centre 5', { country: 'DE' });
  await browser.click(stateSelector);
  await setSelector(7);
  const centres = await getCentreByName('Example Centre 5');
  return browser
    .expect(
      centres.filter((centre: { state: string }) => centre.state === 'MV')
        .length
    )
    .gte(1);
});

test('selected state updates when changed elsewhere', async browser => {
  await updateCentre('Example Centre 5', { country: 'DE', state: 'BW' });
  return browser
    .expect(stateSelector.find('span').textContent)
    .eql('Baden-Württemberg');
});

test('selected state is correct on load', async browser => {
  await updateCentre('Example Centre 5', { country: 'DE', state: 'HE' });
  await browser
    .expect(url())
    .eql(`${baseUrl}/centre-setup?nextUrl=/clients`)
    .eval(() => location.reload());
  return browser.expect(stateSelector.find('span').textContent).eql('Hessen');
});
