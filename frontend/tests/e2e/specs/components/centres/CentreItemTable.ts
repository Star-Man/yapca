import { baseUrl, destroyDb, seedDb } from '../../../globals';
import { addUserCentre, removeUserCentre } from '../../../generic-functions';
import { admin, nurse } from '../../../custom-commands';
import {
  centreSubSwitch,
  centresTableRow1,
  needsExpander,
  needsTable,
  policiesExpander,
  policiesTable
} from '../../../generic-selectors/Centres';

fixture`components/centres/CentreItemTable`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres']);
    return browser.navigateTo(baseUrl).resizeWindow(1280, 720);
  })
  .after(destroyDb);

test('needs/policies are visible when subscribed to centre', async browser => {
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/centres`)
    .click(centresTableRow1)
    .click(needsExpander)
    .expect(needsTable.visible)
    .ok()
    .click(policiesExpander)
    .expect(policiesTable.visible)
    .ok();
});

test('policies/needs row is not visible when not subscribed to centre', async browser => {
  await addUserCentre('testAdmin', 'Example Centre 4');
  await admin();
  return browser
    .navigateTo(`${baseUrl}/centres`)
    .click(centresTableRow1)
    .click(centreSubSwitch)
    .expect(needsExpander.visible)
    .notOk()
    .expect(policiesExpander.visible)
    .notOk();
});

test('policies/needs row becomes visible when subscribed elsewhere', async browser => {
  await addUserCentre('testAdmin', 'Example Centre 2');
  await admin();
  await browser.navigateTo(`${baseUrl}/centres`).click(centresTableRow1);
  await removeUserCentre('testAdmin', 'Example Centre');
  await browser
    .expect(needsExpander.visible)
    .notOk()
    .expect(policiesExpander.visible)
    .notOk();
  await addUserCentre('testAdmin', 'Example Centre');
  await browser
    .expect(needsExpander.visible)
    .ok()
    .expect(policiesExpander.visible)
    .ok();
});
