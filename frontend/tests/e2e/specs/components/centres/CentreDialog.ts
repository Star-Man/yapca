import { Selector } from 'testcafe';
import { format, formatISO, startOfDay } from 'date-fns';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  addUserCentre,
  createCentreEvent,
  deleteCentre,
  getCentreByName,
  getUserByName,
  removeUserCentre,
  updateCentre
} from '../../../generic-functions';
import { admin, nurse } from '../../../custom-commands';
import { infoSnackbar, snackbarText } from '../../../generic-selectors/Auth';
import {
  calendarTitle,
  centreCards,
  centreExpanderColorSelect,
  centreExpanderLocationSelect,
  centreMatchingColorDialog,
  centreSubSwitch,
  centresTableRow1,
  centresTableRow2,
  centresTableRows,
  countrySelector,
  day3,
  eventDeleteButton,
  events
} from '../../../generic-selectors/Centres';
import {
  accentColorSelectors,
  closeDialogButton,
  confirmButton,
  deleteConfirmButton,
  goBackButton
} from '../../../generic-selectors/Layout';

const centreDeleteButton = centreCards
  .nth(0)
  .find('.deleteCentreButton')
  .nth(0);
const centreSubSwitchLabel = centreSubSwitch.child('.q-toggle__label');
const createCloseEventDialog = Selector('#createCloseEventDialog');

const centreMatchingColorCentreHeaders = Selector(
  '.centreMatchingColorCentreHeader'
);
const centreMatchingColorInfoText = Selector('#centreMatchingColorInfoText');
const selectedAccentColor = Selector('#colorInput')
  .find('.accentSelectButton')
  .find('.q-icon')
  .find('svg')
  .parent('.accentSelectButton');
const centreDeleteDialog = Selector('#centreDeleteDialog');
const centreExpanderClosingDays = Selector('#centreExpanderClosingDays');

fixture`components/centres/CentreDialog`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres']);
    return browser.navigateTo(`${baseUrl}/centres`).resizeWindow(1280, 720);
  })
  .after(destroyDb);

test('delete centre button is visible for admin', async browser => {
  await admin();
  return browser
    .click(centresTableRow1)
    .expect(centreDeleteButton.visible)
    .ok();
});

test('delete centre button is not visible for nurse', async browser => {
  await nurse();
  return browser
    .click(centresTableRow1)
    .expect(centreDeleteButton.visible)
    .notOk();
});

test('delete centre button opens confirm dialog', async browser => {
  await admin();
  return browser
    .click(centresTableRow1)
    .expect(centreDeleteDialog.visible)
    .notOk()
    .click(centreDeleteButton)
    .expect(centreDeleteDialog.visible)
    .ok();
});

test('delete centre dialog closes on pressing go back button', async browser => {
  await admin();
  return browser
    .click(centresTableRow1)
    .click(centreDeleteButton)
    .click(deleteConfirmButton)
    .expect(centreDeleteDialog.visible)
    .notOk();
});

test('delete centre confirm button updates database', async browser => {
  await admin();
  return browser
    .click(centresTableRow1)
    .click(centreDeleteButton)
    .click(deleteConfirmButton)
    .then(() =>
      getCentreByName('Example Centre').then(centres =>
        browser.expect(centres.length).eql(0)
      )
    );
});

test('delete centre confirm button removes centre from table', async browser => {
  await addUserCentre('testAdmin', 'Example Centre 2');
  await admin();
  return browser
    .expect(centresTableRows.count)
    .eql(4)
    .click(centresTableRow1)
    .click(centreDeleteButton)
    .click(deleteConfirmButton)
    .expect(centresTableRows.count)
    .eql(3);
});

test('delete centre confirm button closes confirm dialog', async browser => {
  await admin();
  return browser
    .click(centresTableRow1)
    .click(centreDeleteButton)
    .click(deleteConfirmButton)
    .expect(centreDeleteDialog.visible)
    .notOk();
});

test('delete centre confirm button closes centre dialog', async browser => {
  await admin();
  return browser
    .click(centresTableRow1)
    .click(centreDeleteButton)
    .click(deleteConfirmButton)
    .expect(centreCards.visible)
    .notOk();
});

test('centres update if deleted elsewhere', async browser => {
  await admin();
  await browser.click(centresTableRow1);
  await deleteCentre('Example Centre 2');
  await browser.expect(centresTableRows.count).eql(3);
});

test('last centre does not show delete centre button', async browser => {
  await admin();
  await browser.click(centresTableRow1);
  await deleteCentre('Example Centre 2');
  await browser.expect(centreDeleteButton.visible).ok();
  await deleteCentre('Example Centre 3');
  await browser.expect(centreDeleteButton.visible).ok();
  await deleteCentre('Example Centre 4');
  await browser.expect(centreDeleteButton.visible).notOk();
});

test('subscribe switch is correct on load', async browser => {
  await admin();
  return browser
    .click(centresTableRow1)
    .expect(centreSubSwitch.getAttribute('aria-checked'))
    .eql('true');
});

test('subscribe switch text is correct on load', async browser => {
  await admin();
  return browser
    .click(centresTableRow1)
    .expect(centreSubSwitchLabel.textContent)
    .eql('You are subscribed to this centre');
});

test('subscribe switch text changes on click', async browser => {
  await admin();
  await addUserCentre('testAdmin', 'Example Centre 4');
  return browser
    .click(centresTableRow1)
    .click(centreSubSwitch)
    .expect(centreSubSwitchLabel.textContent)
    .eql('You are not subscribed to this centre');
});

test('subscribe switch changes when updated elsewhere', async browser => {
  await admin();
  await browser.click(centresTableRow2);
  await browser
    .expect(centreSubSwitch.getAttribute('aria-checked'))
    .eql('false');
  await addUserCentre('testAdmin', 'Example Centre 2');
  return browser
    .expect(centreSubSwitch.getAttribute('aria-checked'))
    .eql('true');
});

test('subscribe switch label changes when updated elsewhere', async browser => {
  await admin();
  await browser.click(centresTableRow1);
  await addUserCentre('testAdmin', 'Example Centre 2');
  await removeUserCentre('testAdmin', 'Example Centre');
  return browser
    .expect(centreSubSwitchLabel.textContent)
    .eql('You are not subscribed to this centre');
});

test('centre dialog closes when unsubscribed as nurse', async browser => {
  await nurse();
  await browser.click(centresTableRow1);
  await removeUserCentre('testNurse', 'Example Centre');
  return browser.expect(centreCards.visible).notOk();
});

test('subscribe switch updates in database on click', async browser => {
  await admin();
  const centreOneId = (await getCentreByName('Example Centre'))[0].id;
  await addUserCentre('testAdmin', 'Example Centre 2');
  return browser
    .click(centresTableRow1)
    .click(centreSubSwitch)
    .expect(centreSubSwitch.getAttribute('aria-checked'))
    .eql('false')
    .then(() =>
      getUserByName('testAdmin').then(users =>
        browser
          .expect(
            users.filter(
              (user: { centreId: number }) => user.centreId === centreOneId
            ).length
          )
          .eql(0)
      )
    );
});

test('user auto-subs to only centre on load', async browser => {
  await admin();
  const centreOneId = (await getCentreByName('Example Centre'))[0].id;
  await deleteCentre('Example Centre 2');
  await deleteCentre('Example Centre 3');
  await deleteCentre('Example Centre 4');
  await browser
    .click(centresTableRow1)
    .then(() =>
      getUserByName('testAdmin').then(users =>
        browser
          .expect(
            users.filter(
              (user: { centreId: number }) => user.centreId === centreOneId
            ).length
          )
          .gte(1)
      )
    );
});

test('subscribe switch does not change when last centre', async browser => {
  await admin();
  return browser
    .click(centresTableRow1)
    .click(centreSubSwitch)
    .expect(centreSubSwitch.getAttribute('aria-checked'))
    .eql('true');
});

test('notification appears when unsubbing from last centre', async browser => {
  await admin();
  return browser
    .click(centresTableRow1)
    .click(centreSubSwitch)
    .expect(infoSnackbar.exists)
    .ok()
    .expect(snackbarText.textContent)
    .eql('You must be subscribed to at least one centre')
    .expect(infoSnackbar.getStyleProperty('background-color'))
    .eql('rgb(49, 204, 236)');
});

test('notification appears when autosubbing to new default centre', async browser => {
  await admin();
  await deleteCentre('Example Centre 3');
  await deleteCentre('Example Centre 4');
  await browser.navigateTo(`${baseUrl}/centres`).click(centresTableRow1);
  await deleteCentre('Example Centre');
  return browser
    .expect(infoSnackbar.exists)
    .ok()
    .expect(snackbarText.textContent)
    .eql(
      'Your only subscribed centre was removed, you were auto subscribed to Example Centre 2'
    )
    .expect(infoSnackbar.getStyleProperty('background-color'))
    .eql('rgb(49, 204, 236)');
});

test('switch changes when autosubbing to new default centre', async browser => {
  await admin();
  await deleteCentre('Example Centre 3');
  await deleteCentre('Example Centre 4');
  await browser.navigateTo(`${baseUrl}/centres`);
  await deleteCentre('Example Centre 2');
  return browser
    .click(centresTableRow1)
    .expect(centreSubSwitch.getAttribute('aria-checked'))
    .eql('true');
});

test('color selector shows centre color', async browser => {
  await nurse();
  return browser
    .click(centresTableRow1)
    .click(centreExpanderColorSelect)
    .expect(selectedAccentColor.getStyleProperty('background-color'))
    .eql('rgb(0, 151, 167)');
});

test('changing color updates database', async browser => {
  await admin();
  await browser
    .click(centresTableRow1)
    .click(centreExpanderColorSelect)
    .expect(accentColorSelectors.count)
    .eql(26)
    .click(accentColorSelectors.nth(3))
    .expect(selectedAccentColor.getStyleProperty('background-color'))
    .eql('rgb(123, 31, 162)');
  const centreOne = await getCentreByName('Example Centre');
  return browser
    .expect(
      centreOne.filter(
        (centre: { color: string }) => centre.color === '#7B1FA2'
      ).length
    )
    .gt(0);
});

test('cannot change color as nurse', async browser => {
  await nurse();
  await browser
    .click(centresTableRow1)
    .click(centreExpanderColorSelect)
    .click(accentColorSelectors.nth(3));
  const centreOne = await getCentreByName('Example Centre');
  return browser
    .expect(
      centreOne.filter(
        (centre: { color: string }) => centre.color === '#7B1FA2'
      ).length
    )
    .eql(0);
});

test('color updates when changed elsewhere', async browser => {
  await nurse();
  await browser.click(centresTableRow1).click(centreExpanderColorSelect);
  await updateCentre('Example Centre', { color: '#FFAB91' });
  return browser
    .expect(selectedAccentColor.getStyleProperty('background-color'))
    .eql('rgb(255, 171, 145)');
});

test('clicking a non conflicting color does not open dialog', async browser => {
  await admin();
  return browser

    .click(centresTableRow1)
    .click(centreExpanderColorSelect)
    .click(accentColorSelectors.nth(17))
    .expect(centreMatchingColorDialog.visible)
    .notOk();
});

test('clicking conflicting color opens dialog', async browser => {
  await admin();
  return browser

    .click(centresTableRow1)
    .click(centreExpanderColorSelect)
    .click(accentColorSelectors.nth(1))
    .expect(centreMatchingColorDialog.visible)
    .ok();
});

test('clicking conflicting color does nothing as nurse', async browser => {
  await nurse();
  return browser

    .click(centresTableRow1)
    .click(centreExpanderColorSelect)
    .click(accentColorSelectors.nth(1))
    .expect(centreMatchingColorDialog.visible)
    .notOk();
});

test('clicking go back on dialog closes dialog', async browser => {
  await admin();
  return browser

    .click(centresTableRow1)
    .click(centreExpanderColorSelect)
    .click(accentColorSelectors.nth(1))
    .click(goBackButton)
    .expect(centreMatchingColorDialog.visible)
    .notOk();
});

test('clicking go back on dialog reverts selected color to previous', async browser => {
  await admin();
  return browser
    .click(centresTableRow1)
    .click(centreExpanderColorSelect)
    .click(accentColorSelectors.nth(1))
    .click(goBackButton)
    .expect(selectedAccentColor.getStyleProperty('background-color'))
    .eql('rgb(0, 151, 167)');
});

test('clicking go back does not update database', async browser => {
  await admin();
  await browser
    .click(centresTableRow1)
    .click(centreExpanderColorSelect)
    .click(accentColorSelectors.nth(1))
    .click(goBackButton);
  const centreOne = await getCentreByName('Example Centre');
  return browser
    .expect(
      centreOne.filter(
        (centre: { color: string }) => centre.color === '#0097A7'
      ).length
    )
    .gt(0)
    .expect(
      centreOne.filter(
        (centre: { color: string }) => centre.color === '#D32F2F'
      ).length
    )
    .eql(0);
});

test('clicking continue on dialog closes dialog', async browser => {
  await admin();
  return browser
    .click(centresTableRow1)
    .click(centreExpanderColorSelect)
    .click(accentColorSelectors.nth(1))
    .click(confirmButton)
    .expect(centreMatchingColorDialog.visible)
    .notOk();
});

test('clicking continue on conflict dialog updates database', async browser => {
  await admin();
  await browser
    .click(centresTableRow1)
    .click(centreExpanderColorSelect)
    .click(accentColorSelectors.nth(1))
    .click(confirmButton);
  const centreOne = await getCentreByName('Example Centre');
  return browser
    .expect(
      centreOne.filter(
        (centre: { color: string }) => centre.color === '#0097A7'
      ).length
    )
    .eql(0)
    .expect(
      centreOne.filter(
        (centre: { color: string }) => centre.color === '#D32F2F'
      ).length
    )
    .gt(0);
});

test('clicking continue on conflict dialog updates selected color', async browser => {
  await admin();
  return browser
    .click(centresTableRow1)
    .click(centreExpanderColorSelect)
    .click(accentColorSelectors.nth(1))
    .click(confirmButton)
    .expect(selectedAccentColor.getStyleProperty('background-color'))
    .eql('rgb(211, 47, 47)');
});

test('conflict dialog shows list of conflicting centres', async browser => {
  await updateCentre('Example Centre 4', { color: '#D32F2F' });
  await admin();
  return browser
    .click(centresTableRow1)
    .click(centreExpanderColorSelect)
    .click(accentColorSelectors.nth(1))
    .expect(centreMatchingColorCentreHeaders.count)
    .eql(2)
    .expect(centreMatchingColorCentreHeaders.nth(0).textContent)
    .eql('Example Centre 2')
    .expect(centreMatchingColorCentreHeaders.nth(1).textContent)
    .eql('Example Centre 4');
});

test('conflict dialog sub text is correct with one centre', async browser => {
  await admin();
  return browser
    .click(centresTableRow1)
    .click(centreExpanderColorSelect)
    .click(accentColorSelectors.nth(1))
    .expect(centreMatchingColorInfoText.textContent)
    .eql(
      ' The centre below has the same colour as the colour you selected. ' +
        'This will make it hard to tell these centres apart in the calendar. ' +
        'Are you sure you want to pick this colour? '
    );
});

test('conflict dialog sub text is correct with multiple centres', async browser => {
  await updateCentre('Example Centre 4', { color: '#D32F2F' });
  await admin();
  return browser
    .click(centresTableRow1)
    .click(centreExpanderColorSelect)
    .click(accentColorSelectors.nth(1))
    .expect(centreMatchingColorInfoText.textContent)
    .eql(
      ' The centres below have the same colour as the colour you selected. ' +
        'This will make it hard to tell these centres apart in the calendar. ' +
        'Are you sure you want to pick this colour? '
    );
});

test('location selector is visible', async browser => {
  await updateCentre('Example Centre 3', { color: '#D32F2F' });
  await nurse();
  return browser
    .click(centresTableRow1)
    .click(centreExpanderLocationSelect)
    .expect(countrySelector.visible)
    .ok();
});

test('close button closes modal', async browser => {
  await admin();
  return browser
    .click(centresTableRow1)
    .click(closeDialogButton)
    .expect(centreCards.visible)
    .notOk();
});

test('calendar is visible closing days expander', async browser => {
  await admin();
  return browser
    .click(centresTableRow1)
    .click(centreExpanderClosingDays)
    .expect(calendarTitle.textContent)
    .eql(format(new Date(), 'MMMM yyyy'));
});

test('cannot open create event modal as nurse user', async browser => {
  await nurse();
  return browser
    .click(centresTableRow1)
    .click(centreExpanderClosingDays)
    .click(day3)
    .expect(createCloseEventDialog.exists)
    .notOk();
});

test('cannot delete close event as nurse user', async browser => {
  const [centre] = await getCentreByName('Example Centre');
  await createCentreEvent({
    centre: centre.id,
    name: 'Example Close Event 1',
    start: formatISO(startOfDay(new Date())),
    timePeriod: 172800,
    recurrences: 'yearly',
    eventType: 'User Close Event',
    allDay: true
  });
  await nurse();
  return browser
    .click(centresTableRow1)
    .click(centreExpanderClosingDays)
    .click(events.withText('Example Close Event 1'))
    .expect(eventDeleteButton.exists)
    .notOk();
});
