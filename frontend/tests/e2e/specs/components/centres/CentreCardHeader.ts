import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  addUserCentre,
  getCentreByName,
  removeUserCentre,
  updateCentre
} from '../../../generic-functions';
import { admin, inputValidationMessage, nurse } from '../../../custom-commands';
import {
  centreCards,
  centreExpanderColorSelect,
  centreSubSwitch,
  centresTableRow1
} from '../../../generic-selectors/Centres';

const centreCardTitle = Selector('.centreTitleText').nth(0);
const centreNameInput = centreCards
  .nth(0)
  .find('.centreNameInput')
  .find('input');
const cardHeader = Selector('#centreDialog').find('.cardHeader');

fixture`components/centres/CentreCardHeader`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres']);
    return browser.navigateTo(baseUrl).resizeWindow(1280, 720);
  })
  .after(destroyDb);

test('centres card should have centre name as title', async browser => {
  await admin();
  return browser
    .navigateTo(`${baseUrl}/centres`)
    .click(centresTableRow1)
    .expect(centreCardTitle.textContent)
    .eql('Example Centre');
});

test('centre name becomes input when clicked by admin', async browser => {
  await admin();
  return browser
    .navigateTo(`${baseUrl}/centres`)
    .click(centresTableRow1)
    .expect(centreNameInput.visible)
    .notOk()
    .click(centreCardTitle)
    .expect(centreNameInput.visible)
    .ok();
});

test('centre name does not become input when clicked by nurse', async browser => {
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/centres`)
    .click(centresTableRow1)
    .expect(centreNameInput.visible)
    .notOk()
    .click(centreCardTitle)
    .expect(centreNameInput.visible)
    .notOk();
});

test('changed centre name updates in database', async browser => {
  await admin();
  await browser
    .navigateTo(`${baseUrl}/centres`)
    .click(centresTableRow1)
    .click(centreCardTitle)
    .typeText(centreNameInput, '1234')
    .pressKey('enter');
  const centres = await getCentreByName('Example Centre1234');
  return browser.expect(centres.length).gt(0);
});

test('centre name updates when updated elsewhere', async browser => {
  await admin();
  await browser.navigateTo(`${baseUrl}/centres`).click(centresTableRow1);
  await updateCentre('Example Centre', { name: 'New Centre Name' });
  await browser.expect(centreCardTitle.textContent).eql('New Centre Name');
});

test('centre name reverts to text when unselected', async browser => {
  await admin();
  return browser
    .navigateTo(`${baseUrl}/centres`)
    .click(centresTableRow1)
    .click(centreCardTitle)
    .expect(centreNameInput.visible)
    .ok()
    .click(centreExpanderColorSelect)
    .expect(centreNameInput.visible)
    .notOk();
});

test('centre name shows validation error when empty', async browser => {
  await admin();
  return browser
    .navigateTo(`${baseUrl}/centres`)
    .click(centresTableRow1)
    .click(centreCardTitle)
    .selectText(centreNameInput)
    .pressKey('delete')
    .expect(inputValidationMessage(centreNameInput))
    .eql("Centre name can't be blank");
});

test('centre name does not change if validation error', async browser => {
  await admin();
  return browser
    .navigateTo(`${baseUrl}/centres`)
    .click(centresTableRow1)
    .click(centreCardTitle)
    .selectText(centreNameInput)
    .pressKey('delete')
    .click(centreExpanderColorSelect)
    .expect(inputValidationMessage(centreNameInput))
    .eql("Centre name can't be blank");
});

test('centre shows validation error if name is not unique', async browser => {
  await admin();
  return browser
    .navigateTo(`${baseUrl}/centres`)
    .click(centresTableRow1)
    .click(centreCardTitle)
    .selectText(centreNameInput)
    .pressKey('delete')
    .typeText(centreNameInput, 'Example Centre 2')
    .expect(inputValidationMessage(centreNameInput))
    .eql('Centre name must be unique');
});

test('centre header color is correct on load', async browser => {
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/centres`)
    .click(centresTableRow1)
    .expect(cardHeader.getStyleProperty('background-color'))
    .eql('rgb(0, 151, 167)');
});

test('centre header color changes when updated elsewhere', async browser => {
  await admin();
  await browser.navigateTo(`${baseUrl}/centres`).click(centresTableRow1);
  await addUserCentre('testAdmin', 'Example Centre 2');
  await removeUserCentre('testAdmin', 'Example Centre');
  return browser
    .expect(cardHeader.getStyleProperty('background-color'))
    .eql('rgb(158, 158, 158)');
});

test('centre header color changes on click', async browser => {
  await admin();
  await addUserCentre('testAdmin', 'Example Centre 4');
  return browser
    .navigateTo(`${baseUrl}/centres`)
    .click(centresTableRow1)
    .click(centreSubSwitch)
    .expect(cardHeader.getStyleProperty('background-color'))
    .eql('rgb(158, 158, 158)');
});
