import { Selector } from 'testcafe';
import { admin, nurse, url } from '../../../custom-commands';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  createIncompleteCentres,
  getCentreByName,
  updateCentre
} from '../../../generic-functions';
import {
  deleteConfirmButton,
  deleteConfirmDialog,
  selectedStepHighlight
} from '../../../generic-selectors/Layout';

const centreNavBar = Selector('#centreNavBar');
const createCentreButton = Selector('#createCentreButton');
const centreSetupStepper = Selector('#centreSetupStepper');
const logoutIcon = Selector('#sidebar-icon-logout');
const finishCentreButtons = Selector('.finishCentreButton');

fixture`components/centres/AddCentreNavBar Setup Database`
  .skipJsErrors({
    message: /.*resizeobserver.*/gi
  })
  .beforeEach(async browser => {
    await seedDb(['base', 'centres']);
    await browser.resizeWindow(1280, 720).navigateTo(`${baseUrl}/centres`);
    return admin();
  })
  .after(destroyDb);

test('nav bar is visible for admin', browser =>
  browser.expect(centreNavBar.visible).ok());

test('nav bar is not visible for nurse', async browser => {
  await browser.click(logoutIcon);
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/centres`)
    .expect(centreNavBar.visible)
    .notOk();
});

test('nav bar shows centre create button', browser =>
  browser
    .expect(createCentreButton.visible)
    .ok()
    .expect(createCentreButton.textContent)
    .eql('Add New Centre'));

test('create centre button opens centre setup page', browser =>
  browser.click(createCentreButton).expect(centreSetupStepper.visible).ok());

test('create centre button passes current page url to centre setup page', browser =>
  browser
    .click(createCentreButton)
    .expect(url())
    .eql(`${baseUrl}/centre-setup?nextUrl=/centres`));

test('nav bar shows no incomplete centre buttons if none exist', browser =>
  browser.expect(finishCentreButtons.exists).notOk());

test('nav bar shows incomplete centre buttons if some exist', async browser => {
  await createIncompleteCentres();
  return browser.expect(finishCentreButtons.count).eql(6);
});

test('incomplete centre buttons show name of incomplete centres', async browser => {
  await createIncompleteCentres();
  return browser
    .expect(finishCentreButtons.nth(0).textContent)
    .eql('Finish setting up Example Centre 5')
    .expect(finishCentreButtons.nth(1).textContent)
    .eql('Cancel setting up Example Centre 5')
    .expect(finishCentreButtons.nth(2).textContent)
    .eql('Finish setting up Example Centre 6')
    .expect(finishCentreButtons.nth(3).textContent)
    .eql('Cancel setting up Example Centre 6')
    .expect(finishCentreButtons.nth(4).textContent)
    .eql('Finish setting up Example Centre 7')
    .expect(finishCentreButtons.nth(5).textContent)
    .eql('Cancel setting up Example Centre 7');
});

test('incomplete buttons appear when added from elsewhere', async browser => {
  await browser.expect(finishCentreButtons.count).eql(0);
  await createIncompleteCentres();
  return browser.expect(finishCentreButtons.count).eql(6);
});

test('incomplete buttons disappear when updated from elsewhere', async browser => {
  await createIncompleteCentres();
  await browser.expect(finishCentreButtons.count).eql(6);
  await updateCentre('Example Centre 7', { setupComplete: true });
  await browser.expect(finishCentreButtons.count).eql(4);
});

test('incomplete centre button passes current page url to centre setup page', async browser => {
  await createIncompleteCentres();
  return browser
    .resizeWindow(1600, 720)
    .click(finishCentreButtons.nth(2))
    .expect(selectedStepHighlight.visible)
    .ok()
    .expect(selectedStepHighlight.textContent)
    .eql('3')
    .expect(url())
    .eql(`${baseUrl}/centre-setup?nextUrl=/centres`);
});

test('delete centre button opens confirm dialog', async browser => {
  await createIncompleteCentres();
  return browser
    .expect(deleteConfirmDialog.visible)
    .notOk()
    .click(finishCentreButtons.nth(1))
    .expect(deleteConfirmDialog.visible)
    .ok();
});

test('delete centre confirm button updates database', async browser => {
  await createIncompleteCentres();
  return browser
    .click(finishCentreButtons.nth(1))
    .click(deleteConfirmButton)
    .then(() =>
      getCentreByName('Example Centre 5').then(centres =>
        browser.expect(centres.length).eql(0)
      )
    );
});

test('delete centre confirm button removes centre from page', async browser => {
  await createIncompleteCentres();
  return browser
    .click(finishCentreButtons.nth(1))
    .click(deleteConfirmButton)
    .expect(finishCentreButtons.count)
    .eql(4);
});

test('delete centre confirm button closes dialog', async browser => {
  await createIncompleteCentres();
  return browser
    .click(finishCentreButtons.nth(1))
    .click(deleteConfirmButton)
    .expect(deleteConfirmDialog.visible)
    .notOk();
});
