import { Selector } from 'testcafe';
import { format, startOfYesterday } from 'date-fns';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  createCarePlans,
  getClientByName,
  inputCarePlans,
  removeUserCentre,
  updateCarePlan
} from '../../../generic-functions';
import {
  aggregateCarePlansRows,
  carePlanHistoryDialog,
  carePlanMenuIcon,
  historyButton,
  historyTable
} from '../../../generic-selectors/CarePlans';
import { inputLabel, nurse } from '../../../custom-commands';
import { cardTitle } from '../../../generic-selectors/Layout';

const headers = historyTable.find('.responsiveTableHeader');
const rows = historyTable.find('.grid-data-row');
const data = historyTable.find('.grid-data-cell');
const sortButton = historyTable.find('.tableHeaderExpander');

const noDataMessage = historyTable.find('.q-table__bottom');
const historySearch = Selector('#historySearch');
const closeModalButton = carePlanHistoryDialog.find('#closeDialogButton');

fixture`components/carePlans/CarePlanHistory`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await createCarePlans();
    await browser.navigateTo(`${baseUrl}/carePlans`).resizeWindow(1280, 720);
    await nurse();
    return browser
      .click(aggregateCarePlansRows.nth(4))
      .click(carePlanMenuIcon)
      .click(historyButton);
  })
  .after(destroyDb);

test('title is correct', async browser =>
  browser
    .expect(cardTitle.nth(2).textContent)
    .eql("Test Client's Care Plan History"));

test('table has correct headers', async browser =>
  browser
    .expect(headers.count)
    .eql(7)
    .expect(headers.nth(0).textContent)
    .eql('Date')
    .expect(headers.nth(1).textContent)
    .eql('Centre')
    .expect(headers.nth(2).textContent)
    .eql('Need')
    .expect(headers.nth(3).textContent)
    .eql('Plan')
    .expect(headers.nth(4).textContent)
    .eql('Comment')
    .expect(headers.nth(5).textContent)
    .eql('Completed?')
    .expect(headers.nth(6).textContent)
    .eql('Added By'));

test('table shows message when no data', async browser =>
  browser
    .expect(noDataMessage.textContent)
    .eql('Test Client has no previous care plans'));

test('care plan history table has correct data', async browser => {
  const clientData = await getClientByName('5');
  const { clientNeedId } = clientData.find(
    (client: { need: string }) => client.need === 'Example Need 5'
  )!;
  await updateCarePlan(clientNeedId, '2018-11-18', {
    comment: 'new comment',
    completed: true
  });
  await inputCarePlans('5', '2018-11-18');
  await inputCarePlans('5', format(startOfYesterday(), 'y-M-d'));
  return browser
    .expect(data.count)
    .eql(14) // 7*2
    .expect(data.nth(0).textContent)
    .eql(format(startOfYesterday(), 'dd/MM/yy'))
    .expect(data.nth(1).textContent)
    .eql('Example Centre 4')
    .expect(data.nth(2).textContent)
    .eql('Example Need 5')
    .expect(data.nth(3).textContent)
    .eql('Example Plan 5')
    .expect(data.nth(11).textContent)
    .eql('new comment')
    .expect(data.nth(12).find('.completedIcon').visible)
    .ok()
    .expect(data.nth(5).find('.notCompletedIcon').visible)
    .ok()
    .expect(data.nth(6).textContent)
    .eql('Every Centre User');
});

test('care plan history is sorted by date by default', async browser => {
  await inputCarePlans('5', '2018-11-18');
  await inputCarePlans('5', format(startOfYesterday(), 'y-M-d'));
  return browser
    .expect(data.nth(0).textContent)
    .eql(format(startOfYesterday(), 'dd/MM/yy'))
    .expect(data.nth(7).textContent)
    .eql('18/11/18')
    .click(sortButton)
    .click(headers.nth(0))
    .click(headers.nth(0))
    .expect(data.nth(0).textContent)
    .eql('18/11/18')
    .expect(data.nth(7).textContent)
    .eql(format(startOfYesterday(), 'dd/MM/yy'));
});

test('search bar has correct text', async browser =>
  browser
    .expect(inputLabel(historySearch))
    .eql("Search Test Client's History"));

test('care plan history search bar filters data', async browser => {
  await inputCarePlans('5', '2018-11-18');
  await inputCarePlans('5', format(startOfYesterday(), 'y-M-d'));
  return browser
    .typeText(historySearch, '18/11/18')
    .expect(rows.count)
    .eql(7)
    .expect(data.nth(0).textContent)
    .eql('18/11/18')
    .selectText(historySearch)
    .pressKey('delete')
    .typeText(historySearch, format(startOfYesterday(), 'dd/MM/yy'))
    .expect(rows.count)
    .eql(7)
    .expect(data.nth(0).textContent)
    .eql(format(startOfYesterday(), 'dd/MM/yy'));
});

test('close button closes dialog', async browser =>
  browser.click(closeModalButton).expect(headers.visible).notOk());

test('care plan history disappears when user unsubs from centre', async browser => {
  await browser
    .pressKey('esc') // Close care plan history
    .pressKey('esc') // Close client menu
    .pressKey('esc') // Close client dialog
    .click(aggregateCarePlansRows.nth(2))
    .click(carePlanMenuIcon)
    .click(historyButton);
  await inputCarePlans('5', '2018-11-18');
  await browser.expect(rows.count).eql(7);
  await removeUserCentre('testNurse', 'Example Centre 2');
  await removeUserCentre('testNurse', 'Example Centre 4');
  return browser.expect(rows.count).eql(0);
});
