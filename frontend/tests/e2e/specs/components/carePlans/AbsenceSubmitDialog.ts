import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  createCarePlans,
  getAbsenceRecords,
  getCarePlans,
  getCentreByName,
  getClientByName,
  getUserByName,
  removeUserCentre
} from '../../../generic-functions';
import { aggregateCarePlansRows } from '../../../generic-selectors/CarePlans';
import { nurse, textAreaLabel } from '../../../custom-commands';
import { cardTitle } from '../../../generic-selectors/Layout';

const clientAbsentButton = Selector('#clientAbsentButton');
const createAbsenceTitle = Selector('#createAbsenceTitle');
const submitAbsenceButton = Selector('#submitAbsenceButton');
const centreListItems = Selector('.centreListItem');
const reasonForAbsenceTextFields = Selector('.reasonForAbsenceTextField');
const absenceSubmitCloseModalIcon = Selector('#absenceSubmitDialog').find(
  '#closeDialogButton'
);
const carePlansCloseModalButton =
  Selector('#carePlanDialog').find('#closeDialogButton');
const carePlans = Selector('#carePlansTable').find('.grid-data-cell');

fixture`components/carePlans/AbsenceSubmitDialog`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await createCarePlans();
    await browser.navigateTo(`${baseUrl}/carePlans`).resizeWindow(1280, 720);
    await nurse();
    return browser
      .click(aggregateCarePlansRows.nth(4))
      .click(clientAbsentButton);
  })
  .after(destroyDb);

test('dialog has correct title', async browser =>
  browser
    .expect(cardTitle.nth(2).textContent)
    .eql('Create Absence For Test Client on 18/11/19'));

test('centre select is visible', async browser =>
  browser
    .expect(centreListItems.count)
    .eql(2)
    .expect(centreListItems.nth(0).textContent)
    .eql('Example Centre 2')
    .expect(centreListItems.nth(1).textContent)
    .eql('Example Centre 4'));

test('submit button is disabled when no centres selected', async browser =>
  browser
    .expect(submitAbsenceButton.hasAttribute('disabled'))
    .ok()
    .click(centreListItems.nth(0))
    .expect(submitAbsenceButton.hasAttribute('disabled'))
    .notOk()
    .click(centreListItems.nth(0))
    .expect(submitAbsenceButton.hasAttribute('disabled'))
    .ok());

test('selecting a centre displays the reason for absence text field', async browser =>
  browser
    .expect(reasonForAbsenceTextFields.exists)
    .notOk()
    .click(centreListItems.nth(0))
    .expect(reasonForAbsenceTextFields.nth(0).exists)
    .ok()
    .expect(textAreaLabel(reasonForAbsenceTextFields.nth(0)))
    .eql('Reason For Absence From Example Centre 2 (If Known)')
    .click(centreListItems.nth(0))
    .expect(reasonForAbsenceTextFields.exists)
    .notOk());

test('submitting single centre updates database', async browser => {
  await browser
    .click(centreListItems.nth(0))
    .typeText(reasonForAbsenceTextFields.nth(0), 'reason for absence')
    .click(submitAbsenceButton)
    .expect(createAbsenceTitle.visible)
    .notOk();
  const absenceRecords = await getAbsenceRecords();
  const client5Id = (await getClientByName('5'))[0].id;
  const centre2Id = (await getCentreByName('Example Centre 2'))[0].id;
  const nurseId = (await getUserByName('testNurse'))[0].id;
  return browser.expect(absenceRecords).eql([
    {
      id: absenceRecords[0].id,
      date: new Date('2019-11-18T00:00:00.000Z'),
      active: true,
      reasonForInactivity: '',
      reasonForAbsence: 'reason for absence',
      addedBy_id: nurseId,
      centre_id: centre2Id,
      client_id: client5Id
    }
  ]);
});

test('submitting multiple centres updates database', async browser => {
  await browser
    .click(centreListItems.nth(0))
    .click(centreListItems.nth(1))
    .typeText(reasonForAbsenceTextFields.nth(0), 'reason for absence')
    .click(submitAbsenceButton);
  const absenceRecords = await getAbsenceRecords();
  const client5Id = (await getClientByName('5'))[0].id;
  const centre4Id = (await getCentreByName('Example Centre 4'))[0].id;
  const nurseId = (await getUserByName('testNurse'))[0].id;
  return browser
    .expect(absenceRecords.length)
    .eql(2)
    .expect(absenceRecords[1])
    .eql({
      id: absenceRecords[1].id,
      date: new Date('2019-11-18T00:00:00.000Z'),
      active: true,
      reasonForInactivity: '',
      reasonForAbsence: '',
      addedBy_id: nurseId,
      centre_id: centre4Id,
      client_id: client5Id
    });
});

test('submitting deletes care plans for that date', async browser => {
  const originalCarePlans = await getCarePlans();
  await browser
    .expect(originalCarePlans.length)
    .eql(11)
    .click(centreListItems.nth(0))
    .typeText(reasonForAbsenceTextFields.nth(0), 'reason for absence')
    .click(submitAbsenceButton);
  const updatedCarePlans = await getCarePlans();
  return browser.expect(updatedCarePlans.length).eql(9);
});

test("centre select is not visible when there's only one centre", async browser =>
  browser
    .click(absenceSubmitCloseModalIcon)
    .click(carePlansCloseModalButton)
    .click(aggregateCarePlansRows.nth(1))
    .click(clientAbsentButton)
    .expect(centreListItems.exists)
    .notOk()
    .expect(reasonForAbsenceTextFields.nth(0).exists)
    .ok()
    .expect(textAreaLabel(reasonForAbsenceTextFields.nth(0)))
    .eql('Reason For Absence From Example Centre (If Known)')
    .expect(submitAbsenceButton.hasAttribute('disabled'))
    .notOk());

test('selected centres clears when reopening', async browser =>
  browser
    .click(centreListItems.nth(0))
    .click(centreListItems.nth(1))
    .click(absenceSubmitCloseModalIcon)
    .click(carePlansCloseModalButton)
    .expect(clientAbsentButton.visible)
    .notOk()
    .click(aggregateCarePlansRows.nth(2))
    .click(clientAbsentButton)
    .click(absenceSubmitCloseModalIcon)
    .click(carePlansCloseModalButton)
    .expect(clientAbsentButton.visible)
    .notOk()
    .click(aggregateCarePlansRows.nth(4))
    .click(clientAbsentButton)
    .expect(reasonForAbsenceTextFields.nth(0).exists)
    .notOk());

test('close button closes modal', async browser =>
  browser
    .click(absenceSubmitCloseModalIcon)
    .expect(createAbsenceTitle.visible)
    .notOk());

test('centres update when user unsubscribes elsewhere', async browser => {
  await removeUserCentre('testNurse', 'Example Centre 2');
  await removeUserCentre('testNurse', 'Example Centre');
  return browser
    .expect(centreListItems.exists)
    .notOk()
    .expect(reasonForAbsenceTextFields.nth(0).exists)
    .ok()
    .expect(textAreaLabel(reasonForAbsenceTextFields.nth(0)))
    .eql('Reason For Absence From Example Centre 4 (If Known)')
    .expect(submitAbsenceButton.hasAttribute('disabled'))
    .notOk();
});

test('absence submit modal closes if user unsubs from all centres', async browser => {
  await removeUserCentre('testNurse', 'Example Centre 4');
  await removeUserCentre('testNurse', 'Example Centre 2');
  return browser.expect(createAbsenceTitle.visible).notOk();
});

test('submitting absence hides care plans in UI', async browser => {
  return browser
    .expect(aggregateCarePlansRows.count)
    .eql(6)
    .expect(carePlans.count)
    .eql(15)
    .click(centreListItems.nth(0))
    .typeText(reasonForAbsenceTextFields.nth(0), 'reason for absence')
    .click(submitAbsenceButton)
    .expect(createAbsenceTitle.visible)
    .notOk()
    .expect(carePlans.count)
    .eql(5)
    .expect(aggregateCarePlansRows.count)
    .eql(6)
    .click(clientAbsentButton)
    .click(submitAbsenceButton)
    .expect(aggregateCarePlansRows.count)
    .eql(5);
});
