import { Selector } from 'testcafe';
import { formatISO, startOfToday } from 'date-fns';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  createCarePlans,
  deleteClient,
  getCarePlans,
  getClientByName,
  getNeedByDescription,
  removeUserCentre,
  updateCarePlan,
  updateCentre,
  updateClientNeed,
  updateNeed
} from '../../../generic-functions';

import {
  absenceButton,
  aggregateCarePlansRows,
  carePlanDialog,
  carePlanMenuIcon,
  historyButton
} from '../../../generic-selectors/CarePlans';
import { clientDialogHeader } from '../../../generic-selectors/Clients';

import { nurse } from '../../../custom-commands';
import { cardTitle } from '../../../generic-selectors/Layout';

const viewClientButton = Selector('#viewClientButton');
const table = Selector('#carePlansTable');
const data = table.find('.grid-data-cell');

const headers = table.find('.responsiveTableHeader');
const carePlanSubmitButton = Selector('#carePlanSubmitButton');
const closeModalButton = carePlanDialog.find('#closeDialogButton');
const carePlanCompleteAllCheckbox = Selector('.carePlanCompleteAllCheckbox');

fixture`components/carePlans/CarePlanDialog`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await createCarePlans();
    await browser.navigateTo(`${baseUrl}/carePlans`).resizeWindow(1280, 720);
    await nurse();
    return browser.click(aggregateCarePlansRows.nth(0));
  })
  .after(destroyDb);

test('title is correct', async browser =>
  browser
    .expect(cardTitle.nth(1).textContent)
    .eql("Test Client's Care Plans For Today"));

test('view client button has correct text', async browser =>
  browser
    .click(carePlanMenuIcon)
    .expect(viewClientButton.textContent)
    .eql("Test Client's Profile"));

test('history button has correct text', async browser =>
  browser
    .click(carePlanMenuIcon)
    .expect(historyButton.textContent)
    .eql("Test Client's Care Plan History"));

test('absence button has correct text', async browser =>
  browser
    .click(carePlanMenuIcon)
    .expect(absenceButton.textContent)
    .eql("Test Client's Absence History"));

test('view client button opens client dialog', async browser =>
  browser
    .expect(clientDialogHeader.visible)
    .notOk()
    .click(carePlanMenuIcon)
    .click(viewClientButton)
    .expect(clientDialogHeader.textContent)
    .eql('Test Client 4'));

test('dialog closes if client is deleted', async browser => {
  await deleteClient('4');
  return browser.expect(carePlanDialog.visible).notOk();
});

test('client dialog and care plan dialog closes if client is deleted', async browser => {
  await browser.click(carePlanMenuIcon).click(viewClientButton);
  await deleteClient('4');
  return browser
    .expect(carePlanDialog.visible)
    .notOk()
    .expect(clientDialogHeader.visible)
    .notOk();
});

test('care plans table has correct headers', async browser =>
  browser
    .expect(headers.count)
    .eql(5)
    .expect(headers.nth(0).textContent)
    .eql('Centre')
    .expect(headers.nth(1).textContent)
    .eql('Need')
    .expect(headers.nth(2).textContent)
    .eql('Plan')
    .expect(headers.nth(3).textContent)
    .eql('Comment')
    .expect(headers.nth(4).textContent)
    .eql('Completed?'));

test('care plans table has correct data', async browser =>
  browser
    .expect(data.count)
    .eql(10)
    .expect(data.nth(0).textContent)
    .eql('Example Centre')
    .expect(data.nth(1).textContent)
    .eql('Example Need 1')
    .expect(data.nth(2).textContent)
    .eql('Example Plan 1')
    .expect(data.nth(3).find('.carePlanCommentTextArea').visible)
    .ok()
    .expect(data.nth(3).find('.carePlanCommentTextArea').find('textarea').value)
    .eql('')
    .expect(
      data
        .nth(4)
        .find('.carePlanCompletedCheckbox')
        .getAttribute('aria-checked')
    )
    .eql('false')
    .expect(data.nth(4).find('.carePlanCompletedCheckbox').visible)
    .ok()
    .expect(data.nth(6).textContent)
    .eql('Example Need 3')
    .expect(data.nth(7).textContent)
    .eql('Example Plan 3'));

test('data updates when changed elsewhere', async browser => {
  const clientData = await getClientByName('4');
  const { clientNeedId } = clientData.find(
    (client: { need: string }) => client.need === 'Example Need 1'
  )!;
  await updateCarePlan(clientNeedId, formatISO(startOfToday()), {
    comment: 'new comment',
    completed: true
  });
  return browser
    .expect(data.nth(3).find('.carePlanCommentTextArea').find('textarea').value)
    .eql('new comment')
    .expect(
      data
        .nth(4)
        .find('.carePlanCompletedCheckbox')
        .getAttribute('aria-checked')
    )
    .eql('true');
});

test('row disappears when inputted is set to true', async browser => {
  const clientData = await getClientByName('4');
  const { clientNeedId } = clientData.find(
    (client: { need: string }) => client.need === 'Example Need 1'
  )!;
  await updateCarePlan(clientNeedId, formatISO(startOfToday()), {
    inputted: true
  });
  return browser
    .expect(data.count)
    .eql(5)
    .expect(data.nth(2).textContent)
    .eql('Example Plan 3');
});

test('dialog closes when all rows are gone', async browser => {
  const clientData = await getClientByName('4');
  const clientNeed1Id = clientData.find(
    (client: { need: string }) => client.need === 'Example Need 1'
  )!.clientNeedId;
  const clientNeed3Id = clientData.find(
    (client: { need: string }) => client.need === 'Example Need 3'
  )!.clientNeedId;

  await updateCarePlan(clientNeed1Id, formatISO(startOfToday()), {
    inputted: true
  });
  await updateCarePlan(clientNeed3Id, formatISO(startOfToday()), {
    inputted: true
  });
  return browser.expect(carePlanDialog.visible).notOk();
});

test('changing comment updates database', async browser => {
  await browser
    .typeText(
      data.nth(3).find('.carePlanCommentTextArea').find('textarea'),
      'new care plan comment'
    )
    .click(cardTitle.nth(1));
  const carePlans = await getCarePlans();
  return browser
    .expect(
      carePlans.filter(
        (carePlan: { comment: string }) =>
          carePlan.comment === 'new care plan comment'
      ).length
    )
    .gt(0);
});

test('changing completed status updates database', async browser => {
  await browser.click(data.nth(4).find('.carePlanCompletedCheckbox'));
  const carePlans = await getCarePlans();
  return browser
    .expect(
      carePlans.filter((carePlan: { completed: boolean }) => carePlan.completed)
        .length
    )
    .gt(0);
});

test('plan changes when updated elsewhere', async browser => {
  await updateClientNeed('Example Plan 1', '4', { plan: 'New Need Plan' });
  return browser.expect(data.nth(2).textContent).eql('New Need Plan');
});

test('need description updates when changed elsewhere', async browser => {
  await updateNeed('Example Need 1', 'New Need Name');
  return browser.expect(data.nth(1).textContent).eql('New Need Name');
});

test('need disappears when deleted from centre', async browser => {
  const [exampleNeed3] = await getNeedByDescription('Example Need 3');
  await updateCentre('Example Centre', { needs: [exampleNeed3.id] });
  return browser
    .expect(data.count)
    .eql(5)
    .expect(data.nth(1).textContent)
    .eql('Example Need 3');
});

test('need disappears when unsubscribed from client', async browser => {
  await updateClientNeed('Example Plan 1', '4', { isActive: false });
  return browser
    .expect(data.count)
    .eql(5)
    .expect(data.nth(1).textContent)
    .eql('Example Need 3');
});

test('need reappears when resubscribed from client', async browser => {
  await updateClientNeed('Example Plan 1', '4', { isActive: false });
  await updateClientNeed('Example Plan 1', '4', { isActive: true });
  return browser.expect(data.count).eql(10);
});

test('pressing submit updates care plan view table', async browser =>
  browser
    .expect(aggregateCarePlansRows.count)
    .eql(6)
    .click(carePlanSubmitButton)
    .expect(aggregateCarePlansRows.count)
    .eql(5));

test('pressing submit updates database', async browser => {
  await browser
    .click(carePlanSubmitButton)
    .expect(carePlanDialog.visible)
    .notOk();
  const carePlans = await getCarePlans();
  return browser
    .expect(
      carePlans.filter((carePlan: { inputted: boolean }) => carePlan.inputted)
        .length
    )
    .eql(2);
});

test('close button closes modal', async browser =>
  browser.click(closeModalButton).expect(carePlanDialog.visible).notOk());

test('care plans disappear when user unsubs from centre', async browser => {
  await browser.click(closeModalButton).click(aggregateCarePlansRows.nth(4));
  await removeUserCentre('testNurse', 'Example Centre');
  await removeUserCentre('testNurse', 'Example Centre 2');
  return browser
    .expect(data.count)
    .eql(5)
    .expect(data.nth(0).textContent)
    .eql('Example Centre 4');
});

test('modal closes if user unsubs from all centres', async browser => {
  await removeUserCentre('testNurse', 'Example Centre');
  await removeUserCentre('testNurse', 'Example Centre 2');
  return browser.expect(carePlanDialog.visible).notOk();
});

test('complete all button marks all as complete', async browser => {
  await browser
    .click(data.nth(4).find('.carePlanCompletedCheckbox'))
    .click(carePlanCompleteAllCheckbox);
  const carePlans = await getCarePlans();
  await browser
    .expect(
      carePlans.filter((carePlan: { completed: boolean }) => carePlan.completed)
        .length
    )
    .eql(2);
  await browser.click(carePlanCompleteAllCheckbox);
  const unselectedCarePlans = await getCarePlans();
  return browser
    .expect(
      unselectedCarePlans.filter(
        (carePlan: { completed: boolean }) => carePlan.completed
      ).length
    )
    .eql(0);
});
