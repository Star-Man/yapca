import { Selector } from 'testcafe';
import { formatISO } from 'date-fns';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  deleteMedication,
  getClientByName,
  updateMedication
} from '../../../generic-functions';

import {
  buttonLabel,
  inputLabel,
  inputValidationMessage,
  nurse
} from '../../../custom-commands';
import {
  addMedicationButton,
  clientsTableRow2,
  createMedicationSubmitButton,
  medicationCreateDosageField,
  medicationCreateFrequencyField,
  medicationCreateNameField,
  medicationsTable
} from '../../../generic-selectors/Clients';

const headers = medicationsTable.find('th');
const data = medicationsTable.find('.grid-data-cell');
const medicationEditNameField = Selector('#medicationEditNameField');
const medicationEditDosageField = Selector('#medicationEditDosageField');
const medicationEditFrequencyField = Selector('#medicationEditFrequencyField');
const createMedicationDialog = Selector('#createMedicationDialog');
const archiveDialog = Selector('#archiveDialog');
const openArchiveButton = Selector('#openArchiveButton');
const archivedMedicationsTable = Selector('#archivedMedicationsTable');
const archiveTableHeaders = archivedMedicationsTable.find('th');
const archiveTableData = archivedMedicationsTable.find('.grid-data-cell');
const deleteMedicationButtons = Selector('.deleteMedicationButton');
const archiveMedicationButtons = Selector('.archiveMedicationButton');
const medicationsExpander = Selector('#medicationsExpander');

fixture`components/clients/medicationsTable`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.resizeWindow(1280, 720).navigateTo(baseUrl);
    await nurse();
    return browser.click(clientsTableRow2).click(medicationsExpander);
  })
  .after(destroyDb);

test('table shows correct headers', async browser =>
  browser
    .expect(headers.count)
    .eql(4)
    .expect(headers.nth(0).textContent)
    .eql('Name')
    .expect(headers.nth(1).textContent)
    .eql('Dosage')
    .expect(headers.nth(2).textContent)
    .eql('Frequency')
    .expect(headers.nth(3).textContent)
    .eql('Actions'));

test('table shows correct data', browser =>
  browser
    .expect(data.count)
    .eql(8)
    .expect(data.nth(0).textContent)
    .eql('Test Med 1')
    .expect(data.nth(1).textContent)
    .eql('10mg')
    .expect(data.nth(2).textContent)
    .eql('weekly')
    .expect(data.nth(4).textContent)
    .eql('Test Med 2')
    .expect(data.nth(5).textContent)
    .eql('100mg')
    .expect(data.nth(6).textContent)
    .eql('daily'));

test('name is editable', async browser =>
  browser
    .click(data.nth(0))
    .expect(medicationEditNameField.visible)
    .ok()
    .expect(medicationEditNameField.value)
    .eql('Test Med 1')
    .typeText(medicationEditNameField, '234')
    .expect(medicationEditNameField.value)
    .eql('Test Med 1234')
    .click(medicationsTable)
    .expect(data.nth(0).textContent)
    .eql('Test Med 1234'));

test('name edit label is correct', async browser =>
  browser
    .click(data.nth(0))
    .selectText(medicationEditNameField)
    .pressKey('delete')
    .expect(inputLabel(medicationEditNameField))
    .eql('Medication Name'));

test('pressing enter closes edit dialog + saves to db', async browser =>
  browser
    .click(data.nth(0))
    .typeText(medicationEditNameField, '234')
    .pressKey('enter')
    .expect(medicationEditNameField.visible)
    .notOk()
    .expect(data.nth(0).textContent)
    .eql('Test Med 1234')
    .then(() =>
      getClientByName('4').then(clients =>
        browser
          .expect(
            clients.filter(
              (client: { medicationName: string }) =>
                client.medicationName === 'Test Med 1234'
            ).length
          )
          .gt(0)
      )
    ));

test('empty name field shows error', async browser =>
  browser
    .click(data.nth(0))
    .selectText(medicationEditNameField)
    .pressKey('delete')
    .expect(inputValidationMessage(medicationEditNameField))
    .eql('Name is required'));

test('database does not update if field name empty', async browser =>
  browser
    .click(data.nth(0))
    .selectText(medicationEditNameField)
    .pressKey('delete')
    .pressKey('enter')
    .expect(medicationEditNameField.visible)
    .notOk()
    .then(() =>
      getClientByName('4').then(clients =>
        browser
          .expect(
            clients.filter(
              (client: { medicationName: string }) =>
                client.medicationName === 'Test Med 1'
            ).length
          )
          .gt(0)
      )
    ));

test('dosage is editable', async browser =>
  browser
    .click(data.nth(1))
    .expect(medicationEditDosageField.visible)
    .ok()
    .expect(medicationEditDosageField.value)
    .eql('10mg')
    .typeText(medicationEditDosageField, '234')
    .expect(medicationEditDosageField.value)
    .eql('10mg234')
    .click(medicationsTable)
    .expect(data.nth(1).textContent)
    .eql('10mg234'));

test('dosage edit label is correct', async browser =>
  browser
    .click(data.nth(1))
    .selectText(medicationEditDosageField)
    .pressKey('delete')
    .expect(inputLabel(medicationEditDosageField))
    .eql('Medication Dosage'));

test('updating dosage updates in database', async browser =>
  browser
    .click(data.nth(1))
    .typeText(medicationEditDosageField, ' 1234')
    .pressKey('enter')
    .then(() =>
      getClientByName('4').then(clients =>
        browser
          .expect(
            clients.filter(
              (client: { medicationDosage: string }) =>
                client.medicationDosage === '10mg 1234'
            ).length
          )
          .gt(0)
      )
    ));

test('empty dosage field shows error', async browser =>
  browser
    .click(data.nth(1))
    .selectText(medicationEditDosageField)
    .pressKey('delete')
    .expect(inputValidationMessage(medicationEditDosageField))
    .eql('Dosage is required'));

test('frequency is editable', async browser =>
  browser
    .click(data.nth(2))
    .expect(medicationEditFrequencyField.visible)
    .ok()
    .expect(medicationEditFrequencyField.value)
    .eql('weekly')
    .typeText(medicationEditFrequencyField, '234')
    .expect(medicationEditFrequencyField.value)
    .eql('weekly234')
    .click(medicationsTable)
    .expect(data.nth(2).textContent)
    .eql('weekly234'));

test('frequency edit label is correct', async browser =>
  browser
    .click(data.nth(2))
    .selectText(medicationEditFrequencyField)
    .pressKey('delete')
    .expect(inputLabel(medicationEditFrequencyField))
    .eql('Medication Frequency'));

test('updating frequency updates in database', async browser =>
  browser
    .click(data.nth(2))
    .typeText(medicationEditFrequencyField, ' 1234')
    .pressKey('enter')
    .then(() =>
      getClientByName('4').then(clients =>
        browser
          .expect(
            clients.filter(
              (client: { medicationFrequency: string }) =>
                client.medicationFrequency === 'weekly 1234'
            ).length
          )
          .gt(0)
      )
    ));

test('empty frequency field shows error', async browser =>
  browser
    .click(data.nth(2))
    .selectText(medicationEditFrequencyField)
    .pressKey('delete')
    .expect(inputValidationMessage(medicationEditFrequencyField))
    .eql('Frequency is required'));

test('clicking add button opens dialog', browser =>
  browser
    .expect(createMedicationDialog.visible)
    .notOk()
    .click(addMedicationButton)
    .expect(createMedicationDialog.visible)
    .ok());

test('add medication dialog labels are correct', browser =>
  browser
    .click(addMedicationButton)
    .expect(inputLabel(medicationCreateNameField))
    .eql('Name')
    .expect(inputLabel(medicationCreateDosageField))
    .eql('Dosage')
    .expect(inputLabel(medicationCreateFrequencyField))
    .eql('Frequency'));

test('add medication button has correct label', browser =>
  browser.expect(buttonLabel(addMedicationButton)).eql('Add Medication'));

test('create medication button has correct label', browser =>
  browser
    .click(addMedicationButton)
    .expect(buttonLabel(createMedicationSubmitButton))
    .eql('Create Medication'));

test('create button is disabled when no name and dosage', browser =>
  browser
    .click(addMedicationButton)
    .expect(createMedicationSubmitButton.hasAttribute('disabled'))
    .ok()
    .typeText(medicationCreateNameField, 'Example Name')
    .expect(createMedicationSubmitButton.hasAttribute('disabled'))
    .ok()
    .typeText(medicationCreateDosageField, '1234567')
    .expect(createMedicationSubmitButton.hasAttribute('disabled'))
    .ok()
    .typeText(medicationCreateFrequencyField, '7654321')
    .expect(createMedicationSubmitButton.hasAttribute('disabled'))
    .notOk()
    .selectText(medicationCreateNameField)
    .pressKey('delete')
    .expect(createMedicationSubmitButton.hasAttribute('disabled'))
    .ok());

test('clicking create button closes dialog', browser =>
  browser
    .click(addMedicationButton)
    .typeText(medicationCreateNameField, 'Example Name')
    .typeText(medicationCreateDosageField, '1234567')
    .typeText(medicationCreateFrequencyField, '7654321')
    .click(createMedicationSubmitButton)
    .expect(createMedicationDialog.visible)
    .notOk());

test('clicking create adds medication to database', browser =>
  browser
    .click(addMedicationButton)
    .typeText(medicationCreateNameField, 'Example Name')
    .typeText(medicationCreateDosageField, '1234567')
    .typeText(medicationCreateFrequencyField, '7654321')
    .click(createMedicationSubmitButton)
    .then(() =>
      getClientByName('4').then(clients =>
        browser
          .expect(
            clients.filter(
              (client: { medicationDosage: string }) =>
                client.medicationDosage === '1234567'
            ).length
          )
          .gt(0)
          .expect(
            clients.filter(
              (client: { medicationName: string }) =>
                client.medicationName === 'Example Name'
            ).length
          )
          .gt(0)
          .expect(
            clients.filter(
              (client: { medicationFrequency: string }) =>
                client.medicationFrequency === '7654321'
            ).length
          )
          .gt(0)
      )
    ));

test('clicking create adds medication to table', browser =>
  browser
    .click(addMedicationButton)
    .typeText(medicationCreateNameField, 'Example Name')
    .typeText(medicationCreateDosageField, '1234567')
    .typeText(medicationCreateFrequencyField, '7654321')
    .click(createMedicationSubmitButton)
    .expect(data.count)
    .eql(12)
    .expect(data.nth(0).textContent)
    .eql('Example Name')
    .expect(data.nth(1).textContent)
    .eql('1234567')
    .expect(data.nth(2).textContent)
    .eql('7654321'));

test('delete button updates table', async browser =>
  browser
    .expect(data.count)
    .eql(8)
    .click(deleteMedicationButtons.nth(1))
    .expect(data.count)
    .eql(4));

test('delete button updates database', async browser =>
  browser
    .click(deleteMedicationButtons.nth(1))
    .then(() =>
      getClientByName('4').then(clients =>
        browser
          .expect(
            clients.filter(
              (client: { medicationName: string }) =>
                client.medicationName === 'Test Med 2'
            ).length
          )
          .eql(0)
      )
    ));

test('table is not visible when there are no medications', async browser => {
  await browser.expect(medicationsTable.visible).ok();
  await deleteMedication('4', 'Test Med 1');
  await deleteMedication('4', 'Test Med 2');
  return browser.expect(medicationsTable.visible).notOk();
});

test('create dialog name field is auto focused', browser =>
  browser
    .click(addMedicationButton)
    .expect(medicationCreateNameField.focused)
    .ok()
    .pressKey('t e s t')
    .expect(medicationCreateNameField.value)
    .eql('test'));

test('clicking archive button opens dialog', browser =>
  browser
    .expect(archiveDialog.visible)
    .notOk()
    .click(openArchiveButton)
    .expect(archiveDialog.visible)
    .ok());

test('archive button has correct text', browser =>
  browser.expect(buttonLabel(openArchiveButton)).eql('Past Medications'));

test('archive table shows correct headers', async browser =>
  browser
    .click(openArchiveButton)
    .expect(archiveTableHeaders.count)
    .eql(5)
    .expect(archiveTableHeaders.nth(0).textContent)
    .eql('Name')
    .expect(archiveTableHeaders.nth(1).textContent)
    .eql('Dosage')
    .expect(archiveTableHeaders.nth(2).textContent)
    .eql('Frequency')
    .expect(archiveTableHeaders.nth(3).textContent)
    .eql('End Date')
    .expect(archiveTableHeaders.nth(4).textContent)
    .eql('Actions'));

test('archive table shows correct data', browser =>
  browser
    .click(openArchiveButton)
    .expect(archiveTableData.count)
    .eql(5)
    .expect(archiveTableData.nth(0).textContent)
    .eql('Test Med 3')
    .expect(archiveTableData.nth(1).textContent)
    .eql('5mg')
    .expect(archiveTableData.nth(2).textContent)
    .eql('hourly')
    .expect(archiveTableData.nth(3).textContent)
    .eql('02/03/11'));

test('clicking archive button removes row from table', browser =>
  browser.click(archiveMedicationButtons.nth(1)).expect(data.count).eql(4));

test('clicking archive button updates database', async browser => {
  await browser.click(archiveMedicationButtons.nth(1));

  const clients = await getClientByName('4');
  return browser
    .expect(
      clients.filter(client => {
        if (client.medicationInactiveDate) {
          return (
            formatISO(client.medicationInactiveDate, {
              representation: 'date'
            }) === formatISO(new Date(), { representation: 'date' })
          );
        }
        return false;
      }).length
    )
    .gt(0);
});

test('clicking archive button adds row to archive table', browser =>
  browser
    .click(archiveMedicationButtons.nth(1))
    .click(openArchiveButton)
    .expect(archiveTableData.count)
    .eql(10));

test('archive button not visible when archive is empty', browser =>
  browser
    .click(openArchiveButton)
    .click(deleteMedicationButtons.nth(2))
    .expect(openArchiveButton.visible)
    .notOk());

test('archive table updates when updated from elsewhere', async browser => {
  await browser.click(openArchiveButton);
  await updateMedication('Test Med 2', { inactiveDate: '2020-01-01' });
  return browser.expect(archiveTableData.count).eql(10);
});

test('archive button not visible when archive is empty from elsewhere', async browser => {
  await browser.click(openArchiveButton);
  await updateMedication('Test Med 3', { inactiveDate: null });
  return browser.expect(archiveDialog.visible).notOk();
});

test('clicking unarchive button removes row from table', browser =>
  browser
    .click(openArchiveButton)
    .click(archiveMedicationButtons.nth(2))
    .expect(archiveTableData.count)
    .eql(0));

test('clicking unarchive button updates database', async browser => {
  await browser.click(openArchiveButton).click(archiveMedicationButtons.nth(2));
  const clients = await getClientByName('4');
  return browser
    .expect(
      clients.filter(client => {
        if (client.medicationInactiveDate) {
          return (
            formatISO(client.medicationInactiveDate, {
              representation: 'date'
            }) ===
            formatISO(new Date(2011, 3, 2), {
              representation: 'date'
            })
          );
        }
        return false;
      }).length
    )
    .eql(0);
});
