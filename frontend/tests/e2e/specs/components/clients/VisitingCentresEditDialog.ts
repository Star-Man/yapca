import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  getCentreByName,
  getClientByName,
  updateClientCentre
} from '../../../generic-functions';
import { nurse } from '../../../custom-commands';
import {
  clientUnagreedPoliciesDialog,
  clientVisitingCentreHeaders,
  clientsTableRow2,
  visitingCentresEditIcon
} from '../../../generic-selectors/Clients';
import { infoSnackbar, snackbarText } from '../../../generic-selectors/Auth';
import { centresList } from '../../../generic-selectors/Centres';

fixture`components/clients/VisitingCentresEditDialog`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.resizeWindow(1280, 720).navigateTo(baseUrl);
    await nurse();
    return browser.click(clientsTableRow2).click(visitingCentresEditIcon);
  })
  .after(destroyDb);

test('should show a list of the users subbed centres', async browser =>
  browser
    .expect(centresList.count)
    .eql(3)
    .expect(centresList.nth(0).textContent)
    .eql('Example Centre')
    .expect(centresList.nth(1).textContent)
    .eql('Example Centre 2')
    .expect(centresList.nth(2).textContent)
    .eql('Example Centre 4'));

test('clients current visiting centres should be selected on load', async browser =>
  browser
    .expect(
      centresList
        .nth(0)
        .find('.q-checkbox__inner')
        .hasClass('q-checkbox__inner--truthy')
    )
    .ok()
    .expect(
      centresList
        .nth(1)
        .find('.q-checkbox__inner')
        .hasClass('q-checkbox__inner--truthy')
    )
    .notOk()
    .expect(
      centresList
        .nth(2)
        .find('.q-checkbox__inner')
        .hasClass('q-checkbox__inner--truthy')
    )
    .notOk());

test('clients visiting centres should change when updated elsewhere', async browser => {
  const centre1Id = (await getCentreByName('Example Centre'))[0].id;
  const centre4Id = (await getCentreByName('Example Centre 4'))[0].id;
  await updateClientCentre('testNurse', '4', centre4Id, true);
  await updateClientCentre('testNurse', '4', centre1Id, false);
  return browser
    .expect(
      centresList
        .nth(0)
        .find('.q-checkbox__inner')
        .hasClass('q-checkbox__inner--truthy')
    )
    .notOk()
    .expect(
      centresList
        .nth(1)
        .find('.q-checkbox__inner')
        .hasClass('q-checkbox__inner--truthy')
    )
    .notOk()
    .expect(
      centresList
        .nth(2)
        .find('.q-checkbox__inner')
        .hasClass('q-checkbox__inner--truthy')
    )
    .ok();
});

test('clicking centre should update database', async browser => {
  const centre2Id = (await getCentreByName('Example Centre 2'))[0].id;
  await browser
    .click(centresList.nth(1))
    .expect(
      centresList
        .nth(1)
        .find('.q-checkbox__inner')
        .hasClass('q-checkbox__inner--truthy')
    )
    .ok();
  const clients = await getClientByName('4');
  return browser
    .expect(
      clients.filter(
        (client: { centreId: number }) => client.centreId === centre2Id
      ).length
    )
    .gte(1);
});

test('clicking centre should update client dialog', async browser =>
  browser
    .expect(clientVisitingCentreHeaders.count)
    .eql(1)
    .click(centresList.nth(1))
    .expect(clientVisitingCentreHeaders.count)
    .eql(2));

test('adding centre should should open policy agree dialog', async browser =>
  browser
    .click(centresList.nth(1))
    .expect(clientUnagreedPoliciesDialog.visible)
    .ok());

test('cannot remove last centre', async browser =>
  browser
    .expect(centresList.count)
    .eql(3)
    .click(centresList.nth(0))
    .expect(infoSnackbar.exists)
    .ok()
    .expect(snackbarText.textContent)
    .eql('Client must have at least one centre.')
    .expect(
      centresList
        .nth(0)
        .find('.q-checkbox__inner')
        .hasClass('q-checkbox__inner--truthy')
    )
    .ok());
