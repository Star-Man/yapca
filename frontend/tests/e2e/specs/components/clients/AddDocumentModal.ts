import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import { deleteAllClients, getClientByName } from '../../../generic-functions';
import { inputValidationMessage, nurse } from '../../../custom-commands';
import { clientsTableRow2 } from '../../../generic-selectors/Clients';
import { confirmButton } from '../../../generic-selectors/Layout';

const addDocumentModal = Selector('#addDocumentModal');
const documentUploadInput = Selector('#documentUploadInput');
const row = Selector('#addDocumentTable').find('tbody').find('tr');
const addDocumentNameInput = Selector('.addDocumentNameInput');
const documentUploadButton = Selector('#documentUploadButton');
const removeFileButton = Selector('.removeFileButton');
const warningMessage = Selector('.fileSizeWarningTooltip');
const warningIcon = Selector('.fileSizeWarningIcon').find('svg');
const fileOverStorageLimitAlert = Selector('#fileOverStorageLimitAlert').find(
  '.q-banner__content'
);

fixture`components/clients/AddDocumentModal`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients', 'documents']);
    await browser.resizeWindow(1280, 720).navigateTo(baseUrl);
    await nurse();
    return browser
      .click(clientsTableRow2)
      .click(confirmButton)
      .setFilesToUpload(documentUploadInput, [
        '../../../media/example_file_1.txt',
        '../../../media/example_file_2.txt',
        '../../../media/example_file_3.txt',
        '../../../media/example_file_4.txt'
      ]);
  })
  .afterEach(() => deleteAllClients().then(destroyDb));

test('modal is visible after selecting files', browser =>
  browser.expect(addDocumentModal.visible).ok());

test('file list data is correct', browser =>
  browser
    .expect(row.count)
    .eql(4)
    .expect(addDocumentNameInput.nth(0).find('input').value)
    .eql('example_file_1')
    .expect(row.nth(1).find('td').nth(1).textContent)
    .eql('example_file_2.txt')
    .expect(row.nth(2).find('td').nth(2).textContent)
    .eql('0 B'));

test('upload button is disabled after clicking', browser =>
  browser
    .click(documentUploadButton)
    .expect(documentUploadButton.visible)
    .notOk());

test('clicking upload non error uploads documents', async browser => {
  await browser.click(documentUploadButton);
  const clientDetails = await getClientByName('4');
  return browser
    .expect(
      clientDetails.filter(
        (client: { documentOriginalName: string }) =>
          client.documentOriginalName === 'example_file_1.txt'
      ).length
    )
    .gte(1)
    .expect(
      clientDetails.filter(
        (client: { documentOriginalName: string }) =>
          client.documentOriginalName === 'example_file_2.txt'
      ).length
    )
    .gte(1)
    .expect(
      clientDetails.filter(
        (client: { documentOriginalName: string }) =>
          client.documentOriginalName === 'example_file_3.txt'
      ).length
    )
    .eql(0)
    .expect(
      clientDetails.filter(
        (client: { documentOriginalName: string }) =>
          client.documentOriginalName === 'example_file_4.txt'
      ).length
    )
    .eql(0);
});

test('modal closes automatically when upload completes', browser =>
  browser.click(documentUploadButton).expect(addDocumentModal.visible).notOk());

test('removing file removes row', browser =>
  browser
    .expect(row.count)
    .eql(4)
    .click(removeFileButton.nth(1))
    .expect(row.count)
    .eql(3));

test('remove button is not visible when uploading', browser =>
  browser
    .click(documentUploadButton)
    .expect(removeFileButton.nth(1).visible)
    .notOk());

test('removed files are not uploaded', async browser => {
  await browser.click(removeFileButton.nth(1)).click(documentUploadButton);
  const clientDetails = await getClientByName('4');
  return browser
    .expect(
      clientDetails.filter(
        (client: { documentOriginalName: string }) =>
          client.documentOriginalName === 'example_file_1.txt'
      ).length
    )
    .gte(1)
    .expect(
      clientDetails.filter(
        (client: { documentOriginalName: string }) =>
          client.documentOriginalName === 'example_file_2.txt'
      ).length
    )
    .eql(0);
});

test('warning is shown on empty files', async browser =>
  browser
    .expect(warningIcon.count)
    .eql(2)
    .hover(warningIcon.nth(0))
    .expect(warningMessage.textContent)
    .eql('File is empty and will not be uploaded'));

test('upload button is disabled when a file has no name', async browser =>
  browser
    .selectText(addDocumentNameInput.nth(0).find('input'))
    .pressKey('delete')
    .expect(documentUploadButton.hasAttribute('disabled'))
    .ok()
    .typeText(addDocumentNameInput.nth(0), 'test')
    .expect(documentUploadButton.hasAttribute('disabled'))
    .notOk());

test('validation error appears when file has no name', async browser =>
  browser
    .selectText(addDocumentNameInput.nth(0).find('input'))
    .pressKey('delete')
    .expect(inputValidationMessage(addDocumentNameInput.nth(0)))
    .eql('Document name is required'));

test('error appears if documents are over max size limit', async browser =>
  browser
    .expect(fileOverStorageLimitAlert.visible)
    .notOk()
    .setFilesToUpload(documentUploadInput, [
      '../../../media/example_file_1.txt',
      '../../../media/example_file_2.txt',
      '../../../media/example_file_3.txt',
      '../../../media/example_file_4.txt',
      '../../../media/example_file_5.txt',
      '../../../media/example_file_6.txt'
    ])
    .expect(fileOverStorageLimitAlert.visible)
    .ok()
    .expect(documentUploadButton.hasAttribute('disabled'))
    .ok()
    .expect(fileOverStorageLimitAlert.textContent)
    .eql(
      ' There is only 19 B of space left on the server. The uploaded files ' +
        'total 27 B. Please reduce this upload by 8 B'
    ));

test('max size limit error updates when files are removed', async browser =>
  browser
    .setFilesToUpload(documentUploadInput, [
      '../../../media/example_file_1.txt',
      '../../../media/example_file_2.txt',
      '../../../media/example_file_3.txt',
      '../../../media/example_file_4.txt',
      '../../../media/example_file_5.txt',
      '../../../media/example_file_6.txt'
    ])
    .click(removeFileButton.nth(2))
    .expect(documentUploadButton.hasAttribute('disabled'))
    .ok()
    .expect(fileOverStorageLimitAlert.textContent)
    .eql(
      ' There is only 19 B of space left on the server. The uploaded files ' +
        'total 22 B. Please reduce this upload by 3 B'
    )
    .click(removeFileButton.nth(2))
    .expect(fileOverStorageLimitAlert.visible)
    .notOk()
    .expect(documentUploadButton.hasAttribute('disabled'))
    .notOk());

test('warning is shown on file larger than max file size', async browser =>
  browser
    .hover(warningIcon.nth(1))
    .expect(warningMessage.textContent)
    .eql(
      "File is too large and won't be uploaded, file must be smaller than 10 B"
    ));
