import { Selector } from 'testcafe';
import { differenceInDays, parse } from 'date-fns';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  changeClientType,
  createClientDocument,
  deleteClientDocument,
  getClientByName,
  getClientDocumentByName
} from '../../../generic-functions';
import { deleteConfirmDialog } from '../../../generic-selectors/Centres';
import {
  inputLabel,
  inputValidationMessage,
  nurse
} from '../../../custom-commands';
import { clientsTableRow2 } from '../../../generic-selectors/Clients';
import { deleteConfirmButton } from '../../../generic-selectors/Layout';
import { snackbarText } from '../../../generic-selectors/Auth';

const clientDocumentsHeader = Selector('#clientDocumentsHeader');
const table = Selector('#documentsTable');
const headers = table.find('.responsiveTableHeader');
const data = table.find('tbody').find('td');
const rows = table.find('tbody').find('tr');
const noDataText = table.find('.q-table__bottom');
const imagePreview = Selector('#imagePreview');
const pdfPreview = Selector('#pdfPreview');
const deleteDocumentButtons = Selector('.deleteDocumentButton');
const editNameField = Selector('#documentNameField');
const documentPreviewDialog = Selector('#documentPreviewDialog');
const previewCardTitle = Selector('#documentPreviewDialog').find('.cardTitle');
const downloadDocumentButton = Selector('.downloadDocumentButton');
const closeDocumentPreviewDialog = Selector('#closeDocumentPreviewDialog');

fixture`components/clients/ClientDocuments`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients', 'documents']);
    await browser.resizeWindow(1280, 720).navigateTo(baseUrl);
    await nurse();
    return browser.click(clientsTableRow2);
  })
  .after(destroyDb);

test('dialog shows correct header', browser =>
  browser.expect(clientDocumentsHeader.textContent).eql('Documents'));

test('table shows correct headers', browser =>
  browser
    .expect(headers.count)
    .eql(4)
    .expect(headers.nth(0).textContent)
    .eql('Name')
    .expect(headers.nth(1).textContent)
    .eql('Upload Date')
    .expect(headers.nth(2).textContent)
    .eql('Size')
    .expect(headers.nth(3).textContent)
    .eql('Actions'));

test('client documents table has correct data', async browser =>
  browser
    .expect(data.count)
    .eql(12)
    .expect(data.nth(0).textContent)
    .eql('Example Document 1')
    .expect(
      differenceInDays(
        Date.now(),
        parse(await data.nth(1).textContent, 'dd/MM/yy', new Date())
      )
    )
    .lte(1)
    .expect(data.nth(2).textContent)
    .eql('36 B')
    .expect(data.nth(6).textContent)
    .eql('27 B')
    .expect(data.nth(10).textContent)
    .eql('18 B'));

test('client documents table shows message when no data', async browser => {
  await browser
    .expect(noDataText.textContent)
    .notEql('Test Client has no documents yet');
  await deleteClientDocument('Example Document 1');
  await deleteClientDocument('Example Document 2');
  await browser.expect(data.count).eql(4);
  await deleteClientDocument('Example Document 3');
  return browser
    .expect(noDataText.textContent)
    .eql('Test Client has no documents yet');
});

test('client documents table can be sorted by size', browser =>
  browser
    .click(headers.nth(2))
    .expect(data.nth(2).textContent)
    .eql('18 B')
    .expect(data.nth(10).textContent)
    .eql('36 B')
    .click(headers.nth(2))
    .expect(data.nth(2).textContent)
    .eql('36 B')
    .expect(data.nth(10).textContent)
    .eql('18 B'));

test('client documents table updates when changed elsewhere', async browser => {
  const [client] = await getClientByName('4');
  await createClientDocument(
    client.id,
    `new_file.csv`,
    `New Document`,
    'a,b,c,d,e'
  );
  return browser
    .expect(data.count)
    .eql(16)
    .expect(data.nth(12).textContent)
    .eql('New Document')
    .expect(
      differenceInDays(
        Date.now(),
        parse(await data.nth(13).textContent, 'dd/MM/yy', new Date())
      )
    )
    .lte(1)
    .expect(data.nth(14).textContent)
    .eql('9 B');
});

test('preview icon is visible for images and pdfs', browser =>
  browser
    .expect(rows.nth(0).find('.previewDocumentButton').visible)
    .notOk()
    .expect(rows.nth(1).find('.previewDocumentButton').visible)
    .ok()
    .expect(rows.nth(2).find('.previewDocumentButton').visible)
    .ok());

test('clicking preview on image opens image modal', browser =>
  browser
    .click(rows.nth(1).find('.previewDocumentButton'))
    .expect(imagePreview.visible)
    .ok()
    .expect(pdfPreview.exists)
    .notOk()
    .expect(previewCardTitle.textContent)
    .eql('Example Document 2'));

test('clicking preview on pdf opens pdf modal', browser =>
  browser
    .click(rows.nth(2).find('.previewDocumentButton'))
    .expect(pdfPreview.exists)
    .ok()
    .expect(imagePreview.visible)
    .notOk()
    .expect(previewCardTitle.textContent)
    .eql('Example Document 3'));

test('delete document button opens confirm dialog', browser =>
  browser
    .expect(deleteConfirmDialog.visible)
    .notOk()
    .click(deleteDocumentButtons.nth(1))
    .expect(deleteConfirmDialog.visible)
    .ok());

test('delete document confirm button updates database', async browser => {
  await browser.click(deleteDocumentButtons.nth(1)).click(deleteConfirmButton);
  const documents = await getClientDocumentByName('Example Document 2');
  return browser.expect(documents.length).eql(0);
});

test('delete document confirm button removes centre from page', browser =>
  browser
    .click(deleteDocumentButtons.nth(1))
    .click(deleteConfirmButton)
    .expect(deleteDocumentButtons.count)
    .eql(2));

test('delete document confirm button closes dialog', browser =>
  browser
    .click(deleteDocumentButtons.nth(1))
    .click(deleteConfirmButton)
    .expect(deleteConfirmDialog.visible)
    .notOk());

test('table updates when document deleted elsewhere', async browser => {
  await deleteClientDocument('Example Document 2');
  return browser.expect(data.count).eql(8);
});

test('name is editable', browser =>
  browser
    .click(data.nth(0))
    .expect(editNameField.visible)
    .ok()
    .expect(editNameField.value)
    .eql('Example Document 1')
    .typeText(editNameField, '234')
    .expect(editNameField.value)
    .eql('Example Document 1234')
    .click(table)
    .expect(data.nth(0).textContent)
    .eql('Example Document 1234'));

test('pressing enter closes edit dialog', browser =>
  browser
    .click(data.nth(0))
    .typeText(editNameField, '234')
    .pressKey('enter')
    .expect(data.nth(0).textContent)
    .eql('Example Document 1234'));

test('name edit label is correct', browser =>
  browser
    .click(data.nth(0))
    .selectText(editNameField)
    .pressKey('delete')
    .expect(inputLabel(editNameField))
    .eql('Document Name'));

test('updating name updates in database', async browser => {
  await browser
    .click(data.nth(0))
    .typeText(editNameField, '234')
    .pressKey('enter');
  const documents = await getClientDocumentByName('Example Document 1234');
  return browser.expect(documents.length).gt(0);
});

test('empty name field shows error', browser =>
  browser
    .click(data.nth(0))
    .selectText(editNameField)
    .pressKey('delete')
    .expect(inputValidationMessage(editNameField))
    .eql('Name is required'));

test('database does not update if client document name empty', async browser => {
  await browser
    .click(data.nth(0))
    .expect(editNameField.visible)
    .ok()
    .selectText(editNameField)
    .pressKey('delete')
    .pressKey('enter')
    .expect(editNameField.visible)
    .notOk();
  const documents = await getClientDocumentByName('Example Document 1');
  return browser
    .expect(
      documents.filter(
        (document: { name: string }) => document.name === 'Example Document 1'
      ).length
    )
    .gt(0);
});

test('close button closes document preview modal', async browser =>
  browser
    .click(rows.nth(2).find('.previewDocumentButton'))
    .expect(documentPreviewDialog.visible)
    .ok()
    .click(closeDocumentPreviewDialog)
    .expect(documentPreviewDialog.visible)
    .notOk());

test('preview icon for pdfs is not visible on android', async browser => {
  await changeClientType('mobile');
  return browser
    .expect(rows.nth(0).find('.previewDocumentButton').visible)
    .notOk()
    .expect(rows.nth(1).find('.previewDocumentButton').visible)
    .ok()
    .expect(rows.nth(2).find('.previewDocumentButton').visible)
    .notOk();
});

test('notification appears after file download', browser =>
  browser
    .click(downloadDocumentButton.nth(0))
    .expect(snackbarText.textContent)
    .eql('File downloaded, check your downloads folder to find it'));
