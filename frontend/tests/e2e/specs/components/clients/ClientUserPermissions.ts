import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import { nurse } from '../../../custom-commands';
import {
  clientHamburgerMenuIcon,
  clientUserPermissionsButton,
  clientsTableRow1
} from '../../../generic-selectors/Clients';
import {
  getUserByName,
  updateClientPermissions
} from '../../../generic-functions';

const canViewOwnDataToggle = Selector('#canViewOwnDataToggle');
const canViewCalendarToggle = Selector('#canViewCalendarToggle');

fixture`components/clients/ClientUserPermissions`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.resizeWindow(1280, 720).navigateTo(baseUrl);
    await nurse();
    return browser
      .click(clientsTableRow1)
      .click(clientHamburgerMenuIcon)
      .click(clientUserPermissionsButton);
  })
  .after(destroyDb);

test('cannot disable both options at once', async browser => {
  await browser
    .expect(
      canViewOwnDataToggle.find('.q-checkbox').getAttribute('aria-checked')
    )
    .eql('true')
    .expect(
      canViewCalendarToggle.find('.q-checkbox').getAttribute('aria-checked')
    )
    .eql('true')
    .click(canViewCalendarToggle)
    .expect(
      canViewCalendarToggle.find('.q-checkbox').getAttribute('aria-checked')
    )
    .eql('false')
    .click(canViewOwnDataToggle)
    .expect(
      canViewOwnDataToggle.find('.q-checkbox').getAttribute('aria-checked')
    )
    .eql('true');
});

test('changing permission updates database', async browser => {
  await browser
    .expect(canViewOwnDataToggle.textContent)
    .contains('Select this to allow Test Client to view their own medical info')
    .expect(canViewCalendarToggle.textContent)
    .contains('Select this to allow Test Client to view a calendar page')
    .click(canViewCalendarToggle);
  const testClientData = await getUserByName('clientUser');
  return browser
    .expect(
      testClientData.filter(
        user => user.permission === 'client_can_view_calendar'
      ).length
    )
    .eql(0)
    .expect(
      testClientData.filter(
        user => user.permission === 'client_can_view_own_data'
      ).length
    )
    .gt(0);
});

test('checkbox updates when changed elsewhere', async browser => {
  await updateClientPermissions('1', ['client_can_view_calendar']);
  return browser
    .expect(
      canViewCalendarToggle.find('.q-checkbox').getAttribute('aria-checked')
    )
    .eql('true')
    .expect(
      canViewOwnDataToggle.find('.q-checkbox').getAttribute('aria-checked')
    )
    .eql('false');
});
