import { RequestLogger, Selector } from 'testcafe';
import {
  clientHamburgerMenuIcon,
  clientsTableRow1
} from '../../../../generic-selectors/Clients';
import { nurse } from '../../../../custom-commands';
import { backendUrl, baseUrl, destroyDb, seedDb } from '../../../../globals';
import { snackbarText } from '../../../../generic-selectors/Auth';

import { getUserByName } from '../../../../generic-functions';

const addNewClientCard = Selector('#makeClientUserDialog');
const makeClientUserButton = Selector('#makeClientUserButton');
const confirmButton = Selector('#makeClientUserConfirmButton');
const cardTitle = addNewClientCard.find('.cardTitle');
const disableClientInfo = Selector('#disableClientInfo');
const notifyByEmailToggle = Selector('#notifyByEmailToggle');

const logger = RequestLogger(
  new RegExp(`${backendUrl}/api/set-client-user-activation`),
  { logRequestBody: true, stringifyRequestBody: true }
);

fixture`components/clients/MakeClientUser (Deactivate User)`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.navigateTo(baseUrl);
    await nurse();
    return browser
      .click(clientsTableRow1)
      .click(clientHamburgerMenuIcon)
      .click(makeClientUserButton);
  })
  .after(destroyDb)
  .requestHooks(logger);

test('text is correct', browser =>
  browser
    .expect(cardTitle.textContent)
    .eql('Prevent Test Client From Logging In')
    .expect(disableClientInfo.textContent)
    .contains(" Press confirm below to disable Test Client's account")
    .expect(confirmButton.textContent)
    .eql('Deactivate Account'));

test('pressing submit disables user', async browser => {
  await browser
    .click(confirmButton)
    .expect(snackbarText.textContent)
    .contains('Account Deactivated!')
    .expect(addNewClientCard.visible)
    .notOk();
  await browser
    .expect(
      logger.contains(
        r => r.request.body === '{"active":false,"sendEmail":true}'
      )
    )
    .eql(true);

  const clientUser = (await getUserByName('clientUser'))[0];
  await browser.expect(clientUser.isActive).eql(false);
});

test('email is not sent on switch press', async browser => {
  await browser
    .click(notifyByEmailToggle)
    .click(confirmButton)
    .expect(addNewClientCard.visible)
    .notOk();
  return browser
    .expect(
      logger.contains(
        r => r.request.body === '{"active":false,"sendEmail":false}'
      )
    )
    .eql(true);
});
