import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../../../../globals';
import {
  getClientByName,
  updateClient
} from '../../../../../generic-functions';

import {
  inputLabel,
  inputValidationMessage,
  nurse
} from '../../../../../custom-commands';
import { clientsTableRow2 } from '../../../../../generic-selectors/Clients';

const tableWrapper = Selector('.clientDialogTableWrapper').nth(1);
const headers = tableWrapper.find('.clientDialogTableHeader');
const data = tableWrapper.find('.clientDialogTableData');
const clientFieldEdit = Selector('.clientFieldEdit')
  .filterVisible()
  .find('input');
const clientFieldTextareaEdit = Selector('.clientFieldEdit')
  .filterVisible()
  .find('textarea');
const clientDialogTableMainHeader = tableWrapper.find(
  '.clientDialogTableMainHeader'
);

fixture`components/clients/clientDialogTableServices (Normal User)`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.resizeWindow(1280, 720).navigateTo(baseUrl);
    await nurse();
    return browser.click(clientsTableRow2);
  })
  .after(destroyDb);

test('dialog shows correct header', async browser =>
  browser.expect(clientDialogTableMainHeader.textContent).eql('Services'));

test('table shows correct headers', async browser =>
  browser
    .expect(headers.count)
    .eql(10)
    .expect(headers.nth(0).textContent)
    .eql('Public Health Nurse')
    .expect(headers.nth(1).textContent)
    .eql('Public Health Nurse Number')
    .expect(headers.nth(2).textContent)
    .eql('Home Help')
    .expect(headers.nth(3).textContent)
    .eql('Home Help Number')
    .expect(headers.nth(4).textContent)
    .eql('GP')
    .expect(headers.nth(5).textContent)
    .eql('GP Number')
    .expect(headers.nth(6).textContent)
    .eql('Chemist')
    .expect(headers.nth(7).textContent)
    .eql('Chemist Number')
    .expect(headers.nth(8).textContent)
    .eql('Medical History')
    .expect(headers.nth(9).textContent)
    .eql('Surgical History'));

test('table shows correct data', async browser =>
  browser
    .expect(data.count)
    .eql(10)
    .expect(data.nth(0).textContent)
    .eql('PHN 4')
    .expect(data.nth(1).textContent)
    .eql('PHN 4444444')
    .expect(data.nth(2).textContent)
    .eql('HH 4')
    .expect(data.nth(3).textContent)
    .eql('HH 4444444')
    .expect(data.nth(4).textContent)
    .eql('GP 4')
    .expect(data.nth(5).textContent)
    .eql('GP 4444444')
    .expect(data.nth(6).textContent)
    .eql('C 4')
    .expect(data.nth(7).textContent)
    .eql('C 4444444')
    .expect(data.nth(8).textContent)
    .eql('Client 4 Medical History')
    .expect(data.nth(9).textContent)
    .eql('Client 4 Surgical History'));

test('table updates when data changed elsewhere', async browser => {
  await updateClient('4', {
    homeHelpName: 'New HH Name',
    homeHelpPhoneNumber: 'New HH Number'
  });
  return browser
    .expect(data.nth(2).textContent)
    .eql('New HH Name')
    .expect(data.nth(3).textContent)
    .eql('New HH Number');
});

test('home help name is editable', async browser =>
  browser
    .click(data.nth(2))
    .expect(clientFieldEdit.visible)
    .ok()
    .expect(clientFieldEdit.value)
    .eql('HH 4')
    .typeText(clientFieldEdit, ' 1234')
    .expect(clientFieldEdit.value)
    .eql('HH 4 1234')
    .click(headers)
    .expect(data.nth(2).textContent)
    .eql('HH 4 1234'));

test('pressing enter closes edit dialog', async browser =>
  browser
    .click(data.nth(2))
    .typeText(clientFieldEdit, ' 1234')
    .pressKey('enter')
    .expect(data.nth(2).textContent)
    .eql('HH 4 1234'));

test('home help name edit label is correct', async browser =>
  browser
    .click(data.nth(2))
    .selectText(clientFieldEdit)
    .pressKey('delete')
    .expect(inputLabel(clientFieldEdit))
    .eql('Home Help'));

test('updating home help name updates in database', async browser =>
  browser
    .click(data.nth(2))
    .typeText(clientFieldEdit, ' 1234')
    .pressKey('enter')
    .then(() =>
      getClientByName('4').then(clients =>
        browser
          .expect(
            clients.filter(
              (client: { homeHelpName: string }) =>
                client.homeHelpName === 'HH 4 1234'
            ).length
          )
          .gt(0)
      )
    ));

test('empty home help name field shows error', async browser =>
  browser
    .click(data.nth(2))
    .selectText(clientFieldEdit)
    .pressKey('delete')
    .expect(inputValidationMessage(clientFieldEdit))
    .eql('Home Help is required'));

test('database does not update if field name empty', async browser =>
  browser
    .click(data.nth(2))
    .selectText(clientFieldEdit)
    .pressKey('delete')
    .pressKey('enter')
    .expect(clientFieldEdit.visible)
    .notOk()
    .then(() =>
      getClientByName('4').then(clients =>
        browser
          .expect(
            clients.filter(
              (client: { homeHelpName: string }) =>
                client.homeHelpName === 'HH 4'
            ).length
          )
          .gt(0)
      )
    ));

test('GP number edit box is a text field', async browser =>
  browser
    .click(data.nth(5))
    .expect(clientFieldEdit.count)
    .eql(1)
    .expect(clientFieldTextareaEdit.count)
    .eql(0));

test('medical history edit box is a text area', async browser =>
  browser
    .click(data.nth(8))
    .expect(clientFieldEdit.count)
    .eql(0)
    .expect(clientFieldTextareaEdit.count)
    .eql(1));

test('surgical history is editable', async browser =>
  browser
    .click(data.nth(9))
    .expect(clientFieldTextareaEdit.visible)
    .ok()
    .expect(clientFieldTextareaEdit.value)
    .eql('Client 4 Surgical History')
    .typeText(clientFieldTextareaEdit, ' 1234')
    .expect(clientFieldTextareaEdit.value)
    .eql('Client 4 Surgical History 1234')
    .click(headers)
    .expect(data.nth(9).textContent)
    .eql('Client 4 Surgical History 1234'));
