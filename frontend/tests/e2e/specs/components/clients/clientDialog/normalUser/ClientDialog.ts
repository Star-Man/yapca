import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../../../../globals';
import {
  addUserCentre,
  deleteClient,
  getClientByName,
  removeUserCentre,
  updateClient
} from '../../../../../generic-functions';
import { nurse } from '../../../../../custom-commands';
import {
  clientDialogHeader,
  clientHamburgerMenuIcon,
  clientVisitingCentreHeaders,
  clientsTableRow2,
  clientsTableRows,
  deleteClientButton,
  visitingCentresEditIcon
} from '../../../../../generic-selectors/Clients';
import {
  absenceHistoryDialog,
  historyButton as carePlanHistoryButton,
  carePlanHistoryDialog
} from '../../../../../generic-selectors/CarePlans';
import { deleteConfirmDialogContinueButton } from '../../../../../generic-selectors/Centres';
import { absenceHistoryButton } from '../../../../../generic-selectors/AbsenceRecords';
import {
  infoSnackbar,
  snackbarText
} from '../../../../../generic-selectors/Auth';

const visitingCentresEditDialog = Selector('#visitingCentresEditDialog');
const closeDialogButton = Selector('#clientDialog').find('#closeDialogButton');
const cardHeader = Selector('#clientDialog').find('.cardHeader');
const deleteClientDialog = Selector('#deleteClientDialog');

fixture`components/clients/clientDialog (Normal User)`
  .page(baseUrl)
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.resizeWindow(1280, 720).navigateTo(baseUrl);
    await nurse();
    return browser.click(clientsTableRow2);
  })
  .after(destroyDb);

test('dialog shows client name in header', async browser =>
  browser.expect(clientDialogHeader.textContent).eql('Test Client 4'));

test('header updates when data changed elsewhere', async browser => {
  await updateClient('4', {
    firstName: 'New First Name',
    surname: 'New Surname'
  });
  return browser
    .expect(clientDialogHeader.textContent)
    .eql('New First Name New Surname');
});

test('client header color is correct', async browser => {
  await browser
    .expect(cardHeader.getStyleProperty('background-color'))
    .eql('rgb(211, 47, 47)');
  await updateClient('4', {
    isActive: false
  });
  await browser
    .expect(cardHeader.getStyleProperty('background-color'))
    .eql('rgb(158, 158, 158)');
});

test('hamburger menu has correct options', async browser =>
  browser
    .expect(deleteClientButton.visible)
    .notOk()
    .click(clientHamburgerMenuIcon)
    .expect(deleteClientButton.textContent)
    .eql("Delete Test Client's Account")
    .expect(carePlanHistoryButton.textContent)
    .eql("Test Client's Care Plan History")
    .expect(absenceHistoryButton.textContent)
    .eql("Test Client's Absence History"));

test('delete client button opens confirm dialog', async browser =>
  browser
    .click(clientHamburgerMenuIcon)
    .click(deleteClientButton)
    .expect(deleteClientDialog.visible)
    .ok());

test('delete client confirm button updates database', async browser =>
  browser
    .click(clientHamburgerMenuIcon)
    .click(deleteClientButton)
    .click(deleteConfirmDialogContinueButton)
    .expect(deleteClientDialog.visible)
    .notOk()
    .then(() =>
      getClientByName('4').then(clientData =>
        browser.expect(clientData.length).eql(0)
      )
    ));

test('delete client confirm button removes client from table', async browser =>
  browser
    .expect(clientsTableRows.count)
    .eql(4)
    .click(clientHamburgerMenuIcon)
    .click(deleteClientButton)
    .click(deleteConfirmDialogContinueButton)
    .expect(clientsTableRows.count)
    .eql(3));

test('delete client confirm button hides client dialog', async browser =>
  browser
    .click(clientHamburgerMenuIcon)
    .click(deleteClientButton)
    .click(deleteConfirmDialogContinueButton)
    .expect(clientDialogHeader.visible)
    .notOk());

test('clicking edit on centres opens visiting centre edit dialog', async browser =>
  browser
    .click(visitingCentresEditIcon)
    .expect(visitingCentresEditDialog.visible)
    .ok());

test('list of client centres is visible', browser =>
  browser
    .expect(clientVisitingCentreHeaders.count)
    .eql(1)
    .expect(clientVisitingCentreHeaders.nth(0).textContent)
    .eql('Example Centre'));

test('client centres update on user change', async browser => {
  await addUserCentre('testNurse', 'Example Centre 3');
  return browser
    .expect(clientVisitingCentreHeaders.count)
    .eql(2)
    .expect(clientVisitingCentreHeaders.nth(1).textContent)
    .eql('Example Centre 3');
});

test('dialog disappears when deleted elsewhere', async browser => {
  await deleteClient('4');
  return browser
    .expect(clientDialogHeader.visible)
    .notOk()
    .expect(deleteClientButton.visible)
    .notOk();
});

test('care plan history button opens dialog', async browser =>
  browser
    .click(clientHamburgerMenuIcon)
    .click(carePlanHistoryButton)
    .expect(carePlanHistoryDialog.visible)
    .ok());

test('absence history button opens dialog', async browser =>
  browser
    .click(clientHamburgerMenuIcon)
    .click(absenceHistoryButton)
    .expect(absenceHistoryDialog.visible)
    .ok());

test('close button closes dialog', async browser =>
  browser.click(closeDialogButton).expect(clientDialogHeader.visible).notOk());

test('dialog closes with warning when not in same centres', async browser => {
  await removeUserCentre('testNurse', 'Example Centre');
  return browser
    .expect(clientDialogHeader.visible)
    .notOk()
    .expect(infoSnackbar.exists)
    .ok()
    .expect(snackbarText.textContent)
    .contains('Your access to this client has been removed by an admin');
});
