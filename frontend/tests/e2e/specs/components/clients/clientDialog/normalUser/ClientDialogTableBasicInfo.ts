import { Selector } from 'testcafe';
import { differenceInYears } from 'date-fns';
import { baseUrl, destroyDb, seedDb } from '../../../../../globals';
import {
  createClientRegisterEmail,
  getClientByName,
  updateClient
} from '../../../../../generic-functions';

import {
  inputLabel,
  inputValidationMessage,
  nurse
} from '../../../../../custom-commands';
import { clientsTableRow2 } from '../../../../../generic-selectors/Clients';
import {
  datePickerDay,
  selectorMenu
} from '../../../../../generic-selectors/Layout';

const tableWrapper = Selector('.clientDialogTableWrapper').nth(0);
const headers = tableWrapper.find('.clientDialogTableHeader');
const data = tableWrapper.find('.clientDialogTableData');
const clientFieldEdit = Selector('.clientFieldEdit')
  .filterVisible()
  .find('input');
const clientFieldTextareaEdit = Selector('.clientFieldEdit')
  .filterVisible()
  .find('textarea');
const dateFieldEdit = Selector('.clientDateFieldEdit');
const clientDialogTableMainHeader = tableWrapper.find(
  '.clientDialogTableMainHeader'
);

fixture`components/clients/clientDialogTableBasicInfo (Normal User)`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.resizeWindow(1280, 720).navigateTo(baseUrl);
    await nurse();
    return browser.click(clientsTableRow2);
  })
  .after(destroyDb);

test('dialog shows correct header', async browser =>
  browser.expect(clientDialogTableMainHeader.textContent).eql('Basic Info'));

test('basic info table shows correct headers', async browser =>
  browser
    .expect(headers.count)
    .eql(10)
    .expect(headers.nth(0).textContent)
    .eql('First Name')
    .expect(headers.nth(1).textContent)
    .eql('Surname')
    .expect(headers.nth(2).textContent)
    .eql('Gender')
    .expect(headers.nth(3).textContent)
    .eql('Active?')
    .expect(headers.nth(4).textContent)
    .eql('Account Status')
    .expect(headers.nth(5).textContent)
    .eql('Date Of Birth')
    .expect(headers.nth(6).textContent)
    .eql('Address')
    .expect(headers.nth(7).textContent)
    .eql('Phone Number')
    .expect(headers.nth(8).textContent)
    .eql('Admitted By')
    .expect(headers.nth(9).textContent)
    .eql('Created Date'));

test('table shows correct data', async browser => {
  await browser
    .expect(data.count)
    .eql(10)
    .expect(data.nth(0).textContent)
    .eql('Test Client')
    .expect(data.nth(1).textContent)
    .eql('4')
    .expect(data.nth(2).textContent)
    .eql('Unknown')
    .expect(data.nth(3).getAttribute('aria-checked'))
    .eql('true')
    .expect(data.nth(4).textContent)
    .eql('No Account');
  const age = differenceInYears(Date.now(), new Date('1971-07-08'));
  return browser
    .expect(data.nth(5).textContent)
    .eql(`08/07/71 (${age})`)
    .expect(data.nth(6).textContent)
    .eql('Example Address 4')
    .expect(data.nth(7).textContent)
    .eql('4444444')
    .expect(data.nth(8).textContent)
    .eql('Test Nurse')
    .expect(data.nth(9).textContent)
    .eql('16/04/23');
});

test('table updates when data changed elsewhere', async browser => {
  await updateClient('4', {
    firstName: 'New First Name',
    surname: 'New Surname'
  });
  return browser
    .expect(data.nth(0).textContent)
    .eql('New First Name')
    .expect(data.nth(1).textContent)
    .eql('New Surname');
});

test('first name is editable', async browser =>
  browser
    .click(data.nth(0))
    .expect(clientFieldEdit.visible)
    .ok()
    .expect(clientFieldEdit.value)
    .eql('Test Client')
    .typeText(clientFieldEdit, ' 1234')
    .expect(clientFieldEdit.value)
    .eql('Test Client 1234')
    .click(headers)
    .expect(data.nth(0).textContent)
    .eql('Test Client 1234'));

test('pressing enter closes edit dialog', async browser =>
  browser
    .click(data.nth(0))
    .typeText(clientFieldEdit, ' 1234')
    .pressKey('enter')
    .expect(data.nth(0).textContent)
    .eql('Test Client 1234'));

test('first name edit label is correct', async browser =>
  browser
    .click(data.nth(0))
    .selectText(clientFieldEdit)
    .pressKey('delete')
    .expect(inputLabel(clientFieldEdit))
    .eql('First Name'));

test('updating first name updates in database', async browser =>
  browser
    .click(data.nth(0))
    .typeText(clientFieldEdit, ' 1234')
    .pressKey('enter')
    .then(() =>
      getClientByName('4').then(clients =>
        browser
          .expect(
            clients.filter(
              (client: { firstName: string }) =>
                client.firstName === 'Test Client 1234'
            ).length
          )
          .gt(0)
      )
    ));

test('empty first name field shows error', async browser =>
  browser
    .click(data.nth(0))
    .selectText(clientFieldEdit)
    .pressKey('delete')
    .expect(inputValidationMessage(clientFieldEdit))
    .eql('First Name is required'));

test('database does not update if field name empty', async browser =>
  browser
    .click(data.nth(0))
    .selectText(clientFieldEdit)
    .pressKey('delete')
    .pressKey('enter')
    .expect(clientFieldEdit.visible)
    .notOk()
    .then(() =>
      getClientByName('4').then(clients =>
        browser
          .expect(
            clients.filter(
              (client: { firstName: string }) =>
                client.firstName === 'Test Client'
            ).length
          )
          .gt(0)
      )
    ));

test('surname is editable', async browser =>
  browser
    .click(data.nth(1))
    .expect(clientFieldEdit.visible)
    .ok()
    .expect(clientFieldEdit.value)
    .eql('4')
    .typeText(clientFieldEdit, ' 1234')
    .expect(clientFieldEdit.value)
    .eql('4 1234')
    .click(headers)
    .expect(data.nth(1).textContent)
    .eql('4 1234'));

test('date of birth is editable', async browser => {
  await browser
    .click(data.nth(5))
    .expect(dateFieldEdit.visible)
    .ok()
    .expect(datePickerDay.count)
    .eql(31)
    .click(datePickerDay.nth(14));
  const age = differenceInYears(Date.now(), new Date('1971-07-15'));
  await browser.expect(data.nth(5).textContent).eql(`15/07/71 (${age})`);
});

test('surname edit label is correct', async browser =>
  browser
    .click(data.nth(1))
    .selectText(clientFieldEdit)
    .pressKey('delete')
    .expect(inputLabel(clientFieldEdit))
    .eql('Surname'));

test('updating surname updates in database', async browser =>
  browser
    .click(data.nth(1))
    .typeText(clientFieldEdit, ' 1234')
    .pressKey('enter')
    .then(() =>
      getClientByName('4 1234').then(clients =>
        browser
          .expect(
            clients.filter(
              (client: { surname: string }) => client.surname === '4 1234'
            ).length
          )
          .gt(0)
      )
    ));

test('empty surname field shows error', async browser =>
  browser
    .click(data.nth(1))
    .selectText(clientFieldEdit)
    .pressKey('delete')
    .expect(inputValidationMessage(clientFieldEdit))
    .eql('Surname is required'));

test('admitted by is not editable', async browser =>
  browser.click(data.nth(8)).expect(clientFieldEdit.visible).notOk());

test('created date is not editable', async browser =>
  browser.click(data.nth(9)).expect(clientFieldEdit.visible).notOk());

test('clicking active switch updates it', async browser =>
  browser
    .click(data.nth(3))
    .expect(data.nth(3).getAttribute('aria-checked'))
    .eql('false'));

test('clicking active switch updates database', async browser =>
  browser
    .click(data.nth(3))
    .then(() =>
      getClientByName('4').then(clients =>
        browser
          .expect(
            clients.filter((client: { isActive: boolean }) => !client.isActive)
              .length
          )
          .gt(0)
      )
    ));

test('surname edit box is a text field', async browser =>
  browser
    .click(data.nth(1))
    .expect(clientFieldEdit.count)
    .eql(1)
    .expect(clientFieldTextareaEdit.count)
    .eql(0));

test('address edit box is a text area', async browser =>
  browser
    .click(data.nth(6))
    .expect(clientFieldEdit.count)
    .eql(0)
    .expect(clientFieldTextareaEdit.count)
    .eql(1));

test('reason for inactivity option is only visible when client inactive', async browser =>
  browser
    .click(data.nth(3))
    .expect(headers.count)
    .eql(11)
    .expect(headers.nth(4).textContent)
    .eql('Reason For Inactivity')
    .click(data.nth(3))
    .expect(headers.count)
    .eql(10)
    .expect(headers.nth(5).textContent)
    .eql('Date Of Birth'));

test('reason for inactivity changes visibility when active updated elsewhere', async browser => {
  await updateClient('4', {
    isActive: false
  });
  await browser
    .expect(headers.count)
    .eql(11)
    .expect(headers.nth(4).textContent)
    .eql('Reason For Inactivity');

  await updateClient('4', {
    isActive: true
  });
  return browser
    .expect(headers.count)
    .eql(10)
    .expect(headers.nth(5).textContent)
    .eql('Date Of Birth');
});

test('reason for inactivity updates database', async browser => {
  await browser
    .click(data.nth(3))
    .expect(data.nth(4).textContent)
    .eql('Deceased')
    .click(data.nth(4))
    .click(selectorMenu.nth(2))
    .expect(data.nth(4).textContent)
    .eql('No Longer Attending');

  const clientData = await getClientByName('4');
  return browser
    .expect(
      clientData.filter(
        (client: { reasonForInactivity: string }) =>
          client.reasonForInactivity === 'No Longer Attending'
      ).length
    )
    .gt(0);
});

test('reason for inactivity updates when changed elsewhere', async browser => {
  await updateClient('4', {
    isActive: false,
    reasonForInactivity: 'Entered Long Term Care'
  });
  return browser.expect(data.nth(4).textContent).eql('Entered Long Term Care');
});

test('changing inactivity does not affect reason for inactivity', async browser => {
  await updateClient('4', {
    isActive: false,
    reasonForInactivity: 'Entered Long Term Care'
  });
  await updateClient('4', {
    isActive: true
  });
  await updateClient('4', {
    isActive: false
  });
  return browser.expect(data.nth(4).textContent).eql('Entered Long Term Care');
});

test('gender selector has correct options', async browser =>
  browser
    .click(data.nth(2))
    .expect(selectorMenu.count)
    .eql(3)
    .expect(selectorMenu.nth(0).textContent)
    .eql('Female')
    .expect(selectorMenu.nth(1).textContent)
    .eql('Male')
    .expect(selectorMenu.nth(2).textContent)
    .eql('Unknown'));

test('changing gender updates database', async browser => {
  await browser
    .click(data.nth(2))
    .click(selectorMenu.nth(1))
    .expect(data.nth(2).textContent)
    .eql('Male');
  const clientData = await getClientByName('4');
  return browser
    .expect(
      clientData.filter(
        (client: { gender: string }) => client.gender === 'Male'
      ).length
    )
    .gt(0);
});

test('changing gender updates elsewhere', async browser => {
  await updateClient('4', { gender: 'Female' });
  return browser.expect(data.nth(2).textContent).eql('Female');
});

test('"Has account" becomes pending when client email is sent', async browser => {
  const client4Id = (await getClientByName('4'))[0].id;
  await createClientRegisterEmail(
    client4Id,
    'testemail2@test.com',
    true,
    false
  );
  return browser.expect(data.nth(4).textContent).eql('Pending');
});
