import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../../../../globals';
import { updateClient } from '../../../../../generic-functions';

import { clientUser } from '../../../../../custom-commands';

const tableWrapper = Selector('.clientDialogTableWrapper').nth(2);
const headers = tableWrapper.find('.clientDialogTableHeader');
const data = tableWrapper.find('.clientDialogTableData');
const clientFieldEdit = Selector('#clientFieldEdit');
const clientDialogTableMainHeader = tableWrapper.find(
  '.clientDialogTableMainHeader'
);

fixture`components/clients/clientDialogTableAbility (Client User)`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.resizeWindow(1280, 720).navigateTo(baseUrl);
    return clientUser();
  })
  .after(destroyDb);

test('dialog shows correct header', async browser =>
  browser.expect(clientDialogTableMainHeader.textContent).eql('Ability'));

test('table shows correct headers', async browser =>
  browser
    .expect(headers.count)
    .eql(7)
    .expect(headers.nth(0).textContent)
    .eql('Mobility')
    .expect(headers.nth(1).textContent)
    .eql('Toileting')
    .expect(headers.nth(2).textContent)
    .eql('Hygiene')
    .expect(headers.nth(3).textContent)
    .eql('Sight')
    .expect(headers.nth(4).textContent)
    .eql('Hearing')
    .expect(headers.nth(5).textContent)
    .eql('Dentures?')
    .expect(headers.nth(6).textContent)
    .eql('Case Formulation'));

test('table shows correct data', async browser =>
  browser
    .expect(data.count)
    .eql(7)
    .expect(data.nth(0).textContent)
    .eql('Walks With Frame')
    .expect(data.nth(1).textContent)
    .eql('Fully Continent')
    .expect(data.nth(2).textContent)
    .eql('Full Hoist Shower')
    .expect(data.nth(3).textContent)
    .eql('Glasses')
    .expect(data.nth(4).textContent)
    .eql('Impaired')
    .expect(data.nth(5).getAttribute('aria-checked'))
    .eql('true')
    .expect(data.nth(6).textContent)
    .eql('Client 1 Case Formulation'));

test('table updates when data changed elsewhere', async browser => {
  await updateClient('1', {
    hearing: 'Normal',
    caseFormulation: 'New Client 1 Case Formulation'
  });
  return browser
    .expect(data.nth(4).textContent)
    .eql('Normal')
    .expect(data.nth(6).textContent)
    .eql('New Client 1 Case Formulation');
});

test('hygiene is not editable', async browser =>
  browser.click(data.nth(2)).expect(clientFieldEdit.visible).notOk());
