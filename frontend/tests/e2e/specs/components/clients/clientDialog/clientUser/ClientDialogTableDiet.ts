import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../../../../globals';
import { updateClient } from '../../../../../generic-functions';

import { clientUser } from '../../../../../custom-commands';

const tableWrapper = Selector('.clientDialogTableWrapper').nth(3);
const headers = tableWrapper.find('.clientDialogTableHeader');
const data = tableWrapper.find('.clientDialogTableData');
const clientDialogTableMainHeader = tableWrapper.find(
  '.clientDialogTableMainHeader'
);
const slider = Selector('.clientEditSlider').find('.q-slider__thumb');
const feedingRisksInput = Selector('#feedingRisksInput');

fixture`components/clients/clientDialogTableDiet (Client User)`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.resizeWindow(1280, 720).navigateTo(baseUrl);
    return clientUser();
  })
  .after(destroyDb);

test('dialog shows correct header', async browser =>
  browser.expect(clientDialogTableMainHeader.textContent).eql('Diet'));

test('table shows correct headers', async browser =>
  browser
    .expect(headers.count)
    .eql(5)
    .expect(headers.nth(0).textContent)
    .eql('Allergies')
    .expect(headers.nth(1).textContent)
    .eql('Intolerances')
    .expect(headers.nth(2).textContent)
    .eql('Requires Assistance Eating?')
    .expect(headers.nth(3).textContent)
    .eql('Thickner Grade')
    .expect(headers.nth(4).textContent)
    .eql('Feeding Risks'));

test('table shows correct data', async browser =>
  browser
    .expect(data.count)
    .eql(4)
    .expect(data.nth(0).textContent)
    .eql('')
    .expect(data.nth(1).textContent)
    .eql('')
    .expect(data.nth(2).getAttribute('aria-checked'))
    .eql('false')
    .expect(data.nth(3).textContent)
    .eql('4')
    .expect(feedingRisksInput.find('.q-chip__content').count)
    .eql(0));

test('table updates when data changed elsewhere', async browser => {
  await updateClient('1', {
    thicknerGrade: 2,
    requiresAssistanceEating: false
  });
  return browser
    .expect(data.nth(2).getAttribute('aria-checked'))
    .eql('false')
    .expect(data.nth(3).textContent)
    .eql('2');
});

test('thickner grade is not editable', async browser =>
  browser.click(data.nth(3)).expect(slider.visible).notOk());
