import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../../../../globals';
import {
  deleteClient,
  getCentreByName,
  updateClientCentre
} from '../../../../../generic-functions';
import { clientUser } from '../../../../../custom-commands';
import {
  clientDialogHeader,
  clientVisitingCentreHeaders
} from '../../../../../generic-selectors/Clients';
import { usernameInput } from '../../../../../generic-selectors/Login';
import {
  infoSnackbar,
  snackbarText
} from '../../../../../generic-selectors/Auth';

const visitingCentresEditDialog = Selector('#visitingCentresEditDialog');

fixture`components/clients/clientDialog (Client User)`
  .page(baseUrl)
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.resizeWindow(1280, 720).navigateTo(baseUrl);
    return clientUser();
  })
  .after(destroyDb);

test('no dialog/header exists', async browser =>
  browser.expect(clientDialogHeader.exists).notOk());

test('no edit button visible on centres', async browser =>
  browser.expect(visitingCentresEditDialog.exists).notOk());

test('list of client centres is visible', browser =>
  browser
    .expect(clientVisitingCentreHeaders.count)
    .eql(1)
    .expect(clientVisitingCentreHeaders.nth(0).textContent)
    .eql('Example Centre'));

test('client centres update on user change', async browser => {
  const centre4Id = (await getCentreByName('Example Centre 4'))[0].id;
  await updateClientCentre('everyCentreUser', '1', centre4Id, true);
  return browser
    .expect(clientVisitingCentreHeaders.count)
    .eql(2)
    .expect(clientVisitingCentreHeaders.nth(1).textContent)
    .eql('Example Centre 4');
});

test('client is logged out when deleted elsewhere', async browser => {
  await deleteClient('1');
  return browser
    .expect(usernameInput.visible)
    .ok()
    .expect(infoSnackbar.exists)
    .ok()
    .expect(snackbarText.textContent)
    .contains('An admin has deleted your account');
});
