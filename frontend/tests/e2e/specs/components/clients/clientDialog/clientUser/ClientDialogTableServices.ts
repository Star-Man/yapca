import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../../../../globals';
import { updateClient } from '../../../../../generic-functions';

import { clientUser } from '../../../../../custom-commands';

const tableWrapper = Selector('.clientDialogTableWrapper').nth(1);
const headers = tableWrapper.find('.clientDialogTableHeader');
const data = tableWrapper.find('.clientDialogTableData');
const clientFieldEdit = Selector('.clientFieldEdit')
  .filterVisible()
  .find('input');
const clientFieldTextareaEdit = Selector('.clientFieldEdit')
  .filterVisible()
  .find('textarea');
const clientDialogTableMainHeader = tableWrapper.find(
  '.clientDialogTableMainHeader'
);

fixture`components/clients/clientDialogTableServices (Client User)`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.resizeWindow(1280, 720).navigateTo(baseUrl);
    return clientUser();
  })
  .after(destroyDb);

test('dialog shows correct header', async browser =>
  browser.expect(clientDialogTableMainHeader.textContent).eql('Services'));

test('table shows correct headers', async browser =>
  browser
    .expect(headers.count)
    .eql(10)
    .expect(headers.nth(0).textContent)
    .eql('Public Health Nurse')
    .expect(headers.nth(1).textContent)
    .eql('Public Health Nurse Number')
    .expect(headers.nth(2).textContent)
    .eql('Home Help')
    .expect(headers.nth(3).textContent)
    .eql('Home Help Number')
    .expect(headers.nth(4).textContent)
    .eql('GP')
    .expect(headers.nth(5).textContent)
    .eql('GP Number')
    .expect(headers.nth(6).textContent)
    .eql('Chemist')
    .expect(headers.nth(7).textContent)
    .eql('Chemist Number')
    .expect(headers.nth(8).textContent)
    .eql('Medical History')
    .expect(headers.nth(9).textContent)
    .eql('Surgical History'));

test('table shows correct data', async browser =>
  browser
    .expect(data.count)
    .eql(10)
    .expect(data.nth(0).textContent)
    .eql('PHN 1')
    .expect(data.nth(1).textContent)
    .eql('PHN 1111111')
    .expect(data.nth(2).textContent)
    .eql('HH 1')
    .expect(data.nth(3).textContent)
    .eql('HH 1111111')
    .expect(data.nth(4).textContent)
    .eql('GP 1')
    .expect(data.nth(5).textContent)
    .eql('GP 1111111')
    .expect(data.nth(6).textContent)
    .eql('C 1')
    .expect(data.nth(7).textContent)
    .eql('C 1111111')
    .expect(data.nth(8).textContent)
    .eql('Client 1 Medical History')
    .expect(data.nth(9).textContent)
    .eql('Client 1 Surgical History'));

test('table updates when data changed elsewhere', async browser => {
  await updateClient('1', {
    homeHelpName: 'New HH Name',
    homeHelpPhoneNumber: 'New HH Number'
  });
  return browser
    .expect(data.nth(2).textContent)
    .eql('New HH Name')
    .expect(data.nth(3).textContent)
    .eql('New HH Number');
});

test('home help name is not editable', async browser =>
  browser.click(data.nth(2)).expect(clientFieldEdit.visible).notOk());

test('surgical history is not editable', async browser =>
  browser.click(data.nth(9)).expect(clientFieldTextareaEdit.visible).notOk());
