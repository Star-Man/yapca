import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  addUserCentre,
  agreeToPolicies,
  createDifferentExtraPolicies,
  createExtraPolicies,
  getClientByName,
  removeUserCentre
} from '../../../generic-functions';
import { nurse } from '../../../custom-commands';
import {
  clientUnagreedPoliciesDialog,
  clientsTableRow2
} from '../../../generic-selectors/Clients';

const unagreedPoliciesCentreHeader = Selector('.unagreedPoliciesCentreHeader');
const unagreedPoliciesCentrePolicyText = Selector(
  '.unagreedPoliciesCentrePolicyText'
);
const policyAgreeInfoText = Selector('#policyAgreeInfoText');
const confirmButton = Selector('#policyAgreeButton');

fixture`components/clients/ClientUnagreedPolicyDialog`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await createExtraPolicies();
    await browser.resizeWindow(1280, 720).navigateTo(baseUrl);
    await nurse();
    return browser.click(clientsTableRow2);
  })
  .after(destroyDb);

test('dialog appears when client has unagreed policies', async browser =>
  browser.expect(clientUnagreedPoliciesDialog.visible).ok());

test('dialog does not appear when client has no unagreed policies', async browser => {
  await agreeToPolicies('4');
  return browser
    .navigateTo(`${baseUrl}/clients`)
    .click(clientsTableRow2)
    .expect(clientUnagreedPoliciesDialog.visible)
    .notOk();
});

test('dialog disappears when client agrees to policies elsewhere', async browser => {
  await agreeToPolicies('4');
  return browser.expect(clientUnagreedPoliciesDialog.visible).notOk();
});

test('dialog shows centres', async browser =>
  browser
    .expect(unagreedPoliciesCentreHeader.count)
    .eql(1)
    .expect(unagreedPoliciesCentreHeader.nth(0).textContent)
    .eql('Example Centre'));

test('centres change when updated elsewhere', async browser => {
  await createDifferentExtraPolicies();
  return browser.expect(unagreedPoliciesCentreHeader.count).eql(1);
});

test('dialog shows unagreed policies for centres', async browser =>
  browser
    .expect(unagreedPoliciesCentrePolicyText.count)
    .eql(3)
    .expect(unagreedPoliciesCentrePolicyText.nth(0).textContent)
    .eql('Example Policy 101')
    .expect(unagreedPoliciesCentrePolicyText.nth(1).textContent)
    .eql('Example Policy 102')
    .expect(unagreedPoliciesCentrePolicyText.nth(2).textContent)
    .eql('Example Policy 103'));

test('dialog updates on change user centres', async browser => {
  await addUserCentre('testNurse', 'Example Centre 3');
  await removeUserCentre('testNurse', 'Example Centre 2');
  await removeUserCentre('testNurse', 'Example Centre 4');
  return browser
    .expect(unagreedPoliciesCentrePolicyText.count)
    .eql(4)
    .expect(unagreedPoliciesCentrePolicyText.nth(3).textContent)
    .eql('Example Policy 102')
    .expect(unagreedPoliciesCentreHeader.count)
    .eql(2)
    .expect(unagreedPoliciesCentreHeader.nth(0).textContent)
    .eql('Example Centre')
    .expect(unagreedPoliciesCentreHeader.nth(1).textContent)
    .eql('Example Centre 3');
});

test('unagreed policies change when updated elsewhere', async browser => {
  await createDifferentExtraPolicies();
  return browser
    .expect(unagreedPoliciesCentrePolicyText.count)
    .eql(1)
    .expect(unagreedPoliciesCentrePolicyText.nth(0).textContent)
    .eql('Example Policy 104');
});

test('dialog shows correct count for unagreed policies', async browser =>
  browser
    .expect(policyAgreeInfoText.textContent)
    .eql(
      '3 new policies have been added. Before this client can be edited you must click agree. By clicking agree you accept that this client has accepted the policies below. '
    ));

test('count changes when updated elsewhere', async browser => {
  await createDifferentExtraPolicies();
  return browser
    .expect(policyAgreeInfoText.textContent)
    .eql(
      'A new policy has been added. Before this client can be edited you must click agree. By clicking agree you accept that this client has accepted the policy below. '
    );
});

test('clicking agree updates database', async browser => {
  await browser
    .click(confirmButton)
    .expect(clientUnagreedPoliciesDialog.visible)
    .notOk();
  const client = await getClientByName('4');
  return browser
    .expect(
      client.filter(
        (clientData: { policyDescription: string }) =>
          clientData.policyDescription === 'Example Policy 101'
      ).length
    )
    .gte(1)
    .expect(
      client.filter(
        (clientData: { policyDescription: string }) =>
          clientData.policyDescription === 'Example Policy 102'
      ).length
    )
    .gte(1)
    .expect(
      client.filter(
        (clientData: { policyDescription: string }) =>
          clientData.policyDescription === 'Example Policy 103'
      ).length
    )
    .gte(1)
    .expect(
      client.filter(
        (clientData: { policyDescription: string }) =>
          clientData.policyDescription === 'Example Policy 1'
      ).length
    )
    .eql(0)
    .expect(
      client.filter(
        (clientData: { policyDescription: string }) =>
          clientData.policyDescription === 'Example Policy 3'
      ).length
    )
    .eql(0)
    .expect(
      client.filter(
        (clientData: { policyDescription: string }) =>
          clientData.policyDescription === 'Example Policy 5'
      ).length
    )
    .eql(0);
});
