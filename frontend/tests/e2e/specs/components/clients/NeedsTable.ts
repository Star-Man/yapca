import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  getClientByName,
  getNeedByDescription,
  updateCentre,
  updateClientNeed
} from '../../../generic-functions';

import { clientUser, nurse } from '../../../custom-commands';
import {
  clientVisitingCentreHeaders,
  clientsTableRow2
} from '../../../generic-selectors/Clients';
import { logoutIcon } from '../../../generic-selectors/Layout';

const needsTable = Selector('#needsTable');
const headers = needsTable.find('th');
const data = needsTable.find('tbody').find('td');
const planData = needsTable.find('.td-plan');
const tableData = Selector('.needsTable').nth(0).find('tbody').find('td');
const needEditPlanField = Selector('#needEditPlanField');
const noDataLabel = needsTable.find('.q-table__bottom');

fixture`components/clients/NeedsTable`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.navigateTo(baseUrl).resizeWindow(1280, 720);
    await nurse();
    return browser
      .click(clientsTableRow2)
      .click(clientVisitingCentreHeaders.nth(0));
  })
  .after(destroyDb);

test('table shows correct headers', async browser =>
  browser
    .expect(headers.count)
    .eql(2)
    .expect(headers.nth(0).textContent)
    .eql('Need')
    .expect(headers.nth(1).textContent)
    .eql('Plan'));

test('table shows correct data', async browser =>
  browser
    .expect(data.count)
    .eql(6)
    .expect(planData.count)
    .eql(3)
    .expect(data.nth(0).textContent)
    .eql('Example Need 1')
    .expect(data.nth(2).textContent)
    .eql('Example Need 3')
    .expect(data.nth(4).textContent)
    .eql('Example Need 5')
    .expect(planData.nth(0).textContent)
    .eql('Example Plan 1')
    .expect(planData.nth(1).textContent)
    .eql('Example Plan 3')
    .expect(planData.nth(2).textContent)
    .eql('Click to add plan'));

test('click to add plan not visible for client', async browser => {
  await browser.pressKey('esc').click(logoutIcon);
  await clientUser();
  return browser
    .click(clientVisitingCentreHeaders.nth(0))
    .expect(data.nth(0).textContent)
    .eql('Example Need 1')
    .expect(planData.nth(0).textContent)
    .eql('');
});

test('client needs table shows message when no data', async browser => {
  await updateCentre('Example Centre', {
    needs: []
  });
  return browser
    .expect(tableData.count)
    .eql(0)
    .expect(noDataLabel.textContent)
    .eql(
      'This client has no needs for this centre, click the pencil to add some'
    );
});

test('editing plan updates database', async browser => {
  await browser
    .expect(planData.count)
    .eql(3)
    .expect(planData.nth(1).visible)
    .ok()
    .click(planData.nth(1))
    .selectText(needEditPlanField)
    .pressKey('delete')
    .typeText(needEditPlanField, 'New plan')
    .click(headers);
  const clients = await getClientByName('4');
  return browser
    .expect(
      clients.filter(
        (client: { needPlan: string }) => client.needPlan === 'New plan'
      ).length
    )
    .gte(1);
});

test('need cannot be edited', browser =>
  browser.click(data.nth(0)).expect(needEditPlanField.visible).notOk());

test('editing plan updates displayed data', browser =>
  browser
    .expect(planData.count)
    .eql(3)
    .expect(planData.nth(1).visible)
    .ok()
    .click(planData.nth(1))
    .selectText(needEditPlanField)
    .pressKey('delete')
    .typeText(needEditPlanField, 'New plan')
    .click(planData.nth(0))
    .expect(planData.nth(1).textContent)
    .eql('New plan'));

test('need plans update when changed elsewhere', async browser => {
  await updateClientNeed('Example Plan 3', '4', { plan: 'Test Need Plan' });
  await updateClientNeed('Example Plan 1', '4', { isActive: false });
  await browser
    .expect(data.count)
    .eql(4)
    .expect(data.nth(0).textContent)
    .eql('Example Need 3')
    .expect(planData.nth(0).textContent)
    .eql('Test Need Plan');
});

test('table updates when centre needs changed', async browser => {
  const [exampleNeed3] = await getNeedByDescription('Example Need 3');
  const [exampleNeed4] = await getNeedByDescription('Example Need 4');

  await updateCentre('Example Centre', {
    needs: [exampleNeed3.id, exampleNeed4.id]
  });
  await browser
    .expect(data.count)
    .eql(2)
    .expect(data.nth(0).textContent)
    .eql('Example Need 3')
    .expect(planData.nth(0).textContent)
    .eql('Example Plan 3');
});
