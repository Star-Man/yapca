import { Selector } from 'testcafe';
import { baseUrl } from '../globals';
import {
  changeClientType,
  changeClientVersion,
  changePlatform
} from '../generic-functions';
import {
  infoSnackbar,
  snackbarButton,
  snackbarText
} from '../generic-selectors/Auth';

fixture`App`.page(baseUrl).beforeEach(async browser => {
  await browser.resizeWindow(1280, 720);
  return changePlatform('windows');
});

const distroSelectButtons = Selector('#distroSelectButtons');
const rpmFileSelectButton = Selector('#rpmFileSelectButton');

test('snackbar appears when app out of date', async browser => {
  await changeClientVersion('0.0.0');
  await changeClientType('electron');
  return browser
    .expect(infoSnackbar.exists)
    .ok()
    .expect(snackbarText.textContent)
    .eql('A new version of this app is available!')
    .expect(snackbarButton.visible)
    .ok()
    .expect(snackbarButton.textContent)
    .eql('Download');
});

test('snackbar does not appear when not out of date', async browser => {
  await changeClientVersion('999999.0.0');
  await changeClientType('electron');
  return browser.expect(infoSnackbar.exists).notOk();
});

test('completed download shows snackbar message', async browser => {
  await changeClientVersion('0.0.0');
  await changeClientType('electron');
  return browser
    .click(snackbarButton)
    .expect(distroSelectButtons.exists)
    .notOk()
    .expect(infoSnackbar.exists)
    .ok({ timeout: 60000 })
    .expect(snackbarText.textContent)
    .eql('Update downloaded, please run the downloaded file');
});

test('linux version opens distro selector', async browser => {
  await changePlatform('linux');
  await changeClientVersion('0.0.0');
  await changeClientType('electron');
  return browser
    .click(snackbarButton)
    .expect(infoSnackbar.exists)
    .notOk()
    .expect(distroSelectButtons.visible)
    .ok()
    .click(rpmFileSelectButton)
    .expect(distroSelectButtons.visible)
    .notOk()
    .expect(infoSnackbar.exists)
    .ok({ timeout: 60000 })
    .expect(snackbarText.textContent)
    .eql('Update downloaded, please run the downloaded file');
});
