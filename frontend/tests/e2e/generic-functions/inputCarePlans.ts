import axios from 'axios';
import * as dotenv from 'dotenv';
import { database } from '../globals';
import type LoginResponse from 'src/types/axios/LoginResponse';
import type { AxiosResponse } from 'axios';

dotenv.config({
  path: `../../../config/${process.env.YAPCA_ENV ?? 'local'}.env`
});

const http = axios.create({
  baseURL: `${process.env.VUE_APP_API_PROTOCOL ?? ''}://${
    process.env.VUE_APP_API_HOST ?? ''
  }:${process.env.VUE_APP_API_PORT ?? ''}${
    process.env.VUE_APP_API_PATH ?? ''
  }/api/`
});

// eslint-disable-next-line @typescript-eslint/ban-types
const inputCarePlans = (
  surname: string,
  date: string
): Promise<AxiosResponse> =>
  database()
    .select()
    .from('client_client')
    .leftJoin(
      'authentication_customuser',
      'client_client.admittedBy_id',
      'authentication_customuser.id'
    )
    .where('surname', surname)
    .columns(['client_client.id'])
    .then((client: { id: number }[]) =>
      http
        .post('login/', {
          username: 'everyCentreUser',
          password: 'testPassword1'
        })
        .then((response: LoginResponse) =>
          http.get(`carePlans/${client[0].id}/${date}/input/`, {
            headers: {
              Authorization: `Token ${response.data.key}`
            }
          })
        )
    );

export default inputCarePlans;
