import { ClientFunction } from 'testcafe';

const goBack = ClientFunction(() => window.history.back());

export default goBack;
