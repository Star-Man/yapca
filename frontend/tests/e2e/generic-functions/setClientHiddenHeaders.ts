import axios from 'axios';

import * as dotenv from 'dotenv';
import getUserByName from './getUserByName';
import type LoginResponse from 'src/types/axios/LoginResponse';
import type { AxiosResponse } from 'axios';

dotenv.config({
  path: `../../../config/${process.env.YAPCA_ENV ?? 'local'}.env`
});
const http = axios.create({
  baseURL: `${process.env.VUE_APP_API_PROTOCOL ?? ''}://${
    process.env.VUE_APP_API_HOST ?? ''
  }:${process.env.VUE_APP_API_PORT ?? ''}${
    process.env.VUE_APP_API_PATH ?? ''
  }/api/`
});

const allHeaders = [
  'First Name',
  'Surname',
  'Admitted By',
  'Gender',
  'Date Added',
  'Date Of Birth',
  'Address',
  'Phone Number',
  'Home Help',
  'Home Help Number',
  'Public Health Nurse',
  'Public Health Nurse Number',
  'GP',
  'GP Number',
  'Chemist',
  'Chemist Number',
  'Mobility',
  'Toileting',
  'Hygiene',
  'Sight',
  'Hearing',
  'Thickner Grade'
];

const setClientHiddenHeaders = (
  username: string,
  hiddenHeaders: string[],
  subtract = false
): Promise<AxiosResponse> => {
  let headers = [...hiddenHeaders];
  if (subtract) {
    headers = allHeaders.filter(header => !hiddenHeaders.includes(header));
  }
  return getUserByName(username).then((user: { id: number }[]) =>
    http
      .post('login/', {
        username: 'testAdmin',
        password: 'testPassword1'
      })
      .then((response: LoginResponse) =>
        http.patch(
          `users/${user[0].id}/update/`,
          { userClientTableHiddenRows: JSON.stringify(headers) },
          {
            headers: {
              Authorization: `Token ${response.data.key}`
            }
          }
        )
      )
  );
};

export default setClientHiddenHeaders;
