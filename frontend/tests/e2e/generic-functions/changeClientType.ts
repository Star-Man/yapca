import { ClientFunction } from 'testcafe';
import type ClientApp from '../types/ClientApp';

const changeClientType = ClientFunction((clientType: string) => {
  const app = document.querySelector('#q-app') as unknown as ClientApp;
  const store = app.__vue_app__!.config.globalProperties.$pinia.state.value;
  const platform = store.basic.platform;
  platform.electron = clientType === 'electron';
  platform.mobile = clientType === 'mobile';
  platform.desktop = clientType === 'desktop';
});

export default changeClientType;
