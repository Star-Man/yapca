import { database } from '../globals';
import type CarePlan from 'src/types/CarePlan';

const getCarePlans = (): Promise<CarePlan[]> =>
  database().select().from('client_careplan');
export default getCarePlans;
