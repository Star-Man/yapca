import { database } from '../globals';
import getClientByName from './getClientByName';
import getUserByName from './getUserByName';
import getCentreByName from './getCentreByName';

const createAbsenceRecords = async (): Promise<void> => {
  const nurseId = (await getUserByName('testNurse'))[0].id;
  const client4Id = (await getClientByName('4'))[0].id;
  const centre1Id = (await getCentreByName('Example Centre'))[0].id;
  const centre2Id = (await getCentreByName('Example Centre 2'))[0].id;

  return database()
    .table('client_attendancerecord')
    .insert([
      {
        date: '2020-01-03',
        active: false,
        reasonForInactivity: 'Deceased',
        reasonForAbsence: '',
        addedBy_id: null,
        centre_id: centre1Id,
        client_id: client4Id
      },
      {
        date: '2020-01-04',
        active: true,
        reasonForAbsence: 'Sick',
        reasonForInactivity: '',
        addedBy_id: nurseId,
        centre_id: centre2Id,
        client_id: client4Id
      }
    ]);
};

export default createAbsenceRecords;
