import { database } from '../globals';

const getMedicationByName = (
  name: string
): Promise<{ name: string; id: number }[]> =>
  database().select().from('client_medication').where('name', name);
export default getMedicationByName;
