import axios from 'axios';
import * as dotenv from 'dotenv';
import type LoginResponse from 'src/types/axios/LoginResponse';
import type PoliciesResponse from 'src/types/axios/PoliciesResponse';

dotenv.config({
  path: `../../../config/${process.env.YAPCA_ENV ?? 'local'}.env`
});
const http = axios.create({
  baseURL: `${process.env.VUE_APP_API_PROTOCOL ?? ''}://${
    process.env.VUE_APP_API_HOST ?? ''
  }:${process.env.VUE_APP_API_PORT ?? ''}${
    process.env.VUE_APP_API_PATH ?? ''
  }/api/`
});

const createIncompleteCentres = async (): Promise<void> => {
  const loginResponse: LoginResponse = await http.post('login/', {
    username: 'testAdmin',
    password: 'testPassword1'
  });
  const policies: PoliciesResponse = await http.post(
    'policies/',
    [
      { description: 'Example Policy 1' },
      { description: 'Example Policy 2' },
      { description: 'Example Policy 3' },
      { description: 'Example Policy 4' },
      { description: 'Example Policy 5' }
    ],
    {
      headers: {
        Authorization: `Token ${loginResponse.data.key}`
      }
    }
  );
  const policyArray = policies.data.map((policy: { id: number }) => policy.id);

  await http.post(
    'centres/',
    {
      name: 'Example Centre 5',
      openingDaysFirstSetupComplete: true,
      closingDaysFirstSetupComplete: false,
      policiesFirstSetupComplete: false,
      setupComplete: false,
      needsFirstSetupComplete: false,
      openingDays: [2, 3, 5],
      policies: [policyArray[0], policyArray[2], policyArray[4]],
      needs: [],
      color: '#d32f6f'
    },
    {
      headers: {
        Authorization: `Token ${loginResponse.data.key}`
      }
    }
  );

  await http.post(
    'centres/',
    {
      name: 'Example Centre 6',
      openingDaysFirstSetupComplete: true,
      policiesFirstSetupComplete: false,
      setupComplete: false,
      needsFirstSetupComplete: false,
      openingDays: [1, 4, 6],
      policies: [policyArray[1], policyArray[3]],
      needs: []
    },
    {
      headers: {
        Authorization: `Token ${loginResponse.data.key}`
      }
    }
  );

  await http.post(
    'centres/',
    {
      name: 'Example Centre 7',
      openingDaysFirstSetupComplete: true,
      policiesFirstSetupComplete: true,
      needsFirstSetupComplete: false,
      setupComplete: false,
      openingDays: [1, 3, 4],
      policies: [],
      needs: []
    },
    {
      headers: {
        Authorization: `Token ${loginResponse.data.key}`
      }
    }
  );
};
export default createIncompleteCentres;
