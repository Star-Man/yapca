import { database } from '../globals';
import deleteClient from './deleteClient';
import type DbClient from '../types/DbClient';
import type { AxiosResponse } from 'axios';

const deleteAllClients = async (): Promise<AxiosResponse[]> => {
  const clients: DbClient[] = await database().select().from('client_client');
  const deletePromises: Promise<AxiosResponse>[] = [];
  clients.forEach(client => {
    deletePromises.push(deleteClient(client.surname));
  });
  return Promise.all(deletePromises);
};
export default deleteAllClients;
