import { database } from '../globals';
import type DbUser from '../types/DbUser';

const getUserByName = (username: string): Promise<DbUser[]> =>
  database()
    .select()
    .from('authentication_customuser')
    .leftJoin(
      'authentication_customuser_centres',
      'authentication_customuser.id',
      'authentication_customuser_centres.customuser_id'
    )
    .leftJoin(
      'centre_centre',
      'centre_centre.id',
      'authentication_customuser_centres.centre_id'
    )
    .leftJoin(
      'authentication_customuser_groups',
      'authentication_customuser.id',
      'authentication_customuser_groups.customuser_id'
    )
    .leftJoin(
      'auth_group',
      'auth_group.id',
      'authentication_customuser_groups.group_id'
    )
    .leftJoin(
      'authentication_customuser_permissions',
      'authentication_customuser_permissions.customuser_id',
      'authentication_customuser.id'
    )

    .leftJoin(
      'authentication_userpermission',
      'authentication_customuser_permissions.userpermission_id',
      'authentication_userpermission.id'
    )
    .where('username', username)
    .columns([
      'authentication_customuser.id AS id',
      'auth_group.name AS groupName',
      'centre_centre.id AS centreId',
      'centre_centre.name AS centreName',
      'authentication_customuser.is_active AS isActive',
      'authentication_customuser.darkTheme AS darkTheme',
      'authentication_customuser.accentColor AS accentColor',
      'authentication_customuser.displayName AS displayName',
      'authentication_customuser.userClientTableHiddenRows AS userClientTableHiddenRows',
      'authentication_customuser.otpDeviceKey AS otpDeviceKey',
      'authentication_customuser.email AS email',
      'authentication_customuser.username AS username',
      'authentication_customuser.password AS password',
      'authentication_userpermission.codename AS permission'
    ]);
export default getUserByName;
