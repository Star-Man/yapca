import axios from 'axios';

import * as dotenv from 'dotenv';
import updateCentre from './updateCentre';
import type LoginResponse from 'src/types/axios/LoginResponse';
import type PoliciesResponse from 'src/types/axios/PoliciesResponse';

dotenv.config({
  path: `../../../config/${process.env.YAPCA_ENV ?? 'local'}.env`
});
const http = axios.create({
  baseURL: `${process.env.VUE_APP_API_PROTOCOL ?? ''}://${
    process.env.VUE_APP_API_HOST ?? ''
  }:${process.env.VUE_APP_API_PORT ?? ''}${
    process.env.VUE_APP_API_PATH ?? ''
  }/api/`
});

const createExtraPolicies = async (): Promise<void> => {
  const loginResponse: LoginResponse = await http.post('login/', {
    username: 'testAdmin',
    password: 'testPassword1'
  });
  const policies: PoliciesResponse = await http.post(
    'policies/',
    [
      { description: 'Example Policy 101' },
      { description: 'Example Policy 102' },
      { description: 'Example Policy 103' }
    ],
    {
      headers: {
        Authorization: `Token ${loginResponse.data.key}`
      }
    }
  );
  const policyArray = policies.data.map((policy: { id: number }) => policy.id);

  await updateCentre('Example Centre', { policies: policyArray });
  await updateCentre('Example Centre 3', { policies: [policyArray[1]] });
};
export default createExtraPolicies;
