import { formatISO, startOfToday, startOfYesterday } from 'date-fns';
import { database } from '../globals';
import getClientByName from './getClientByName';
import getCentreByName from './getCentreByName';

const createCarePlans = async (): Promise<void> => {
  const client4 = await getClientByName('4');
  const client5 = await getClientByName('5');

  const [centre1] = await getCentreByName('Example Centre');
  const [centre3] = await getCentreByName('Example Centre 3');
  const [centre4] = await getCentreByName('Example Centre 4');

  const client4Need1Id = client4.find(
    (client: { need: string }) => client.need === 'Example Need 1'
  )!.clientNeedId;
  const client5Need2Id = client5.find(
    (client: { need: string }) => client.need === 'Example Need 2'
  )!.clientNeedId;
  const client4Need3Id = client4.find(
    (client: { need: string }) => client.need === 'Example Need 3'
  )!.clientNeedId;
  const client5Need4Id = client5.find(
    (client: { need: string }) => client.need === 'Example Need 4'
  )!.clientNeedId;
  const client4Centre1Need5Id = client4.find(
    (client: { need: string; centreId: number }) =>
      client.need === 'Example Need 5' && client.centreId === centre1.id
  )!.clientNeedId;
  const client5Centre3Need6Id = client5.find(
    (client: { need: string; centreId: number }) =>
      client.need === 'Example Need 6' && client.centreId === centre3.id
  )!.clientNeedId;
  const client5Centre4Need5Id = client5.find(
    (client: { need: string; centreId: number }) =>
      client.need === 'Example Need 5' && client.centreId === centre4.id
  )!.clientNeedId;

  return database()
    .table('client_careplan')
    .insert([
      {
        date: formatISO(startOfToday()),
        need_id: client4Need1Id,
        comment: '',
        completed: false,
        inputted: false
      },
      {
        date: formatISO(startOfToday()),
        need_id: client4Need3Id,
        comment: '',
        completed: false,
        inputted: false
      },
      {
        date: formatISO(startOfYesterday()),
        need_id: client4Centre1Need5Id,
        comment: '',
        completed: false,
        inputted: false
      },
      {
        date: formatISO(startOfYesterday()),
        need_id: client5Centre4Need5Id,
        comment: '',
        completed: false,
        inputted: false
      },
      {
        date: '2019-11-20',
        need_id: client4Need1Id,
        comment: '',
        completed: false,
        inputted: false
      },
      {
        date: '2019-11-20',
        need_id: client4Need3Id,
        comment: '',
        completed: false,
        inputted: false
      },
      {
        date: '2019-11-18',
        need_id: client5Need2Id,
        comment: '',
        completed: false,
        inputted: false
      },
      {
        date: '2019-11-18',
        need_id: client5Need4Id,
        comment: '',
        completed: false,
        inputted: false
      },
      {
        date: '2019-11-18',
        need_id: client5Centre3Need6Id,
        comment: '',
        completed: false,
        inputted: false
      },
      {
        date: '2019-11-18',
        need_id: client5Centre4Need5Id,
        comment: '',
        completed: false,
        inputted: false
      },
      {
        date: '2018-11-18',
        need_id: client5Centre4Need5Id,
        comment: '',
        completed: false,
        inputted: false
      }
    ]);
};
export default createCarePlans;
