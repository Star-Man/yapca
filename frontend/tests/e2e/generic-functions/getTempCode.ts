import { database } from '../globals';
import type DbTempCode from '../types/DbTempCode';

const getTempCode = (code: string): Promise<DbTempCode[]> =>
  database().select().from('authentication_tempcode').where('code', code);
export default getTempCode;
