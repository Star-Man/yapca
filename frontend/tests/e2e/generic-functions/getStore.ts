import { ClientFunction } from 'testcafe';
import type ClientApp from '../types/ClientApp';

const getStore = ClientFunction(() => {
  const app = document.querySelector('#q-app') as unknown as ClientApp;
  return app.__vue_app__!.config.globalProperties.$pinia.state.value;
});

export default getStore;
