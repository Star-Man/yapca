import { database } from '../globals';
import type DbAbsenceRecord from '../types/DbAbsenceRecord';

const getAbsenceRecords = (): Promise<DbAbsenceRecord[]> =>
  database().select().from('client_attendancerecord');
export default getAbsenceRecords;
