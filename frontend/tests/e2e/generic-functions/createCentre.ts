import { database } from '../globals';

const createCentre = () =>
  database().table('centre_centre').insert({
    name: 'Example Centre',
    openingDaysFirstSetupComplete: false,
    closingDaysFirstSetupComplete: false,
    policiesFirstSetupComplete: false,
    needsFirstSetupComplete: false,
    setupComplete: false,
    color: '#D32F2F',
    created: '2020-02-26 21:17:56',
    closedOnPublicHolidays: true,
    country: 'IE',
    state: ''
  });

export default createCentre;
