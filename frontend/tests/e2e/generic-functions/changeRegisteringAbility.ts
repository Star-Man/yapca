import { database } from '../globals';

const changeRegisteringAbility = (canRegister: boolean): Promise<void> =>
  database().table('administration_registerability').update({
    canRegister
  });

export default changeRegisteringAbility;
