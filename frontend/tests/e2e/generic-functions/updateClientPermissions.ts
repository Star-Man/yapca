import axios from 'axios';
import * as dotenv from 'dotenv';
import loginRequest from './loginRequest';
import getClientByName from './getClientByName';
import type LoginResponse from 'src/types/axios/LoginResponse';
import type { AxiosResponse } from 'axios';

dotenv.config({
  path: `../../../config/${process.env.YAPCA_ENV ?? 'local'}.env`
});

const http = axios.create({
  baseURL: `${process.env.VUE_APP_API_PROTOCOL ?? ''}://${
    process.env.VUE_APP_API_HOST ?? ''
  }:${process.env.VUE_APP_API_PORT ?? ''}${
    process.env.VUE_APP_API_PATH ?? ''
  }/api/`
});

const updateClientPermissions = async (
  surname: string,
  permissions: string[]
): Promise<AxiosResponse> => {
  const loginResponse: LoginResponse = await loginRequest(
    'testAdmin',
    'testPassword1'
  );
  const clientId = (await getClientByName(surname))[0].id;
  return http.post(
    `clients/update-permissions/${clientId}/`,
    {
      permissions
    },
    {
      headers: {
        Authorization: `Token ${loginResponse.data.key}`
      }
    }
  );
};

export default updateClientPermissions;
