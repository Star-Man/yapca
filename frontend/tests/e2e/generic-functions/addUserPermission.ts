import { database } from '../globals';
import type { Knex } from 'knex';

const addUserPermission = async (
  username: string,
  permissionCode: string
): Promise<Knex.QueryBuilder> => {
  const user: { id: number } = (
    await database()
      .select()
      .from('authentication_customuser')
      .where('username', username)
      .columns(['authentication_customuser.id'])
  )[0] as { id: number };
  const permission = (
    await database()
      .select()
      .from('authentication_userpermission')
      .where('codename', permissionCode)
      .columns(['authentication_userpermission.id'])
  )[0] as { id: number };
  return database()
    .table('authentication_customuser_permissions')
    .insert([{ customuser_id: user.id, userpermission_id: permission.id }]);
};

export default addUserPermission;
