import { database } from '../globals';
import type { Knex } from 'knex';

const removeUserPermission = (
  username: string,
  permissionCode: string
): Knex.QueryBuilder =>
  database()
    .where('authentication_customuser.username', '=', username)
    .where('authentication_userpermission.codename', '=', permissionCode)
    .leftJoin(
      'authentication_customuser',
      'authentication_customuser_permissions.customuser_id',
      'authentication_customuser.id'
    )
    .leftJoin(
      'authentication_userpermission',
      'authentication_customuser_permissions.userpermission_id',
      'authentication_userpermission.id'
    )
    .table('authentication_customuser_permissions')
    .del();
export default removeUserPermission;
