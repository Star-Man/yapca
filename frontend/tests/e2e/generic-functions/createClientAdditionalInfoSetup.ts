import axios from 'axios';

import * as dotenv from 'dotenv';
import getCentreByName from './getCentreByName';
import getUserByName from './getUserByName';
import type LoginResponse from 'src/types/axios/LoginResponse';

dotenv.config({
  path: `../../../config/${process.env.YAPCA_ENV ?? 'local'}.env`
});
const http = axios.create({
  baseURL: `${process.env.VUE_APP_API_PROTOCOL ?? ''}://${
    process.env.VUE_APP_API_HOST ?? ''
  }:${process.env.VUE_APP_API_PORT ?? ''}${
    process.env.VUE_APP_API_PATH ?? ''
  }/api/`
});

const createClientAdditionalInfoSetup = async (): Promise<void> => {
  const loginResponse: LoginResponse = await http.post('login/', {
    username: 'testAdmin',
    password: 'testPassword1'
  });

  const centre1 = await getCentreByName('Example Centre');
  const admin = await getUserByName('testAdmin');
  await http.post(
    'clients/',
    {
      admittedBy: admin[0].id,
      firstName: 'Test Client',
      surname: '11',
      address: 'Example Address 11',
      phoneNumber: '1111111',
      dateOfBirth: '1967-02-11',
      centres: [centre1[0].id],
      isActive: true,
      basicInfoSetup: true,
      additionalInfoSetup: true,
      setupComplete: false
    },
    {
      headers: {
        Authorization: `Token ${loginResponse.data.key}`
      }
    }
  );
};
export default createClientAdditionalInfoSetup;
