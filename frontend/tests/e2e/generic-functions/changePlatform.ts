import { ClientFunction } from 'testcafe';
import type ClientApp from '../types/ClientApp';

const changePlatform = ClientFunction((newPlatform: string) => {
  const app = document.querySelector('#q-app') as unknown as ClientApp;
  const store = app.__vue_app__!.config.globalProperties.$pinia.state.value;
  const platform = store.basic.platform;
  platform.win = newPlatform === 'windows';
  platform.linux = newPlatform === 'linux';
});

export default changePlatform;
