import createTempCode from './createTempCode';

const createRegistrationTempCode = (
  code: string,
  clientId: number | null = null,
  permissions: string[] | null = null
) => {
  let registrationType = 'registration';
  if (clientId) {
    registrationType = 'client-registration';
  }
  return createTempCode(
    new Date(Date.now() + 5 * 60000),
    registrationType,
    code,
    null,
    'newuseremail@test.com',
    clientId,
    permissions
  );
};

export default createRegistrationTempCode;
