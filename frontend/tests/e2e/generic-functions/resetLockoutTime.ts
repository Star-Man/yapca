import { ClientFunction } from 'testcafe';
import type ClientApp from '../types/ClientApp';

const resetLockoutTime = ClientFunction(() => {
  const app = document.querySelector('#q-app') as unknown as ClientApp;
  const store = app.__vue_app__!.config.globalProperties.$pinia.state.value;
  store.user.lockoutEndTime = new Date(2020);
});

export default resetLockoutTime;
