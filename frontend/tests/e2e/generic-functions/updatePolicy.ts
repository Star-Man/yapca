import axios from 'axios';

import * as dotenv from 'dotenv';
import getPolicyByDescription from './getPolicyByDescription';
import type { AxiosResponse } from 'axios';
import type LoginResponse from 'src/types/axios/LoginResponse';

dotenv.config({
  path: `../../../config/${process.env.YAPCA_ENV ?? 'local'}.env`
});
const http = axios.create({
  baseURL: `${process.env.VUE_APP_API_PROTOCOL ?? ''}://${
    process.env.VUE_APP_API_HOST ?? ''
  }:${process.env.VUE_APP_API_PORT ?? ''}${
    process.env.VUE_APP_API_PATH ?? ''
  }/api/`
});

const updatePolicy = (
  policyDescription: string,
  newPolicyDescription: string
): Promise<AxiosResponse> =>
  getPolicyByDescription(policyDescription).then((policy: { id: number }[]) =>
    http
      .post('login/', {
        username: 'testAdmin',
        password: 'testPassword1'
      })
      .then((response: LoginResponse) =>
        http.patch(
          `policies/${policy[0].id}/update/`,
          { description: newPolicyDescription },
          {
            headers: {
              Authorization: `Token ${response.data.key}`
            }
          }
        )
      )
  );

export default updatePolicy;
