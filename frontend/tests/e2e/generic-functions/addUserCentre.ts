import axios from 'axios';
import * as dotenv from 'dotenv';
import getCentreByName from './getCentreByName';
import getUserByName from './getUserByName';
import loginRequest from './loginRequest';
import type LoginResponse from 'src/types/axios/LoginResponse';
import type { AxiosResponse } from 'axios';

dotenv.config({
  path: `../../../config/${process.env.YAPCA_ENV ?? 'local'}.env`
});

const http = axios.create({
  baseURL: `${process.env.VUE_APP_API_PROTOCOL ?? ''}://${
    process.env.VUE_APP_API_HOST ?? ''
  }:${process.env.VUE_APP_API_PORT ?? ''}${
    process.env.VUE_APP_API_PATH ?? ''
  }/api/`
});

const addUserCentre = async (
  username: string,
  centreName: string
): Promise<AxiosResponse> => {
  const [user] = await getUserByName(username);
  const [centre] = await getCentreByName(centreName);
  const loginResponse: LoginResponse = await loginRequest(
    'testAdmin',
    'testPassword1'
  );
  return http.post(
    `users/centres/update/`,
    {
      user: user.id,
      centre: centre.id
    },
    {
      headers: {
        Authorization: `Token ${loginResponse.data.key}`
      }
    }
  );
};

export default addUserCentre;
