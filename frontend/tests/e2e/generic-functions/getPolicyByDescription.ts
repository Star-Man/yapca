import { database } from '../globals';
import type Policy from 'src/types/Policy';

const getPolicyByDescription = (policyDescription: string): Promise<Policy[]> =>
  database()
    .select()
    .from('centre_policy')
    .where('description', policyDescription);
export default getPolicyByDescription;
