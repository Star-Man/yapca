import axios from 'axios';
import * as dotenv from 'dotenv';
import loginRequest from './loginRequest';
import getClientByName from './getClientByName';
import type LoginResponse from 'src/types/axios/LoginResponse';
import type { AxiosResponse } from 'axios';

dotenv.config({
  path: `../../../config/${process.env.YAPCA_ENV ?? 'local'}.env`
});

const http = axios.create({
  baseURL: `${process.env.VUE_APP_API_PROTOCOL ?? ''}://${
    process.env.VUE_APP_API_HOST ?? ''
  }:${process.env.VUE_APP_API_PORT ?? ''}${
    process.env.VUE_APP_API_PATH ?? ''
  }/api/`
});

const setClientUserActivation = async (
  surname: string,
  isActive: boolean
): Promise<AxiosResponse> => {
  const loginResponse: LoginResponse = await loginRequest(
    'testAdmin',
    'testPassword1'
  );
  const clientId = (await getClientByName(surname))[0].id;
  return http.post(
    `set-client-user-activation/${clientId}/`,
    {
      active: isActive,
      sendEmail: false
    },
    {
      headers: {
        Authorization: `Token ${loginResponse.data.key}`
      }
    }
  );
};

export default setClientUserActivation;
