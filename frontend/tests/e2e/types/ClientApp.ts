import type VueApp from './VueApp';

export default interface ClientApp {
  __vue_app__?: VueApp;
}
