export default interface BasicStore {
  clientVersion: string;
  clientType: string;
  backendPath: string;
  token: string;
  appServerAddress: string;
  platform: {
    electron: boolean;
    desktop: boolean;
    mobile: boolean;
    linux: boolean;
    win: boolean;
  };
  backendDataLoaded: boolean;
}
