export default interface DbCentre {
  id: number;
  name: string;
  country: string;
  state: string;
  closedOnPublicHolidays: boolean;
  dayId: number;
  policyDescription: string;
  needDescription: string;
  color: string;
}
