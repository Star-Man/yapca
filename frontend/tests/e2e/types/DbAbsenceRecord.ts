export default interface DbAbsenceRecord {
  id: number;
  date: Date;
  active: boolean;
  reasonForInactivity: string;
  reasonForAbsence: string;
  addedBy_id: number;
  centre_id: number;
  client_id: number;
}
