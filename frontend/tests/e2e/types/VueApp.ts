import type GlobalProperties from './GlobalProperties';

export default interface VueApp {
  config: {
    globalProperties: GlobalProperties;
  };
}
