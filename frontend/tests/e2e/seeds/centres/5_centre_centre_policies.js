exports.seed = knex =>
  knex('centre_centre_policies').insert([
    {
      centre_id: 1,
      policy_id: 5
    },
    {
      centre_id: 1,
      policy_id: 1
    },
    {
      centre_id: 1,
      policy_id: 3
    },
    {
      centre_id: 2,
      policy_id: 4
    },
    {
      centre_id: 2,
      policy_id: 2
    },
    {
      centre_id: 3,
      policy_id: 6
    },
    {
      centre_id: 4,
      policy_id: 5
    }
  ]);
