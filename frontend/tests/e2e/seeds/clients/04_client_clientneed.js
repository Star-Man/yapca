exports.seed = knex =>
  knex('client_clientneed').insert([
    {
      plan: '',
      centre_id: 1,
      client_id: 1,
      need_id: 1,
      isActive: true
    },
    {
      plan: 'Example Plan 1',
      centre_id: 1,
      client_id: 4,
      need_id: 1,
      isActive: true
    },
    {
      plan: 'Example Plan 3',
      centre_id: 1,
      client_id: 4,
      need_id: 3,
      isActive: true
    },
    {
      plan: '',
      centre_id: 1,
      client_id: 4,
      need_id: 5,
      isActive: true
    },
    {
      plan: 'Example Plan 2',
      centre_id: 2,
      client_id: 5,
      need_id: 2,
      isActive: true
    },
    {
      plan: 'Example Plan 5',
      centre_id: 4,
      client_id: 5,
      need_id: 5,
      isActive: true
    },
    {
      plan: 'Example Plan 4',
      centre_id: 2,
      client_id: 5,
      need_id: 4,
      isActive: true
    },
    {
      plan: 'Client 5 Centre 3 Need 6 Example Plan',
      centre_id: 3,
      client_id: 5,
      need_id: 6,
      isActive: true
    }
  ]);
