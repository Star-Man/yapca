const hashedPassword =
  'pbkdf2_sha256$120000$ug2WOCLCuKse$1TINlkBwEfviiHfvgcXes1zGK5O+5YBpe0+8dXWJpeA=';

exports.seed = async knex => {
  const [nurseGroup] = await knex
    .select('id')
    .from('auth_group')
    .where({ name: 'nurse' });
  const [adminGroup] = await knex
    .select('id')
    .from('auth_group')
    .where({ name: 'admin' });
  return knex('authentication_customuser')
    .insert(
      [
        {
          username: 'testAdmin',
          displayName: 'Test Admin',
          password: hashedPassword,
          accentColor: '#D32F2F',
          is_superuser: false,
          first_name: '',
          last_name: '',
          is_staff: false,
          is_active: true,
          userClientTableHiddenRows: '',
          date_joined: '2020-02-26 21:17:56',
          darkTheme: false,
          otpDeviceKey: ''
        },
        {
          username: 'testNurse',
          displayName: 'Test Nurse',
          password: hashedPassword,
          accentColor: '#D32F2F',
          is_superuser: false,
          first_name: '',
          last_name: '',
          is_staff: false,
          userClientTableHiddenRows: '',
          is_active: true,
          date_joined: '2020-02-26 21:17:56',
          darkTheme: false,
          otpDeviceKey: '',
          email: 'testemail@test.com'
        },
        {
          username: 'testOTPUser',
          displayName: 'Test OTP User',
          password: hashedPassword,
          accentColor: '#D32F2F',
          is_superuser: false,
          first_name: '',
          last_name: '',
          is_staff: false,
          userClientTableHiddenRows: '',
          is_active: true,
          date_joined: '2021-02-26 21:17:56',
          darkTheme: false,
          otpDeviceKey: 'JBSWY3DPEHPK3PXP'
        },
        {
          username: 'everyCentreUser',
          displayName: 'Every Centre User',
          password: hashedPassword,
          accentColor: '#D32F2F',
          is_superuser: false,
          first_name: '',
          last_name: '',
          is_staff: false,
          userClientTableHiddenRows: '',
          is_active: true,
          date_joined: '2021-02-26 21:17:56',
          darkTheme: false,
          otpDeviceKey: ''
        }
      ],
      ['id']
    )
    .then(ids =>
      knex('authentication_customuser_groups').insert([
        {
          customuser_id: ids[0].id,
          group_id: adminGroup.id
        },
        {
          customuser_id: ids[1].id,
          group_id: nurseGroup.id
        },
        {
          customuser_id: ids[2].id,
          group_id: nurseGroup.id
        },
        {
          customuser_id: ids[3].id,
          group_id: nurseGroup.id
        }
      ])
    );
};
