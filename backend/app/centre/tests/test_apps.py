"""apps.py Tests."""

from django.apps import apps
from django.test import TestCase

from ..apps import CentreAppConfig


class CentreAppConfigTestCase(TestCase):
    """Tests for the centre app configuration."""

    def test_name(self):
        """Tests name is correct."""
        self.assertEqual(CentreAppConfig.name, "app.centre")
        self.assertEqual(apps.get_app_config("centre").name, "app.centre")
