"""Profile Model Tests."""

from django.core.exceptions import ValidationError
from django.test import TestCase

from ..models import Centre, Day, Need, Policy


class DayTestCase(TestCase):
    """Tests for the Day model."""

    def test_day_is_limited_to_enum(self):
        """Day object should be limited to enum options."""
        test_day = Day.objects.create(day="example")
        self.assertRaises(ValidationError, test_day.full_clean)

    def test_days_created_by_default(self):
        """Days should automatically be created."""
        self.assertEqual(Day.objects.count(), 7)
        self.assertTrue(Day.objects.filter(day="Monday").exists())
        self.assertTrue(Day.objects.filter(day="Tuesday").exists())
        self.assertTrue(Day.objects.filter(day="Wednesday").exists())
        self.assertTrue(Day.objects.filter(day="Thursday").exists())
        self.assertTrue(Day.objects.filter(day="Friday").exists())
        self.assertTrue(Day.objects.filter(day="Saturday").exists())
        self.assertTrue(Day.objects.filter(day="Sunday").exists())


class CentreTestCase(TestCase):
    """Tests for the Centre model."""

    description = "Example Description"

    def test_normal(self):
        """Test normally creating a centre."""
        Policy.objects.bulk_create(
            [
                Policy(description=self.description),
                Policy(description=self.description),
                Policy(description=self.description),
                Policy(description=self.description),
            ]
        )
        Need.objects.bulk_create(
            [
                Need(description=self.description),
                Need(description=self.description),
                Need(description=self.description),
                Need(description=self.description),
            ]
        )
        test_centre = Centre.objects.create(country="US", state="TN", color="F")
        test_centre.save()
        test_centre.policies.add(1, 3)
        test_centre.needs.add(1, 2, 3)
        test_centre.openingDays.add(1, 2, 5, 6)
        self.assertEqual(test_centre.openingDays.count(), 4)
        self.assertEqual(test_centre.policies.count(), 2)
        self.assertEqual(test_centre.needs.count(), 3)
        self.assertTrue(test_centre.openingDays.filter(day="Saturday").exists())
        self.assertTrue(
            test_centre.policies.filter(description=self.description).exists()
        )
        self.assertTrue(test_centre.needs.filter(description=self.description).exists())
        self.assertTrue(test_centre.closedOnPublicHolidays)
        self.assertEqual(test_centre.country, "US")
        self.assertEqual(test_centre.state, "TN")

    def test_centre_name_unique(self):
        """Should fail to create centre with same name."""
        Centre.objects.create(name="Test Centre")
        test_centre = Centre(name="Test Centre")
        self.assertRaises(ValidationError, test_centre.full_clean)
