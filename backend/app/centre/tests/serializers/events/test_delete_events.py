"""Delete Events Tests."""

import datetime
from datetime import timedelta

import pytz
import recurrence
from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_test_user
from ....models import Centre, Event
from ...utils import create_centre

REGISTER_URL = "/api/registration/"
TOKEN_AUTH = "Token "
NO_AUTH_MESSAGE = "Authentication credentials were not provided."
URL = "/api/events/"
RECURRENCE_WEEKLY = recurrence.Rule(recurrence.WEEKLY)
RECURRENCE_YEARLY = recurrence.Rule(recurrence.YEARLY)
EVENT_1 = "event 1"
EVENT_2 = "event 2"


class DeleteEventsTestCase(TestCase):
    """Delete Events Tests."""

    def create_events(self):
        """Create events for test."""
        centre_1_close_event = Event.objects.create(
            name=EVENT_1,
            centre=self.centres["centre_1"],
            start=datetime.datetime(2020, 1, 3, 16, 30, 00, tzinfo=pytz.UTC),
            timePeriod=timedelta(minutes=15),
            recurrences=recurrence.Recurrence(
                rrules=[RECURRENCE_WEEKLY],
            ),
            createdBy=self.users["user_1"],
            eventType="User Close Event",
        )
        centre_1_normal_event = Event.objects.create(
            name=EVENT_2,
            centre=self.centres["centre_1"],
            start=datetime.datetime(2019, 1, 1, 16, 30, 00, tzinfo=pytz.UTC),
            timePeriod=timedelta(minutes=30),
            recurrences=recurrence.Recurrence(
                rrules=[RECURRENCE_YEARLY],
            ),
            createdBy=self.users["user_1"],
            eventType="On-Site",
        )
        centre_2_normal_event = Event.objects.create(
            name="event 3",
            centre=self.centres["centre_2"],
            start=datetime.datetime(2020, 1, 6, 16, 30, 00, tzinfo=pytz.UTC),
            timePeriod=timedelta(days=3),
            createdBy=self.users["user_1"],
            eventType="On-Site",
        )
        centre_2_close_event = Event.objects.create(
            name="event 4",
            centre=self.centres["centre_2"],
            start=datetime.datetime(2020, 1, 6, 11, 15, 00, tzinfo=pytz.UTC),
            timePeriod=timedelta(days=56),
            createdBy=self.users["user_1"],
            eventType="Online",
        )
        public_holiday = Event.objects.create(
            name="event 5",
            centre=self.centres["centre_1"],
            start=datetime.datetime(2010, 1, 6, 11, 15, 00, tzinfo=pytz.UTC),
            timePeriod=timedelta(days=56),
            createdBy=self.users["user_1"],
            eventType="Public Holiday",
        )
        return {
            "centre_1_normal_event": centre_1_normal_event,
            "centre_2_normal_event": centre_2_normal_event,
            "centre_1_close_event": centre_1_close_event,
            "centre_2_close_event": centre_2_close_event,
            "public_holiday": public_holiday,
        }

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        nurse_token = register_test_user(
            self.client, username="user2", email="user2@test.test"
        )
        self.users = {
            "user_1": CustomUser.objects.get(id=token.data["user"]["id"]),
            "nurse": CustomUser.objects.get(id=nurse_token.data["user"]["id"]),
            "nurse_token": nurse_token,
        }
        self.centres = {
            "centre_1": create_centre(
                users=[self.users["user_1"], self.users["nurse"]]
            ),
            "centre_2": Centre.objects.create(name="centre 2"),
        }
        self.client.credentials(HTTP_AUTHORIZATION=TOKEN_AUTH + token.data["key"])
        self.events = self.create_events()

    def test_no_auth_delete(self):
        """No results should return when authentication isn't provided."""
        self.client.credentials()
        response = self.client.delete(f"{URL}{self.events['centre_2_close_event'].id}/")
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string=NO_AUTH_MESSAGE,
                    code="not_authenticated",
                )
            },
        )

    def test_invalid_id(self):
        """Should return error when event doesn't exist."""
        response = self.client.delete(f"{URL}999/")
        self.assertEqual(response.status_code, 404)
        self.assertEqual(
            response.data,
            {"detail": ErrorDetail(string="Not found.", code="not_found")},
        )

    def test_delete(self):
        """Should delete close event."""
        response = self.client.delete(f"{URL}{self.events['centre_2_close_event'].id}/")
        self.assertEqual(response.status_code, 204)
        self.assertEqual(Event.objects.count(), 4)

    def test_invalid_nurse_delete_close_event(self):
        """Should not be able to delete close events as nurse."""
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + self.users["nurse_token"].data["key"]
        )
        response = self.client.delete(f"{URL}{self.events['centre_1_close_event'].id}/")
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )

    def test_valid_nurse_delete(self):
        """Can delete regular events as nurse."""
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + self.users["nurse_token"].data["key"]
        )
        response = self.client.delete(
            f"{URL}{self.events['centre_1_normal_event'].id}/"
        )
        self.assertEqual(response.status_code, 204)

    def test_invalid_nurse_delete_normal_event(self):
        """Can delete regular events as nurse."""
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + self.users["nurse_token"].data["key"]
        )
        response = self.client.delete(
            f"{URL}{self.events['centre_2_normal_event'].id}/"
        )
        self.assertEqual(response.status_code, 403)

    def test_invalid_delete_public_holiday(self):
        """Cannot delete public holidays."""
        response = self.client.delete(f"{URL}{self.events['public_holiday'].id}/")
        self.assertEqual(response.status_code, 403)
