"""Policy Serializer Tests."""

import json

from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from ....authentication.tests.utils import register_test_user
from ...models import Policy
from ..utils import setup_needs_policies

REGISTER_URL = "/api/registration/"
TOKEN_AUTH = "Token "
NEW_DESCRIPTION = "New Description"
NO_AUTH_MESSAGE = "Authentication credentials were not provided."
POLICY_DESCRIPTION = "Example Description"
DISPLAY_NAME_2 = "Test User 2"
CONTENT_TYPE = "application/json"
EMAIL = "test@test.com"
EMAIL_2 = "test2@test.com"


class PolicySerializerTestCase(TestCase):
    """Tests for PolicySerializer."""

    post_url = "/api/policies/"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        (
            self.nurse_token,
            self.centre,
            self.centre_2,
        ) = setup_needs_policies(self.client)
        self.get_url = f"/api/centres/{self.centre.id}/policies/"

    def test_no_auth_get(self):
        """No results should return when authentication isn't provided."""
        self.client.credentials()
        response = self.client.get(
            self.post_url,
            {},
        )
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string=NO_AUTH_MESSAGE,
                    code="not_authenticated",
                )
            },
        )

    def test_valid_nurse_post(self):
        """Should be able to post data when nurse."""
        self.client = APIClient()
        token = register_test_user(
            self.client,
            username="testUser2",
            email=EMAIL_2,
            display_name=DISPLAY_NAME_2,
        )
        self.client.credentials(HTTP_AUTHORIZATION=TOKEN_AUTH + token.data["key"])
        data = [
            {"description": "Test Nurse Policy 1"},
            {"description": "Test Nurse Policy 2"},
        ]
        response = self.client.post(
            self.post_url, json.dumps(data), content_type=CONTENT_TYPE
        )

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data[0]["description"], "Test Nurse Policy 1")
        self.assertEqual(response.data[1]["description"], "Test Nurse Policy 2")

    def test_valid_get(self):
        """Test sending a valid get request to the serializer."""
        policy1 = Policy.objects.create(description=POLICY_DESCRIPTION)
        policy2 = Policy.objects.create(description=POLICY_DESCRIPTION)
        self.centre.policies.add(policy1.id, policy2.id)
        response = self.client.get(self.get_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data[0]["description"], POLICY_DESCRIPTION)
        self.assertEqual(response.data[1]["description"], POLICY_DESCRIPTION)

    def test_valid_post(self):
        """Test sending a valid post request to the serializer."""
        data = [{"description": "Test Policy 1"}, {"description": "Test Policy 2"}]
        response = self.client.post(
            self.post_url, json.dumps(data), content_type=CONTENT_TYPE
        )

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data[0]["description"], "Test Policy 1")
        self.assertEqual(response.data[1]["description"], "Test Policy 2")

    def test_missing_data(self):
        """Test missing data being sent to serializer."""
        data = [{}]
        response = self.client.post(
            self.post_url, json.dumps(data), content_type=CONTENT_TYPE
        )
        self.assertEqual(response.status_code, 400)
        error = "This field is required."
        self.assertEqual(
            response.data[0],
            {"description": [ErrorDetail(string=error, code="required")]},
        )

    def test_no_auth_put(self):
        """Should be unable to put data when not authenticated."""
        test_policy = Policy.objects.create(description="test")
        test_policy.save()
        self.client.credentials()
        response = self.client.put(
            f"{self.post_url}{test_policy.id}/update/",
            {},
        )
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string=NO_AUTH_MESSAGE,
                    code="not_authenticated",
                )
            },
        )

    def test_valid_nurse_put(self):
        """Should be able to put data when nurse group."""
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + self.nurse_token.data["key"]
        )
        test_policy = Policy.objects.create(description="test")
        test_policy.save()
        self.centre.policies.add(test_policy)
        response = self.client.put(
            f"{self.post_url}{test_policy.id}/update/",
            {"description": NEW_DESCRIPTION},
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["description"], NEW_DESCRIPTION)
        updated_test_policy = Policy.objects.get(id=test_policy.id)
        self.assertEqual(updated_test_policy.description, NEW_DESCRIPTION)

    def test_valid_update(self):
        """Test sending a valid put request to the serializer."""
        test_policy = Policy.objects.create(description="test")
        test_policy.save()
        self.centre.policies.add(test_policy)
        response = self.client.put(
            f"{self.post_url}{test_policy.id}/update/",
            {"description": NEW_DESCRIPTION},
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["description"], NEW_DESCRIPTION)
        updated_test_policy = Policy.objects.get(id=test_policy.id)
        self.assertEqual(updated_test_policy.description, NEW_DESCRIPTION)

    def test_invalid_update_no_centre(self):
        """Cannot update policy that is not part of subbed centres."""
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + self.nurse_token.data["key"]
        )
        test_policy = Policy.objects.create(description="test")
        test_policy.save()
        self.centre_2.policies.add(test_policy)
        response = self.client.put(
            f"{self.post_url}{test_policy.id}/update/",
            {"description": NEW_DESCRIPTION},
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )
