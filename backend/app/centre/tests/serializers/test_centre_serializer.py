"""Centre Serializer Tests."""

from collections import OrderedDict

from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from ....authentication.models import CustomUser
from ....authentication.tests.utils import register_client_user, register_test_user
from ...models import Centre, Policy
from ..utils import create_centre

REGISTER_URL = "/api/registration/"
TOKEN_AUTH = "Token "
NO_AUTH_MESSAGE = "Authentication credentials were not provided."
POLICY_DESCRIPTION = "Example Description"
NO_PERMISSION = "You do not have permission to perform this action."
DISPLAY_NAME_2 = "Test User 2"
EMAIL = "test@test.com"
EMAIL_2 = "test2@test.com"


def create_centres(user):
    """Create multiple centres for testing."""
    test_policy = Policy.objects.create(description=POLICY_DESCRIPTION)
    test_centre = create_centre(users=[user])
    create_centre(name="Test Centre 2")
    test_centre.policies.add(test_policy.id)
    test_centre.openingDays.add(1, 2, 5, 6)
    return test_policy, test_centre


class CentreSerializerTestCase(TestCase):
    """Tests for CentreSerializer."""

    url = "/api/centres/"
    centreName = "Test Centre"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        admin_token = register_test_user(self.client)
        self.nurse_token = register_test_user(
            self.client, username="nurse1", email="nurseemail@test.test"
        )
        self.client_token = register_client_user(self.client)
        self.admin_user = CustomUser.objects.get(id=admin_token.data["user"]["id"])
        self.nurse_user = CustomUser.objects.get(id=self.nurse_token.data["user"]["id"])
        self.client_user = CustomUser.objects.get(
            id=self.client_token.data["user"]["id"]
        )
        self.client.credentials(HTTP_AUTHORIZATION=TOKEN_AUTH + admin_token.data["key"])

    def test_no_auth_get(self):
        """No results should return when authentication isn't provided."""
        self.client.credentials()
        response = self.client.get(
            self.url,
            {},
        )
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string=NO_AUTH_MESSAGE,
                    code="not_authenticated",
                )
            },
        )

    def test_no_auth_put(self):
        """Should be unable to put data when not authenticated."""
        test_centre = Centre.objects.create()
        test_centre.save()
        self.client.credentials()
        response = self.client.put(
            f"{self.url}{test_centre.id}/update/",
            {},
        )
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string=NO_AUTH_MESSAGE,
                    code="not_authenticated",
                )
            },
        )

    def test_valid_nurse_put(self):
        """Should be able to put policies data when nurse."""
        self.client = APIClient()
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + self.nurse_token.data["key"]
        )
        test_policy = Policy.objects.create(description=POLICY_DESCRIPTION)
        test_centre = create_centre(users=[self.nurse_user])
        response = self.client.put(
            f"{self.url}{test_centre.id}/update/",
            {"policies": [test_policy.id], "color": "#FCFCFC"},
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["policies"][0], test_policy.id)
        updated_test_centre = Centre.objects.get(id=test_centre.id)
        self.assertTrue(
            updated_test_centre.policies.filter(description=POLICY_DESCRIPTION).exists()
        )

    def test_invalid_nurse_put(self):
        """Should not be able to put opening days data when nurse."""
        self.client = APIClient()
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + self.nurse_token.data["key"]
        )
        test_centre = create_centre(users=[self.nurse_user])
        response = self.client.put(
            f"{self.url}{test_centre.id}/update/",
            {"openingDays": [1, 2, 3]},
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["openingDays"], [])
        updated_test_centre = Centre.objects.get(id=test_centre.id)
        self.assertEqual(len(updated_test_centre.openingDays.all()), 0)

    def test_invalid_nurse_centre(self):
        """Should not be able to put when not subbed to centre as nurse."""
        self.client = APIClient()
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + self.nurse_token.data["key"]
        )
        test_policy = Policy.objects.create(description=POLICY_DESCRIPTION)
        test_centre = create_centre(users=[])
        response = self.client.put(
            f"{self.url}{test_centre.id}/update/",
            {"policies": [test_policy.id], "color": "#FCFCFC"},
        )
        self.assertEqual(response.status_code, 403)

    def test_invalid_group_post(self):
        """Should be unable to post data when not an admin."""
        self.client = APIClient()
        token = register_test_user(
            self.client,
            username="testUser2",
            email=EMAIL_2,
            display_name=DISPLAY_NAME_2,
        )
        self.client.credentials(HTTP_AUTHORIZATION=TOKEN_AUTH + token.data["key"])
        response = self.client.post(
            self.url,
            {},
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string=NO_PERMISSION,
                    code="permission_denied",
                )
            },
        )

    def test_valid_get_admin(self):
        """
        Test sending a valid get request to the serializer as admin.

        Returns all centres for admin user.
        """
        test_policy, _ = create_centres(self.admin_user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            [
                OrderedDict(
                    [
                        ("id", response.data[0]["id"]),
                        ("name", "testCentre"),
                        ("openingDays", [1, 2, 5, 6]),
                        ("policies", [test_policy.id]),
                        ("needs", []),
                        ("policiesFirstSetupComplete", False),
                        ("needsFirstSetupComplete", False),
                        ("openingDaysFirstSetupComplete", False),
                        ("closingDaysFirstSetupComplete", False),
                        ("setupComplete", False),
                        ("color", ""),
                        ("created", response.data[0]["created"]),
                        ("closedOnPublicHolidays", True),
                        ("country", "IE"),
                        ("state", ""),
                    ]
                ),
                OrderedDict(
                    [
                        ("id", response.data[1]["id"]),
                        ("name", "Test Centre 2"),
                        ("openingDays", []),
                        ("policies", []),
                        ("needs", []),
                        ("policiesFirstSetupComplete", False),
                        ("needsFirstSetupComplete", False),
                        ("openingDaysFirstSetupComplete", False),
                        ("closingDaysFirstSetupComplete", False),
                        ("setupComplete", False),
                        ("color", ""),
                        ("created", response.data[1]["created"]),
                        ("closedOnPublicHolidays", True),
                        ("country", "IE"),
                        ("state", ""),
                    ]
                ),
            ],
        )

    def test_valid_get_nurse(self):
        """
        Test sending a valid get request to the serializer as nurse.

        Only the first centre will be returned since that's all the nurse
        user subs to.
        """
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + self.nurse_token.data["key"]
        )
        create_centres(self.nurse_user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]["name"], "testCentre")

    def test_valid_get_client_user(self):
        """
        Test sending a valid get request to the serializer as a client.

        """
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + self.client_token.data["key"]
        )
        _, test_centre = create_centres(self.nurse_user)
        self.client_user.client.centres.add(test_centre)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]["name"], "testCentre")

    def test_invalid_get_client_user(self):
        """
        Test sending an invalid get request to the serializer as a client.

        Should return 0 centres as client has no centres.
        """
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + self.client_token.data["key"]
        )
        create_centres(self.nurse_user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 0)

    def test_valid_post(self):
        """Test sending a valid post request to the serializer."""
        test_policy = Policy.objects.create(description=POLICY_DESCRIPTION)
        response = self.client.post(
            self.url,
            {
                "name": self.centreName,
                "openingDays": [1, 4, 5, 6],
                "policies": [test_policy.id],
                "color": "#FAFAFA",
            },
        )
        self.assertEqual(response.status_code, 201)
        self.assertEqual(
            response.data,
            {
                "id": response.data["id"],
                "name": self.centreName,
                "openingDays": [1, 4, 5, 6],
                "policies": [test_policy.id],
                "needs": [],
                "policiesFirstSetupComplete": False,
                "needsFirstSetupComplete": False,
                "openingDaysFirstSetupComplete": False,
                "closingDaysFirstSetupComplete": False,
                "setupComplete": False,
                "color": "#FAFAFA",
                "created": response.data["created"],
                "closedOnPublicHolidays": False,
                "country": "IE",
                "state": "",
            },
        )

    def test_invalid_country_update(self):
        """Test sending an invalid country to the serializer."""
        test_centre = Centre.objects.create()
        response = self.client.put(
            f"{self.url}{test_centre.id}/update/",
            {"name": self.centreName, "country": "XRZ"},
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {"country": [ErrorDetail(string="Country is invalid", code="invalid")]},
        )

    def test_invalid_state_update(self):
        """Test sending an invalid state to the serializer."""
        test_centre = Centre.objects.create(country="DK")
        response = self.client.put(
            f"{self.url}{test_centre.id}/update/",
            {"name": self.centreName, "state": "XRZ"},
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {"state": [ErrorDetail(string="State is invalid", code="invalid")]},
        )

    def test_valid_update(self):
        """Test sending a valid put request to the serializer."""
        new_name = "New Centre Name"
        test_policy = Policy.objects.create(description=POLICY_DESCRIPTION)
        test_centre = Centre.objects.create()
        test_centre.save()
        test_centre.policies.add(test_policy.id)
        test_centre.openingDays.add(1, 2, 5, 6)
        response = self.client.put(
            f"{self.url}{test_centre.id}/update/",
            {
                "name": new_name,
                "openingDays": [1, 4, 5, 6],
                "policies": [test_policy.id],
                "country": "AU",
                "state": "NT",
            },
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["name"], new_name)
        updated_test_centre = Centre.objects.get(id=test_centre.id)
        self.assertEqual(updated_test_centre.name, new_name)
        self.assertEqual(updated_test_centre.state, "NT")
        self.assertEqual(updated_test_centre.country, "AU")

    def test_missing_data(self):
        """Test missing data being sent to serializer."""
        response = self.client.post(
            self.url,
            {},
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {"name": [ErrorDetail(string="This field is required.", code="required")]},
        )

    def test_incorrect_policy(self):
        """Test creating a centre with a policy that doesn't exist."""
        response = self.client.post(
            self.url,
            {"name": self.centreName, "openingDays": [1, 4, 5, 6], "policies": [1]},
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "policies": [
                    ErrorDetail(
                        string='Invalid pk "1" - object does not exist.',
                        code="does_not_exist",
                    )
                ]
            },
        )

    def test_valid_delete(self):
        """Test sending a valid delete request to the serializer."""
        test_centre = Centre.objects.create()
        test_centre.save()
        response = self.client.delete(f"{self.url}{test_centre.id}/delete/")
        self.assertEqual(response.status_code, 204)

    def test_invalid_group_delete(self):
        """Should be unable to delete data when not an admin."""
        test_centre = Centre.objects.create()
        test_centre.save()
        self.client = APIClient()
        token = register_test_user(
            self.client,
            username="testUser2",
            email=EMAIL_2,
            display_name=DISPLAY_NAME_2,
        )
        self.client.credentials(HTTP_AUTHORIZATION=TOKEN_AUTH + token.data["key"])
        response = self.client.delete(f"{self.url}{test_centre.id}/delete/")
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string=NO_PERMISSION,
                    code="permission_denied",
                )
            },
        )

    def test_no_auth_delete(self):
        """Should be unable to delete data when not authenticated."""
        test_centre = Centre.objects.create()
        test_centre.save()
        self.client.credentials()
        response = self.client.delete(f"{self.url}{test_centre.id}/delete/")
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string=NO_AUTH_MESSAGE,
                    code="not_authenticated",
                )
            },
        )
