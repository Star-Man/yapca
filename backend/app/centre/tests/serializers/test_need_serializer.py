"""Need Serializer Tests."""

import json

from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from ....authentication.models import CustomUser
from ....authentication.tests.utils import register_client_user, register_test_user
from ...models import Need
from ..utils import setup_needs_policies

REGISTER_URL = "/api/registration/"
TOKEN_AUTH = "Token "
NO_AUTH_MESSAGE = "Authentication credentials were not provided."
NEED_DESCRIPTION = "Example Description"
DISPLAY_NAME_2 = "Test User 2"
CONTENT_TYPE = "application/json"
NEW_DESCRIPTION = "New Description"


class NeedSerializerTestCase(TestCase):
    """Tests for NeedSerializer."""

    post_url = "/api/needs/"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        (
            self.nurse_token,
            self.centre,
            self.centre_2,
        ) = setup_needs_policies(self.client)
        self.get_url = f"/api/centres/{self.centre.id}/needs/"

    def test_no_auth_get(self):
        """No results should return when authentication isn't provided."""
        self.client.credentials()
        response = self.client.get(
            self.get_url,
            {},
        )
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string=NO_AUTH_MESSAGE,
                    code="not_authenticated",
                )
            },
        )

    def test_valid_nurse_post(self):
        """Should be able to post data when nurse."""
        self.client = APIClient()
        token = register_test_user(
            self.client,
            username="testUser2",
            email="test2@test.com",
            display_name=DISPLAY_NAME_2,
        )
        self.client.credentials(HTTP_AUTHORIZATION=TOKEN_AUTH + token.data["key"])
        data = [
            {"description": "Test Nurse Need 1"},
            {"description": "Test Nurse Need 2"},
        ]
        response = self.client.post(
            self.post_url, json.dumps(data), content_type=CONTENT_TYPE
        )

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data[0]["description"], "Test Nurse Need 1")
        self.assertEqual(response.data[1]["description"], "Test Nurse Need 2")

    def test_valid_get(self):
        """Test sending a valid get request to the serializer."""
        need1 = Need.objects.create(description=NEED_DESCRIPTION)
        need2 = Need.objects.create(description=NEED_DESCRIPTION)
        self.centre.needs.add(need1.id, need2.id)
        response = self.client.get(self.get_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data[0]["description"], NEED_DESCRIPTION)
        self.assertEqual(response.data[1]["description"], NEED_DESCRIPTION)

    def test_valid_post(self):
        """Test sending a valid post request to the serializer."""
        data = [{"description": "Test Need 1"}, {"description": "Test Need 2"}]
        response = self.client.post(
            self.post_url, json.dumps(data), content_type=CONTENT_TYPE
        )

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data[0]["description"], "Test Need 1")
        self.assertEqual(response.data[1]["description"], "Test Need 2")

    def test_missing_data(self):
        """Test missing data being sent to serializer."""
        data = [{}]
        response = self.client.post(
            self.post_url, json.dumps(data), content_type=CONTENT_TYPE
        )
        self.assertEqual(response.status_code, 400)
        error = "This field is required."
        self.assertEqual(
            response.data[0],
            {"description": [ErrorDetail(string=error, code="required")]},
        )

    def test_invalid_update_no_centre(self):
        """Cannot update need that is not part of subbed centres."""
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + self.nurse_token.data["key"]
        )
        test_need = Need.objects.create(description="new_need")
        test_need.save()
        self.centre_2.needs.add(test_need)
        response = self.client.put(
            f"{self.post_url}{test_need.id}/update/",
            {"description": NEW_DESCRIPTION},
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )

    def test_no_auth_put(self):
        """Should be unable to put data when not authenticated."""
        test_need = Need.objects.create(description="test")
        test_need.save()
        self.client.credentials()
        response = self.client.put(
            f"{self.post_url}{test_need.id}/update/",
            {},
        )
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string=NO_AUTH_MESSAGE,
                    code="not_authenticated",
                )
            },
        )

    def test_valid_nurse_put(self):
        """Should be able to put data when nurse group."""
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + self.nurse_token.data["key"]
        )
        test_need = Need.objects.create(description="test")
        test_need.save()
        self.centre.needs.add(test_need)
        response = self.client.put(
            f"{self.post_url}{test_need.id}/update/",
            {"description": NEW_DESCRIPTION},
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["description"], NEW_DESCRIPTION)
        updated_test_need = Need.objects.get(id=test_need.id)
        self.assertEqual(updated_test_need.description, NEW_DESCRIPTION)

    def test_valid_update(self):
        """Test sending a valid put request to the serializer."""
        test_need = Need.objects.create(description="test")
        test_need.save()
        self.centre.needs.add(test_need)
        response = self.client.put(
            f"{self.post_url}{test_need.id}/update/",
            {"description": NEW_DESCRIPTION},
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["description"], NEW_DESCRIPTION)
        updated_test_need = Need.objects.get(id=test_need.id)
        self.assertEqual(updated_test_need.description, NEW_DESCRIPTION)

    def test_valid_get_client_user(self):
        """
        Test sending a valid get request to the serializer as a client.

        """
        need1 = Need.objects.create(description=NEED_DESCRIPTION)
        need2 = Need.objects.create(description=NEED_DESCRIPTION)
        self.centre.needs.add(need1.id, need2.id)
        client_token = register_client_user(self.client)
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + client_token.data["key"]
        )
        client_user = CustomUser.objects.get(id=client_token.data["user"]["id"])
        client_user.client.centres.add(self.centre)
        response = self.client.get(self.get_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data[0]["description"], NEED_DESCRIPTION)
        self.assertEqual(response.data[1]["description"], NEED_DESCRIPTION)

    def test_invalid_get_client_user(self):
        """
        Test sending an invalid get request to the serializer as a client.

        Should return 0 needs as client not in centre.
        """
        need1 = Need.objects.create(description=NEED_DESCRIPTION)
        self.centre.needs.add(need1.id)
        client_token = register_client_user(self.client)
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + client_token.data["key"]
        )
        client_user = CustomUser.objects.get(id=client_token.data["user"]["id"])
        client_user.client.centres.add(self.centre_2)
        response = self.client.get(self.get_url)
        self.assertEqual(response.status_code, 403)
