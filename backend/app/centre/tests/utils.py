"""Reusable test functions."""
from ...authentication.models import CustomUser
from ...authentication.tests.utils import register_test_user
from ..models import Centre


def create_centre(  # pylint: disable=R0913
    name="testCentre", users=None, policies=None, needs=None, country=None, state=None
):
    """Create a centre and add a user to it."""
    centre = Centre.objects.create(name=name)
    if users:
        for user in users:
            user.centres.add(centre)
    if policies:
        centre.policies.add(*policies)
    if needs:
        centre.needs.add(*needs)
    if country:
        centre.country = country
    if state:
        centre.state = state
    centre.save()
    return centre


def setup_needs_policies(client):
    """Setup config for needs/polices."""
    admin_token = register_test_user(client)
    nurse_token = register_test_user(
        client, username="testNurse", email="testnurseemail@test.test"
    )
    CustomUser.objects.get(id=admin_token.data["user"]["id"])
    nurse_user = CustomUser.objects.get(id=nurse_token.data["user"]["id"])
    centre = create_centre(users=[nurse_user])
    centre_2 = create_centre(name="centre_2", users=[])
    client.credentials(HTTP_AUTHORIZATION="Token " + admin_token.data["key"])
    return nurse_token, centre, centre_2
