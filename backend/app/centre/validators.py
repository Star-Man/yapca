"""Model and serializer validators."""
from datetime import timedelta

import holidays
from rest_framework.exceptions import PermissionDenied, ValidationError


def validate_country(country):
    """Validate that the string provided is a country code."""
    try:
        holidays.country_holidays(country)
    except NotImplementedError as error:
        raise ValidationError("Country is invalid") from error


def validate_state(country, state):
    """Validate that the string provided is a state belonging to selected country."""
    try:
        holidays.country_holidays(country, subdiv=state)
    except NotImplementedError as not_implemented:
        raise ValidationError({"state": ["State is invalid"]}) from not_implemented


def validate_clients_in_centre(clients, centre):
    """All clients of an event must be subbed to the event centre."""
    for client in clients:
        if not client.centres.filter(id=centre.id).exists():
            raise PermissionDenied(
                (
                    "One of the provided clients is not subscribed to this centre. "
                    f"Centre: {centre.name}. "
                    f"Client: {client.firstName} {client.surname}."
                )
            )


def validate_event_length(recurrences, time_period):
    """Validate timePeriod is not too long for recurring event."""
    if recurrences == "daily" and time_period > timedelta(hours=20):
        raise ValidationError(
            {"timePeriod": ["Daily events cannot be longer than 20 hours."]}
        )
    if recurrences == "weekly" and time_period > timedelta(days=5):
        raise ValidationError(
            {"timePeriod": ["Weekly events cannot be longer than 5 days."]}
        )
    if recurrences == "monthly" and time_period > timedelta(days=20):
        raise ValidationError(
            {"timePeriod": ["Monthly events cannot be longer than 20 days."]}
        )
    if recurrences == "yearly" and time_period > timedelta(days=90):
        raise ValidationError(
            {"timePeriod": ["Yearly events cannot be longer than 90 days."]}
        )


def validate_all_day(attrs):
    """Make sure allDay is True when setting close event."""
    if attrs["eventType"] == "User Close Event" and (
        "allDay" not in attrs or attrs["allDay"] is False
    ):
        raise ValidationError(
            '"allDay" must be True when "eventType" is CLOSE_EVENT',
            code="invalid_choice",
        )
