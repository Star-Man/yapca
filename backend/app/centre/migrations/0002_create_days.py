from django.db import migrations


def create_days(apps, schema_editor):
    Day = apps.get_model("centre", "Day")
    Day.objects.bulk_create(
        [
            Day(day="Monday"),
            Day(day="Tuesday"),
            Day(day="Wednesday"),
            Day(day="Thursday"),
            Day(day="Friday"),
            Day(day="Saturday"),
            Day(day="Sunday"),
        ]
    )


class Migration(migrations.Migration):

    dependencies = [
        ("centre", "0001_initial"),
    ]

    operations = [
        migrations.RunPython(create_days),
    ]
