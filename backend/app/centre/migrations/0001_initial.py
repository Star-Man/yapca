# Generated by Django 3.0.8 on 2020-08-07 11:52

from django.db import migrations, models

import app.authentication.validators


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Day",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "day",
                    models.CharField(
                        choices=[
                            ("Monday", "Monday"),
                            ("Tuesday", "Tuesday"),
                            ("Wednesday", "Wednesday"),
                            ("Thursday", "Thursday"),
                            ("Friday", "Friday"),
                            ("Saturday", "Saturday"),
                            ("Sunday", "Sunday"),
                        ],
                        max_length=9,
                        unique=True,
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Need",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("description", models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name="Policy",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("description", models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name="Centre",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "name",
                    models.CharField(
                        error_messages={
                            "unique": "A centre with that name already exists."
                        },
                        max_length=255,
                        unique=True,
                    ),
                ),
                ("policiesFirstSetupComplete", models.BooleanField(default=False)),
                ("needsFirstSetupComplete", models.BooleanField(default=False)),
                ("openingDaysFirstSetupComplete", models.BooleanField(default=False)),
                ("setupComplete", models.BooleanField(default=False)),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("needs", models.ManyToManyField(blank=True, to="centre.Need")),
                ("openingDays", models.ManyToManyField(blank=True, to="centre.Day")),
                ("policies", models.ManyToManyField(blank=True, to="centre.Policy")),
                (
                    "color",
                    models.CharField(
                        blank=True,
                        max_length=7,
                        validators=[
                            app.authentication.validators.validate_accent_color
                        ],
                    ),
                ),
            ],
        ),
    ]
