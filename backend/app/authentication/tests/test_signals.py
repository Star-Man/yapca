"""Signals Tests."""
import time

from django.conf import settings
from django.core import mail
from django.test import TestCase
from rest_framework.test import APIClient

from ..models import EmailChangeRecord
from .utils import create_default_groups, register_client_user, register_test_user

NEW_EMAIL = "newemail@email.com"
AUTH_TOKEN = "Token "
DEFAULT_EMAIL = "test@email.com"
EMAIL_CHANGED_MESSAGE = "Your YAPCA Email was Changed"
DO_NOT_RESPOND = "Do not respond to this email"


class SendRegistrationEmail(TestCase):
    """Registration Email tests."""

    def setUp(self):
        """Create groups."""
        create_default_groups()

    def test_registering_user_sends_email(self):
        """Email is sent on registration."""
        client = APIClient()
        register_test_user(client, email=DEFAULT_EMAIL)
        time.sleep(0.1)
        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual(email.subject, "Welcome to YAPCA")
        self.assertEqual(email.to, [DEFAULT_EMAIL])
        self.assertEqual(email.from_email, settings.DEFAULT_FROM_EMAIL)
        self.assertIn("Hello Test User 1!", email.body)
        self.assertIn(
            "You've just finished your account registration with YAPCA", email.body
        )
        self.assertIn(DO_NOT_RESPOND, email.body)


class SendEmailChangeEmail(TestCase):
    """Change Email tests."""

    url = "/api/users/"

    def setUp(self):
        """Create groups."""
        create_default_groups()
        self.client = APIClient()
        self.admin = register_test_user(
            self.client, username="testAdmin", email="testadmin@email.com"
        )
        self.nurse = register_test_user(
            self.client, username="testNurse", email=DEFAULT_EMAIL
        )
        client_user = register_client_user(self.client, email="clientemail@email.com")
        self.admin_id = self.admin.data["user"]["id"]
        self.nurse_id = self.nurse.data["user"]["id"]
        self.client_user_id = client_user.data["user"]["id"]

    def test_changing_user_email_sends_email(self):
        """Email is sent on email change."""
        self.client.credentials(HTTP_AUTHORIZATION=AUTH_TOKEN + self.nurse.data["key"])
        self.client.patch(
            f"{self.url}{self.nurse_id}/update/",
            {"email": NEW_EMAIL},
        )
        self.assertEqual(len(mail.outbox), 4)  # 3 user registrations + 1 email change
        email = mail.outbox[3]
        self.assertEqual(email.subject, EMAIL_CHANGED_MESSAGE)
        self.assertEqual(email.to, [NEW_EMAIL])
        self.assertEqual(email.from_email, settings.DEFAULT_FROM_EMAIL)
        self.assertIn("Hello Test User 1!", email.body)
        self.assertIn("All future emails will be sent to this address.", email.body)
        self.assertIn(DO_NOT_RESPOND, email.body)
        email_change_record = EmailChangeRecord.objects.first()
        self.assertEqual(email_change_record.user.id, self.nurse_id)

    def test_changing_user_email_as_client(self):
        """Email is sent on email change as clientUser."""
        self.client.credentials(HTTP_AUTHORIZATION=AUTH_TOKEN + self.admin.data["key"])
        self.client.patch(
            f"{self.url}{self.client_user_id}/update/",
            {"email": NEW_EMAIL},
        )
        self.assertEqual(len(mail.outbox), 4)  # 3 user registrations + 1 email change
        email = mail.outbox[3]
        self.assertEqual(email.subject, EMAIL_CHANGED_MESSAGE)
        self.assertEqual(email.to, [NEW_EMAIL])
        self.assertEqual(email.from_email, settings.DEFAULT_FROM_EMAIL)
        self.assertIn("Hello test!", email.body)
        self.assertIn(DO_NOT_RESPOND, email.body)
        email_change_record = EmailChangeRecord.objects.first()
        self.assertEqual(email_change_record.user.id, self.client_user_id)

    def test_changing_user_email_to_same_email(self):
        """Email is not sent if email is same."""
        self.client.credentials(HTTP_AUTHORIZATION=AUTH_TOKEN + self.nurse.data["key"])
        self.client.patch(
            f"{self.url}{self.nurse_id}/update/",
            {"email": DEFAULT_EMAIL},
        )
        self.assertEqual(len(mail.outbox), 3)  # 3 user registrations
        email_change_record_exists = EmailChangeRecord.objects.filter().exists()
        self.assertFalse(email_change_record_exists)

    def test_changing_user_email_as_admin_sends_email(self):
        """Email is sent on email change by admin."""
        self.client.credentials(HTTP_AUTHORIZATION=AUTH_TOKEN + self.admin.data["key"])
        self.client.patch(
            f"{self.url}{self.nurse_id}/update/",
            {"email": NEW_EMAIL},
        )
        self.assertEqual(len(mail.outbox), 4)  # 3 user registrations + 1 email change
        email = mail.outbox[3]
        self.assertEqual(email.subject, EMAIL_CHANGED_MESSAGE)
        email_change_record = EmailChangeRecord.objects.first()
        self.assertEqual(email_change_record.user.id, self.nurse_id)
