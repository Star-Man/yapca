"""Tests for sending post/delete requests to the UserCentresSerializer."""
from django.contrib.auth.models import Group
from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from ....centre.tests.utils import create_centre
from ...models import CustomUser
from ..utils import register_test_user


class UserCentresSerializerTestCase(TestCase):
    """Tests for sending post/delete requests to the UserCentresSerializer."""

    url = "/api/users/centres/update/"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        self.user = CustomUser.objects.get(id=token.data["user"]["id"])
        self.centre = create_centre(users=[self.user])
        self.centre_2 = create_centre(name="testCentre2")
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.data["key"])

    def test_valid_remove(self):
        """Should be able to remove a user centre."""
        response = self.client.delete(
            self.url,
            {"user": self.user.id, "centre": self.centre.id},
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, {"user": self.user.id, "centres": []})

    def test_valid_add(self):
        """Should be able to add a user centre."""
        response = self.client.post(
            self.url,
            {"user": self.user.id, "centre": self.centre_2.id},
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            {
                "user": self.user.id,
                "centres": [self.centre.id, self.centre_2.id],
            },
        )

    def test_no_group_post(self):
        """Should be unable to post data when not in valid group."""
        self.user.groups.clear()
        nurse = Group.objects.get(name="nurse")
        self.user.groups.add(nurse)
        response = self.client.post(
            self.url,
            {"user": self.user.id, "centre": self.centre.id},
            format="json",
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )

    def test_no_group_delete(self):
        """Should be unable to delete data when not in valid group."""
        self.user.groups.clear()
        response = self.client.delete(
            self.url,
            {"user": self.user.id, "centre": self.centre.id},
            format="json",
        )
        self.assertEqual(response.status_code, 403)

    def test_invalid_add_already_exists(self):
        """Returns error if adding centre that already exists."""
        response = self.client.post(
            self.url,
            {"user": self.user.id, "centre": self.centre.id},
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            "Failed to add centre, user is already subscribed to this centre.",
        )

    def test_invalid_remove_does_not_exists(self):
        """Returns error if removing centre that user is not subbed to."""
        response = self.client.delete(
            self.url,
            {"user": self.user.id, "centre": self.centre_2.id},
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            "Failed to remove centre, user is not subscribed to this centre.",
        )

    def test_missing_data(self):
        """Returns error if missing data."""
        response = self.client.delete(
            self.url,
            {},
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "centre": [
                    ErrorDetail(string="This field is required.", code="required")
                ],
                "user": [
                    ErrorDetail(string="This field is required.", code="required")
                ],
            },
        )
