"""Tests for Group Serializers."""
from collections import OrderedDict

from django.test import TestCase
from rest_framework.test import APIClient

from ..utils import register_test_user

REGISTER_URL = "/api/registration/"
TOKEN_AUTH = "Token "
DISPLAY_NAME = "Test User"
DISPLAY_NAME_2 = "Test User 2"
EMAIL = "test@test.com"
EMAIL_2 = "test2@test.com"


class GroupSerializer(TestCase):
    """Tests for GroupSerializer."""

    url = "/api/groups/"
    user = None

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        self.user = register_test_user(self.client)
        self.client.credentials(HTTP_AUTHORIZATION=TOKEN_AUTH + self.user.data["key"])

    def test_valid_get_as_admin(self):
        """Test sending a valid get request to the serializer as admin."""
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 3)
        self.assertEqual(
            response.data,
            [
                OrderedDict([("id", 1), ("name", "admin")]),
                OrderedDict([("id", 2), ("name", "nurse")]),
                OrderedDict([("id", 3), ("name", "client")]),
            ],
        )

    def test_get_as_nurse(self):
        """Test sending a invalid get request to the serializer as nurse."""
        nurse_user = register_test_user(
            self.client,
            username="testUser2",
            display_name=DISPLAY_NAME_2,
            email=EMAIL_2,
        )
        self.client.credentials(HTTP_AUTHORIZATION=TOKEN_AUTH + nurse_user.data["key"])
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)
