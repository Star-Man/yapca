"""Tests for Request Totp Reset Serializers."""
import time
from datetime import timedelta

from django.conf import settings
from django.core import mail
from django.test import TestCase, override_settings
from django.utils import timezone
from mock import patch
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from ...models import CustomUser, TempCode
from ..utils import EMAIL_OVERRIDES, JSON_RELEASE_DATA, MockResponse, register_test_user


@override_settings(**EMAIL_OVERRIDES)
@patch(
    "app.authentication.views.get", return_value=MockResponse(JSON_RELEASE_DATA, 200)
)
class RequestTotpResetSerializerTestCase(TestCase):
    """Tests for RequestTotpReset."""

    url = "/api/request-reset-totp/"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        register_response = register_test_user(self.client, email="useremail@email.com")
        self.user = CustomUser.objects.get(id=register_response.data["user"]["id"])
        self.client.credentials(
            HTTP_AUTHORIZATION="Token " + register_response.data["key"]
        )
        mail.outbox = []

    def test_valid_request(self, get_mock):
        """
        Test sending a valid request to the serializer.

        Check email is sent and it contains correct links to download.
        Check email contains link to temp_code query.
        Check email contains link to website if WEB_VERSION_EXISTS==True
        Check email renders expiry date correctly.
        """
        response = self.client.get(f"{self.url}?userId={self.user.id}")
        self.assertEqual(response.status_code, 200)
        time.sleep(0.1)
        self.assertEqual(len(mail.outbox), 1)

        email = mail.outbox[0]
        temp_code = TempCode.objects.last()
        self.assertEqual(temp_code.action, "reset-totp")
        self.assertEqual(temp_code.associatedUser, self.user)
        self.assertEqual(email.subject, "2FA Device Reset Request")
        self.assertEqual(email.to, ["useremail@email.com"])
        self.assertEqual(email.from_email, settings.DEFAULT_FROM_EMAIL)
        self.assertIn("Hello Test User 1!", email.body)
        self.assertIn("https://windows-release.example.com", email.body)
        self.assertIn("https://all-release.example.com", email.body)
        self.assertIn(
            (
                "https://star-man.gitlab.io/yapca-site/#/uri-linker?address=http"
                + f"://127.0.0.1:9000&action=reset-totp&code={temp_code.code}&"
                + "username=testUser1"
            ),
            email.body,
        )
        self.assertIn(
            (
                "https://star-man.gitlab.io/yapca-site/#/uri-linker?address=http"
                + f"://127.0.0.1:9000&action=reset-totp&code={temp_code.code}&"
                + "username=testUser1"
            ),
            email.body,
        )
        self.assertIn(
            "How to for YAPCA web version",
            email.body,
        )
        self.assertIn(
            (
                "https://127.0.0.6:1234/yapca/temp-code?action=reset-totp&cod"
                + f"e={temp_code.code}&username=testUser1"
            ),
            email.body,
        )
        formatted_end_time = timezone.localtime(temp_code.endTime).strftime(
            "%H:%M %b %d"
        )
        self.assertIn(
            f"2FA device will end: {formatted_end_time}",
            email.body,
        )
        get_mock.assert_called_with(
            "https://gitlab.com/api/v4/projects/16681524/releases",
            headers={"Accept": "*/*"},
            timeout=5,
        )

    @override_settings(WEB_VERSION_EXISTS=False)
    def test_web_disabled(self, _):
        """Should not show website related text if web not enabled."""
        self.client.get(f"{self.url}?userId={self.user.id}")
        time.sleep(0.1)
        email = mail.outbox[0]

        self.assertNotIn(
            "How to for YAPCA web version",
            email.body,
        )
        self.assertNotIn(
            "https://127.0.0.6:1234/#yapca/temp-code?action=reset-totp&cod",
            email.body,
        )

    @override_settings(BACKEND_PORT=80)
    @override_settings(FRONTEND_PORT="")
    def test_no_port_values(self, _):
        """Should generate correct frontend/backend links without port."""
        self.client.get(f"{self.url}?userId={self.user.id}")
        time.sleep(0.1)
        email = mail.outbox[0]

        self.assertIn("address=http://127.0.0.1&", email.body)
        self.assertIn("https://127.0.0.6/yapca/", email.body)

    def test_wrong_user(self, _):
        """If no associated user, no email sent and 400 status returned."""
        response = self.client.get(f"{self.url}?userId=9872")
        time.sleep(0.1)
        self.assertEqual(len(mail.outbox), 0)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data, {"error": ErrorDetail(string="User was not found", code=400)}
        )

    def test_already_requested(self, _):
        """If tempCode already exists, email is not sent."""
        TempCode.objects.create(
            associatedUser=self.user,
            endTime=(timezone.now() + timedelta(days=1)),
            action="reset-totp",
            code="EVw3gXaZWCHNTl8bpQYUQWTJby7ZPpKO",
        )
        response = self.client.get(f"{self.url}?userId={self.user.id}")
        time.sleep(0.1)
        self.assertEqual(len(mail.outbox), 0)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "error": ErrorDetail(
                    string="User was already sent email in last 24 hours", code=400
                )
            },
        )
