"""Tests for TempCode Serializers."""
from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from ...models import CustomUser, TempCode
from ..utils import setup_temp_codes

REQUIRED_ERROR = "This field is required."
INVALID_TEMP_CODE_MESSAGE = "Code/User is invalid or outdated"
INVALID_LENGTH_ERROR = "Ensure this field has at least 32 characters."


class ValidateTempCode(TestCase):
    """Tests for ValidateTempCode Serializer."""

    url = "/api/temp-code/validate/"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        setup_temp_codes()

    def test_valid_code_validation(self):
        """Returns success for valid code."""
        response = self.client.post(
            self.url,
            {
                "username": "testUser1",
                "action": "reset-totp",
                "code": "Y789B629G897R236F7U24F23F223R33S",
                "email": "",
            },
        )
        self.assertEqual(response.status_code, 200)

    def test_outdated_code_validation(self):
        """Returns fail for outdated code."""
        response = self.client.post(
            self.url,
            {
                "username": "testUser1",
                "action": "reset-totp",
                "code": "FYCFbisIE5luA0NMbfwFVdOgI2R8L5mQ",
                "email": "",
            },
        )
        self.assertEqual(
            response.data,
            {"error": ErrorDetail(string=INVALID_TEMP_CODE_MESSAGE, code=400)},
        )

    def test_invalid_user_validation(self):
        """Returns fail for wrong user."""
        response = self.client.post(
            self.url,
            {
                "username": "differentUser",
                "action": "reset-totp",
                "code": "FYCFbisIE5luA0NMbfwFVdOgI2R8L5mQ",
                "email": "",
            },
        )
        self.assertEqual(
            response.data,
            {"error": ErrorDetail(string=INVALID_TEMP_CODE_MESSAGE, code=400)},
        )

    def test_blank_user_validation(self):
        """Returns fail for blank user."""
        response = self.client.post(
            self.url,
            {
                "username": "",
                "action": "reset-totp",
                "code": "FYCFbisIE5luA0NMbfwFVdOgI2R8L5mQ",
                "email": "",
            },
        )
        self.assertEqual(
            response.data,
            {"error": ErrorDetail(string=INVALID_TEMP_CODE_MESSAGE, code=400)},
        )

    def test_invalid_code_validation(self):
        """Returns fail for wrong code."""
        response = self.client.post(
            self.url,
            {
                "username": "testUser1",
                "action": "reset-totp",
                "code": "FYCFbisIE5luA0NMbfwFVdOgI2R8L5mA",
                "email": "",
            },
        )
        self.assertEqual(
            response.data,
            {"error": ErrorDetail(string=INVALID_TEMP_CODE_MESSAGE, code=400)},
        )

    def test_invalid_data(self):
        """Returns fail for invalid data."""
        response = self.client.post(
            self.url,
            {
                "username": "testUser1",
                "action": "reset-totps",
                "code": "aaaaaa",
                "email": "",
            },
        )
        self.assertEqual(
            response.data,
            {
                "action": [
                    ErrorDetail(
                        string='"reset-totps" is not a valid choice.',
                        code="invalid_choice",
                    )
                ],
                "code": [
                    ErrorDetail(
                        string=INVALID_LENGTH_ERROR,
                        code="min_length",
                    )
                ],
            },
        )

    def test_missing_data(self):
        """Returns fail for missing data."""
        response = self.client.post(
            self.url,
            {},
        )
        self.assertEqual(
            response.data,
            {
                "code": [ErrorDetail(string=REQUIRED_ERROR, code="required")],
                "username": [ErrorDetail(string=REQUIRED_ERROR, code="required")],
                "action": [ErrorDetail(string=REQUIRED_ERROR, code="required")],
                "email": [ErrorDetail(string=REQUIRED_ERROR, code="required")],
            },
        )


class ResetTotpKey(TestCase):
    """Tests for ResetTotpKey Serializer."""

    url = "/api/temp-code/reset-totp/"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        setup_temp_codes()

    def test_valid_totp_reset(self):
        """Returns success for valid code."""
        response = self.client.put(
            self.url,
            {
                "username": "testUser1",
                "code": "Y789B629G897R236F7U24F23F223R33S",
                "otpSecret": "newOtpDeviceKey",
                "email": "",
            },
        )
        self.assertEqual(response.status_code, 200)
        temp_code_exists = TempCode.objects.filter(
            code="Y789B629G897R236F7U24F23F223R33S"
        ).exists()
        self.assertFalse(temp_code_exists)
        user = CustomUser.objects.get(username="testUser1")
        self.assertEqual(user.otpDeviceKey, "newOtpDeviceKey")

    def test_outdated_code_validation(self):
        """Returns fail for outdated code."""
        response = self.client.put(
            self.url,
            {
                "username": "testUser1",
                "code": "FYCFbisIE5luA0NMbfwFVdOgI2R8L5mQ",
                "otpSecret": "newOtpDeviceKey",
                "email": "",
            },
        )
        self.assertEqual(
            response.data,
            {"error": ErrorDetail(string=INVALID_TEMP_CODE_MESSAGE, code=400)},
        )
        temp_code_exists = TempCode.objects.filter(
            code="Y789B629G897R236F7U24F23F223R33S"
        ).exists()
        self.assertTrue(temp_code_exists)
        user = CustomUser.objects.get(username="testUser1")
        self.assertEqual(user.otpDeviceKey, None)

    def test_invalid_user_validation(self):
        """Returns fail for wrong user."""
        response = self.client.put(
            self.url,
            {
                "username": "differentUser",
                "otpSecret": "newOtpDeviceKey",
                "code": "FYCFbisIE5luA0NMbfwFVdOgI2R8L5mQ",
                "email": "",
            },
        )
        self.assertEqual(
            response.data,
            {"error": ErrorDetail(string=INVALID_TEMP_CODE_MESSAGE, code=400)},
        )

    def test_invalid_code_validation(self):
        """Returns fail for wrong code."""
        response = self.client.put(
            self.url,
            {
                "username": "testUser1",
                "otpSecret": "newOtpDeviceKey",
                "code": "FYCFbisIE5luA0NMbfwFVdOgI2R8L5mA",
                "email": "",
            },
        )
        self.assertEqual(
            response.data,
            {"error": ErrorDetail(string=INVALID_TEMP_CODE_MESSAGE, code=400)},
        )

    def test_invalid_data(self):
        """Returns fail for invalid data."""
        response = self.client.put(
            self.url,
            {
                "username": "testUser1",
                "otpSecret": "newOtpDeviceKey",
                "code": "aaaaaa",
                "email": "",
            },
        )
        self.assertEqual(
            response.data,
            {"code": [ErrorDetail(string=INVALID_LENGTH_ERROR, code="min_length")]},
        )

    def test_missing_data(self):
        """Returns fail for missing data."""
        response = self.client.put(
            self.url,
            {},
        )
        self.assertEqual(
            response.data,
            {
                "code": [ErrorDetail(string=REQUIRED_ERROR, code="required")],
                "username": [ErrorDetail(string=REQUIRED_ERROR, code="required")],
                "otpSecret": [ErrorDetail(string=REQUIRED_ERROR, code="required")],
            },
        )


class ResetPasswordTempCode(TestCase):
    """Tests for ResetPasswordTempCode Serializer."""

    url = "/api/temp-code/reset-password/"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        setup_temp_codes()

    def test_valid_password_reset(self):
        """Returns success for valid code."""
        response = self.client.put(
            self.url,
            {
                "username": "testUser1",
                "code": "96DIgcpP0yN7PWVdFbE9Okun8IB1qnUy",
                "password": "newPassword",
            },
        )
        self.assertEqual(response.status_code, 200)
        temp_code_exists = TempCode.objects.filter(
            code="96DIgcpP0yN7PWVdFbE9Okun8IB1qnUy"
        ).exists()
        self.assertFalse(temp_code_exists)
        user = CustomUser.objects.get(username="testUser1")
        self.assertEqual(user.check_password("newPassword"), True)

    def test_outdated_code_validation(self):
        """Returns fail for outdated code."""
        response = self.client.put(
            self.url,
            {
                "username": "testUser1",
                "code": "UNJWDqcm6ofTAUfXojwcm6wHwWNSEo2b",
                "password": "newPassword",
            },
        )
        self.assertEqual(
            response.data,
            {"error": ErrorDetail(string=INVALID_TEMP_CODE_MESSAGE, code=400)},
        )
        temp_code_exists = TempCode.objects.filter(
            code="UNJWDqcm6ofTAUfXojwcm6wHwWNSEo2b"
        ).exists()
        self.assertTrue(temp_code_exists)
        user = CustomUser.objects.get(username="testUser1")
        self.assertEqual(user.check_password("newPassword"), False)

    def test_invalid_user_validation(self):
        """Returns fail for wrong user."""
        response = self.client.put(
            self.url,
            {
                "username": "differentUser",
                "password": "newPassword",
                "code": "96DIgcpP0yN7PWVdFbE9Okun8IB1qnUy",
            },
        )
        self.assertEqual(
            response.data,
            {"error": ErrorDetail(string=INVALID_TEMP_CODE_MESSAGE, code=400)},
        )

    def test_invalid_code_validation(self):
        """Returns fail for wrong code."""
        response = self.client.put(
            self.url,
            {
                "username": "testUser1",
                "password": "newPassword",
                "code": "FYCFbisIE5luA0NMbf66666662R8L5mA",
            },
        )
        self.assertEqual(
            response.data,
            {"error": ErrorDetail(string=INVALID_TEMP_CODE_MESSAGE, code=400)},
        )

    def test_invalid_data(self):
        """Returns fail for invalid data."""
        response = self.client.put(
            self.url,
            {"username": "testUser1", "password": "newPassword", "code": "aaaaaa"},
        )
        self.assertEqual(
            response.data,
            {"code": [ErrorDetail(string=INVALID_LENGTH_ERROR, code="min_length")]},
        )

    def test_missing_data(self):
        """Returns fail for missing data."""
        response = self.client.put(
            self.url,
            {},
        )
        self.assertEqual(
            response.data,
            {
                "code": [ErrorDetail(string=REQUIRED_ERROR, code="required")],
                "username": [ErrorDetail(string=REQUIRED_ERROR, code="required")],
                "password": [ErrorDetail(string=REQUIRED_ERROR, code="required")],
            },
        )

    def test_invalid_action_code(self):
        """Returns fail for valid code with different action."""
        response = self.client.put(
            self.url,
            {
                "username": "testUser1",
                "code": "Y789B629G897R236F7U24F23F223R33S",
                "password": "newPassword",
            },
        )
        self.assertEqual(
            response.data,
            {"error": ErrorDetail(string=INVALID_TEMP_CODE_MESSAGE, code=400)},
        )
