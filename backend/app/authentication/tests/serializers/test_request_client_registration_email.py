"""Tests for Request Client Registration Email Serializers."""
import time

from django.conf import settings
from django.core import mail
from django.test import TestCase, override_settings
from django.utils import timezone
from mock import patch
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from ....centre.tests.utils import create_centre
from ....client.tests.utils import create_client
from ...models import CustomUser, TempCode, UserRegistrationRequestRecord
from ..utils import EMAIL_OVERRIDES, JSON_RELEASE_DATA, MockResponse, register_test_user

EMAIL = "test1@email.com"


@override_settings(**EMAIL_OVERRIDES)
@patch(
    "app.authentication.views.get", return_value=MockResponse(JSON_RELEASE_DATA, 200)
)
class RequestClientRequestRegistrationEmailSerializerTestCase(TestCase):
    """Tests for RequestClientRegistrationEmail."""

    url = "/api/request-client-registration-email/"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        register_response = register_test_user(self.client, email="useremail@email.com")
        self.user = CustomUser.objects.get(id=register_response.data["user"]["id"])
        self.centre = create_centre(users=[self.user])

        self.centre_2 = create_centre(name="testCentre2", users=[self.user])
        self.test_client = create_client(centres=[self.centre])
        self.client.credentials(
            HTTP_AUTHORIZATION="Token " + register_response.data["key"]
        )
        mail.outbox = []

    def test_valid_request(self, get_mock):
        """
        Test sending a valid request to the serializer.

        Check email is sent and it contains correct links to download.
        Check email contains link to temp_code query.
        Check email contains link to website if WEB_VERSION_EXISTS==True
        Check email renders expiry date correctly.
        """
        response = self.client.post(
            f"{self.url}{self.test_client.id}/",
            {"email": EMAIL, "canViewCalendar": True, "canViewOwnData": False},
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        time.sleep(0.1)
        self.assertEqual(len(mail.outbox), 1)

        email = mail.outbox[0]
        temp_code = TempCode.objects.last()
        self.assertEqual(temp_code.action, "client-registration")
        self.assertEqual(temp_code.email, EMAIL)
        permissions = list(temp_code.permissions.all().values_list())
        self.assertEqual(
            permissions,
            [
                (
                    permissions[0][0],
                    "Client Can View Calendar",
                    "client_can_view_calendar",
                )
            ],
        )
        self.assertEqual(temp_code.associatedUser, None)
        self.assertEqual(email.subject, "You've been invited to YAPCA!")
        self.assertEqual(email.to, [EMAIL])
        self.assertEqual(email.from_email, settings.DEFAULT_FROM_EMAIL)
        self.assertIn("Hello test!", email.body)
        self.assertIn("https://windows-release.example.com", email.body)
        self.assertIn("https://all-release.example.com", email.body)
        self.assertIn(
            (
                "https://star-man.gitlab.io/yapca-site/#/uri-linker?address=http"
                + f"://127.0.0.1:9000&action=client-registration&code={temp_code.code}&"
                + "email=test1@email.com"
            ),
            email.body,
        )
        self.assertIn(
            (
                "https://star-man.gitlab.io/yapca-site/#/uri-linker?address=http"
                + f"://127.0.0.1:9000&action=client-registration&code={temp_code.code}&"
                + "email=test1@email.com"
            ),
            email.body,
        )
        self.assertIn(
            "How to for YAPCA web version",
            email.body,
        )
        self.assertIn(
            (
                "https://127.0.0.6:1234/yapca/temp-code?action=client-registration&cod"
                + f"e={temp_code.code}&email=test1@email.com"
            ),
            email.body,
        )
        formatted_end_time = timezone.localtime(temp_code.endTime).strftime(
            "%H:%M %b %d"
        )
        self.assertIn(
            f"register will end: {formatted_end_time}",
            email.body,
        )
        get_mock.assert_called_with(
            "https://gitlab.com/api/v4/projects/16681524/releases",
            headers={"Accept": "*/*"},
            timeout=5,
        )
        self.assertTrue(
            UserRegistrationRequestRecord.objects.filter(email=EMAIL).exists()
        )

    @override_settings(WEB_VERSION_EXISTS=False)
    def test_web_disabled(self, _):
        """Should not show website related text if web not enabled."""
        self.client.post(
            f"{self.url}{self.test_client.id}/",
            {"email": EMAIL, "canViewCalendar": True, "canViewOwnData": True},
            format="json",
        )
        time.sleep(0.1)
        email = mail.outbox[0]

        self.assertNotIn(
            "How to for YAPCA web version",
            email.body,
        )
        self.assertNotIn(
            "https://127.0.0.6:1234/yapca",
            email.body,
        )

    @override_settings(BACKEND_PORT=80)
    @override_settings(FRONTEND_PORT="")
    def test_no_port_values(self, _):
        """Should generate correct frontend/backend links without port."""
        self.client.post(
            f"{self.url}{self.test_client.id}/",
            {"email": EMAIL, "canViewCalendar": False, "canViewOwnData": True},
            format="json",
        )
        time.sleep(0.1)
        email = mail.outbox[0]

        self.assertIn("address=http://127.0.0.1&", email.body)
        self.assertIn("https://127.0.0.6/yapca/", email.body)

    def test_email_taken(self, _):
        """Should throw error when email is taken."""
        response = self.client.post(
            f"{self.url}{self.test_client.id}/",
            {
                "email": "useremail@email.com",
                "canViewCalendar": True,
                "canViewOwnData": False,
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {"email": [ErrorDetail(string="Email is already in use.", code="invalid")]},
        )

    def test_email_invalid(self, _):
        """Should throw error when email is invalid."""
        response = self.client.post(
            f"{self.url}{self.test_client.id}/",
            {
                "email": "test",
                "canViewCalendar": True,
                "canViewOwnData": False,
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "email": [
                    ErrorDetail(string="Enter a valid email address.", code="invalid")
                ]
            },
        )

    def test_email_already_sent(self, _):
        """Should throw error when email already sent."""
        self.client.post(
            f"{self.url}{self.test_client.id}/",
            {
                "email": EMAIL,
                "canViewCalendar": True,
                "canViewOwnData": False,
            },
            format="json",
        )
        response = self.client.post(
            f"{self.url}{self.test_client.id}/",
            {
                "email": EMAIL,
                "canViewCalendar": True,
                "canViewOwnData": False,
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "email": [
                    ErrorDetail(
                        string=(
                            "Registration was already requested"
                            + " for this email address in the last 12 hours"
                        ),
                        code="invalid",
                    )
                ]
            },
        )

    def test_missing_data(self, _):
        """Test missing data being sent to serializer."""
        response = self.client.post(
            f"{self.url}{self.test_client.id}/",
            {},
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {"email": [ErrorDetail(string="This field is required.", code="required")]},
        )

    def test_invalid_client_centre(self, _):
        """Cannot post when user centres don't match client centres."""
        self.user.centres.remove(self.centre)
        self.user.centres.add(self.centre_2)
        response = self.client.post(
            f"{self.url}{self.test_client.id}/",
            {
                "email": EMAIL,
                "canViewCalendar": True,
                "canViewOwnData": False,
            },
            format="json",
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )

    def test_invalid_client(self, _):
        """Cannot post when client is not valid."""
        response = self.client.post(
            f"{self.url}675/",
            {
                "email": EMAIL,
                "canViewCalendar": True,
                "canViewOwnData": False,
            },
            format="json",
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )

    def test_invalid_permissions(self, _):
        """Cannot post when no permissions are true."""
        response = self.client.post(
            f"{self.url}{self.test_client.id}/",
            {
                "email": EMAIL,
                "canViewCalendar": False,
                "canViewOwnData": False,
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "error": [
                    ErrorDetail(
                        string="Client must be able to "
                        + "access either calendar or user data.",
                        code=400,
                    )
                ]
            },
        )
