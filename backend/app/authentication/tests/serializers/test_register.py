"""Tests for Register Serializers."""
from collections import OrderedDict

from django.test import TestCase
from rest_framework.exceptions import ErrorDetail

from ....administration.models import RegisterAbility
from ....centre.models import Centre
from ...models import TempCode
from ..utils import register_test_user

DISPLAY_NAME = "Test User"
REQUIRED_ERROR = "This field is required."
EMAIL = "test@test.com"
NO_AUTH_MESSAGE = "Authentication credentials were not provided."
INVALID_TEMP_CODE_ERROR = "Code/User is invalid or outdated"


class CustomRegisterSerializerTestCase(TestCase):
    """Tests for CustomRegisterSerializer."""

    url = "/api/registration/"

    def setUp(self):
        """Set up RegisterAbility."""
        RegisterAbility.objects.filter(id=1).update(canRegister=False)

    def test_valid(self):
        """Test sending a valid post request to the serializer."""
        response = register_test_user(
            self.client,
            email=EMAIL,
            display_name=DISPLAY_NAME,
        )
        self.assertEqual(response.status_code, 201)
        self.assertIn("key", response.data)
        self.assertIn("id", response.data["user"])
        self.assertEqual(response.data["user"]["username"], "testUser1")
        self.assertEqual(response.data["user"]["displayName"], DISPLAY_NAME)
        self.assertEqual(response.data["user"]["accentColor"], "#FFFFFF")
        self.assertEqual(response.data["user"]["darkTheme"], False)
        self.assertEqual(
            response.data["user"]["groups"],
            [OrderedDict([("id", 1), ("name", "admin")])],
        )
        self.assertFalse(
            TempCode.objects.filter(
                code="wzxL8piy4J5YfF0DMQdGbrtYrENUpx0V",
            ).exists()
        )

    def test_invalid_accent_color(self):
        """Test sending an invalid accent color to the serializer."""
        response = register_test_user(self.client, accent_color="test")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "accentColor": [
                    ErrorDetail(
                        string="Accent color must be a hex value starting with #",
                        code="invalid",
                    ),
                    ErrorDetail(
                        string="Ensure this field has at least 7 characters.",
                        code="min_length",
                    ),
                ]
            },
        )
        self.assertTrue(
            TempCode.objects.filter(
                code="wzxL8piy4J5YfF0DMQdGbrtYrENUpx0V",
            ).exists()
        )

    def test_invalid_password(self):
        """Test sending an invalid password to the serializer."""
        response = register_test_user(
            self.client,
            email=EMAIL,
            display_name=DISPLAY_NAME,
            password1="aa",
            password2="aa",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "password1": [
                    ErrorDetail(
                        string="Password must be a minimum of 6 characters.",
                        code="invalid",
                    )
                ]
            },
        )

    def test_missing_data(self):
        """Test missing data being sent to serializer."""
        response = self.client.post(
            self.url,
            {},
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "username": [ErrorDetail(string=REQUIRED_ERROR, code="required")],
                "displayName": [ErrorDetail(string=REQUIRED_ERROR, code="required")],
                "password1": [ErrorDetail(string=REQUIRED_ERROR, code="required")],
                "password2": [ErrorDetail(string=REQUIRED_ERROR, code="required")],
                "accentColor": [ErrorDetail(string=REQUIRED_ERROR, code="required")],
                "email": [ErrorDetail(string=REQUIRED_ERROR, code="required")],
            },
        )

    def test_register_disabled_requires_temp_code(self):
        """Temp code is required when registerAbility is False."""
        response = self.client.post(
            self.url,
            {
                "username": "testUser",
                "displayName": DISPLAY_NAME,
                "password1": "testPassword",
                "password2": "testPassword",
                "accentColor": "#FFFFFF",
                "darkTheme": False,
                "email": EMAIL,
                "tempCode": "",
            },
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "tempCode": ErrorDetail(
                    string="This field is required.", code="required"
                )
            },
        )

    def test_register_disabled_requires_full_temp_code(self):
        """Temp code must be 32 chars when registerAbility is False."""
        response = self.client.post(
            self.url,
            {
                "username": "testUser",
                "displayName": DISPLAY_NAME,
                "password1": "testPassword",
                "password2": "testPassword",
                "accentColor": "#FFFFFF",
                "darkTheme": False,
                "email": EMAIL,
                "tempCode": "aaaaaaa",
            },
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "tempCode": [
                    ErrorDetail(
                        string="Ensure this field has at least 32 characters.",
                        code="min_length",
                    )
                ]
            },
        )

    def test_register_enabled_temp_code_not_required(self):
        """Temp code not required when registerAbility is True."""
        RegisterAbility.objects.filter(id=1).update(canRegister=True)
        response = self.client.post(
            self.url,
            {
                "username": "testUser",
                "displayName": DISPLAY_NAME,
                "password1": "testPassword",
                "password2": "testPassword",
                "accentColor": "#FFFFFF",
                "darkTheme": False,
                "email": EMAIL,
            },
        )
        self.assertEqual(response.status_code, 201)

    def test_invalid_email(self):
        """Rejects non-emails."""
        response = register_test_user(self.client, email="notanemail")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "email": [
                    ErrorDetail(string="Enter a valid email address.", code="invalid")
                ]
            },
        )

    def test_invalid_temp_code(self):
        """Rejects invalid temp code."""
        response = register_test_user(
            self.client, temp_code="zUs5eqenhgG6wk3ysVs7Svlj307YtfXe"
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {"error": ErrorDetail(string=INVALID_TEMP_CODE_ERROR, code=400)},
        )

    def test_outdated_temp_code(self):
        """Rejects outdated temp code."""
        response = register_test_user(
            self.client, temp_code="Y789B629G897R236F7U24F23F223R33S"
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {"error": ErrorDetail(string=INVALID_TEMP_CODE_ERROR, code=400)},
        )

    def test_invalid_email_for_temp_code(self):
        """Rejects temp code where email is wrong."""
        response = register_test_user(
            self.client, temp_code_email="different_email@test.test"
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {"error": ErrorDetail(string=INVALID_TEMP_CODE_ERROR, code=400)},
        )

    def test_provided_centres(self):
        """Sets user centres if provided by temp_code."""
        centre = Centre.objects.create(color="#CCCCCC", state="TX", country="US")
        response = register_test_user(
            self.client, email=EMAIL, display_name=DISPLAY_NAME, centres=[centre]
        )
        self.assertEqual(response.data["user"]["centres"], [centre.id])
