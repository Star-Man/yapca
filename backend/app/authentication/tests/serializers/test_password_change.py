"""Tests for Password Change Serializers."""
from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from ...models import CustomUser
from ..utils import register_test_user

REQUIRED_FIELD = "This field is required."


class PasswordChangeSerializer(TestCase):
    """Tests for PasswordChangeSerializer."""

    url = "/api/change-password/"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        register_response = register_test_user(self.client)
        self.user = CustomUser.objects.get(id=register_response.data["user"]["id"])
        self.client.credentials(
            HTTP_AUTHORIZATION="Token " + register_response.data["key"]
        )

    def test_valid_password_change(self):
        """A user should be able to change their password."""
        response = self.client.post(
            self.url,
            {
                "old_password": "testPassword",
                "new_password1": "newPassword",
                "new_password2": "newPassword",
            },
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, {"detail": "New password has been saved."})
        self.user.refresh_from_db()
        self.assertFalse(self.user.check_password("testPassword"))
        self.assertTrue(self.user.check_password("newPassword"))

    def test_non_matching_passwords(self):
        """Should return validation error when passwords don't match."""
        response = self.client.post(
            self.url,
            {
                "old_password": "testPassword",
                "new_password1": "newPassword",
                "new_password2": "wrongPassword",
            },
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "new_password2": [
                    ErrorDetail(
                        string="The two password fields didn’t match.", code="invalid"
                    )
                ]
            },
        )

    def test_missing_data(self):
        """Should return validation error when missing data."""
        response = self.client.post(
            self.url,
            {},
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "old_password": [ErrorDetail(string=REQUIRED_FIELD, code="required")],
                "new_password1": [ErrorDetail(string=REQUIRED_FIELD, code="required")],
                "new_password2": [ErrorDetail(string=REQUIRED_FIELD, code="required")],
            },
        )

    def test_old_password_wrong(self):
        """Should return validation error when old password is wrong."""
        response = self.client.post(
            self.url,
            {
                "old_password": "wrong",
                "new_password1": "newPassword",
                "new_password2": "newPassword",
            },
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "old_password": [
                    ErrorDetail(
                        string=(
                            "Your old password was entered"
                            + " incorrectly. Please enter it again."
                        ),
                        code="invalid",
                    )
                ]
            },
        )
