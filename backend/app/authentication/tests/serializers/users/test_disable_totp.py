"""Tests for DisableTOTP."""
from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from ....models import CustomUser
from .utils import setup_users_for_get


class DisableTOTP(TestCase):
    """Tests for DisableTOTP."""

    url = "/api/users/disable-totp/"
    user = None

    def setUp(self):
        """Create users for auth."""
        self.client = APIClient()
        self.nurse_user_1_token, _, _, self.client_token = setup_users_for_get(
            self.client
        )
        self.client_user = CustomUser.objects.get(
            id=self.client_token.data["user"]["id"]
        )
        self.client_user.otpDeviceKey = "AAAAAAAAA"
        self.client_user.save()

    def test_valid_get(self):
        """Test valid totp disable"""
        self.client.credentials(
            HTTP_AUTHORIZATION="Token " + self.client_token.data["key"]
        )
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.client_user.refresh_from_db()
        self.assertEqual(self.client_user.otpDeviceKey, None)

    def test_invalid_get_as_nurse(self):
        """Cannot call endpoint as nurse"""
        self.client.credentials(
            HTTP_AUTHORIZATION="Token " + self.nurse_user_1_token.data["key"]
        )
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )
