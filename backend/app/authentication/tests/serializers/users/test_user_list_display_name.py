"""Tests for UserListDisplayName Serializer."""
from django.test import TestCase
from rest_framework.test import APIClient

from .utils import setup_users_for_get


class UserListDisplayName(TestCase):
    """Tests for UserListDisplayName."""

    url = "/api/users/display-names/"
    user = None

    def setUp(self):
        """Create users for auth."""
        self.client = APIClient()
        self.nurse_user_1_token, _, _, self.client_token = setup_users_for_get(
            self.client
        )

    def test_valid_get_as_admin(self):
        """
        Test sending a valid get request to the serializer as admin.

        Should get users in related centres.
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            [
                {"id": response.data[0]["id"], "displayName": "admin", "client": None},
                {"id": response.data[1]["id"], "displayName": "nurse1", "client": None},
                {"id": response.data[2]["id"], "displayName": "nurse2", "client": None},
            ],
        )

    def test_valid_get_as_nurse(self):
        """
        Test sending a valid get request to the serializer as nurse.

        Should get users in related centres.
        """
        self.client.credentials(
            HTTP_AUTHORIZATION="Token " + self.nurse_user_1_token.data["key"]
        )
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            [
                {"id": response.data[0]["id"], "displayName": "admin", "client": None},
                {"id": response.data[1]["id"], "displayName": "nurse1", "client": None},
            ],
        )

    def test_valid_get_as_client_user(self):
        """
        Test sending a valid get request to the serializer as client user.

        Should get users clients centres.
        """
        self.client.credentials(
            HTTP_AUTHORIZATION="Token " + self.client_token.data["key"]
        )
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            [
                {"id": response.data[0]["id"], "displayName": "admin", "client": None},
                {"id": response.data[1]["id"], "displayName": "nurse2", "client": None},
            ],
        )
