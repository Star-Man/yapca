"""Tests for UserListSharedData Serializer."""
from collections import OrderedDict

from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .utils import setup_users_for_get


class UserListSharedData(TestCase):
    """Tests for UserListSharedData."""

    url = "/api/users/shared-data/"
    user = None

    def setUp(self):
        """Create users for auth."""
        self.client = APIClient()
        self.nurse_user_1_token, _, _, _ = setup_users_for_get(self.client)

    def test_valid_get_as_admin(self):
        """
        Test sending a valid get request to the serializer as admin.

        Should get all users.
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            [
                OrderedDict(
                    [
                        ("id", response.data[0]["id"]),
                        ("displayName", "admin"),
                        ("username", "testUser1"),
                        ("isActive", True),
                        ("email", "admin@test.test"),
                        ("groups", [OrderedDict([("id", 1), ("name", "admin")])]),
                        (
                            "centres",
                            [
                                response.data[0]["centres"][0],
                                response.data[0]["centres"][1],
                            ],
                        ),
                        ("client", None),
                    ]
                ),
                OrderedDict(
                    [
                        ("id", response.data[1]["id"]),
                        ("displayName", "nurse1"),
                        ("username", "nurse1"),
                        ("isActive", True),
                        ("email", "test1@test.test"),
                        ("groups", [OrderedDict([("id", 2), ("name", "nurse")])]),
                        (
                            "centres",
                            [
                                response.data[1]["centres"][0],
                            ],
                        ),
                        ("client", None),
                    ]
                ),
                OrderedDict(
                    [
                        ("id", response.data[2]["id"]),
                        ("displayName", "nurse2"),
                        ("username", "nurse2"),
                        ("isActive", True),
                        ("email", "test2@test.test"),
                        ("groups", [OrderedDict([("id", 2), ("name", "nurse")])]),
                        (
                            "centres",
                            [
                                response.data[2]["centres"][0],
                            ],
                        ),
                        ("client", None),
                    ]
                ),
                OrderedDict(
                    [
                        ("id", response.data[3]["id"]),
                        ("displayName", None),
                        ("username", "clientUser1"),
                        ("isActive", True),
                        ("email", "client_user@test.com"),
                        ("groups", [OrderedDict([("id", 3), ("name", "client")])]),
                        ("centres", []),
                        ("client", response.data[3]["client"]),
                    ]
                ),
            ],
        )

    def test_invalid_get_as_nurse(self):
        """Test cannot get as nurse."""
        self.client.credentials(
            HTTP_AUTHORIZATION="Token " + self.nurse_user_1_token.data["key"]
        )
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )
