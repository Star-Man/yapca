"""Test websocket consumers for user model."""
import asyncio

import pytest
from channels.db import database_sync_to_async
from channels.testing import WebsocketCommunicator
from django.contrib.auth.models import Group
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from ...centre.models import Centre
from ...routing import APPLICATION
from ..models import CustomUser
from .utils import create_default_groups

WEBSOCKET_URL = "ws/main/"
CENTRE_NAME = "Test Centre"
EMAIL = "testemail@test.com"


@database_sync_to_async
def update_user_centres():
    """Update a users centres."""
    client = APIClient()
    admin = CustomUser.objects.get(username="testUser")
    nurse = CustomUser.objects.get(username="testUser1")
    test_centre = Centre.objects.create(name="clientCentre")
    user_token = Token.objects.get(user=admin)
    client.credentials(HTTP_AUTHORIZATION="Token " + user_token.key)
    client.post(
        "/api/users/centres/update/",
        {"centre": test_centre.id, "user": nurse.id},
        format="json",
    )
    return test_centre, nurse


@database_sync_to_async
def tokens():
    """Create users and return tokens."""
    create_default_groups()
    admin, _ = CustomUser.objects.get_or_create(username="testUser")
    admin_token, _ = Token.objects.get_or_create(user=admin)
    nurse, _ = CustomUser.objects.get_or_create(username="testUser1", email=EMAIL)
    nurse_token, _ = Token.objects.get_or_create(user=nurse)
    return {
        "nurse": nurse_token,
        "admin": admin_token,
    }


@database_sync_to_async
def create_user(username):
    """Create a user model."""
    test_user = CustomUser.objects.create(
        username=username,
        userClientTableHiddenRows='["test", "test 2"]',
        email=EMAIL,
    )
    test_user.save()


@database_sync_to_async
def update_nurse():
    """Update the nurse user."""
    nurse_user = CustomUser.objects.get(username="testUser1")
    nurse_user.accentColor = "#CCCCCC"
    nurse_user.userClientTableHiddenRows = '["test", "test 2"]'
    return nurse_user.save()


@database_sync_to_async
def update_groups():
    """Update the nurse users group."""
    nurse_user = CustomUser.objects.get(username="testUser1")
    admin = Group.objects.get(name="admin")
    return nurse_user.groups.add(admin)


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_on_save_user_model_admin():
    """Validate websocket message is sent to 'admin' on update to User."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["admin"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    await create_user("testUser5")
    # User data sent to other users
    response = await communicator.receive_json_from()
    assert {
        "type": "update",
        "action": "updateUser",
        "data": {
            "id": response["data"]["id"],
            "displayName": None,
            "username": "testUser5",
            "isActive": True,
            "email": EMAIL,
            "groups": [
                {
                    "id": response["data"]["groups"][0]["id"],
                    "name": "nurse",
                    "permissions": [],
                }
            ],
        },
    } == response
    await communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_on_save_user_model_self():
    """Validate websocket message is sent to own user on update to User."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nurse"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    await update_nurse()
    response = await communicator.receive_json_from()
    assert {
        "type": "update",
        "action": "updateUser",
        "data": {
            "id": response["data"]["id"],
            "displayName": None,
            "username": "testUser1",
            "isActive": True,
            "email": EMAIL,
            "accentColor": "#CCCCCC",
            "darkTheme": False,
            "userClientTableHiddenRows": ["test", "test 2"],
            "hasTOTP": False,
        },
    } == response
    await communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_on_save_user_model_nurse():
    """Validate websocket message is sent to 'nurse' on update to User."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nurse"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    await create_user("testUser6")
    response = await communicator.receive_json_from()
    assert {
        "type": "update",
        "action": "updateUser",
        "data": {"id": response["data"]["id"], "displayName": None},
    } == response
    await communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_on_save_user_model_external():
    """Validate websocket message is not sent to 'external' on update to User."""
    await database_sync_to_async(create_default_groups)()
    communicator = WebsocketCommunicator(APPLICATION, WEBSOCKET_URL)
    connected, _ = await communicator.connect()
    assert connected
    await create_user("testUser7")
    with pytest.raises(asyncio.TimeoutError):
        await communicator.receive_json_from(timeout=0.5)


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_on_user_centre_update():
    """Validate websocket message is sent when updating a users centre."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["admin"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    test_centre, nurse = await update_user_centres()
    await communicator.receive_json_from()
    response = await communicator.receive_json_from()
    assert response["data"] == {"userId": nurse.id, "centreIds": [test_centre.id]}
    await communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_on_user_group_update():
    """Validate websocket message is sent when updating a users group."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nurse"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    await update_groups()
    response = await communicator.receive_json_from()
    assert response["data"]["groups"][0]["name"] == "admin"
    await communicator.disconnect()
