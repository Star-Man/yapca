"""Model and serializer validators."""
import re

from django.core.exceptions import ValidationError


def validate_accent_color(accent_color):
    """Validate that the accent colour is a HEX colour value."""
    if not re.search(r"^#(?:[0-9a-fA-F]{3}){1,2}$", accent_color):
        raise ValidationError("Accent color must be a hex value starting with #")
