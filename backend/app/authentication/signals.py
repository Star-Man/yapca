"""Signals to send websocket messages on model change."""

import json

from axes.signals import user_locked_out
from django.contrib.auth.models import Group
from django.db.models.signals import m2m_changed, post_save, pre_save
from django.dispatch import receiver
from django.forms.models import model_to_dict
from rest_framework.exceptions import PermissionDenied

from ..websockets.utils import send_to_permitted_users
from .models import CustomUser, EmailChangeRecord
from .utils import EmailThread, get_email_templates


def send_email_changed_email(instance):
    """Send email to user on email change."""
    if instance.client:
        display_name = instance.client.firstName
    else:
        display_name = instance.displayName
    context = {"displayName": display_name}
    template = get_email_templates("email_change_user", context)
    EmailThread(
        "Your YAPCA Email was Changed",
        template["plain"],
        template["html"],
        [instance.email],
    ).start()


def update_user_websocket(instance, groups=False):
    """Send the updated/created user to the websocket consumer."""
    serialized_obj = model_to_dict(instance)
    display_name = {
        "id": serialized_obj["id"],
        "displayName": serialized_obj["displayName"],
    }
    shared_user_data = {
        **display_name,
        "username": serialized_obj["username"],
        "isActive": serialized_obj["is_active"],
        "email": serialized_obj["email"],
    }

    if groups:
        shared_user_data["groups"] = [model_to_dict(instance.groups.first())]

    # Create a user_data object with info only the user wants
    user_data = dict(shared_user_data)
    user_data.update(
        {
            "accentColor": serialized_obj["accentColor"],
            "darkTheme": serialized_obj["darkTheme"],
            "hasTOTP": instance.hasTOTP,
        }
    )
    if serialized_obj["userClientTableHiddenRows"]:
        user_data["userClientTableHiddenRows"] = json.loads(
            serialized_obj["userClientTableHiddenRows"]
        )
    user_update_data = {
        "action": "updateUser",
        "id": user_data["id"],
    }
    send_to_permitted_users(
        {
            "can_view_user_all_info": {**user_update_data, "data": user_data},
            "can_view_user_basic_info": {**user_update_data, "data": shared_user_data},
            "can_view_user_display_name": {**user_update_data, "data": display_name},
        }
    )


@receiver(post_save, sender=CustomUser)
def save_user(
    sender, instance, created, update_fields, **kwargs
):  # pylint: disable=W0613
    """Set the first user to be an admin, and the following users to nurse."""
    if created:
        if CustomUser.objects.count() < 2:
            selected_group = Group.objects.get(name="admin")
        elif not instance.client:
            selected_group = Group.objects.get(name="nurse")
        else:
            selected_group = Group.objects.get(name="client")
        instance.groups.add(selected_group)
    update_user_websocket(instance)


@receiver(pre_save, sender=CustomUser)
def check_email_changed(sender, instance, **kwargs):  # pylint: disable=W0613
    """
    Check if email changed before save.

    If email has changed, send a "changed email" email to the new address.
    """
    if instance.id is not None:
        previous_user_settings = CustomUser.objects.get(id=instance.id)
        if (
            previous_user_settings.email
            and previous_user_settings.email != instance.email
        ):
            send_email_changed_email(instance)
            EmailChangeRecord.objects.create(user=instance)


@receiver(m2m_changed, sender=CustomUser.groups.through)
def save_group(sender, instance, action, **kwargs):  # pylint: disable=W0613
    """Update websocket if user group changes."""
    if action == "post_add":
        update_user_websocket(instance, groups=True)


@receiver(user_locked_out)
def raise_permission_denied(*args, **kwargs):
    """Throw permission denied error after failed login attempts."""
    raise PermissionDenied("Too many failed login attempts")


def save_user_centres(centres, user_id):
    """Send ws message with user centres."""
    data = {"userId": user_id, "centreIds": centres}
    send_to_permitted_users(
        {
            "can_view_user_basic_info": {
                "action": "updateUserCentres",
                "data": data,
                "id": user_id,
            }
        }
    )
