"""Reusable functions for authentication."""
import os
from threading import Thread

from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string

dir_path = os.path.dirname(os.path.realpath(__file__))


def email_allowed(recipient_list):
    """Validate that all emails in a list are valid"""
    valid = True
    for email in recipient_list:
        if email.endswith(".test") or email.endswith("test.com"):
            valid = False
            break
    return valid


class EmailThread(Thread):
    """Send emails as separate thread."""

    def __init__(self, subject, plain_content, html_content, recipient_list):
        """Specify email parameters."""
        self.subject = subject
        self.recipient_list = recipient_list
        self.plain_content = plain_content
        self.html_content = html_content
        Thread.__init__(self)

    def run(self):
        """Send the email."""
        if email_allowed(self.recipient_list):
            send_mail(
                self.subject,
                self.plain_content,
                settings.DEFAULT_FROM_EMAIL,
                self.recipient_list,
                html_message=self.html_content,
                fail_silently=True,
            )


def get_email_templates(template_name, context):
    """Return html and plain text email templates for provided string."""
    html_message = render_to_string(
        dir_path + f"/templates/email/{template_name}/{template_name}.html",
        context,
    )
    plain_message = render_to_string(
        dir_path + f"/templates/email/{template_name}/{template_name}.txt", context
    )
    return {"html": html_message, "plain": plain_message}
