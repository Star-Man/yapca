Hello {{ displayName }}!

An admin has requested that you reset your two factor authentication (2FA) device. Please follow the instructions below to change your device.

{% if app_version_exists %}
How to for YAPCA apps:

    1. Make sure you have YAPCA installed on your device. Once that's done go to step 2.
        If you do not, select a version below and install it before continuing.

        YAPCA For Windows: {{ links.windows }}
        YAPCA For Linux: {{ links.linux }}


    2. To reset your password click below
        https://star-man.gitlab.io/yapca-site/#/uri-linker?address={{ server_address }}&action={{ action }}&code={{ temp_code }}&username={{ username }}

        If you're having trouble with the link above, login normally, then where you type in the 2FA code click "An Admin Reset My Device" and in the next page enter the temporary code: "{{ temp_code }}"

{% endif %}
{% if web_version_exists %}
How to for YAPCA web version:
    If you access yapca through a browser, click below:
        {{ frontend_address }}temp-code?action={{ action }}&code={{ temp_code }}&username={{ username }}
{% endif %}

The window to reset your 2FA device will end: {{ expiry_time|date:'H:i M d' }}. If the time expires you will have to request another code.

If you did not request to have your 2FA device reset contact an admin as soon as you can.
{% include "email/footer/footer.txt" %}
