Hello {{ displayName }}!

An administrator has reactivated your YAPCA account! You can now log in again.

Please follow the instructions below to log in.

{% if app_version_exists %}
How to for YAPCA apps:

    Make sure you have YAPCA installed on your device.
    If you do not, select a version below and install it before continuing.

    YAPCA For Windows: {{ links.windows }}
    YAPCA For Linux: {{ links.linux }}
{% endif %}
{% if web_version_exists %}
How to for YAPCA web version:
    If you access yapca through a browser, click below:
        {{ frontend_address }}
{% endif %}

{% include "email/footer/footer.txt" %}