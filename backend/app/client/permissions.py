"""Client related permissions functions."""

from ..authentication.permissions import IsClient, IsNurse, user_is_admin
from .models import CarePlan, Client, ClientDocument, ClientNeed, Medication, NextOfKin


def client_can_view_client(user):
    """Check if clientUser has permission to view clients."""
    if user.groups.filter(name="client") and user.permissions.filter(
        codename="client_can_view_own_data"
    ):
        return True
    return False


def user_can_view_client(user, client, client_centres=None, permissions_update=False):
    """
    Check if user has correct permissions to view a client.

    Returns bool if provided user is subbed to a centre that
    the provded client is subbed to.

    Accepts client_centres value to override the clients centres
    in the case of a client changing subscribed centres and the
    user losing access to the client should be notified.
    """
    user_centres = user.centres.all()
    if not client_centres:
        client_centres = client.centres.all()
    if user.client and user.client == client:
        return client_can_view_client(user) or permissions_update
    return user_centres.filter(id__in=client_centres).exists()


def client_exists_and_user_can_view(
    user, data_id, client_centres=None, permissions_update=False
):
    """First check if client exists then validate user can view."""
    try:
        client = Client.objects.get(id=data_id)
    except Client.DoesNotExist:
        return False
    return user_can_view_client(
        user, client, client_centres, permissions_update=permissions_update
    )


def can_view_client_or_admin(user, data_id):
    """Is admin or can view centre."""
    return user_is_admin(user) or client_exists_and_user_can_view(user, data_id)


def can_view_client_permissions(user, data_id):
    """Is admin or can view centre with permissions."""
    return user_is_admin(user) or client_exists_and_user_can_view(
        user, data_id, permissions_update=True
    )


def user_can_view_client_from_client_need(user, data_id, client_centres=None):
    """Validate user can view client with provided clientNeed."""
    client_need = ClientNeed.objects.get(id=data_id)
    client = client_need.client
    return user_can_view_client(user, client, client_centres)


def user_can_view_client_from_next_of_kin(user, data_id, client_centres=None):
    """Validate user can view client with provided nextOfKin."""
    next_of_kin = NextOfKin.objects.get(id=data_id)
    client = next_of_kin.client
    return user_can_view_client(user, client, client_centres)


def user_can_view_client_from_medication(user, data_id, client_centres=None):
    """Validate user can view client with provided medication."""
    medication = Medication.objects.get(id=data_id)
    client = medication.client
    return user_can_view_client(user, client, client_centres)


def user_can_view_client_from_care_plan(user, data_id, client_centres=None):
    """Validate user can view client with provided carePlan."""
    care_plan = CarePlan.objects.get(id=data_id)
    client = care_plan.need.client
    return user_can_view_client(user, client, client_centres)


def can_view_client_document(user, document_id):
    """
    Validate user can view client document.

    Includes admins.
    """
    return user_is_admin(user) or can_edit_client_document(user, document_id)


def can_edit_client_document(user, document_id):
    """Validate user can edit client document."""
    try:
        document = ClientDocument.objects.get(id=document_id)
    except ClientDocument.DoesNotExist:
        return False
    client = document.client
    return user_can_view_client(user, client)


class CanEditClientNeed(IsClient):
    """Check if the user is allowed to view a client need."""

    def has_permission(self, request, view):
        """
        Check if user can edit client need.

        First, check if nurse/admin.
        If patch, check if subbed to needs centre.
        If post, check the provided client is subbed to the same centre
        as the user.
        """
        if super().has_permission(request, view):
            if request.method == "PATCH" and view.kwargs.get("pk"):
                client_need_id = view.kwargs.get("pk")
                return user_can_view_client_from_client_need(
                    request.user, client_need_id
                )
            if request.method == "POST" and "client" in request.data:
                client_id = request.data["client"]
                return client_exists_and_user_can_view(request.user, client_id)
            if request.method == "GET" and view.kwargs.get("client"):
                client_id = view.kwargs.get("client")
                return client_exists_and_user_can_view(request.user, client_id)
        return False


class CanEditClientNextOfKin(IsNurse):
    """Check if the user is allowed to view/edit a next of kin."""

    def has_permission(self, request, view):
        """Check if user can edit next of kin."""
        if super().has_permission(request, view):
            if request.method == "POST" and "client" in request.data:
                client_id = request.data["client"]
                return client_exists_and_user_can_view(request.user, client_id)
            nok_id = view.kwargs.get("pk")
            return user_can_view_client_from_next_of_kin(request.user, nok_id)
        return False


class CanEditClientMedication(IsNurse):
    """Check if the user is allowed to view/edit a medication."""

    def has_permission(self, request, view):
        """Check if user can edit medication."""
        if super().has_permission(request, view):
            if request.method == "POST" and "client" in request.data:
                client_id = request.data["client"]
                return client_exists_and_user_can_view(request.user, client_id)
            nok_id = view.kwargs.get("pk")
            return user_can_view_client_from_medication(request.user, nok_id)
        return False


class CanEditClientFeedingRisk(IsNurse):
    """Check if the user is allowed to post a feeding rick."""

    def has_permission(self, request, view):
        """Check if user can edit feeding risk."""
        if super().has_permission(request, view):
            has_permission_for_all_feeding_risks = True
            for feeding_risk in request.data:
                if "client" not in feeding_risk:
                    has_permission_for_all_feeding_risks = False
                    break
                client_id = feeding_risk["client"]
                if not client_exists_and_user_can_view(request.user, client_id):
                    has_permission_for_all_feeding_risks = False
                    break
            return has_permission_for_all_feeding_risks
        return False


class CanEditClient(IsNurse):
    """Check if the user is allowed to edit a client."""

    def has_permission(self, request, view):
        """Check if user can edit client."""
        if super().has_permission(request, view) and view.kwargs.get("pk"):
            client_id = view.kwargs.get("pk")
            return client_exists_and_user_can_view(request.user, client_id)
        return False


class CanViewClient(IsClient):
    """Check if the user is allowed to view a client."""

    def has_permission(self, request, view):
        """Check if user can edit client."""
        if super().has_permission(request, view) and view.kwargs.get("pk"):
            client_id = view.kwargs.get("pk")
            return client_exists_and_user_can_view(request.user, client_id)
        return False


def can_change_client_centres(request):
    """Check if user can set centre."""
    client_id = request.data["client"]
    centre_id = request.data["centre"]
    return client_exists_and_user_can_view(
        request.user, client_id, client_centres=[centre_id]
    )


class CanEditClientDocuments(IsNurse):
    """Check if the user is allowed to edit a client document."""

    def has_permission(self, request, view):
        """Check if user can edit client document."""
        if super().has_permission(request, view):
            non_post_methods = ["PATCH", "PUT", "DELETE", "GET"]
            if request.method in non_post_methods and view.kwargs.get("pk"):
                client_document_id = view.kwargs.get("pk")
                return can_edit_client_document(request.user, client_document_id)
            if request.method == "POST" and "client" in request.data:
                client_id = request.data["client"]
                return client_exists_and_user_can_view(request.user, client_id)
        return False


class CanEditCarePlan(IsNurse):
    """Check if the user is allowed to edit a care plan."""

    def has_permission(self, request, view):
        """Check if user can edit care plan."""
        if super().has_permission(request, view):
            if request.method in ("PATCH", "PUT") and view.kwargs.get("pk"):
                care_plan_id = view.kwargs.get("pk")
                return user_can_view_client_from_care_plan(request.user, care_plan_id)
            if request.method == "GET":
                if view.kwargs.get("client"):
                    client_id = view.kwargs.get("client")
                    return client_exists_and_user_can_view(request.user, client_id)
                return True
        return False


class CanCreateAttendanceRecords(IsNurse):
    """
    Check if the user is allowed to create attendance records.

    When creating an attendance record the client id and centre is
    provided. Make sure the client is part of that centre and then
    make sure that the request user is part of that centre.

    This check is made against every attendance record and if any
    fail then a permission error is returned.
    """

    def has_permission(self, request, view):
        """Check if user can create attendance records."""
        if super().has_permission(request, view):
            has_permission_for_all_absences = True
            for absence in request.data:
                if (
                    "client" in absence
                    and "centre" in absence
                    and Client.objects.filter(id=absence["client"])
                    .filter(centres__id=absence["centre"])
                    .exists()
                    and client_exists_and_user_can_view(
                        request.user,
                        absence["client"],
                        client_centres=[absence["centre"]],
                    )
                ):
                    continue
                has_permission_for_all_absences = False
            return has_permission_for_all_absences
        return False
