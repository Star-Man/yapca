"""Scheduled tasks relating to clients."""
from datetime import datetime

import arrow
import holidays

from ..centre.models import Event
from .models import AttendanceRecord, CarePlan, Client


def uses_states(country):
    """Return if a country uses states or provinces."""
    return holidays.country_holidays(country).subdivisions != []


def get_country_holidays(country, state, years=None):
    """Get holidays for a country."""
    if years is None:
        years = []
    if state and uses_states(country):
        return holidays.country_holidays(
            country,
            subdiv=state,
            years=years,
        )
    return holidays.country_holidays(country, years=years)


def is_public_holiday_today(centre):
    """Check if the centres country is currently having a public holiday."""
    country_holidays = get_country_holidays(centre.country, centre.state)
    return arrow.now().date() in country_holidays


def is_event_today(centre):
    """Check if the any events are today for a centre."""
    date = arrow.now().date()
    events = Event.objects.filter(centre=centre)
    for event in events:
        occurrences = event.recurrences.between(
            datetime.combine(date, datetime.min.time()),
            datetime.combine(date, datetime.min.time()),
            dtstart=datetime.combine(event.start, datetime.min.time()),
            inc=True,
        )
        if len(occurrences) > 0:
            return True
    return False


def generate_care_plans(clients_visiting_on_day, day):
    """Generate care plans for clients that are visiting on provided day."""
    care_plans = []
    for client in clients_visiting_on_day:
        centres_opening_today = client.centres.filter(openingDays__day=day)
        for need in client.needs.filter(isActive=True).filter(
            centre__in=centres_opening_today
        ):
            # Check if this country uses states or provinces by querying the db
            if (
                not is_public_holiday_today(need.centre)
                or not need.centre.closedOnPublicHolidays
            ) and not is_event_today(need.centre):
                care_plans.append(CarePlan(date=arrow.now().date(), need=need))
    CarePlan.objects.bulk_create(care_plans)


def generate_attendances(inactive_clients_visiting_on_day, day):
    """Create attendance records for inactive clients for a day."""
    attendances = []
    for client in inactive_clients_visiting_on_day:
        visiting_days = client.visitingDays.filter(day__day=day)
        for visiting_day in visiting_days:
            centre = visiting_day.centre
            if (
                not is_public_holiday_today(centre) or not centre.closedOnPublicHolidays
            ) and not is_event_today(centre):
                attendances.append(
                    AttendanceRecord(
                        date=arrow.now().date(),
                        reasonForInactivity=client.reasonForInactivity,
                        active=False,
                        centre=centre,
                        client=client,
                    )
                )
    AttendanceRecord.objects.bulk_create(attendances)


def create_care_plans():
    """Create care plans/attendance records for every client."""
    day = arrow.now().format("dddd")

    clients = Client.objects.filter(setupComplete=True)
    clients_visiting_on_day = clients.filter(isActive=True)
    inactive_clients_visiting_on_day = clients.filter(isActive=False)

    generate_care_plans(clients_visiting_on_day, day)
    generate_attendances(inactive_clients_visiting_on_day, day)
