"""Signals to send websocket messages on model change."""
import datetime

from django.db.models import F, FileField
from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver
from django.forms.models import model_to_dict

from ..websockets.utils import send_to_permitted_users
from .models import CarePlan, Client, ClientDocument, ClientNeed, Medication, NextOfKin


def serialize_m2m_data(instance, data, m2m_field, m2m_id):
    """Serialize extra m2m/m21 info for websocket."""
    data_list = []
    for value in getattr(instance, m2m_field).all():
        if m2m_id:
            data_list.append(value.id)
        else:
            data_list.append(model_to_dict(value))
    data[m2m_field] = data_list
    return data


def handle_all_m2m_data(instance, data, m2m_field, m2m_id, filter_keys=None):
    """Serialize all m2m/m21 data for websocket."""
    if not filter_keys or "nextOfKin" in filter_keys:
        next_of_kin_list = []
        for kin in instance.nextOfKin.all():
            next_of_kin_list.append(model_to_dict(kin))
        data["nextOfKin"] = next_of_kin_list
    if not filter_keys or "medications" in filter_keys:
        medications_list = []
        for med in instance.medications.all():
            med_dict = model_to_dict(med)
            if med_dict["inactiveDate"]:
                med_dict["inactiveDate"] = med_dict["inactiveDate"].isoformat()
            medications_list.append(med_dict)
        data["medications"] = medications_list
    if m2m_field:
        data = serialize_m2m_data(instance, data, m2m_field, m2m_id)
    return data


CLIENT_KEYS = {
    "name": ["id", "firstName", "surname"],
    "data": [
        "gender",
        "admittedBy",
        "isActive",
        "basicInfoSetup",
        "additionalInfoSetup",
        "servicesSetup",
        "abilitySetup",
        "dietSetup",
        "setupComplete",
        "visitingDaysSetup",
        "address",
        "phoneNumber",
        "publicHealthNurseName",
        "publicHealthNursePhoneNumber",
        "homeHelpName",
        "homeHelpPhoneNumber",
        "gpName",
        "gpPhoneNumber",
        "chemistName",
        "chemistPhoneNumber",
        "medicalHistory",
        "surgicalHistory",
        "mobility",
        "toileting",
        "hygiene",
        "sight",
        "hearing",
        "usesDentures",
        "caseFormulation",
        "allergies",
        "intolerances",
        "requiresAssistanceEating",
        "thicknerGrade",
        "storageSize",
        "reasonForInactivity",
        "dateOfBirth",
        "accountStatus",
    ],
}


def filter_client_data(filter_keys, data, serialized_obj, name, instance):
    """Return a filtered client by filterKeys array"""
    for key in filter_keys:
        if key in CLIENT_KEYS["name"]:
            name[key] = serialized_obj[key]
        if key in CLIENT_KEYS["data"]:
            if key == "dateOfBirth":
                data[key] = instance.dateOfBirth.isoformat()
            else:
                data[key] = serialized_obj[key]
    return data, serialized_obj, name


def update_client_websocket(
    instance, action="updateClient", m2m_field=None, m2m_id=False, filter_keys=None
):
    """Send the updated/created client to the websocket consumer."""
    serialized_obj = model_to_dict(instance)
    serialized_obj["accountStatus"] = instance.accountStatus
    name = {}
    data = {}

    if filter_keys:
        filter_keys.append("id")
        data, serialized_obj, name = filter_client_data(
            filter_keys, data, serialized_obj, name, instance
        )
    else:
        for key in CLIENT_KEYS["name"]:
            name[key] = serialized_obj[key]
        for key in CLIENT_KEYS["data"]:
            data[key] = serialized_obj[key]
        data["createdDate"] = instance.createdDate.isoformat()
        if instance.dateOfBirth:
            data["dateOfBirth"] = instance.dateOfBirth.isoformat()
    data = handle_all_m2m_data(instance, data, m2m_field, m2m_id, filter_keys)
    send_to_permitted_users(
        {
            "can_view_client_all_info": {
                "action": action,
                "data": {**name, **data},
                "id": name["id"],
            },
            "can_view_client_basic_info": {
                "action": action,
                "data": name,
                "id": name["id"],
            },
        }
    )


def update_client_permissions(
    instance,
):
    """Send the updated/created client to the websocket consumer."""
    data = {
        "clientId": instance.id,
        "permissions": list(
            instance.user.permissions.all().values_list("codename", flat=True)
        ),
    }
    send_to_permitted_users(
        {
            "can_view_client_permissions": {
                "action": "updateClientPermissions",
                "data": data,
                "id": instance.id,
            }
        }
    )


def create_attendance_records_websocket(records):
    """Send the updated/created attendance record to the websocket consumer."""
    serialized_records = []
    client_id = None
    for record in records:
        client_id = record.client.id
        serialized_obj = model_to_dict(record)
        serialized_obj["client"] = client_id
        serialized_obj["centre"] = record.centre.id
        serialized_obj["date"] = record.date.isoformat()
        serialized_records.append(serialized_obj)
    send_to_permitted_users(
        {
            "can_view_client_attendance_records": {
                "action": "createAttendanceRecords",
                "data": serialized_records,
                "id": client_id,
            }
        }
    )


def save_client_visiting_days(visiting_day_models, client_id):
    """
    Update websocket on update/create.

    Admins should be updated when any client changes their visiting days
    so they can be updated of active clients when attempting to change
    a centres opening days.
    """
    visiting_days = []
    for visiting_day_model in visiting_day_models:
        visiting_days.append(model_to_dict(visiting_day_model))
    send_to_permitted_users(
        {
            "can_view_client_visiting_days": {
                "action": "updateClientVisitingDays",
                "data": {"visitingDays": visiting_days, "clientId": client_id},
                "id": client_id,
            }
        }
    )


def save_client_centres(original_centres, new_centres, client_id):
    """
    Merge original and new centres for a client.

    If client X is subbed to centre Y
    and a user Z is also subbed to centre Y,
    then user Z should still be notified if client X
    unsubscribed from centre Y. This is done by combining the
    previous centre values with the new centre values.
    """
    merged_centres = list(dict.fromkeys(original_centres + new_centres))
    data = {"clientId": client_id, "centreIds": new_centres}
    send_to_permitted_users(
        {
            "can_view_client_centres": {
                "action": "updateClientCentres",
                "data": data,
                "id": client_id,
            }
        },
        client_centres=merged_centres,
    )


def update_client_need_websocket(instance):
    """Send the updated/created client need to the websocket consumer."""
    serialized_obj = model_to_dict(instance)
    send_to_permitted_users(
        {
            "can_view_client_needs": {
                "action": "updateClientNeed",
                "data": serialized_obj,
                "id": serialized_obj["id"],
            }
        }
    )


def update_care_plans_websocket(care_plans, action="updateCarePlans"):
    """Send the updated/created care plans to the websocket consumer."""
    serialized_care_plans = []
    care_plan_id = None
    for care_plan in care_plans:
        care_plan_id = care_plan.id
        serialized_obj = model_to_dict(care_plan)
        serialized_obj["client"] = care_plan.need.client.id
        serialized_obj["date"] = care_plan.date.isoformat()
        serialized_care_plans.append(serialized_obj)
    send_to_permitted_users(
        {
            "can_view_client_care_plans": {
                "action": action,
                "data": serialized_care_plans,
                "id": care_plan_id,
            }
        }
    )


@receiver(post_save, sender=ClientNeed)
def save_client_needs(
    sender, instance, created, update_fields, **kwargs
):  # pylint: disable=W0613
    """Update websocket on update/create."""
    update_client_need_websocket(instance)
    update_client_websocket(
        instance.client, m2m_field="needs", m2m_id=True, filter_keys=["needs"]
    )


@receiver(post_save, sender=NextOfKin)
def save_client_next_of_kin(
    sender, instance, created, update_fields, **kwargs
):  # pylint: disable=W0613
    """Update websocket on update/create."""
    update_client_websocket(instance.client, filter_keys=["nextOfKin"])


@receiver(post_save, sender=Medication)
def save_medications(
    sender, instance, created, update_fields, **kwargs
):  # pylint: disable=W0613
    """Update websocket on update/create."""
    update_client_websocket(instance.client, filter_keys=["medications"])


def save_feeding_risks(feeding_risks, client_id):
    """Update websocket on update/create."""
    data = {"feedingRisks": feeding_risks, "clientId": client_id}
    feeding_risk_data = {"action": "updateFeedingRisks", "data": data, "id": client_id}
    send_to_permitted_users({"can_view_client_feeding_risks": feeding_risk_data})


@receiver(post_save, sender=ClientDocument)
def save_client_documents(
    sender, instance, created, update_fields, **kwargs
):  # pylint: disable=W0613
    """Update websocket on update/create."""
    Client.objects.filter(id=instance.client.id).update(
        storageSize=F("storageSize") + instance.document.size
    )
    new_client_document_size = instance.client.storageSize + instance.document.size

    document_data = {
        "newClientSize": new_client_document_size,
        "document": {
            "client": instance.client.id,
            "createdDate": instance.createdDate.isoformat(),
            "document": str(instance.document),
            "id": instance.id,
            "name": instance.name,
            "originalName": instance.originalName,
            "size": instance.size,
        },
    }
    send_data = {
        "action": "updateClientDocument",
        "data": document_data,
        "id": document_data["document"]["id"],
    }
    send_to_permitted_users({"can_view_client_documents": send_data})


@receiver(post_delete)
def delete_files_when_row_deleted_from_db(sender, instance, **kwargs):
    """Delete related files when their related row is deleted in database."""
    for field in sender._meta.concrete_fields:
        if isinstance(field, FileField):
            instance_file_field = getattr(instance, field.name)
            instance_file_field.delete(False)


@receiver(post_save, sender=CarePlan)
def save_care_plans(
    sender, instance, created, update_fields, **kwargs
):  # pylint: disable=W0613
    """Update websocket on update/create."""
    update_care_plan_websocket(instance)


def update_care_plan_websocket(instance):
    """Send the updated/created care plan to the websocket consumer."""
    serialized_obj = model_to_dict(instance)
    serialized_obj["client"] = instance.need.client.id
    if isinstance(serialized_obj["date"], datetime.date):
        serialized_obj["date"] = instance.date.isoformat()
    care_plan_data = {
        "action": "updateCarePlan",
        "data": serialized_obj,
        "id": serialized_obj["id"],
    }
    send_to_permitted_users({"can_view_client_care_plans": care_plan_data})
