from datetime import date, datetime, time, timedelta

import pytz
from django.db import migrations
from django_q.models import Schedule

timezone = pytz.timezone("Europe/Dublin")
dt_now = date.today()
midnight_without_tzinfo = datetime.combine(dt_now, time()) + timedelta(1)


def create_schedule(apps, schema_editor):
    Schedule.objects.create(
        func="app.client.tasks.create_care_plans",
        schedule_type=Schedule.DAILY,
        repeats=-1,
        next_run=(timezone.localize(midnight_without_tzinfo)),
    )


class Migration(migrations.Migration):

    dependencies = [
        ("client", "0004_careplan"),
        ("django_q", "0017_task_cluster_alter"),
    ]

    operations = [
        migrations.RunPython(create_schedule),
    ]
