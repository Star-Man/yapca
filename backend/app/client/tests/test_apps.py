"""apps.py Tests."""

from django.apps import apps
from django.test import TestCase

from ..apps import ClientAppConfig


class ClientAppConfigTestCase(TestCase):
    """Tests for the client app configuration."""

    def test_name(self):
        """Tests name is correct."""
        self.assertEqual(ClientAppConfig.name, "app.client")
        self.assertEqual(apps.get_app_config("client").name, "app.client")
