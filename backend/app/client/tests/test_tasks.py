"""Task Tests."""
from datetime import timedelta

import arrow
import mock
import recurrence
from django.test import TestCase

from ...authentication.models import CustomUser
from ...centre.models import Centre, Day, Event, Need
from ..models import AttendanceRecord, CarePlan, Client, ClientNeed, ClientVisitingDays
from ..tasks import create_care_plans, get_country_holidays, uses_states

NEED_PLAN = "Example Need Plan"
RECURRENCE_WEEKLY = recurrence.Rule(recurrence.WEEKLY)
REASON_FOR_INACTIVITY = "No Longer Attending"
CLOSE_EVENT = "User Close Event"


def create_inactive_clients(test_centre, test_centre_2):
    """Create various inactive clients for test."""
    today = arrow.now().format("dddd")
    tomorrow = arrow.now().shift(days=1).format("dddd")
    centre_day_today = Day.objects.get(day=today)
    centre_day_tomorrow = Day.objects.get(day=tomorrow)
    test_user = CustomUser.objects.first()
    inactive_client_visiting_today = Client.objects.create(
        admittedBy=test_user,
        setupComplete=True,
        isActive=False,
        reasonForInactivity=REASON_FOR_INACTIVITY,
    )
    inactive_client_visiting_today.centres.add(test_centre, test_centre_2)
    ClientVisitingDays.objects.create(
        client=inactive_client_visiting_today, day=centre_day_today, centre=test_centre
    )
    ClientVisitingDays.objects.create(
        client=inactive_client_visiting_today,
        day=centre_day_today,
        centre=test_centre_2,
    )
    incomplete_inactive_client = Client.objects.create(
        admittedBy=test_user, setupComplete=False, isActive=False
    )
    ClientVisitingDays.objects.create(
        client=incomplete_inactive_client, day=centre_day_today, centre=test_centre
    )
    inactive_client_visiting_tomorrow = Client.objects.create(
        admittedBy=test_user, setupComplete=True
    )
    ClientVisitingDays.objects.create(
        client=inactive_client_visiting_tomorrow,
        day=centre_day_tomorrow,
        centre=test_centre,
    )
    return inactive_client_visiting_today


def create_centres(centre_day_today, centre_day_tomorrow):
    """Create centres and related needs."""
    test_need_1 = Need.objects.create(description="Example Need 1")
    test_need_2 = Need.objects.create(description="Example Need 2")
    test_need_3 = Need.objects.create(description="Example Need 3")

    test_centre = Centre.objects.create(country="US", state="CA")
    test_centre.openingDays.add(centre_day_today.id, centre_day_tomorrow.id)
    test_centre.needs.add(test_need_1.id)
    test_centre.needs.add(test_need_2.id)
    test_centre.needs.add(test_need_3.id)

    test_centre_2 = Centre.objects.create(
        name="Test Centre 2", country="US", state="CA"
    )
    test_centre_2.openingDays.add(centre_day_today.id, centre_day_tomorrow.id)
    test_centre_2.needs.add(test_need_1.id)
    test_centre_2.needs.add(test_need_2.id)
    test_centre_2.needs.add(test_need_3.id)
    return (test_centre, test_centre_2, test_need_1, test_need_2, test_need_3)


def get_centre_days():
    """Get today and tomorrow as days from the day table."""
    today = arrow.now().format("dddd")
    tomorrow = arrow.now().shift(days=1).format("dddd")
    centre_day_today = Day.objects.get(day=today)
    centre_day_tomorrow = Day.objects.get(day=tomorrow)
    return (centre_day_today, centre_day_tomorrow)


def create_clients():
    """Create various clients for test."""
    (centre_day_today, centre_day_tomorrow) = get_centre_days()
    test_user = CustomUser.objects.create(username="testUser")
    (
        test_centre,
        test_centre_2,
        test_need_1,
        test_need_2,
        test_need_3,
    ) = create_centres(centre_day_today, centre_day_tomorrow)

    client_visiting_today = Client.objects.create(
        admittedBy=test_user,
        setupComplete=True,
    )
    client_visiting_today.centres.add(test_centre, test_centre_2)
    ClientVisitingDays.objects.create(
        client=client_visiting_today, day=centre_day_today, centre=test_centre
    )
    client_need_1 = ClientNeed.objects.create(
        client=client_visiting_today,
        need=test_need_1,
        centre=test_centre,
        plan=NEED_PLAN,
    )
    client_need_2 = ClientNeed.objects.create(
        client=client_visiting_today,
        need=test_need_2,
        centre=test_centre,
        plan=NEED_PLAN,
    )
    ClientNeed.objects.create(
        client=client_visiting_today,
        need=test_need_3,
        centre=test_centre,
        plan=NEED_PLAN,
        isActive=False,
    )
    client_need_3 = ClientNeed.objects.create(
        client=client_visiting_today,
        need=test_need_1,
        centre=test_centre_2,
        plan=NEED_PLAN,
    )
    incomplete_client = Client.objects.create(admittedBy=test_user, setupComplete=False)
    inactive_client = Client.objects.create(admittedBy=test_user, isActive=False)
    ClientVisitingDays.objects.create(
        client=incomplete_client, day=centre_day_today, centre=test_centre
    )
    ClientNeed.objects.create(
        client=incomplete_client, need=test_need_1, centre=test_centre, plan=NEED_PLAN
    )
    ClientNeed.objects.create(
        client=incomplete_client, need=test_need_2, centre=test_centre, plan=NEED_PLAN
    )
    ClientVisitingDays.objects.create(
        client=inactive_client, day=centre_day_today, centre=test_centre
    )
    ClientNeed.objects.create(
        client=inactive_client, need=test_need_1, centre=test_centre, plan=NEED_PLAN
    )
    ClientNeed.objects.create(
        client=inactive_client, need=test_need_2, centre=test_centre, plan=NEED_PLAN
    )
    client_visiting_tomorrow = Client.objects.create(
        admittedBy=test_user, setupComplete=True
    )
    ClientVisitingDays.objects.create(
        client=client_visiting_tomorrow, day=centre_day_tomorrow, centre=test_centre
    )
    ClientNeed.objects.create(
        client=client_visiting_tomorrow,
        need=test_need_1,
        centre=test_centre,
        plan=NEED_PLAN,
    )
    ClientNeed.objects.create(
        client=client_visiting_tomorrow,
        need=test_need_2,
        centre=test_centre,
        plan=NEED_PLAN,
    )
    return (client_need_1, client_need_2, client_need_3, test_centre, test_centre_2)


class CreateCarePlansTestCase(TestCase):
    """Tests for the create_care_plans tasks."""

    @mock.patch("app.client.tasks.is_public_holiday_today", return_value=True)
    def test_closing_event_exists(self, _):
        """Care plans/Attendances won't be generated if closing event exists."""
        (_, _, _, test_centre, test_centre_2) = create_clients()
        create_inactive_clients(test_centre, test_centre_2)
        test_user = CustomUser.objects.first()
        Event.objects.create(
            name="event 2",
            centre=test_centre,
            start=arrow.utcnow().shift(days=-2).datetime,
            allDay=True,
            eventType=CLOSE_EVENT,
            createdBy=test_user,
            timePeriod=timedelta(days=1),
            recurrences=recurrence.Recurrence(
                rrules=[RECURRENCE_WEEKLY],
            ),
        )
        Event.objects.create(
            name="event 1",
            centre=test_centre,
            start=arrow.now().shift(days=-7).datetime,
            allDay=True,
            eventType=CLOSE_EVENT,
            createdBy=test_user,
            timePeriod=timedelta(days=1),
            recurrences=recurrence.Recurrence(
                rrules=[RECURRENCE_WEEKLY],
            ),
        )
        test_centre.closedOnPublicHolidays = False
        test_centre.save()
        create_care_plans()
        self.assertEqual(CarePlan.objects.all().count(), 0)
        self.assertEqual(AttendanceRecord.objects.all().count(), 0)

    @mock.patch("app.client.tasks.is_public_holiday_today", return_value=False)
    def test_create_care_plans_creates_plans_for_visitors_on_day(self, _):
        """Client object should create successfully."""
        (
            client_need_1,
            client_need_2,
            client_need_3,
            _,
            _,
        ) = create_clients()
        create_care_plans()
        care_plans = CarePlan.objects.all().order_by("need_id")
        self.assertEqual(care_plans.count(), 3)
        self.assertEqual(
            list(care_plans.values()),
            [
                {
                    "id": care_plans.values()[0]["id"],
                    "date": arrow.now().date(),
                    "need_id": client_need_1.id,
                    "comment": "",
                    "completed": False,
                    "inputted": False,
                    "addedBy_id": None,
                },
                {
                    "id": care_plans.values()[1]["id"],
                    "date": arrow.now().date(),
                    "need_id": client_need_2.id,
                    "comment": "",
                    "completed": False,
                    "inputted": False,
                    "addedBy_id": None,
                },
                {
                    "id": care_plans.values()[2]["id"],
                    "date": arrow.now().date(),
                    "need_id": client_need_3.id,
                    "comment": "",
                    "completed": False,
                    "inputted": False,
                    "addedBy_id": None,
                },
            ],
        )

    @mock.patch("app.client.tasks.is_public_holiday_today", return_value=False)
    def test_create_care_plans_creates_attendances_for_inactive_clients(self, _):
        """Attendance Record should create successfully."""
        (
            _,
            _,
            _,
            test_centre,
            test_centre_2,
        ) = create_clients()
        inactive_client_visiting_today = create_inactive_clients(
            test_centre, test_centre_2
        )
        create_care_plans()
        self.assertEqual(AttendanceRecord.objects.all().count(), 2)
        self.assertEqual(
            list(AttendanceRecord.objects.all().values()),
            [
                {
                    "id": AttendanceRecord.objects.all().values()[0]["id"],
                    "client_id": inactive_client_visiting_today.id,
                    "centre_id": test_centre.id,
                    "date": arrow.now().date(),
                    "active": False,
                    "reasonForInactivity": REASON_FOR_INACTIVITY,
                    "reasonForAbsence": "",
                    "addedBy_id": None,
                },
                {
                    "id": AttendanceRecord.objects.all().values()[1]["id"],
                    "client_id": inactive_client_visiting_today.id,
                    "centre_id": test_centre_2.id,
                    "date": arrow.now().date(),
                    "active": False,
                    "reasonForInactivity": REASON_FOR_INACTIVITY,
                    "reasonForAbsence": "",
                    "addedBy_id": None,
                },
            ],
        )

    @mock.patch(
        "app.client.tasks.get_country_holidays", return_value=[arrow.now().date()]
    )
    def test_care_plans_centre_closed_on_public_holidays(self, _):
        """Care plans shouldn't be generated when closedOnPublicHolidays and holiday."""
        (
            _,
            _,
            _,
            test_centre,
            test_centre_2,
        ) = create_clients()
        create_inactive_clients(test_centre, test_centre_2)
        create_care_plans()
        self.assertEqual(CarePlan.objects.all().count(), 0)
        self.assertEqual(AttendanceRecord.objects.all().count(), 0)

    @mock.patch("app.client.tasks.is_public_holiday_today", return_value=True)
    def test_care_plans_centre_open_on_public_holidays(self, _):
        """Care plans should be generated when closedOnPublicHolidays false."""
        (
            _,
            _,
            _,
            test_centre,
            _,
        ) = create_clients()
        test_centre.closedOnPublicHolidays = False
        test_centre.save()
        create_care_plans()
        self.assertEqual(CarePlan.objects.all().count(), 2)

    @mock.patch("app.client.tasks.is_public_holiday_today", return_value=True)
    def test_attendance_centre_open_on_public_holidays(self, _):
        """Attendnace should be generated when closedOnPublicHolidays false."""
        (
            _,
            _,
            _,
            test_centre,
            test_centre_2,
        ) = create_clients()
        create_inactive_clients(test_centre, test_centre_2)
        test_centre.closedOnPublicHolidays = False
        test_centre.save()
        create_care_plans()
        self.assertEqual(AttendanceRecord.objects.all().count(), 1)

    def test_uses_states(self):
        """Should return true or false if country uses states."""
        create_clients()
        self.assertTrue(uses_states("US"))
        self.assertTrue(uses_states("AU"))
        self.assertFalse(uses_states("IE"))

    def test_get_country_holidays(self):
        """Should get holidays for state and country ."""
        create_clients()
        country_holidays = get_country_holidays("US", "CA")
        self.assertEqual(country_holidays.get("2020-03-31"), "César Chávez Day")
        country_holidays = get_country_holidays("AU", "QLD")
        self.assertEqual(
            country_holidays.get("2020-08-14"), "The Royal Queensland Show"
        )
        country_holidays = get_country_holidays("BE", "")
        self.assertEqual(country_holidays.get("2020-06-01"), "Pinkstermaandag")

    @mock.patch("app.client.tasks.is_public_holiday_today", return_value=True)
    def test_closing_event_exists_different_centre(self, _):
        """Closing Events for other centres are ignored."""
        (
            _,
            _,
            _,
            test_centre,
            test_centre_2,
        ) = create_clients()
        create_inactive_clients(test_centre, test_centre_2)
        test_centre.closedOnPublicHolidays = False
        test_centre.save()
        test_centre_2 = Centre.objects.create(name="Centre 2", country="US", state="CA")
        test_centre_2.closedOnPublicHolidays = False
        test_centre_2.save()
        test_user = CustomUser.objects.first()

        Event.objects.create(
            name="event 2",
            centre=test_centre_2,
            start=arrow.now().shift(days=-2).datetime,
            allDay=True,
            eventType=CLOSE_EVENT,
            createdBy=test_user,
            timePeriod=timedelta(days=1),
            recurrences=recurrence.Recurrence(
                rrules=[RECURRENCE_WEEKLY],
            ),
        )
        Event.objects.create(
            name="event 1",
            centre=test_centre_2,
            start=arrow.now().shift(days=-7).datetime,
            allDay=True,
            eventType=CLOSE_EVENT,
            createdBy=test_user,
            timePeriod=timedelta(days=1),
            recurrences=recurrence.Recurrence(
                rrules=[RECURRENCE_WEEKLY],
            ),
        )

        create_care_plans()
        self.assertEqual(CarePlan.objects.all().count(), 2)
        self.assertEqual(AttendanceRecord.objects.all().count(), 1)
