"""Profile Model Tests."""
import datetime

from django.core.exceptions import ValidationError
from django.test import TestCase

from ...authentication.models import CustomUser
from ...centre.models import Centre, Day, Policy
from ..models import Client, ClientAgreedPolicies, ClientVisitingDays, NextOfKin


class ClientTestCase(TestCase):
    """Tests for the Client model."""

    def test_creating_client_object(self):
        """Client object should create successfully."""
        test_policy = Policy.objects.create(description="Example Description")

        test_user = CustomUser.objects.create(username="testUser")

        test_centre = Centre.objects.create()
        test_centre.save()
        test_centre.openingDays.add(1, 2, 5, 6)
        test_centre.policies.add(test_policy.id)

        test_client = Client.objects.create(
            admittedBy=test_user,
            dateOfBirth=datetime.date(1977, 10, 19),
            address="France",
            phoneNumber="0118 999 881 999 119",
            reasonForInactivity="No Longer Attending",
        )
        NextOfKin.objects.create(
            client=test_client, name="Jregory", phoneNumber="111111111"
        )
        NextOfKin.objects.create(
            client=test_client, name="Jremory", phoneNumber="222222222"
        )
        ClientAgreedPolicies.objects.create(
            client=test_client, policy=test_policy, centre=test_centre
        )
        wednesday = Day.objects.get(pk=3)
        friday = Day.objects.get(pk=5)
        ClientVisitingDays.objects.create(
            client=test_client, day=wednesday, centre=test_centre
        )
        ClientVisitingDays.objects.create(
            client=test_client, day=friday, centre=test_centre
        )
        test_client.centres.add(test_centre.id)
        test_client.save()

        self.assertEqual(test_client.admittedBy.username, "testUser")
        self.assertEqual(test_client.agreedPolicies.count(), 1)
        self.assertEqual(test_client.centres.count(), 1)
        self.assertEqual(test_client.visitingDays.count(), 2)
        self.assertEqual(test_client.isActive, True)
        self.assertEqual(
            test_client.agreedPolicies.all().values()[0]["policy_id"], test_policy.id
        )
        self.assertEqual(
            test_client.agreedPolicies.all().values()[0]["centre_id"], test_centre.id
        )
        self.assertEqual(
            test_client.agreedPolicies.all().values()[0]["policy_id"], test_policy.id
        )
        self.assertEqual(test_client.dateOfBirth, datetime.date(1977, 10, 19))
        self.assertEqual(test_client.address, "France")
        self.assertEqual(test_client.phoneNumber, "0118 999 881 999 119")
        self.assertEqual(test_client.reasonForInactivity, "No Longer Attending")
        self.assertEqual(test_client.nextOfKin.all().values()[0]["name"], "Jregory")
        self.assertEqual(
            test_client.nextOfKin.all().values()[1]["phoneNumber"], "222222222"
        )

    def test_missing_data(self):
        """Should Error when all policies not added."""
        test_client = Client()
        self.assertRaises(ValidationError, test_client.full_clean)
