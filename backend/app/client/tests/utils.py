"""Test Util functions."""
import re

from ...authentication.models import CustomUser
from ..models import Client


def validate_iso8601(str_val):
    """Validate a passed string is in the IS0-8601 format."""
    regex = (
        r"^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])"
        + r"T(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\.[0-9]+)?(Z|[+-](?:2[0-3]"
        + r"|[01][0-9]):[0-5][0-9])?$"
    )
    match_iso8601 = re.compile(regex).match
    return match_iso8601(str_val) is not None


def create_client(centres=None, first_name="test", surname="client"):
    """Create a centre and add a user to it."""
    admitted_by = CustomUser.objects.first()
    client = Client.objects.create(
        admittedBy=admitted_by, firstName=first_name, surname=surname
    )
    if centres:
        for centre in centres:
            client.centres.add(centre)
    return client
