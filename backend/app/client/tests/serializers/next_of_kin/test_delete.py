"""Tests for sending get requests to the NextOfKinSerializer."""
from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_test_user
from .....centre.tests.utils import create_centre
from ....models import NextOfKin
from ...utils import create_client

NO_AUTH_MESSAGE = "Authentication credentials were not provided."


class NextOfKinSerializerDeleteTestCase(TestCase):
    """Tests for sending delete requests to the NextOfKinSerializer."""

    url = "/api/nextOfKin/"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        self.user = CustomUser.objects.get(id=token.data["user"]["id"])
        centre = create_centre(users=[self.user])
        test_client = create_client(centres=[centre])
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.data["key"])
        self.next_of_kin = NextOfKin.objects.create(
            client=test_client, name="Jregory", phoneNumber="111111111"
        )

    def test_valid_delete(self):
        """Test sending a valid delete request to the serializer."""
        response = self.client.delete(f"{self.url}{self.next_of_kin.id}/delete/")
        self.assertEqual(response.status_code, 204)
        self.assertEqual(NextOfKin.objects.filter(name="Jregory").exists(), False)

    def test_no_auth_delete(self):
        """Should be unable to delete data when not authenticated."""
        self.client.credentials()
        response = self.client.delete(f"{self.url}{self.next_of_kin.id}/delete/")
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {"detail": ErrorDetail(string=NO_AUTH_MESSAGE, code="not_authenticated",)},
        )
        self.assertEqual(NextOfKin.objects.filter(name="Jregory").exists(), True)

    def test_no_client_delete(self):
        """Should be unable to delete data when not subbed to client centre."""
        self.user.centres.clear()
        response = self.client.delete(f"{self.url}{self.next_of_kin.id}/delete/")
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )
        self.assertEqual(NextOfKin.objects.filter(name="Jregory").exists(), True)
