"""Tests for sending put/patch requests to the NextOfKinSerializer."""
from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_test_user
from .....centre.tests.utils import create_centre
from ....models import NextOfKin
from ...utils import create_client


class NextOfKinSerializerPutTestCase(TestCase):
    """Tests for sending put/patch requests to the NextOfKinSerializer."""

    url = "/api/nextOfKin/"
    policyDescription = "Example Description"
    nextOfKinName = "Test Name"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        self.user = CustomUser.objects.get(id=token.data["user"]["id"])
        centre = create_centre(users=[self.user])
        self.test_client = create_client(centres=[centre])
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.data["key"])
        self.next_of_kin = NextOfKin.objects.create(
            client=self.test_client, name="Jregory", phoneNumber="111111111"
        )

    def test_no_auth_put(self):
        """Should be unable to put data when not authenticated."""
        self.client.credentials()
        response = self.client.put(f"{self.url}{self.test_client.id}/update/", {},)
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="Authentication credentials were not provided.",
                    code="not_authenticated",
                )
            },
        )

    def test_valid_patch(self):
        """Test sending a valid patch request to the serializer."""
        response = self.client.patch(
            f"{self.url}{self.next_of_kin.id}/update/",
            {"phoneNumber": "0271561", "name": self.nextOfKinName},
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        next_of_kin = NextOfKin.objects.get(pk=response.data["id"])
        self.assertEqual(next_of_kin.client, self.test_client)
        self.assertEqual(next_of_kin.name, self.nextOfKinName)
        self.assertEqual(next_of_kin.phoneNumber, "0271561")
        self.assertEqual(
            response.data,
            {
                "id": next_of_kin.id,
                "name": self.nextOfKinName,
                "phoneNumber": "0271561",
                "client": self.test_client.id,
            },
        )

    def test_no_client_put(self):
        """Should be unable to put data when not subbed to client centre."""
        self.user.centres.clear()
        response = self.client.patch(
            f"{self.url}{self.next_of_kin.id}/update/",
            {"phoneNumber": "0271561", "name": self.nextOfKinName},
            format="json",
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )
