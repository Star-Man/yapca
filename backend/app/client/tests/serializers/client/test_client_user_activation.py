"""Client User Activation Serializer Tests."""

import time

from django.core import mail
from django.test import TestCase
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_client_user, register_test_user
from .....centre.tests.utils import create_centre

REGISTER_URL = "/api/registration/"
TOKEN_AUTH = "Token "
NO_AUTH_MESSAGE = "Authentication credentials were not provided."
NEED_DESCRIPTION = "Example Description"
DISPLAY_NAME_2 = "Test User 2"
CONTENT_TYPE = "application/json"


class ClientUserActivationSerializerTestCase(TestCase):  # pylint: disable=R0902
    """Tests for ClientUserActivationSerializer."""

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client, email="useremail@email.com")
        client_token = register_client_user(self.client, email="clientemail@email.com")
        user = CustomUser.objects.get(id=token.data["user"]["id"])
        centre = create_centre(users=[user], name="centre1")
        self.test_client_user = CustomUser.objects.get(
            id=client_token.data["user"]["id"]
        )
        self.test_client_user.client.centres.add(centre)
        self.client.credentials(HTTP_AUTHORIZATION=TOKEN_AUTH + token.data["key"])

    def test_valid_deactivate(self):
        """Test sending a valid deactivate request to the serializer."""

        response = self.client.post(
            f"/api/set-client-user-activation/{self.test_client_user.client.id}/",
            {
                "active": False,
                "sendEmail": True,
            },
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        self.test_client_user.refresh_from_db()
        self.assertFalse(self.test_client_user.is_active)
        email = mail.outbox[2]
        self.assertIn(
            "An administrator has deactivated your YAPCA account",
            email.body,
        )
        self.assertIn(
            "Hello test!",
            email.body,
        )

    def test_valid_reactivate(self):
        """Test sending a valid reactivate request to the serializer."""
        self.test_client_user.is_active = False
        self.test_client_user.save()
        response = self.client.post(
            f"/api/set-client-user-activation/{self.test_client_user.client.id}/",
            {
                "active": True,
                "sendEmail": True,
            },
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        self.test_client_user.refresh_from_db()
        self.assertTrue(self.test_client_user.is_active)
        email = mail.outbox[2]
        self.assertIn(
            "An administrator has reactivated your YAPCA account",
            email.body,
        )
        self.assertIn(
            "Hello test!",
            email.body,
        )

    def test_invalid_centres(self):
        """Cannot post when invalid centres."""
        centre_2 = create_centre(name="centre2")
        self.test_client_user.client.centres.set([centre_2])
        response = self.client.post(
            f"/api/set-client-user-activation/{self.test_client_user.client.id}/",
            {
                "active": False,
                "sendEmail": True,
            },
            format="json",
        )
        self.assertEqual(response.status_code, 403)
        self.test_client_user.refresh_from_db()
        self.assertTrue(self.test_client_user.is_active)

    def test_email(self):
        """Can prevent user being notified by email."""
        self.client.post(
            f"/api/set-client-user-activation/{self.test_client_user.client.id}/",
            {
                "active": False,
                "sendEmail": False,
            },
            format="json",
        )
        time.sleep(0.1)
        self.assertEqual(len(mail.outbox), 2)
