"""Tests for sending get requests to the ClientSerializer."""
from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_test_user
from .....centre.tests.utils import create_centre
from ....models import Client
from ...utils import create_client

NO_AUTH_MESSAGE = "Authentication credentials were not provided."


class ClientSerializerDeleteTestCase(TestCase):
    """Tests for sending delete requests to the ClientSerializer."""

    url = "/api/clients/"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        self.user = CustomUser.objects.get(id=token.data["user"]["id"])
        centre = create_centre(users=[self.user])
        self.test_client = create_client(centres=[centre])
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.data["key"])

    def test_valid_delete(self):
        """Test sending a valid delete request to the serializer."""
        response = self.client.delete(f"{self.url}{self.test_client.id}/delete/")
        self.assertEqual(response.status_code, 204)
        self.assertEqual(Client.objects.filter(id=self.test_client.id).exists(), False)

    def test_no_auth_delete(self):
        """Should be unable to delete data when not authenticated."""
        self.client.credentials()
        response = self.client.delete(f"{self.url}{self.test_client.id}/delete/")
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {"detail": ErrorDetail(string=NO_AUTH_MESSAGE, code="not_authenticated",)},
        )
        self.assertEqual(Client.objects.filter(id=self.test_client.id).exists(), True)

    def test_no_client_delete(self):
        """Should be unable to delete data when not subbed to client centre."""
        self.user.centres.clear()
        response = self.client.delete(f"{self.url}{self.test_client.id}/delete/")
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )
        self.assertEqual(Client.objects.filter(id=self.test_client.id).exists(), True)
