"""Tests for sending get requests to the ClientSerializer."""
import datetime
import glob
import os
import pathlib
from collections import OrderedDict

from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_client_user, register_test_user
from .....centre.models import Centre, Day, Need, Policy
from .....centre.tests.utils import create_centre
from ....models import (
    Client,
    ClientAgreedPolicies,
    ClientDocument,
    ClientNeed,
    ClientVisitingDays,
    FeedingRisk,
    Medication,
    NextOfKin,
)
from ...utils import create_client as create_new_client
from ...utils import validate_iso8601

URL = "/api/clients/"
POLICY_DESCRIPTION = "Example Description"
NEED_DESCRIPTION = "Example Need"
NEED_PLAN = "Example Need Plan"
CENTRE_NAME = "Test Centre"
PUBLIC_HEALTH_NURSE_NAME = "PHN Name"
PUBLIC_HEALTH_NURSE_PHONE_NUMBER = "PHN Number"
HOME_HELP_NAME = "HH Name"
HOME_HELP_PHONE_NUMBER = "HH Number"
GP_NAME = "GP Name"
GP_PHONE_NUMBER = "GP Number"
CHEMIST_NAME = "C Name"
CHEMIST_PHONE_NUMBER = "C Number"
MEDICAL_HISTORY = "Mx History"
SURGICAL_HISTORY = "Sx History"
MOBILITY = "Walks With Walker"
TOILETING = "Incontinent"
HYGIENE = "Self"
SIGHT = "Glasses"
HEARING = "Normal"
CASE_FORMULATION = "Example Case Formultion"
ALLERGIES = "Example Allergies"
INTOLERANCES = "Example Intolerances"
REASON_FOR_INACTIVITY = "Needs Unmet By Service"
TOKEN_AUTH = "Token "


def create_client():
    """Create a client with extra detail fields."""
    test_policy = Policy.objects.create(description=POLICY_DESCRIPTION)
    test_need = Need.objects.create(description=NEED_DESCRIPTION)
    test_user = CustomUser.objects.get(username="testUser1")
    monday = Day.objects.get(pk=1)
    wednesday = Day.objects.get(pk=3)
    friday = Day.objects.get(pk=5)
    test_centre = Centre.objects.create()

    test_centre.save()
    test_centre.openingDays.add(1, 2, 5, 6)

    test_centre.policies.add(test_policy.id)
    test_centre.needs.add(test_need.id)
    test_user.centres.add(test_centre)
    test_client = Client.objects.create(
        admittedBy=test_user,
        firstName="Second",
        surname="Client",
        gender="Female",
        dateOfBirth=datetime.date(1988, 10, 19),
        address="Macedonia",
        phoneNumber="0118 999 881 999 119",
        publicHealthNurseName=PUBLIC_HEALTH_NURSE_NAME,
        publicHealthNursePhoneNumber=PUBLIC_HEALTH_NURSE_PHONE_NUMBER,
        homeHelpName=HOME_HELP_NAME,
        homeHelpPhoneNumber=HOME_HELP_PHONE_NUMBER,
        gpName=GP_NAME,
        gpPhoneNumber=GP_PHONE_NUMBER,
        chemistName=CHEMIST_NAME,
        chemistPhoneNumber=CHEMIST_PHONE_NUMBER,
        medicalHistory=MEDICAL_HISTORY,
        surgicalHistory=SURGICAL_HISTORY,
        mobility=MOBILITY,
        toileting=TOILETING,
        hygiene=HYGIENE,
        sight=SIGHT,
        hearing=HEARING,
        usesDentures=True,
        caseFormulation=CASE_FORMULATION,
        allergies=ALLERGIES,
        intolerances=INTOLERANCES,
        requiresAssistanceEating=True,
        thicknerGrade=3,
        storageSize=500,
        reasonForInactivity=REASON_FOR_INACTIVITY,
    )
    medication = Medication.objects.create(
        client=test_client, name="Example Med", dosage="300mg", frequency="daily"
    )
    document = ClientDocument.objects.create(
        client=test_client,
        name="Example Document",
        size=200,
        document=SimpleUploadedFile("example_file.txt", b"these are the file contents"),
    )
    feeding_risk = FeedingRisk.objects.create(client=test_client, feedingRisk="Chewing")
    NextOfKin.objects.create(client=test_client, name="Magory", phoneNumber="33333333")
    NextOfKin.objects.create(client=test_client, name="Orogary", phoneNumber="44444444")
    test_client.centres.add(test_centre.id)
    ClientAgreedPolicies.objects.create(
        client=test_client, policy=test_policy, centre=test_centre
    )
    client_need = ClientNeed.objects.create(
        client=test_client, need=test_need, centre=test_centre, plan=NEED_PLAN
    )
    ClientVisitingDays.objects.create(
        client=test_client, day=monday, centre=test_centre
    )
    ClientVisitingDays.objects.create(
        client=test_client, day=wednesday, centre=test_centre
    )
    ClientVisitingDays.objects.create(
        client=test_client, day=friday, centre=test_centre
    )
    return (
        test_client,
        test_user,
        test_centre,
        test_policy,
        medication,
        feeding_risk,
        test_need,
        document,
        client_need,
    )


class ClientSerializerGetTestCase(TestCase):
    """Tests for sending get requests to the ClientSerializer."""

    def validate_clients(self, response, test_centre):
        """Validate clients data."""
        self.assertEqual(len(response.data), 1)
        self.assertNotIn("agreedPolicies", response.data[0])
        self.assertNotIn("needs", response.data[0])
        self.assertNotIn("medicalHistory", response.data[0])
        self.assertNotIn("surgicalHistory", response.data[0])
        self.assertNotIn("caseFormulation", response.data[0])
        self.assertNotIn("allergies", response.data[0])
        self.assertNotIn("intolerances", response.data[0])
        self.assertNotIn("documents", response.data[0])

        self.assertEqual(
            response.data[0]["visitingDays"],
            [
                OrderedDict([("centre", test_centre.id), ("day", 1)]),
                OrderedDict([("centre", test_centre.id), ("day", 2)]),
                OrderedDict([("centre", test_centre.id), ("day", 7)]),
            ],
        )
        self.assertEqual(response.data[0]["firstName"], "First")

    def setUp(self):
        """Create a user for auth."""
        files = glob.glob(f"{settings.MEDIA_ROOT}/*")
        for file in files:
            os.remove(file)
        self.client = APIClient()
        token = register_test_user(self.client)
        self.client.credentials(HTTP_AUTHORIZATION=TOKEN_AUTH + token.data["key"])
        settings.MEDIA_ROOT = os.path.join(
            pathlib.Path(__file__).parent.absolute(), "media"
        )

    def test_no_auth_get(self):
        """No results should return when authentication isn't provided."""
        self.client.credentials()
        response = self.client.get(
            URL,
            {},
        )
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="Authentication credentials were not provided.",
                    code="not_authenticated",
                )
            },
        )

    def test_valid_list(self):
        """Test sending a valid list request to the serializer."""
        test_policy = Policy.objects.create(description=POLICY_DESCRIPTION)
        test_need = Need.objects.create(description=NEED_DESCRIPTION)
        test_user = CustomUser.objects.get(username="testUser1")
        test_user.is_active = True
        test_user.save()

        monday = Day.objects.get(pk=1)
        tuesday = Day.objects.get(pk=2)
        wednesday = Day.objects.get(pk=3)
        friday = Day.objects.get(pk=5)
        sunday = Day.objects.get(pk=7)

        test_centre = Centre.objects.create()
        test_centre.save()
        test_centre.openingDays.add(1, 2, 5, 6)
        test_user.centres.add(test_centre)
        test_centre.policies.add(test_policy.id)
        test_centre.needs.add(test_need.id)

        test_client = Client.objects.create(
            admittedBy=test_user,
            firstName="First",
            surname="Client",
            dateOfBirth=datetime.date(1977, 10, 19),
            address="France",
            phoneNumber="0118 999 881 999 119",
        )
        test_client.centres.add(test_centre.id)
        test_client.save()
        NextOfKin.objects.create(
            client=test_client, name="Jregory", phoneNumber="111111111"
        )
        ClientAgreedPolicies.objects.create(
            client=test_client, policy=test_policy, centre=test_centre
        )
        ClientNeed.objects.create(
            client=test_client, need=test_need, centre=test_centre, plan=NEED_PLAN
        )
        ClientVisitingDays.objects.create(
            client=test_client, day=monday, centre=test_centre
        )
        ClientVisitingDays.objects.create(
            client=test_client, day=tuesday, centre=test_centre
        )
        ClientVisitingDays.objects.create(
            client=test_client, day=sunday, centre=test_centre
        )

        test_client_2 = Client.objects.create(
            admittedBy=test_user,
            firstName="Second",
            surname="Client",
            publicHealthNurseName=PUBLIC_HEALTH_NURSE_NAME,
            publicHealthNursePhoneNumber=PUBLIC_HEALTH_NURSE_PHONE_NUMBER,
            homeHelpName=HOME_HELP_NAME,
            homeHelpPhoneNumber=HOME_HELP_PHONE_NUMBER,
            gpName=GP_NAME,
            gpPhoneNumber=GP_PHONE_NUMBER,
            chemistName=CHEMIST_NAME,
            chemistPhoneNumber=CHEMIST_PHONE_NUMBER,
            medicalHistory=MEDICAL_HISTORY,
            surgicalHistory=SURGICAL_HISTORY,
            mobility=MOBILITY,
            toileting=TOILETING,
            hygiene=HYGIENE,
            sight=SIGHT,
            hearing=HEARING,
            usesDentures=True,
            caseFormulation=CASE_FORMULATION,
            allergies=ALLERGIES,
            intolerances=INTOLERANCES,
            requiresAssistanceEating=True,
            thicknerGrade=3,
            storageSize=500,
        )
        ClientAgreedPolicies.objects.create(
            client=test_client_2, policy=test_policy, centre=test_centre
        )
        ClientNeed.objects.create(
            client=test_client_2, need=test_need, centre=test_centre, plan=NEED_PLAN
        )
        ClientVisitingDays.objects.create(
            client=test_client_2, day=monday, centre=test_centre
        )
        ClientVisitingDays.objects.create(
            client=test_client_2, day=wednesday, centre=test_centre
        )
        ClientVisitingDays.objects.create(
            client=test_client_2, day=friday, centre=test_centre
        )

        response = self.client.get(URL)
        self.assertEqual(response.status_code, 200)
        self.validate_clients(response, test_centre)

    def test_valid_retreive(self):
        """Test sending a valid list request to the serializer."""
        (
            test_client,
            test_user,
            test_centre,
            test_policy,
            medication,
            feeding_risk,
            _,
            document,
            client_need,
        ) = create_client()

        response = self.client.get(f"{URL}{test_client.id}/")
        test_nok_1 = NextOfKin.objects.get(name="Magory")
        test_nok_2 = NextOfKin.objects.get(name="Orogary")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            {
                "id": test_client.id,
                "firstName": "Second",
                "surname": "Client",
                "gender": "Female",
                "admittedBy": test_user.id,
                "createdDate": response.data["createdDate"],
                "centres": [test_centre.id],
                "isActive": True,
                "visitingDays": [
                    OrderedDict([("centre", test_centre.id), ("day", 1)]),
                    OrderedDict([("centre", test_centre.id), ("day", 3)]),
                    OrderedDict([("centre", test_centre.id), ("day", 5)]),
                ],
                "basicInfoSetup": False,
                "additionalInfoSetup": False,
                "servicesSetup": False,
                "abilitySetup": False,
                "visitingDaysSetup": False,
                "setupComplete": False,
                "dateOfBirth": "1988-10-19",
                "address": "Macedonia",
                "phoneNumber": "0118 999 881 999 119",
                "publicHealthNurseName": PUBLIC_HEALTH_NURSE_NAME,
                "publicHealthNursePhoneNumber": PUBLIC_HEALTH_NURSE_PHONE_NUMBER,
                "homeHelpName": HOME_HELP_NAME,
                "homeHelpPhoneNumber": HOME_HELP_PHONE_NUMBER,
                "gpName": GP_NAME,
                "gpPhoneNumber": GP_PHONE_NUMBER,
                "chemistName": CHEMIST_NAME,
                "chemistPhoneNumber": CHEMIST_PHONE_NUMBER,
                "mobility": MOBILITY,
                "toileting": TOILETING,
                "hygiene": HYGIENE,
                "sight": SIGHT,
                "hearing": HEARING,
                "usesDentures": True,
                "requiresAssistanceEating": True,
                "thicknerGrade": 3,
                "dietSetup": False,
                "storageSize": 527,
                "reasonForInactivity": REASON_FOR_INACTIVITY,
                "agreedPolicies": [
                    OrderedDict(
                        [("centre", test_centre.id), ("policy", test_policy.id)]
                    )
                ],
                "nextOfKin": [
                    OrderedDict(
                        [
                            ("id", test_nok_1.id),
                            ("name", "Magory"),
                            ("phoneNumber", "33333333"),
                            ("client", test_client.id),
                        ]
                    ),
                    OrderedDict(
                        [
                            ("id", test_nok_2.id),
                            ("name", "Orogary"),
                            ("phoneNumber", "44444444"),
                            ("client", test_client.id),
                        ]
                    ),
                ],
                "medicalHistory": MEDICAL_HISTORY,
                "surgicalHistory": SURGICAL_HISTORY,
                "medications": [
                    OrderedDict(
                        [
                            ("id", medication.id),
                            ("name", "Example Med"),
                            ("dosage", "300mg"),
                            ("frequency", "daily"),
                            ("inactiveDate", None),
                            ("client", test_client.id),
                        ]
                    )
                ],
                "feedingRisks": [
                    OrderedDict(
                        [
                            ("id", feeding_risk.id),
                            ("feedingRisk", "Chewing"),
                            ("client", test_client.id),
                        ]
                    )
                ],
                "caseFormulation": CASE_FORMULATION,
                "allergies": ALLERGIES,
                "intolerances": INTOLERANCES,
                "needs": [client_need.id],
                "documents": [
                    OrderedDict(
                        [
                            ("id", document.id),
                            ("document", "http://testserver/media/example_file.txt"),
                            ("client", test_client.id),
                            ("name", "Example Document"),
                            (
                                "createdDate",
                                response.data["documents"][0]["createdDate"],
                            ),
                            ("originalName", ""),
                            ("size", 200),
                        ]
                    )
                ],
                "accountStatus": "No Account",
                "permissions": None,
            },
        )
        self.assertTrue(validate_iso8601(response.data["documents"][0]["createdDate"]))
        self.assertTrue(validate_iso8601(response.data["createdDate"]))

    def test_valid_retreive_as_client(self):
        """Test sending a valid retrieve request as a client user."""
        test_centre = create_centre()
        client_token = register_client_user(self.client)
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + client_token.data["key"]
        )
        client_user = CustomUser.objects.get(id=client_token.data["user"]["id"])
        client_user.client.centres.add(test_centre)
        response = self.client.get(f"{URL}{client_user.client.id}/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            {
                "id": client_user.client.id,
                "firstName": "test",
                "surname": "client",
                "gender": "Unknown",
                "admittedBy": response.data["admittedBy"],
                "createdDate": response.data["createdDate"],
                "centres": [test_centre.id],
                "isActive": True,
                "visitingDays": [],
                "basicInfoSetup": False,
                "additionalInfoSetup": False,
                "servicesSetup": False,
                "abilitySetup": False,
                "visitingDaysSetup": False,
                "setupComplete": False,
                "dateOfBirth": None,
                "address": "",
                "phoneNumber": "",
                "publicHealthNurseName": "",
                "publicHealthNursePhoneNumber": "",
                "homeHelpName": "",
                "homeHelpPhoneNumber": "",
                "gpName": "",
                "gpPhoneNumber": "",
                "chemistName": "",
                "chemistPhoneNumber": "",
                "mobility": "Walks Unaided",
                "toileting": "Fully Continent",
                "hygiene": "Self",
                "sight": "Normal",
                "hearing": "Normal",
                "usesDentures": False,
                "requiresAssistanceEating": False,
                "thicknerGrade": 0,
                "dietSetup": False,
                "storageSize": 0,
                "reasonForInactivity": "Deceased",
                "accountStatus": "Active",
                "agreedPolicies": [],
                "nextOfKin": [],
                "medicalHistory": "",
                "surgicalHistory": "",
                "medications": [],
                "feedingRisks": [],
                "caseFormulation": "",
                "allergies": "",
                "intolerances": "",
                "needs": [],
                "documents": [],
                "permissions": ["client_can_view_own_data"],
            },
        )

    def test_invalid_client_retreive_as_client(self):
        """Test sending a retrieve request for a different client."""
        test_client = create_new_client()
        client_token = register_client_user(self.client)
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + client_token.data["key"]
        )
        response = self.client.get(f"{URL}{test_client.id}/")
        self.assertEqual(response.status_code, 403)

    def test_invalid_client_permission_as_client(self):
        """Test sending a retrieve request while missing permissions."""
        test_centre = create_centre()
        client_token = register_client_user(self.client)
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + client_token.data["key"]
        )
        client_user = CustomUser.objects.get(id=client_token.data["user"]["id"])
        client_user.client.centres.add(test_centre)
        client_user.permissions.set([])
        response = self.client.get(f"{URL}{client_user.client.id}/")
        self.assertEqual(response.status_code, 403)

    def test_no_auth_retreive(self):
        """Test sending an invalid retrieve request as a no user."""
        client_token = register_client_user(self.client)
        self.client.credentials()
        client_user = CustomUser.objects.get(id=client_token.data["user"]["id"])
        test_centre = create_centre()
        client_user.client.centres.add(test_centre)
        response = self.client.get(f"{URL}{client_user.client.id}/")
        self.assertEqual(response.status_code, 401)

    def test_valid_list_as_client(self):
        """Test sending a valid list request as a client user."""
        client_token = register_client_user(self.client)
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + client_token.data["key"]
        )
        response = self.client.get(URL)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]["firstName"], "test")

    def test_valid_minimal_list_as_client(self):
        """Test sending a valid minimal list request as a client user."""
        client_token = register_client_user(self.client)
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + client_token.data["key"]
        )
        client_user = CustomUser.objects.get(id=client_token.data["user"]["id"])
        client_user.permissions.set([])
        response = self.client.get(URL)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            [
                OrderedDict(
                    [
                        ("id", response.data[0]["id"]),
                        ("firstName", "test"),
                        ("surname", "client"),
                        ("centres", []),
                        ("isActive", True),
                        ("visitingDays", []),
                        ("accountStatus", "Active"),
                    ]
                )
            ],
        )
