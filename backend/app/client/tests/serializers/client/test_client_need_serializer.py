"""Need Serializer Tests."""

from collections import OrderedDict

from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_test_user
from .....centre.models import Centre, Need
from ....models import Client, ClientNeed

REGISTER_URL = "/api/registration/"
TOKEN_AUTH = "Token "
NO_AUTH_MESSAGE = "Authentication credentials were not provided."
NEED_DESCRIPTION = "Example Description"
DISPLAY_NAME_2 = "Test User 2"
CONTENT_TYPE = "application/json"


class ClientNeedSerializerTestCase(TestCase):  # pylint: disable=R0902
    """Tests for ClientNeedSerializer."""

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        self.test_user = CustomUser.objects.get(username="testUser1")

        self.centre = Centre.objects.create()
        self.test_user.centres.add(self.centre)
        self.test_client = Client.objects.create(admittedBy=self.test_user)
        self.test_client.centres.add(self.centre)
        test_client_2 = Client.objects.create(admittedBy=self.test_user)
        self.test_client.centres.add(self.centre.id)
        self.need1 = Need.objects.create(description=NEED_DESCRIPTION)
        self.need2 = Need.objects.create(description=NEED_DESCRIPTION)
        self.centre.needs.add(self.need1)
        self.centre.needs.add(self.need2)
        self.client_need1 = ClientNeed.objects.create(
            client=self.test_client, need=self.need1, centre=self.centre, plan=""
        )
        self.client_need2 = ClientNeed.objects.create(
            client=self.test_client,
            need=self.need2,
            centre=self.centre,
            plan="Example Plan",
        )
        ClientNeed.objects.create(
            client=test_client_2, need=self.need2, centre=self.centre, plan=""
        )
        self.client.credentials(HTTP_AUTHORIZATION=TOKEN_AUTH + token.data["key"])

    def test_no_auth_get(self):
        """No results should return when authentication isn't provided."""
        self.client.credentials()
        response = self.client.get(
            f"/api/clients/{self.test_client.id}/clientNeeds/", {},
        )
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {"detail": ErrorDetail(string=NO_AUTH_MESSAGE, code="not_authenticated",)},
        )

    def test_valid_get(self):
        """Test sending a valid get request to the serializer."""
        response = self.client.get(f"/api/clients/{self.test_client.id}/clientNeeds/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(
            response.data,
            [
                OrderedDict(
                    [
                        ("id", self.client_need1.id),
                        ("centre", self.centre.id),
                        ("need", self.need1.id),
                        ("plan", ""),
                        ("isActive", True),
                        ("client", self.test_client.id),
                    ]
                ),
                OrderedDict(
                    [
                        ("id", self.client_need2.id),
                        ("centre", self.centre.id),
                        ("need", self.need2.id),
                        ("plan", "Example Plan"),
                        ("isActive", True),
                        ("client", self.test_client.id),
                    ]
                ),
            ],
        )

    def test_invalid_get_user_centre(self):
        """Test sending an invalid get request due to user centres."""
        self.test_user.centres.clear()
        response = self.client.get(f"/api/clients/{self.test_client.id}/clientNeeds/")
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )
