"""Need Serializer Tests."""

from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_test_user
from .....centre.models import Need
from .....centre.tests.utils import create_centre
from ....models import ClientNeed
from ...utils import create_client

REGISTER_URL = "/api/registration/"
TOKEN_AUTH = "Token "
NO_AUTH_MESSAGE = "Authentication credentials were not provided."
NEED_DESCRIPTION = "Example Description"
DISPLAY_NAME_2 = "Test User 2"
CONTENT_TYPE = "application/json"


class NeedSerializerTestCase(TestCase):
    """Tests for NeedSerializer."""

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        self.user = CustomUser.objects.get(id=token.data["user"]["id"])
        centre = create_centre(users=[self.user])
        centre_2 = create_centre(name="centre 2", users=[self.user])
        self.test_client = create_client(centres=[centre, centre_2])
        test_client_2 = create_client(centres=[])
        need1 = Need.objects.create(description=NEED_DESCRIPTION)
        need2 = Need.objects.create(description=NEED_DESCRIPTION)
        ClientNeed.objects.create(
            client=self.test_client, need=need1, centre=centre, plan=""
        )
        ClientNeed.objects.create(
            client=self.test_client, need=need2, centre=centre, plan=""
        )
        ClientNeed.objects.create(
            client=test_client_2, need=need2, centre=centre, plan=""
        )
        self.get_need_url = f"/api/clients/{self.test_client.id}/needs/"
        self.client.credentials(HTTP_AUTHORIZATION=TOKEN_AUTH + token.data["key"])

    def test_valid_get(self):
        """Test sending a valid get request to the serializer."""
        response = self.client.get(self.get_need_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0]["description"], NEED_DESCRIPTION)
        self.assertEqual(response.data[1]["description"], NEED_DESCRIPTION)

    def test_invalid_get_bad_client(self):
        """Test cannot get needs if not subbed to centre."""
        self.user.centres.clear()
        response = self.client.get(self.get_need_url)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )

    def test_no_auth_get(self):
        """No results should return when authentication isn't provided."""
        self.client.credentials()
        response = self.client.get(
            self.get_need_url,
            {},
        )
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string=NO_AUTH_MESSAGE,
                    code="not_authenticated",
                )
            },
        )
