"""Tests for sending request to agree to policies."""

from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_test_user
from .....centre.models import Policy
from .....centre.tests.utils import create_centre
from ...utils import create_client

URL = "/api/clients/"


class ClientPolicyAgreeTestCase(TestCase):
    """Tests for sending request to agree to policies."""

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        user = CustomUser.objects.get(id=token.data["user"]["id"])
        self.test_policy_1 = Policy.objects.create(description="example policy 1")
        self.test_policy_2 = Policy.objects.create(description="example policy 2")
        self.test_policy_3 = Policy.objects.create(description="example policy 3")
        test_policy_4 = Policy.objects.create(description="example policy 4")

        self.test_centre_1 = create_centre(
            name="centre 1",
            users=[user],
            policies=[self.test_policy_1, self.test_policy_2],
        )
        self.test_centre_2 = create_centre(
            name="centre 2",
            users=[user],
            policies=[self.test_policy_3],
        )
        create_centre(
            name="centre 3",
            users=[user],
            policies=[test_policy_4],
        )
        self.test_client = create_client(
            centres=[self.test_centre_1, self.test_centre_2]
        )
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.data["key"])

    def test_no_auth_get(self):
        """Should not update when no credentials provided."""
        self.client.credentials()
        response = self.client.get(
            f"{URL}{self.test_client.id}/policies/agree/",
            {},
        )
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="Authentication credentials were not provided.",
                    code="not_authenticated",
                )
            },
        )

    def test_valid_get(self):
        """Should agree to all policies."""
        response = self.client.get(
            f"{URL}{self.test_client.id}/policies/agree/",
            {},
        )
        self.assertEqual(response.status_code, 200)
        agreed_policies = list(
            self.test_client.agreedPolicies.order_by("policy_id").values()
        )
        self.assertEqual(
            agreed_policies,
            [
                {
                    "id": agreed_policies[0]["id"],
                    "client_id": self.test_client.id,
                    "policy_id": self.test_policy_1.id,
                    "centre_id": self.test_centre_1.id,
                },
                {
                    "id": agreed_policies[1]["id"],
                    "client_id": self.test_client.id,
                    "policy_id": self.test_policy_2.id,
                    "centre_id": self.test_centre_1.id,
                },
                {
                    "id": agreed_policies[2]["id"],
                    "client_id": self.test_client.id,
                    "policy_id": self.test_policy_3.id,
                    "centre_id": self.test_centre_2.id,
                },
            ],
        )

    def test_invalid_bad_client(self):
        """Test cannot agree to policies if not subbed to centre."""
        CustomUser.objects.first().centres.clear()
        response = self.client.get(
            f"{URL}{self.test_client.id}/policies/agree/",
            {},
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )
