"""Tests for sending post/delete requests to the ClientCentresSerializer."""
from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_test_user
from .....centre.tests.utils import create_centre
from .....client.tests.utils import create_client


class ClientCentresSerializerTestCase(TestCase):
    """Tests for sending post/delete requests to the ClientCentresSerializer."""

    url = "/api/clients/centres/update/"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        self.user = CustomUser.objects.get(id=token.data["user"]["id"])
        self.centre = create_centre(users=[self.user])
        self.centre_2 = create_centre(name="testCentre2", users=[self.user])
        self.test_client = create_client(centres=[self.centre])
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.data["key"])

    def test_no_group_post(self):
        """Should be unable to post data when not in valid group."""
        self.user.groups.clear()
        response = self.client.post(
            self.url,
            {"client": self.test_client.id, "centre": self.centre.id},
            format="json",
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )

    def test_no_group_delete(self):
        """Should be unable to delete data when not in valid group."""
        self.user.groups.clear()
        response = self.client.delete(
            self.url,
            {"client": self.test_client.id, "centre": self.centre.id},
            format="json",
        )
        self.assertEqual(response.status_code, 403)

    def test_no_centre_post(self):
        """Should be unable to post data when user is not in set centre."""
        self.user.centres.clear()
        new_centre = create_centre(name="newCentre")
        response = self.client.post(
            self.url,
            {"client": self.test_client.id, "centre": new_centre.id},
            format="json",
        )
        self.assertEqual(response.status_code, 403)

    def test_no_centre_delete(self):
        """Should be unable to delete data when user is not in set centre."""
        self.user.centres.clear()
        new_centre = create_centre(name="newCentre")
        response = self.client.delete(
            self.url,
            {"client": self.test_client.id, "centre": new_centre.id},
            format="json",
        )
        self.assertEqual(response.status_code, 403)

    def test_valid_remove(self):
        """Should be able to remove a client centre."""
        response = self.client.delete(
            self.url,
            {"client": self.test_client.id, "centre": self.centre.id},
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, {"client": self.test_client.id, "centres": []})

    def test_valid_add(self):
        """Should be able to add a client centre."""
        response = self.client.post(
            self.url,
            {"client": self.test_client.id, "centre": self.centre_2.id},
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            {
                "client": self.test_client.id,
                "centres": [self.centre.id, self.centre_2.id],
            },
        )

    def test_invalid_add_already_exists(self):
        """Returns error if adding centre that already exists."""
        response = self.client.post(
            self.url,
            {"client": self.test_client.id, "centre": self.centre.id},
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            "Failed to add centre, client is already subscribed to this centre.",
        )

    def test_invalid_remove_does_not_exists(self):
        """Returns error if removing centre that client is not subbed to."""
        response = self.client.delete(
            self.url,
            {"client": self.test_client.id, "centre": self.centre_2.id},
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            "Failed to remove centre, client is not subscribed to this centre.",
        )

    def test_missing_data(self):
        """Returns error if missing data."""
        response = self.client.delete(self.url, {}, format="json",)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "centre": [
                    ErrorDetail(string="This field is required.", code="required")
                ],
                "client": [
                    ErrorDetail(string="This field is required.", code="required")
                ],
            },
        )
