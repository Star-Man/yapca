"""Tests for sending post requests to the MedicationSerializer."""
from collections import OrderedDict

from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_test_user
from .....centre.tests.utils import create_centre
from ....models import Medication
from ...utils import create_client


class MedicationSerializerPostTestCase(TestCase):
    """Tests for sending post requests to the MedicationSerializer."""

    url = "/api/medications/"
    medication_name = "Test Med 1"
    medication_2_name = "Test Med 2"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        self.user = CustomUser.objects.get(id=token.data["user"]["id"])
        centre = create_centre(users=[self.user])
        self.test_client = create_client(centres=[centre])
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.data["key"])

    def test_valid_post(self):
        """Test sending a valid post request to the serializer."""
        response = self.client.post(
            self.url,
            {
                "name": self.medication_name,
                "client": self.test_client.id,
                "dosage": "100ml",
                "frequency": "monthly",
            },
            format="json",
        )
        self.assertEqual(response.status_code, 201)
        test_med = Medication.objects.get(name=self.medication_name)
        self.assertEqual(
            response.data,
            OrderedDict(
                [
                    ("id", test_med.id),
                    ("name", self.medication_name),
                    ("dosage", "100ml"),
                    ("frequency", "monthly"),
                    ("inactiveDate", None),
                    ("client", self.test_client.id),
                ]
            ),
        )

    def test_missing_data(self):
        """Test not sending required data to serializer."""
        response = self.client.post(
            self.url, {"client": self.test_client.id}, format="json"
        )
        self.assertEqual(response.status_code, 400)
        error = "This field is required."
        self.assertEqual(
            response.data,
            {
                "name": [ErrorDetail(string=error, code="required")],
                "dosage": [ErrorDetail(string=error, code="required")],
                "frequency": [ErrorDetail(string=error, code="required")],
            },
        )

    def test_no_client_post(self):
        """Should be unable to post data when not subbed to client centre."""
        self.user.centres.clear()
        response = self.client.post(
            self.url,
            {
                "name": self.medication_name,
                "client": self.test_client.id,
                "dosage": "100ml",
                "frequency": "monthly",
            },
            format="json",
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )
