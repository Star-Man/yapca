"""Tests for sending put/patch requests to the MedicationSerializer."""

from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_test_user
from .....centre.tests.utils import create_centre
from ....models import Medication
from ...utils import create_client


class MedicationSerializerPutTestCase(TestCase):
    """Tests for sending put/patch requests to the MedicationSerializer."""

    url = "/api/medications/"
    medicationName = "New Med Name"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        self.user = CustomUser.objects.get(id=token.data["user"]["id"])
        centre = create_centre(users=[self.user])
        self.test_client = create_client(centres=[centre])
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.data["key"])
        self.medication = Medication.objects.create(
            client=self.test_client, name="med 1", dosage="100ml", frequency="hourly"
        )

    def test_no_auth_put(self):
        """Should be unable to put data when not authenticated."""
        self.client.credentials()
        response = self.client.put(
            f"{self.url}{self.medication.id}/update/",
            {},
        )
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="Authentication credentials were not provided.",
                    code="not_authenticated",
                )
            },
        )

    def test_valid_patch(self):
        """Test sending a valid patch request to the serializer."""
        response = self.client.patch(
            f"{self.url}{self.medication.id}/update/",
            {"name": self.medicationName},
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        medication = Medication.objects.get(pk=response.data["id"])
        self.assertEqual(medication.name, self.medicationName)
        self.assertEqual(
            response.data,
            {
                "id": self.medication.id,
                "name": self.medicationName,
                "dosage": "100ml",
                "frequency": "hourly",
                "inactiveDate": None,
                "client": self.test_client.id,
            },
        )
