"""Reusable test functions."""
import glob
import os
import pathlib

from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_test_user
from .....centre.tests.utils import create_centre
from ....models import ClientDocument
from ...utils import create_client


def create_test_documents(clients):
    """Delete existing document and create new documents."""
    settings.MEDIA_ROOT = os.path.join(
        pathlib.Path(__file__).parent.absolute(), "media"
    )
    files = glob.glob(f"{settings.MEDIA_ROOT}/*")
    for file in files:
        os.remove(file)

    documents = []
    for client in clients:
        documents.append(
            ClientDocument.objects.create(
                client=client,
                name="Example Document",
                size=100,
                document=SimpleUploadedFile(
                    "example_file.txt", b"these are the file contents"
                ),
            )
        )
        ClientDocument.objects.create(
            client=client,
            name="Example Document",
            size=100,
            document=SimpleUploadedFile(
                "example_file.txt", b"these are the file contents"
            ),
        )
    return documents[0]


def document_setup():
    """Setup client, users and document for tests."""
    client = APIClient()
    token = register_test_user(client)
    user = CustomUser.objects.get(id=token.data["user"]["id"])
    centre = create_centre(users=[user])
    test_client = create_client(centres=[centre])
    client.credentials(HTTP_AUTHORIZATION="Token " + token.data["key"])
    document = create_test_documents([test_client])
    return (client, user, test_client, document)
