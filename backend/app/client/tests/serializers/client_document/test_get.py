"""Tests for sending get requests to the ClientDocumentSerializer."""
import glob
import os
import pathlib

from django.conf import settings
from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_test_user
from .....centre.tests.utils import create_centre
from ...utils import create_client
from .utils import create_test_documents

settings.MEDIA_ROOT = os.path.join(pathlib.Path(__file__).parent.absolute(), "media")


class ClientDocumentSerializerPostTestCase(TestCase):
    """Tests for sending get requests to the ClientDocumentSerializer."""

    url = "/api/clientDocuments/"
    fieldRequiredString = "This field is required."

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        self.user = CustomUser.objects.get(id=token.data["user"]["id"])
        centre = create_centre(users=[self.user])
        test_client = create_client(centres=[centre])
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.data["key"])
        self.document = create_test_documents([test_client])

    def test_no_auth_get(self):
        """Should be unable to get data when not authenticated."""
        self.client.credentials()
        response = self.client.get(f"{self.url}{self.document.id}/download/")
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="Authentication credentials were not provided.",
                    code="not_authenticated",
                )
            },
        )

    def test_valid_get(self):
        """Test sending a valid get request to the serializer."""
        response = self.client.get(f"{self.url}{self.document.id}/download/")
        self.assertEqual(
            list(response.streaming_content)[0].decode("utf-8"),
            "these are the file contents",
        )

    def test_invalid_get(self):
        """Test requesting a clientDocument that does not exist."""
        response = self.client.get(f"{self.url}8000/download/")
        self.assertEqual(response.status_code, 403)

    def test_invalid_get_no_file(self):
        """Test requesting a file that does not exist."""
        files = glob.glob(f"{settings.MEDIA_ROOT}/*")
        for file in files:
            os.remove(file)
        response = self.client.get(f"{self.url}{self.document.id}/download/")
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.data, "Client Document File Not Found")

    def test_invalid_get_bad_client(self):
        """Test cannot get document if not subbed to centre."""
        self.user.centres.clear()
        response = self.client.get(f"{self.url}{self.document.id}/download/")
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )
