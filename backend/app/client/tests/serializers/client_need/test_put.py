"""Tests for sending put/patch requests to the ClientNeedSerializer."""
from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_test_user
from .....centre.models import Centre, Need
from ....models import Client, ClientNeed


class ClientNeedSerializerPutTestCase(TestCase):
    """Tests for sending put/patch requests to the ClientNeedSerializer."""

    url = "/api/clientNeeds/"
    needDescription = "Example Need"
    centreName = "Test Centre"
    need_plan = "New Plan"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.data["key"])
        self.test_user = CustomUser.objects.get(username="testUser1")
        self.test_client = Client.objects.create(admittedBy=self.test_user)
        self.test_need = Need.objects.create(description=self.needDescription)
        self.test_centre = Centre.objects.create(name=self.centreName)
        self.test_user.centres.add(self.test_centre)
        self.test_centre.needs.add(self.test_need.id)
        self.test_client.centres.add(self.test_centre.id)
        self.client_need = ClientNeed.objects.create(
            centre=self.test_centre,
            client=self.test_client,
            need=self.test_need,
            plan="Example Plan",
        )

    def test_no_auth_put(self):
        """Should be unable to patch data when not authenticated."""
        self.client.credentials()
        response = self.client.patch(f"{self.url}{self.client_need.id}/update/", {},)
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="Authentication credentials were not provided.",
                    code="not_authenticated",
                )
            },
        )

    def test_valid_patch(self):
        """Test sending a valid patch request to the serializer."""
        response = self.client.patch(
            f"{self.url}{self.client_need.id}/update/",
            {"plan": self.need_plan, "isActive": False},
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        client_need = ClientNeed.objects.get(pk=response.data["id"])
        self.assertEqual(client_need.plan, self.need_plan)
        self.assertEqual(client_need.isActive, False)

    def test_valid_patch_ignores_readonly(self):
        """Test sending a valid patch request to the serializer."""
        response = self.client.patch(
            f"{self.url}{self.client_need.id}/update/",
            {
                "plan": self.need_plan,
                "isActive": False,
                "centre": 999,
                "client": 999,
                "need": 999,
            },
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            {
                "id": self.client_need.id,
                "centre": self.test_centre.id,
                "need": self.test_need.id,
                "plan": self.need_plan,
                "isActive": False,
                "client": self.test_client.id,
            },
        )

    def test_invalid_user_access(self):
        """Patch request rejected if user not subbed to centre."""
        self.test_user.centres.clear()
        response = self.client.patch(
            f"{self.url}{self.client_need.id}/update/",
            {"plan": self.need_plan, "isActive": False},
            format="json",
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )
