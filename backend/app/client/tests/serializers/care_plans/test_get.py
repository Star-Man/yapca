"""Tests for sending get requests to the CarePlanSerializer."""
from collections import OrderedDict

from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.tests.utils import register_test_user
from ....models import CarePlan
from .utils import create_care_plans

URL = "/api/carePlans/"


class CarePlanSerializerTestCase(TestCase):
    """Tests for sending get requests to the CarePlanSerializer."""

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.data["key"])
        self.care_plan, self.client_need, _ = create_care_plans(
            token.data["user"]["id"]
        )

    def test_no_auth_get(self):
        """No results should return when authentication isn't provided."""
        self.client.credentials()
        response = self.client.get(
            URL,
            {},
        )
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="Authentication credentials were not provided.",
                    code="not_authenticated",
                )
            },
        )

    def test_valid_list(self):
        """
        Test sending a valid list request to the serializer.

        Should get care plan 1, 2, and 4 since they are part of the users
        subscribed centres and inputted is False
        """
        response = self.client.get(URL)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 3)
        self.assertEqual(response.data[0]["date"], "2011-08-12")
        care_plan = CarePlan.objects.get(date="2014-08-12")
        self.assertEqual(
            response.data[2],
            OrderedDict(
                [
                    ("id", care_plan.id),
                    ("date", "2014-08-12"),
                    ("need", care_plan.need.id),
                    ("client", care_plan.need.client.id),
                    ("inputted", False),
                    (
                        "clientNeed",
                        OrderedDict(
                            [
                                ("id", care_plan.need.id),
                                ("centre", self.client_need.centre.id),
                                ("need", care_plan.need.need.id),
                                ("plan", ""),
                                ("isActive", True),
                                ("client", care_plan.need.client.id),
                            ]
                        ),
                    ),
                ]
            ),
        )

    def test_invalid_request(self):
        """
        Test sending an invalid request type
        """
        response = self.client.post(URL, {})
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )
