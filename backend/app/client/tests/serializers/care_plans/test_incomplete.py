"""Tests for get requests to mark care plans as incomplete."""
import datetime

from django.test import TestCase

from ....models import CarePlan
from .utils import input_complete_no_auth_test, input_complete_setup

URL = "/api/carePlans/"


class CarePlanIncompletedTestCase(TestCase):
    """Tests for get requests to mark care plans as incomplete."""

    def setUp(self):
        """Create a user for auth."""
        (
            self.client,
            self.care_plan,
            self.test_need,
            self.test_client,
            self.test_user,
        ) = input_complete_setup()

    def test_valid_complete(self):
        """Test marking care plans as completed."""
        response = self.client.get(
            f"{URL}{self.test_client.id}/2012-08-12/incomplete/",
            {},
        )
        self.assertEqual(response.status_code, 200)
        care_plans = list(CarePlan.objects.all().values())
        self.assertEqual(
            care_plans[4],
            {
                "id": care_plans[4]["id"],
                "date": datetime.date(2012, 8, 12),
                "need_id": care_plans[4]["need_id"],
                "comment": "",
                "completed": False,
                "inputted": False,
                "addedBy_id": self.test_user.id,
            },
        )

    def test_invalid_client(self):
        """No results should return when client not in same centres."""
        self.test_client.centres.clear()
        response = self.client.get(
            f"{URL}{self.test_client.id}/2014-08-12/complete/",
            {},
        )
        self.assertEqual(response.status_code, 403)

    def test_no_auth_get(self):
        """No results should return when authentication isn't provided."""
        input_complete_no_auth_test(self)
