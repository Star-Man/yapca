"""Tests for sending put/patch requests to the CarePlanSerializer."""
import datetime

from django.forms.models import model_to_dict
from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.tests.utils import register_test_user
from ....models import CarePlan
from .utils import create_care_plans

COMMENT = "new comment"


class CarePlanSerializerPutTestCase(TestCase):
    """Tests for sending put/patch requests to the CarePlanSerializer."""

    url = "/api/carePlans/"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.data["key"])
        (self.care_plan, self.need, _) = create_care_plans(token.data["user"]["id"])

    def test_no_auth_put(self):
        """Should be unable to put data when not authenticated."""
        self.client.credentials()
        response = self.client.patch(
            f"{self.url}{self.care_plan.id}/update/",
            {"date": "2020-11-03", "comment": COMMENT, "completed": True},
            format="json",
        )
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="Authentication credentials were not provided.",
                    code="not_authenticated",
                )
            },
        )

    def test_valid_patch(self):
        """Test sending a valid patch request to the serializer."""
        response = self.client.patch(
            f"{self.url}{self.care_plan.id}/update/",
            {"date": "2020-11-03", "comment": COMMENT, "completed": True},
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        care_plan = model_to_dict(CarePlan.objects.get(pk=self.care_plan.id))
        self.assertEqual(
            care_plan,
            {
                "id": response.data["id"],
                "date": datetime.date(2011, 8, 12),
                "need": self.need.id,
                "comment": COMMENT,
                "completed": True,
                "inputted": False,
                "addedBy": None,
            },
        )
