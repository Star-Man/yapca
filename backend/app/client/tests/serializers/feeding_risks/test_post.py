"""Tests for sending post requests to the FeedingRiskSerializer."""
import json
from collections import OrderedDict

from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_test_user
from .....centre.tests.utils import create_centre
from ....models import FeedingRisk
from ...utils import create_client

CONTENT_TYPE = "application/json"


class FeedingRiskSerializerPostTestCase(TestCase):
    """Tests for sending post requests to the FeedingRiskSerializer."""

    url = "/api/feedingRisks/"
    feeding_risk_name = "Poor Appetite"
    feeding_risk_2_name = "Chewing"
    feeding_risk_3_name = "Swallowing"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        self.user = CustomUser.objects.get(id=token.data["user"]["id"])
        centre = create_centre(users=[self.user])
        self.test_client = create_client(centres=[centre])
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.data["key"])

    def test_valid_post(self):
        """Test sending a valid post request to the serializer."""
        FeedingRisk.objects.create(client=self.test_client)

        data = [
            {"client": self.test_client.id, "feedingRisk": self.feeding_risk_name},
            {"client": self.test_client.id, "feedingRisk": self.feeding_risk_2_name},
        ]
        response = self.client.post(
            f"{self.url}{self.test_client.id}/",
            json.dumps(data),
            content_type=CONTENT_TYPE,
        )
        self.assertEqual(response.status_code, 201)
        test_feeding_risk_1 = FeedingRisk.objects.get(
            feedingRisk=self.feeding_risk_name
        )
        test_feeding_risk_2 = FeedingRisk.objects.get(
            feedingRisk=self.feeding_risk_2_name
        )
        self.assertEqual(
            response.data,
            [
                OrderedDict(
                    [
                        ("id", test_feeding_risk_1.id),
                        ("feedingRisk", "Poor Appetite"),
                        ("client", self.test_client.id),
                    ]
                ),
                OrderedDict(
                    [
                        ("id", test_feeding_risk_2.id),
                        ("feedingRisk", "Chewing"),
                        ("client", self.test_client.id),
                    ]
                ),
            ],
        )
        self.assertFalse(
            FeedingRisk.objects.filter(feedingRisk=self.feeding_risk_3_name).exists()
        )

    def test_invalid_choice(self):
        """Test sending an invalid choice for feeding risk."""
        FeedingRisk.objects.create(client=self.test_client)

        data = [{"client": self.test_client.id, "feedingRisk": "test"}]
        response = self.client.post(
            f"{self.url}{self.test_client.id}/",
            json.dumps(data),
            content_type=CONTENT_TYPE,
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            [
                {
                    "feedingRisk": [
                        ErrorDetail(
                            string='"test" is not a valid choice.',
                            code="invalid_choice",
                        )
                    ]
                }
            ],
        )

    def test_missing_data(self):
        """Test not sending required data to serializer."""
        response = self.client.post(
            f"{self.url}{self.test_client.id}/",
            json.dumps([{}]),
            content_type=CONTENT_TYPE,
        )
        self.assertEqual(response.status_code, 403)

    def test_valid_empty_post(self):
        """Test sending a valid post request with a delete request."""
        FeedingRisk.objects.create(client=self.test_client)

        data = [
            {"client": self.test_client.id},
        ]
        response = self.client.post(
            f"{self.url}{self.test_client.id}/",
            json.dumps(data),
            content_type=CONTENT_TYPE,
        )
        self.assertEqual(response.status_code, 204)
        self.assertEqual(
            response.data,
            [],
        )

    def test_no_client_post(self):
        """Should be unable to post data when missing client."""
        data = [{"client": self.test_client.id}, {"feedingRisk": ""}]
        response = self.client.post(
            f"{self.url}{self.test_client.id}/",
            json.dumps(data),
            content_type=CONTENT_TYPE,
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )

    def test_no_client_post_subbed_centre(self):
        """Should be unable to post data when invalid client centre."""
        data = [{"client": self.test_client.id}, {"client": 999}]
        response = self.client.post(
            f"{self.url}{self.test_client.id}/",
            json.dumps(data),
            content_type=CONTENT_TYPE,
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )

    def test_no_auth_post(self):
        """Should be unable to post data when not authenticated."""
        data = [{"client": self.test_client.id}, {"client": 999}]
        self.client.credentials()
        response = self.client.post(
            f"{self.url}{self.test_client.id}/",
            json.dumps(data),
            content_type=CONTENT_TYPE,
        )
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="Authentication credentials were not provided.",
                    code="not_authenticated",
                )
            },
        )
