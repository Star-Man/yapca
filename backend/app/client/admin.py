"""Django file to register models to be views on the admin page."""
# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Client

admin.register(Client)(admin.ModelAdmin)
