# Generated by Django 3.0.4 on 2020-04-11 00:02

from django.db import migrations, models


def create_initial(apps, schema_editor):
    register_ability = apps.get_model("administration", "RegisterAbility")
    register_ability(canRegister=True).save()


class Migration(migrations.Migration):
    dependencies = [
        ("administration", "0001_initial"),
    ]

    operations = [
        migrations.RunPython(create_initial),
    ]
