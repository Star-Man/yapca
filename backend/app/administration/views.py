"""A view is a function that takes a Web request and returns a Web response."""

from rest_framework import generics
from rest_framework.permissions import AllowAny

from .models import RegisterAbility
from .serializers import RegisterAbilitySerializer


class RegisterAbilityView(generics.RetrieveAPIView):
    """Get and update RegisterAbility object depending on group."""

    queryset = RegisterAbility.objects.all()
    serializer_class = RegisterAbilitySerializer
    permission_classes = [AllowAny]
