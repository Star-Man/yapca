"""URLs routes that are part of the main django app."""

from django.urls import path

from . import views

urlpatterns = [  # pylint: disable=C0103
    path(
        "register-ability/<int:pk>/",
        views.RegisterAbilityView.as_view(),
        name="registerAbility",
    )
]
