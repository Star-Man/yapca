"""Serializers Tests."""

import json

from django.test import TestCase
from rest_framework.test import APIClient

from ...authentication.tests.utils import register_test_user
from ..models import RegisterAbility

REGISTER_URL = "/api/registration/"
TOKEN_AUTH = "Token "
NO_AUTH_MESSAGE = "Authentication credentials were not provided."
POLICY_DESCRIPTION = "Example Description"
NO_PERMISSION = "You do not have permission to perform this action."
JSON = "application/json"


class RegisterAbilitySerializerTestCase(TestCase):
    """Tests for RegisterAbilitySerializer."""

    url = "/api/register-ability/1/"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        self.client.credentials(HTTP_AUTHORIZATION=TOKEN_AUTH + token.data["key"])
        RegisterAbility.objects.filter(id=1).update(canRegister=True)

    def test_no_auth_get(self):
        """Should return result when authentication isn't provided."""
        self.client.credentials()
        response = self.client.get(self.url, {},)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["canRegister"], True)

    def test_post(self):
        """Should be unable to post as any user."""
        response = self.client.post(
            "/api/register-ability/",
            json.dumps({"canRegister": False}),
            content_type=JSON,
        )
        self.assertEqual(response.status_code, 404)
