"""apps.py Tests."""

from django.apps import apps
from django.test import TestCase

from ..apps import WebsocketsAppConfig


class WebsocketsAppConfigTestCase(TestCase):
    """Tests for the websockets app configuration."""

    def test_name(self):
        """Tests name is correct."""
        self.assertEqual(WebsocketsAppConfig.name, "app.websockets")
        self.assertEqual(apps.get_app_config("websockets").name, "app.websockets")
