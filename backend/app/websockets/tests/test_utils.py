"""Test websocket utils."""
from django.test import TestCase

from ...authentication.models import CustomUser
from ..utils import user_can_view_data


class UtilsTestCase(TestCase):
    """Tests for functions in utils.py."""

    def setUp(self):
        """Create a user for auth."""
        self.user = CustomUser.objects.create(username="testUser5")
        self.user.groups.clear()

    def test_user_can_view_data_for_invalid_client(self):
        """Returns false when invalid client."""
        self.assertEqual(
            user_can_view_data(self.user.id, 999, "can_view_client_all_info", None),
            False,
        )

    def test_user_can_view_data_for_invalid_centre(self):
        """Returns false when invalid centre."""
        self.assertEqual(
            user_can_view_data(self.user.id, 999, "can_view_centre_info", None), False
        )

    def test_can_view_client_documents_for_invalid_client(self):
        """Returns false when invalid client."""
        self.assertEqual(
            user_can_view_data(self.user.id, 999, "can_view_client_documents", None),
            False,
        )
